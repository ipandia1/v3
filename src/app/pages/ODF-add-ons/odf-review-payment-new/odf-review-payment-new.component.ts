import { Component, OnInit, AfterViewInit,ElementRef } from '@angular/core';
import { CommonService } from '../../../services/common.service';
export enum Picker { ADD_ON_ITEM = 10, REMOVE_ITEM };

@Component({
    selector: 'app-odf-review-payment-new',
    templateUrl: './odf-review-payment-new.component.html',
    styleUrls: ['./odf-review-payment-new.component.scss']
})
export class OdfReviewPaymentNewComponent implements OnInit, AfterViewInit {
    datePicker1 = false;
    datePicker2 = false;
    visible = false;
    showValue = 'Starts immediately';
    radioVal = '';
    selectedValue = '';
    isEmpty = '';
    paymentMethod = 'Payment method';
    cardNumber = '****1234';
    iconClass = 'visa-icon';
    /* Logical changes for Date picker of Add on and remove items: Begin*/
    datepicker = Picker;
    private addOnItemIndex = 0;
    private removeItemIndex = 0;
    private pickerRequestFrom: Picker;
    isChecked = false;
    constructor(private commonService: CommonService,private elementref:ElementRef) { }
    public addOnItems: Array<Object> = [
        { descTitle: 'Single line name service', value: '$15.99/mo.', date: 'May 15, 2018', text: 'Change date' },
        { descTitle: 'Single line name service', value: '$15.99/mo.', date: 'May 15, 2018', text: 'Change date' }
    ];
    public removedItems: Array<Object> = [
        { descTitle: 'Single line name service', value: '$15.99/mo.', date: 'May 15, 2018', text: 'Change date' }
    ];

    effectiveDates: Array<object> = [
        { id: '0', descTitle: 'Immediately', value: 'These changes will begin immediately' },
        { id: '1', descTitle: 'Pick a date', value: '' }
    ];
    ngOnInit() {
        if (this.showValue === 'Starts immediately') {
            this.selectedValue = 'Immediately';
        }
    }
    ngAfterViewInit() {
        const btn = document.getElementsByTagName('button');
        for (let i = 0; i < btn.length; i++) {
            const btnClass = btn[i].classList.contains('btn-tab');
            if (!btnClass) {
                btn[i].setAttribute('tabindex', '-1');
            }
        }
    }
    radioChange(val) {
        this.radioVal = val;
       const ele = document.getElementById(val) as HTMLInputElement;
       ele.checked = true;
    }
    close() {
        this.visible = false;
        this.commonService.showScroll(true);
        const changeBtnEl = document.getElementById('changeId');
        if (changeBtnEl) {
            changeBtnEl.focus();
        }
    }
    select() {
        this.visible = false;
        this.commonService.showScroll(true);
        this.selectedValue = this.radioVal;
        if (this.radioVal === 'Immediately') {
            this.showValue = 'Starts immediately';
        } else {
            this.showValue = this.radioVal;
        }
        const changeBtnEl = document.getElementById('changeId');
        if (changeBtnEl) {
            changeBtnEl.focus();
        }
    }

    
    public handleCallBack(callBackValue: string) {
        this.commonService.showScroll(true);
        this.datePicker1 = false;
        this.datePicker2 = false;
        if (callBackValue !== this.isEmpty) {
            if (this.pickerRequestFrom === Picker.ADD_ON_ITEM) {
                this.setDate(this.addOnItems, callBackValue);
            } else if (this.pickerRequestFrom === Picker.REMOVE_ITEM) {
                this.setDate(this.removedItems, callBackValue);
            }
        }
        this.addOnItemIndex = 0;
        this.removeItemIndex = 0;

    }

    private setDate(arrayObject: Array<Object>, dateValue: string) {
        arrayObject.forEach((item, index) => {
            if (index === this.addOnItemIndex) {
                for (const propertyKey in item) {
                    if (propertyKey === 'date') {
                        item[propertyKey] = dateValue;

                    }
                }
            }
        });
    }
    /* Logical changes for Date picker of Add on and remove items: End*/

    change() {
        this.commonService.showScroll(false);
        this.visible = true;
        const dateEl = document.getElementById('effectiveDateTitle');
        if (dateEl) {
            dateEl.focus();
        }
    }
    backTabEvent(event, mainId, lastId) {
        const me = this;
        const allElements = document.getElementById(mainId).querySelectorAll
          ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
        if (document.activeElement === allElements[0]) {
          if (event.keyCode === 9) {
            if (event.shiftKey) {
              event.preventDefault();
              setTimeout(function () {
                me.elementref.nativeElement.querySelector('#' + lastId).focus();
    
              }, 0);
            }
          }
        }
        if (document.activeElement === document.getElementById(mainId)) {
          if (event.keyCode === 9) {
            if (event.shiftKey) {
              event.preventDefault();
              setTimeout(function () {
                me.elementref.nativeElement.querySelector('#' + lastId).focus();
    
              }, 0);
            }
          }
        }
        if (document.activeElement === allElements[allElements.length - 1]) {
          if (event.keyCode === 9) {
            if (event.shiftKey) {
            } else {
              event.preventDefault();
              setTimeout(function () {
                me.elementref.nativeElement.querySelector('#' + mainId).focus();
    
              }, 0);
            }
          }
        }
      }

}
