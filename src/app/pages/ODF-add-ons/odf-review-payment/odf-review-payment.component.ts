import { Component, OnInit } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import { forEach } from '@angular/router/src/utils/collection';

export enum Picker {ADD_ON_ITEM = 10, REMOVE_ITEM};
@Component({
  selector: 'app-odf-review-payment',
  templateUrl: './odf-review-payment.component.html',
  styleUrls: ['./odf-review-payment.component.scss']
})
export class OdfReviewPaymentComponent implements OnInit {
datePicker1 = false;
datePicker2 = false;
visible:boolean=false;
showValue:string="Starts immediately";
radioVal:string="";
selectedValue:string="";
isEmpty = '';
constructor() { }
public addOnItems: Array<Object> = [
    { descTitle: 'Single line name service', value: '15.99/mo.', date: 'May 15, 2018', text: 'Change date' },
    { descTitle: 'Single line name service', value: '15.99/mo.', date: 'May 15, 2018', text: 'Change date' }
  ];
public removedItems: Array<Object> = [
    { descTitle: 'Single line name service', value: '15.99/mo.', date: 'May 15, 2018', text: 'Change date' }
  ];

effectiveDates:Array<object>=[
    {id:'0',descTitle:'Immediately',value:'These changes will begin immediately'},{id:'1',descTitle:'Pick a date',value:''}
    ];
ngOnInit() {
    if(this.showValue=="Starts immediately"){
        this.selectedValue="Immediately";
        }
}
radioChange(val){
    this.radioVal=val;
}
close() {
    this.visible = false;
    this.showScroll();
}
select(){
    this.visible = false;
    this.showScroll();
    this.selectedValue=this.radioVal;
    if(this.radioVal=="Immediately"){
        this.showValue="Starts immediately";
        }
    else{
        this.showValue=this.radioVal;
        }
     }
     paymentMethod = "Payment method";
     cardNumber = "****1234";
     iconClass = "visa-icon";
    /* Logical changes for Date picker of Add on and remove items: Begin*/
    datepicker = Picker;
    private addOnItemIndex: number = 0;
    private removeItemIndex: number = 0;
    private pickerRequestFrom: Picker;
    viewDatePicker(dateValue: string, index: number, isRequestFor: Picker) {
        if (index === 0){
            this.datePicker1 = true;
            this.datePicker2 = false;
        }
       else {
            this.datePicker2 = true;
            this.datePicker1 = false;
       }
        this.hideScroll();
        if (isRequestFor === Picker.ADD_ON_ITEM) {
            this.addOnItemIndex = index;
            this.pickerRequestFrom = Picker.ADD_ON_ITEM;
        } else if (isRequestFor === Picker.REMOVE_ITEM) {
            this.removeItemIndex = index;
            this.pickerRequestFrom = Picker.REMOVE_ITEM;
        }
        
    }
    public handleCallBack(callBackValue: string) {
        this.showScroll();
        this.datePicker1 = false;
        this.datePicker2 = false;
        if (callBackValue !== this.isEmpty) {
            if (this.pickerRequestFrom === Picker.ADD_ON_ITEM) {
                this.setDate(this.addOnItems, callBackValue);
            } else if (this.pickerRequestFrom === Picker.REMOVE_ITEM) {
                this.setDate(this.removedItems, callBackValue);
            }
        }
        this.addOnItemIndex = 0;
        this.removeItemIndex = 0;
     
    }

    private setDate(arrayObject: Array<Object>, dateValue: string) {
        arrayObject.forEach((item, index) => {
            if (index === this.addOnItemIndex) {
                for (const propertyKey in item) {
                    if (propertyKey === 'date') {
                        item[propertyKey] = dateValue;
                        
                    }
                }
            }
        });
    }
    /* Logical changes for Date picker of Add on and remove items: End*/

    showScroll() {
        let body = document.getElementsByTagName('body')[0];
        body.classList.remove("noScroll");
    }
    hideScroll() {
        let body = document.getElementsByTagName('body')[0];
        body.classList.add("noScroll");
    }
    change() {
        this.hideScroll();
        this.visible = true;
    }

}
