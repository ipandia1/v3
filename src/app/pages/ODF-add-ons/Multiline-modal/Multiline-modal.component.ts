import { Component } from '@angular/core';

@Component({
  selector: 'app-multiline-modal',
  templateUrl: './Multiline-modal.component.html',
  styleUrls: ['./Multiline-modal.component.scss']
})
export class MultilineModalComponent {
visible: boolean;
constructor() { }
  close() {
    this.visible = false;
  }
}
