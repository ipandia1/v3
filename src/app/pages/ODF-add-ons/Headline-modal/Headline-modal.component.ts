import { Component } from '@angular/core';

@Component({
  selector: 'app-headline-modal',
  templateUrl: './Headline-modal.component.html',
  styleUrls: ['./Headline-modal.component.scss']
})
export class HeadlineModalComponent {
visible: boolean;
constructor() { }
  close() {
    this.visible = false;
  }
}
