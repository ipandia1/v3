import { Component, OnInit,ElementRef } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-odflanding-new',
  templateUrl: './odflanding-new.component.html',
  styleUrls: ['./odflanding-new.component.scss']
})
export class OdflandingNewComponent implements OnInit {
  visible = false;
  public UserInfo: Array<object> = [
    { name: 'Elton', value: '(206)-555-1111' }
  ];
  odfAddOns: Array<any> = [
    {
      'title': 'Monthly data plan',
      'heading': '',
      'data': [
        {
          id: 1, 'name': 'No data', 'price': 'Free',
          'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires'
        },
        { id: 2, 'name': 'T-Mobile ONE', 'price': 'Free', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 3, 'name': 'ONE Plus Promo', 'price': 'Free', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 4, 'name': 'T-Mobile ONE Plus', 'price': '$15.00/mo', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 5, 'name': 'Global Plus 15GB', 'price': '$50.00/mo', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' }
      ],
      'subHeading': '',
      'promo': 'You currently have ONE Plus Promo',
      'note': 'On all T-Mobile plans, during congestion, the small fraction of customers using >0GB/mo. may notice reduced speeds until next bill cycle due to data prioritization.'
    },
    {
      'title': 'One-time data pass',
      'heading': '',
      'data': [
        { id: 6, 'name': 'HD Video', 'price': 'Free', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 7, 'name': 'International 1 Day Pass', 'price': '$5.00', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 8, 'name': '5GB International Pass', 'price': '$35.00', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 9, 'name': '15GB International Pass', 'price': '$50.00', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' }
      ],
      'subHeading': '',
      'promo': '',
      'note': ''
    },
    {
      'title': 'Services',
      'heading': '',
      'data': [
        { id: 10, 'name': 'Netflix Basic for T-Mobile ($8.99 value)', 'price': 'On us', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 11, 'name': 'Netflix Standard for T-Mobile ($12.99 value)', 'price': '$4.00/mo', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 12, 'name': 'Netflix Premium for T-Mobile ($15.99 value)', 'price': '$7.00/mo', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 13, 'name': 'Voicemail to Text', 'price': '$4.00/mo', 'isChecked': true, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 14, 'name': 'T-Mobile Caller Tunes 5', 'price': '$5.00/mo', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 15, 'name': 'Family Allowances', 'price': 'On us', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 16, 'name': 'Family Mode', 'price': '$10.00/mo', 'isChecked': true, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 17, 'name': 'McAfee Security For T-Mobile', 'price': '$5.00/mo', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 18, 'name': 'North America Stateside International Talk with mobile', 'price': '$15.00/mo', 'isChecked': false, 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' }
      ],
      'subHeading': '',
      'promo': '',
      'note': ''
    },
    {
      'title': '',
      'heading': 'TVision Live TV',
      'data': [
        { id: 19, 'name': 'TES Live TV (30 Channels)', 'price': '$X.00/mo', 'isChecked': false, 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 20, 'name': 'TES + Live TV (44 Channels)', 'price': '$X.00/mo', 'isChecked': false, 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' }
      ],
      'subHeading': 'To add TVision options to your account, please log in via a browser.',
      'promo': '',
      'note': ''
    },
    {
      'title': '',
      'heading': 'TVision Premium Channels',
      'data': [
        { id: 21, 'name': 'Epix', 'price': '$X.00/mo', 'isChecked': false, 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 22, 'name': 'Showtime', 'price': '$X.00/mo', 'isChecked': false, 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 23, 'name': 'Starz', 'price': '$X.00/mo', 'isChecked': false, 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' }
      ],
      'subHeading': 'To add TVision options to your account, please log in via a browser.',
      'promo': '',
      'note': ''
    },
    {
      'title': '',
      'heading': 'DVR',
      'data': [
        { id: 14, 'name': 'DVR - 100GB', 'price': '$X.00', 'isChecked': false, 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 15, 'name': 'DVR - Unlimited', 'price': '$X.00', 'isChecked': false, 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' }
      ],
      'subHeading': 'To add TVision options to your account, please log in via a browser.',
      'promo': '',
      'note': ''
    }
  ];
  odfAddOnAccordion: boolean[][] = [];
  dataPlanTitle = 'Monthly data plan';
  dataPassTitle = 'One-time data pass';
  popUpSubTitle = '';
  id = '';
  constructor(private commonService: CommonService, private elementref: ElementRef) { }

  close() {
    this.visible = false;
    this.commonService.showScroll(true);
    const focusEl = document.getElementById(this.id);
    if (focusEl) {
      focusEl.focus();
    }
  }

  ngOnInit() {
    /*Logic to handle toggle content*/
    for (let i = 0; i < this.odfAddOns.length; i++) {
      this.odfAddOnAccordion.push([]);
      for (let j = 0; j < this.odfAddOns[i].data.length; j++) {
        this.odfAddOnAccordion[i].push(false);
      }
    }
  }

  checkFunc(event, checkStatus) {
    if (event.target.checked || checkStatus) {
      this.id = event.target.id;
      this.visible = !this.visible;
      this.commonService.showScroll(false);
      this.popUpSubTitle = event.target.value;
      const focusEl = document.getElementById('effectiveDatePage');
      if (focusEl) {
        focusEl.focus();
      }
    }
  }

  propagation(event) {
    event.stopPropagation();
  }

  backTabEvent(event, mainId, lastId) {
    const me = this;
    const allElements = document.getElementById(mainId).querySelectorAll
      ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
    if (document.activeElement === allElements[0]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          setTimeout(function () {
            me.elementref.nativeElement.querySelector('#' + lastId).focus();

          }, 0);
        }
      }
    }
    if (document.activeElement === document.getElementById(mainId)) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          setTimeout(function () {
            me.elementref.nativeElement.querySelector('#' + lastId).focus();

          }, 0);
        }
      }
    }
    if (document.activeElement === allElements[allElements.length - 1]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
        } else {
          event.preventDefault();
          setTimeout(function () {
            me.elementref.nativeElement.querySelector('#' + mainId).focus();

          }, 0);
        }
      }
    }
  }
}

