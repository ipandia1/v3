import { Component } from '@angular/core';

@Component({
  selector: 'app-empty-modal',
  templateUrl: './Empty-modal.component.html',
  styleUrls: ['./Empty-modal.component.scss']
})
export class EmptyModalComponent {
visible: boolean;
constructor() { }
  close() {
    this.visible = false;
  }
}
