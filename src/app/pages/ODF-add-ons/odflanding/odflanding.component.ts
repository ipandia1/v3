import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-odflanding',
  templateUrl: './odflanding.component.html',
  styleUrls: ['./odflanding.component.scss']
})
export class OdflandingComponent implements OnInit {
  visible = false;
  odfAddOns: Array<any> = [
    {
      'title': 'Data passes',
      'data': [
        { id: 1, 'name': 'Two-line name of service goes here', 'price': '$15.99', 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 2, 'name': 'Two-line name of service goes here', 'price': '$15.99', 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' }
      ],
      'promo': ''
    },
    {
      'title': 'Data plans',
      'data': [
        { id: 3, 'name': 'Single line name service', 'price': '$15.99', 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 4, 'name': 'Single line name service', 'price': '$15.99', 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' }
      ],
      'promo': ''
    },
    {
      'title': 'Name of service',
      'data': [
        { id: 5, 'name': 'Single line name service', 'price': '$15.99', 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 6, 'name': 'Single line name service', 'price': '$15.99', 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' }
      ],
      'promo': 'You currently have ONE Plus Promo.',
      'unlimitedText': 'Looking for unlimited? Check out',
      'linkText': 'T-Mobile ONE.',
      'warningtext': 'If you\'re on a promo plan, you\'ll lose promotional discounts when migrating to a new plan.',
      'speedwarning': 'On all T-mobile plans, during congestion, the small fraction of customers usin >50GB/mo. may notice reduced speeds until next bill cycle due to data priorization'
    },
    {
      'title': 'Handset Protection and Upgrades',
      'data': [
        { id: 7, 'name': 'Mcafee &#174 Security for T-mobile', 'price': '$5.00', 'isDisabled': false, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 8, 'name': 'JUMP <sup>TM<sup>', 'price': '$12.00', 'isDisabled': false, 'description': 'Upgrade to the latest smart phones earlier and more often while also protecting your investment with Premium Handset Protection and Lookout Mobile Security Premium. Charges will include taxes and fees for customers on tax inclusive rate plans', 'jump': 'true' }
      ]
    },
    {
      'title': 'TVision Live TV',
      'data': [
        { id: 9, 'name': 'TES Live TV (30 Channels)', 'price': '$X.00', 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 10, 'name': 'TES + Live TV (44 Channels)', 'price': '$X.00', 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' }
      ],
      'promo': 'To add TVision options to your account, please log in via a browser.'
    },
    {
      'title': 'TVision Premium Channels',
      'data': [
        { id: 11, 'name': 'Epix', 'price': '$X.00', 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 12, 'name': 'Showtime', 'price': '$X.00', 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 13, 'name': 'Starz', 'price': '$X.00', 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' }
      ],
      'promo': 'To add TVision options to your account, please log in via a browser.'
    },
    {
      'title': 'DVR',
      'data': [
        { id: 14, 'name': 'DVR - 100GB', 'price': '$X.00', 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' },
        { id: 15, 'name': 'DVR - Unlimited', 'price': '$X.00', 'isDisabled': true, 'description': 'Provides up to 500 MB of high-speed data for 24 hours. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires' }
      ],
      'promo': 'To add TVision options to your account, please log in via a browser.'
    }
  ];
  odfAddOnAccordion: boolean[][] = [];
  dataPassTitle = 'Data passes';
  dataPlanTitle = 'Data plans';
  isChecked: boolean = false;
  count: number = 0;
  popUpSubTitle = '';
  
  constructor(private commonService: CommonService) { }
  close() {
    this.visible = false;
    this.commonService.showScroll(true);
  }

  ngOnInit() {
    /*Logic to handle toggle content*/
    for (var i in this.odfAddOns) {
      this.odfAddOnAccordion.push([]);
      for (var k in this.odfAddOns[i].data) {
        this.odfAddOnAccordion[i].push(false);
      }
    }
  }
  change(close1) {
    this.visible = close1;
    this.commonService.showScroll(true);
  }

  checkFunc(event) {
    if (event.target.checked) {
      this.count = this.count + 1;
      this.visible = !this.visible;
      this.commonService.showScroll(false);
      this.popUpSubTitle = event.target.value;
    }
    else {
      this.count = this.count - 1;
    }
    if (this.count > 0) {
      this.isChecked = true;
    }
    else {
      this.isChecked = false;
    }
  }
}

