import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-selection-control',
  templateUrl: './selection-control.component.html',
  styleUrls: ['./selection-control.component.scss']
})
export class SelectionControlComponent implements OnInit {

  showDisplay1Code = false;
  showDisplay2Code = false;
  showDisplay3Code = false;
  showDisplay4Code = false;
  showDisplay5Code = false;
  showDisplay6Code = false;
  showDisplay7Code = false;
  showDisplay8Code = false;
  showDisplay9Code = false;
  showDisplay10Code = false;
  showDisplay11Code =false;
  showDisplay12Code = false

  constructor() { }

  ngOnInit() {
  }

}
