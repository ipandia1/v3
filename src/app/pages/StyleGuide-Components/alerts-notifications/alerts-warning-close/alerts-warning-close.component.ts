import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-alerts-warning-close',
  templateUrl: './alerts-warning-close.component.html',
  styleUrls: ['./alerts-warning-close.component.scss']
})
export class AlertsWarningCloseComponent implements OnInit {
  hideNotification = true;
  repeatData: number[] = new Array(45);
  constructor() {
  }
  ngOnInit() {
  }
  closeAlert(hideAlert) {
    this.hideNotification = hideAlert;
  }
}
