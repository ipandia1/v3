import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../../../../services/common.service';
@Component({
  selector: 'app-alerts-and-notification',
  templateUrl: './alerts-and-notification.component.html',
  styleUrls: ['./alerts-and-notification.component.scss']
})
export class AlertsAndNotificationComponent implements OnInit {
  hideNotification = true;
  resolveError = false;
  moreData = 'Here\'s some additional copy explaining how to resolve. Here\'s some additional copy explaining how to resolve.Here\'s some additional copy explaining how to resolve. Here\'s some additional copy explaining how to resolve';
  moreDataMobile = 'Here\'s some additional copy explaining';
  goToUrl = '/alertsAndNotification'; // This can be any Component
  goToAnchorTag = 'solveIssue'; // an Anchor tag in a component
  repeatData: number[] = new Array(45);
  constructor(private router: Router, private commonService: CommonService) {
    this.commonService.goToAnchorTag(this.router);
  }
  ngOnInit() {
  }
  resolveCriticalAlert() {
    this.resolveError = !this.resolveError;
    this.hideNotification = false;
    // this.showCriticalNotification = this.resolveError == true ? false : true;
    this.router.navigate([this.goToUrl]);
  }
}
