import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../../../../services/common.service';

@Component({
  selector: 'app-alerts-critical-navigation',
  templateUrl: './alerts-critical-navigation.component.html',
  styleUrls: ['./alerts-critical-navigation.component.scss']
})
export class AlertsCriticalNavigationComponent implements OnInit {
  hideNotification = true;
  resolveError = false;
  goToUrl = "/samplePage"; //This can be any Component
  goToAnchorTag = "solveError"; //an Anchor tag in a component
  repeatData:number[] = new Array(45);
  constructor(private router: Router, private commonService: CommonService) {
    this.commonService.goToAnchorTag(this.router);
  }
  ngOnInit() {
  }
  resolveCriticalAlert() {
    this.resolveError = !this.resolveError;
    //this.showCriticalNotification = this.resolveError == true ? false : true;
    this.router.navigate([this.goToUrl]);
  }
  closeAlert(hideAlert) {
    this.hideNotification = hideAlert;
  }
}
