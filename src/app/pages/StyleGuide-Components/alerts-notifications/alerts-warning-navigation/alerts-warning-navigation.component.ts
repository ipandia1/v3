import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../../../../services/common.service';

@Component({
  selector: 'app-alerts-warning-navigation',
  templateUrl: './alerts-warning-navigation.component.html',
  styleUrls: ['./alerts-warning-navigation.component.scss']
})
export class AlertsWarningNavigationComponent implements OnInit {
  hideNotification = true;
  resolveError = false;
  goToUrl = "/samplePage"; //This can be any Component
  repeatData:number[] = new Array(45);
  constructor(private router: Router, private commonService: CommonService) {
    this.commonService.goToAnchorTag(this.router);
  }

  ngOnInit() {
  }
  resolveCriticalAlert() {
    this.resolveError = !this.resolveError;
    //this.showWarningNotification = this.resolveError == true ? false : true;
    this.router.navigate([this.goToUrl]);
    this.hideNotification = false;
  }
}
