import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alerts-critical-close',
  templateUrl: './alerts-critical-close.component.html',
  styleUrls: ['./alerts-critical-close.component.scss']
})
export class AlertsCriticalCloseComponent implements OnInit {
  hideNotification = true;
  repeatData:number[] = new Array(45);
  constructor() {
  }

  ngOnInit() {
  }
  closeAlert(hideAlert) {
    this.hideNotification = hideAlert;
  }
}
