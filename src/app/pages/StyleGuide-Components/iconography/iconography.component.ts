import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-iconography',
  templateUrl: './iconography.component.html',
  styleUrls: ['./iconography.component.scss']
})
export class IconographyComponent implements OnInit {
  showArrowRightCode = false;
  showArrowLeftCode = false;
  showArrowupCode = false;
  showArrowdownCode = false;
  showWarningOutlineCode = false;
  showWarningFillCode = false;
  showValidateOutlineCode = false;
  showValidateFillCode = false;
  showCheckMarkCode = false;
  showTooltipIconCode = false;
  showCheckEmptyCode = false;
  showCheckFilledCode = false;
  showFlagBlackCode = false;

  showCalendarBlackCode = false;
  showErrorFilledBlackCode = false;
  showStarBlackCode = false;
  showStarHalfBlackCode = false;
  showRadioEmptyCode = false;
  showRadioFilledCode = false;
  showClearBlackCode = false;
  showExpandCode = false;
  showZoomInCode = false;
  showBackCode = false;
  showStarBorderBlackCode = false;
  showSettingsBlackCode = false;
  showMessageBlackCode =  false;
  showHeadsetBlackCode = false;
  showThumbUpBlackCode = false;
  showThumbDownBlackCode = false;
  showPrintIconCode = false;

  showVisa = false;
  showVisaDis = false;
  showMaster = false;
  showMasterDis = false;
  showDiscover = false;
  showDiscoverDis = false;
  showAmex = false;
  showAmexDis = false;
  showStar = false;
  showStarDis = false;
  showPulse = false;
  showPulseDis = false;
  showAccel =  false;
  showAccelDis = false;
  showNYCE = false;
  showNYCEDis = false;
  showATM = false;
  showATMDis = false;
  showBank =  false;
  showBankDis = false;
  showCvvAmex = false;
  showcvvVisa = false;
  showRouting = false;

  constructor() { }

  ngOnInit() {
  }

}
