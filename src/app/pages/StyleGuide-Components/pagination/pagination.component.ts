import { Component, OnInit} from '@angular/core';
import { SpinnerService } from '../../../services/spinner.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
 showClr1Code;
 currentpage: number;
 pagedItemsChild: Array<string>;
 noofrecordsperpage: number;
 totalrecords: number;
  TestData: Array<any> = [
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    },
    {
        'name': 'Item 5', 'id': '123'
    },
    {
        'name': 'Item 6', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 7', 'id': '123'
    },
    {
        'name': 'Item 8', 'id': '123'
    },
    {
        'name': 'Item 9', 'id': '123'
    },
    {
        'name': 'Item 10', 'id': '123'
    },
    {
        'name': 'Item 1', 'id': '123'
    },
    {
        'name': 'Item 2', 'id': '123'
    },
    {
        'name': 'Item 3', 'id': '123'
    },
    {
        'name': 'Item 4', 'id': '123'
    }
    ]

    // @ViewChild(PaginationbladeComponent) child: PaginationbladeComponent;
    showLoader: boolean;
    constructor() {
        this.currentpage = 1;
        this.noofrecordsperpage = 30;
      }
    ngOnInit() {
        this.totalrecords = this.TestData.length;
        this.pagedItemsChild = this.TestData.slice(0 , this.noofrecordsperpage)
     }
    moveCurrentPage(event) {
       this.currentpage = event;
        this.pagedItemsChild = this.TestData.slice(this.noofrecordsperpage * (event - 1),  event * this.noofrecordsperpage);
      }
      moveFirstpage() {
       // this.pagedItemsChild = this.TestData.slice(1, this.noofrecordsperpage);
       // this.currentpage = 1;
      }
      moveLastPage() {
        //const lastpage = Math.ceil(this.totalrecords / this.noofrecordsperpage);
       // this.pagedItemsChild = this.TestData.slice(this.noofrecordsperpage * (lastpage - 1),  lastpage * this.noofrecordsperpage);
       // this.currentpage = lastpage ;
      }
}




