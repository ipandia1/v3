import { Component, OnInit, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  inputArray =[
    { title: "Confirmation Payment", link: "/confirmationPayment" },
    { title: "Review Payment", link: "/ReviewPayment" },
    { title: "Upgrade Your Services", link: "/upgradeServices" },
    { title: "Upgrade Your Services Error", link: "/upgradeServicesError" }
  ];
  constructor() { } 
  ngOnInit() {
  }


}