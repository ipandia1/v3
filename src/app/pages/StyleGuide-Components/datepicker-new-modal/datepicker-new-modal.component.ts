import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
enum Month { Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec }
import { CommonService } from '../../../services/common.service';

@Component({
    selector: 'app-datepicker-new-modal',
    templateUrl: './datepicker-new-modal.component.html',
    styleUrls: ['./datepicker-new-modal.component.scss']
})
export class DatepickerNewModalComponent implements OnInit {
    @Input() selectedDateFormat = '';
    @Input('defaultPicker') defaultPicker: boolean = true;
    @Input('visible') visible: boolean = false;

    //DATA FROM PARENT STARTS
    @Input('parentTitle') title: string = 'Short headline here';
    @Input('parentSubTitle') subTitle: string = 'This is an optional content area that could be used';
    @Input('parentShowSubTitle') showSubTitle: boolean = true;
    @Input('parentColumn') column: boolean = true;
    @Input('parentColumn1') column1: boolean = true;
    @Input('parentColumn2') column2: boolean = false;
    @Input('parentIndicator') indicator: boolean;
    @Input('isInputDisabled') isInputDisabled: boolean = true;
    //@Input('isPlaceHolderRequired') isPlaceHolderRequired: string = "Select date";
    //DATA FROM PARENT ENDS
    @Output('callBackValue') callBackValue = new EventEmitter<string>();


    month = ['Jan', 'Feb', 'Mar', 'Apr',
        'May', 'Jun', 'Jul', 'Aug', 'Sep',
        'Oct', 'Nov', 'Dec'];
    weekDay = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
    date: Date;
    dueDate: number = 0;
    currentMonth: number = 0;
    currentMonthTitle: string = '';
    monthTitle: string = '';
    nextMonth: number = 0;
    currentDate: number = 0;
    year: number = 0;
    currentYear: number = 0;
    selectedYear: number = 0;
    //maxPastYear = 0;
    //maxFutureYear = 0;
    monthArray: Array<any>;
    selected: number;
    selectedMonth = '';
    firstDayOfAMonth: number;
    lastDayOfAMonth: number;
    firstDayOfCurrentMonth: number;
    lastDayOfCurrentMonth: number;
    payment = 0;
    btnDisable = true;
    firstWeek: Array<any> = [];
    fifthWeek: Array<any> = [];
    sixthWeek: Array<any> = [];
    nextDates: Array<any> = [];
    prevDates: Array<any> = [];
    showFirstWeek = false;
    showLastWeek = false;
    prevDisabled = true;
    nextDisabled = false;
    nextMonthForDueDate = '';
    selectedWeek = '';
    constructor(private commonService: CommonService) {
        this.date = new Date();
        this.currentDate = this.date.getDate();
        this.currentMonth = this.date.getMonth();
        this.currentMonthTitle = this.month[this.currentMonth];
        this.currentYear = this.date.getFullYear();
        this.dueDate = this.currentDate + 1;
        this.nextMonthForDueDate = this.month[this.currentMonth + 1];
        //Disable prev or next buttons based on years
        //this.maxPastYear = this.currentYear - 2;
        // this.maxFutureYear = this.currentYear + 2;
    }
    ngOnInit() {
        this.monthArray = this.getCurrentMonth(this.currentYear, this.currentMonth);
    }
    showCalendar() {
        this.visible = !this.visible;
        this.commonService.showScroll(false);
    }
    getFirstDayOfAMonth(yearValue, monthValue): number {
        return new Date(yearValue, monthValue, 1).getDay();
    }
    getLastDayOfAMonth(yearValue, monthValue): number {
        return new Date(yearValue, monthValue + 1, 0).getDate();
    }
    getCurrentMonth(yearValue, monthValue): Array<any> {
        this.firstDayOfAMonth = this.getFirstDayOfAMonth(yearValue, monthValue);
        this.lastDayOfAMonth = this.getLastDayOfAMonth(yearValue, monthValue);
        this.monthTitle = this.currentMonthTitle;
        this.year = this.currentYear;
        const numberOfDaysOfAMonth = this.getMonthDateEntries(this.firstDayOfAMonth, this.lastDayOfAMonth);
        let monthArrayData: Array<any>;
        this.firstWeek = numberOfDaysOfAMonth.slice(0, 7);
        this.fifthWeek = numberOfDaysOfAMonth.slice(28, 35);
        this.sixthWeek = numberOfDaysOfAMonth.slice(35, 42);
        this.showFirstWeek = false;
        this.showLastWeek = false;
        const details = [this.firstWeek, 'first'];
        if (this.fifthWeek[0] !== '' && this.sixthWeek[0] !== '') {
            this.firstWeek = this.getOtherMonthDates(details);
            this.sixthWeek = this.getOtherMonthDates(this.sixthWeek);
            monthArrayData = [
                { 'week': this.firstWeek, 'val1': this.showFirstWeek },
                { 'week': numberOfDaysOfAMonth.slice(7, 14) },
                { 'week': numberOfDaysOfAMonth.slice(14, 21) },
                { 'week': numberOfDaysOfAMonth.slice(21, 28) },
                { 'week': numberOfDaysOfAMonth.slice(28, 35) },
                { 'week': this.sixthWeek, 'val2': this.showLastWeek }
            ];
        }
        else if (this.fifthWeek[0] !== '') {
            this.firstWeek = this.getOtherMonthDates(details);
            this.fifthWeek = this.getOtherMonthDates(this.fifthWeek);
            monthArrayData = [
                { 'week': this.firstWeek, 'val1': this.showFirstWeek },
                { 'week': numberOfDaysOfAMonth.slice(7, 14) },
                { 'week': numberOfDaysOfAMonth.slice(14, 21) },
                { 'week': numberOfDaysOfAMonth.slice(21, 28) },
                { 'week': this.fifthWeek, 'val2': this.showLastWeek }
            ];
        }
        return monthArrayData;
    }
    getPrevMonth() {
        this.nextDisabled = false;
        if (this.currentMonth === 0) {
            this.year = this.year - 1;
            this.currentMonth = 11; //Past Year, and set month as December
        } else {
            this.currentMonth = this.currentMonth - 1;
        }
        this.monthTitle = this.month[this.currentMonth];
        if (this.monthTitle === this.currentMonthTitle && this.currentYear === this.year) {
            this.prevDisabled = true;
        }
        this.firstDayOfCurrentMonth = this.getFirstDayOfAMonth(this.year, this.currentMonth);
        this.lastDayOfCurrentMonth = this.getLastDayOfAMonth(this.year, this.currentMonth);
        const currentMonthNumberOfDays = this.getMonthDateEntries(this.firstDayOfCurrentMonth, this.lastDayOfCurrentMonth);
        this.firstWeek = currentMonthNumberOfDays.slice(0, 7);
        this.fifthWeek = currentMonthNumberOfDays.slice(28, 35);
        this.sixthWeek = currentMonthNumberOfDays.slice(35, 42);
        const details = [this.firstWeek, 'first'];
        this.showFirstWeek = false;
        this.showLastWeek = false;
        if (this.fifthWeek[0] !== '' && this.sixthWeek[0] !== '') {
            this.firstWeek = this.getOtherMonthDates(details);
            this.sixthWeek = this.getOtherMonthDates(this.sixthWeek);
            this.monthArray = [
                { 'week': this.firstWeek, 'val1': this.showFirstWeek },
                { 'week': currentMonthNumberOfDays.slice(7, 14) },
                { 'week': currentMonthNumberOfDays.slice(14, 21) },
                { 'week': currentMonthNumberOfDays.slice(21, 28) },
                { 'week': currentMonthNumberOfDays.slice(28, 35) },
                { 'week': this.sixthWeek, 'val2': this.showLastWeek }
            ];
        }
        else if (this.fifthWeek[0] !== '') {
            this.firstWeek = this.getOtherMonthDates(details);
            this.fifthWeek = this.getOtherMonthDates(this.fifthWeek);
            this.monthArray = [
                { 'week': this.firstWeek, 'val1': this.showFirstWeek },
                { 'week': currentMonthNumberOfDays.slice(7, 14) },
                { 'week': currentMonthNumberOfDays.slice(14, 21) },
                { 'week': currentMonthNumberOfDays.slice(21, 28) },
                { 'week': this.fifthWeek, 'val2': this.showLastWeek }
            ];
        }
    }
    getNextMonth() {
        this.prevDisabled = false;
        if (this.currentMonth === 11) {
            this.year = this.year + 1;
            this.currentMonth = 0; //restart of month from January
        } else {
            this.currentMonth = this.currentMonth + 1;
        }
        this.monthTitle = this.month[this.currentMonth];
        if (this.monthTitle === this.nextMonthForDueDate && this.currentYear === this.year) {
            this.nextDisabled = true;
        }
        this.firstDayOfCurrentMonth = this.getFirstDayOfAMonth(this.year, this.currentMonth);
        this.lastDayOfCurrentMonth = this.getLastDayOfAMonth(this.year, this.currentMonth);
        const currentMonthNumberOfDays = this.getMonthDateEntries(this.firstDayOfCurrentMonth, this.lastDayOfCurrentMonth);
        this.firstWeek = currentMonthNumberOfDays.slice(0, 7);
        this.fifthWeek = currentMonthNumberOfDays.slice(28, 35);
        this.sixthWeek = currentMonthNumberOfDays.slice(35, 42);
        const details = [this.firstWeek, 'first'];
        this.showFirstWeek = false;
        this.showLastWeek = false;
        if (this.fifthWeek[0] !== '' && this.sixthWeek[0] !== '') {
            this.firstWeek = this.getOtherMonthDates(details);
            this.sixthWeek = this.getOtherMonthDates(this.sixthWeek);
            this.monthArray = [
                { 'week': this.firstWeek, 'val1': this.showFirstWeek },
                { 'week': currentMonthNumberOfDays.slice(7, 14) },
                { 'week': currentMonthNumberOfDays.slice(14, 21) },
                { 'week': currentMonthNumberOfDays.slice(21, 28) },
                { 'week': currentMonthNumberOfDays.slice(28, 35) },
                { 'week': this.sixthWeek, 'val2': this.showLastWeek }
            ];
        }
        else if (this.fifthWeek[0] !== '') {
            this.firstWeek = this.getOtherMonthDates(details);
            this.fifthWeek = this.getOtherMonthDates(this.fifthWeek);
            this.monthArray = [
                { 'week': this.firstWeek, 'val1': this.showFirstWeek },
                { 'week': currentMonthNumberOfDays.slice(7, 14) },
                { 'week': currentMonthNumberOfDays.slice(14, 21) },
                { 'week': currentMonthNumberOfDays.slice(21, 28) },
                { 'week': this.fifthWeek, 'val2': this.showLastWeek }
            ];
        }
    }
    getOtherMonthDates(firstLastWeekVal): Array<any> {
        let count = 1;
        let prevYear = 0;
        let prevMonth = 0;
        let PrevMonthLastDate = 0;
        let lastVal = 0;
        if (firstLastWeekVal.length === 2) {
            if (this.currentMonth === 0) {
                prevYear = this.year - 1;
                prevMonth = 11;
            } else {
                prevMonth = this.currentMonth - 1;
                prevYear = this.year;
            }
            PrevMonthLastDate = this.getLastDayOfAMonth(prevYear, prevMonth);
            let firstWeekVal = firstLastWeekVal[0];
            for (let i = 0; i < firstWeekVal.length; i++) {
                if (firstWeekVal[i] === '') {
                    count = i;
                    this.showFirstWeek = true;
                }
            }
            if (this.showFirstWeek) {
                this.prevDates = [];
                firstWeekVal = firstWeekVal.filter(emptyVal => emptyVal !== '');
                lastVal = PrevMonthLastDate - count;
                for (let j = 0; j <= count; j++) {
                    this.prevDates[j] = lastVal;
                    lastVal++;
                }
            }
            return firstWeekVal;
        }
        else {
            for (let k = 0; k < firstLastWeekVal.length; k++) {
                if (firstLastWeekVal[k] === '') {
                    this.showLastWeek = true;
                    this.nextDates = [];
                    firstLastWeekVal = firstLastWeekVal.slice(0, k);
                    for (let l = 0; l < (7 - k); l++) {
                        this.nextDates[l] = count;
                        count++;
                    }
                }
            }
            return firstLastWeekVal;
        }
    }
    private getMonthDateEntries(firstDayOfMonth, lastDayOfMonth): Array<any> {
        let dateEntry = 1;
        let i = 0;
        const index = 0;
        const dateEntries = Array<any>(42).fill('');
        while (i < dateEntries.length) {
            if (i >= firstDayOfMonth && i < (firstDayOfMonth + lastDayOfMonth)) {
                dateEntries[i] = dateEntry;
                dateEntry++;
            }
            i++;
        }
        return dateEntries;
    }
    select(item, month, year, weekVal) {
        if (item && month) {
            this.selected = item;
            this.selectedMonth = month;
            this.selectedYear = year;
            this.btnDisable = false;
            this.selectedWeek = weekVal;
            if (weekVal === 'first') {
                this.selectedMonth = this.currentMonthTitle;
            }
            else if (weekVal === 'last') {
                this.selectedMonth = this.nextMonthForDueDate;
            }
        }
    }
    isToday(item, month, year) {
        if (item && month) {
            if (!this.selected) {
                if (this.currentDate === item && this.currentMonthTitle === month && this.currentYear === year) {
                    return true;
                }
            }
            else {
                return false;
            }
        }
    }
    isSelected(item, month, year) {
        if (item && month && this.selected && this.selected === item && this.selectedYear === year) {
            if (this.selectedWeek === 'first') {
                if (this.currentMonthTitle === month) {
                    return this.selected === item;
                }
            }
            if (this.selectedWeek === 'main') {
                if (this.selectedMonth === month) {
                    if (item >= this.currentDate || item <= this.dueDate) {
                        return this.selected === item;
                    }
                }
            }
            if (this.selectedWeek === 'last') {
                if (this.nextMonthForDueDate === month) {
                    return this.selected === item;
                }
            }
        }
    }
    selectEndWeeks(item, month, year, weekVal) {
        if (item && month && this.selected && this.selected === item && this.selectedYear === year) {
            if (weekVal === 'first') {
                if (this.selectedMonth === this.nextMonthForDueDate && this.selectedWeek === 'main') {
                    return false;
                }
                else if (this.nextMonthForDueDate === month) {
                    return this.selected === item;
                }
            }
            else if (weekVal === 'last') {
                if (this.selectedMonth === this.currentMonthTitle && this.selectedWeek === 'main') {
                    return false;
                }
                else if (this.currentMonthTitle === month) {
                    return this.selected === item;
                }
            }
        }
    }
    isDue(item, month, year, weekValue) {
        if (item && this.currentYear === year) {
            if (this.dueDate === item) {
                if (weekValue === 'first') {
                    if (month === this.nextMonthForDueDate) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else if (weekValue === 'last') {
                    if (month === this.currentMonthTitle) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else if (weekValue === 'main') {
                    if (this.nextMonthForDueDate === month) {
                        return true;
                    }
                }
            }
        }
    }
    isDisabled(item, month, year, weekValue) {
        if (item) {
            if (weekValue === 'first') {
                if (item >= this.currentDate && month === this.nextMonthForDueDate && this.currentYear === year) {
                    return false;
                }
                else {
                    return true;
                }
            }
            else if (weekValue === 'last') {
                if (month === this.currentMonthTitle && this.currentYear === year) {
                    return false;
                }
                else {
                    return true;
                }
            }
            else if (weekValue === 'main') {
                if (month === this.currentMonthTitle && this.currentYear === year) {
                    if (item < this.currentDate) {
                        return true;
                    }
                }
                else if (month === this.nextMonthForDueDate && this.currentYear === year) {
                    if (item > this.dueDate) {
                        return true;
                    }
                }
                else if (month !== this.currentMonthTitle) {
                    return true;
                }
                else if (month === this.currentMonthTitle && this.currentYear !== year) {
                    return true;
                }
            }
        }
    }
    selectedBtn() {
        if (this.selected) {
            this.selectedDateFormat = this.selectedMonth + ' ' + this.selected + ', ' + this.selectedYear;
        }
        this.callBackValue.emit(this.selectedDateFormat);
        this.visible = false;
        this.commonService.showScroll(true);
    }
    close() {
        this.visible = false;
        this.commonService.showScroll(true);
        this.callBackValue.emit(this.selectedDateFormat);
    }
}
