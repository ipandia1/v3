import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss']
})
export class TablesComponent implements OnInit {
  tableHeaders = [{
    'displayColName': 'Column headers can be long or short if need',
    'dataColName': 'col1',
    'sortable': true,
    'tooltipText': 'Column headers can be long or short if you need',
    'colWidth': 'col-4',
    'displayColumn': true
  },
  {
    'displayColName': 'Column 2',
    'dataColName': 'col2',
    'sortable': true,
    'tooltipText': 'Column 2',
    'colWidth': 'col-2',
    'displayColumn': false
  },
  {
    'displayColName': 'Column 3',
    'dataColName': 'col3',
    'sortable': true,
    'tooltipText': 'Column 3',
    'colWidth': 'col-2',
    'displayColumn': true
  },
  {
    'displayColName': 'Column 4',
    'dataColName': 'col4',
    'sortable': true,
    'tooltipText': 'Column 4',
    'colWidth': 'col-2',
    'displayColumn': false
  },
  {
    'displayColName': 'Column 5',
    'dataColName': 'col5',
    'sortable': true,
    'tooltipText': 'Column  5',
    'colWidth': 'col-2',
    'displayColumn': true
  }];
  tableHeaders1 = [{
    'displayColName': 'Column headers can be long or short if need',
    'dataColName': 'col1',
    'sortable': true,
    'tooltipText': 'Column headers can be long or short if you need',
    'colWidth': '226px'
  },
  {
    'displayColName': 'Column 2',
    'dataColName': 'col2',
    'sortable': true,
    'tooltipText': 'Column 2',
    'colWidth': '114px'
  },
  {
    'displayColName': 'Column 3',
    'dataColName': 'col3',
    'sortable': true,
    'tooltipText': 'Column 3',
    'colWidth': '114px'
  },
  {
    'displayColName': 'Column 4',
    'dataColName': 'col4',
    'sortable': true,
    'tooltipText': 'Column 4',
    'colWidth': '114px'
  },
  {
    'displayColName': 'Column 5',
    'dataColName': 'col5',
    'sortable': true,
    'tooltipText': 'Column 5',
    'colWidth': '114px'
  },
  {
    'displayColName': 'Column 6',
    'dataColName': 'col4',
    'sortable': true,
    'tooltipText': 'Column 6',
    'colWidth': '114px'
  },
  {
    'displayColName': 'Column 7',
    'dataColName': 'col5',
    'sortable': true,
    'tooltipText': 'Column 7',
    'colWidth': '114px'
  },
  {
    'displayColName': 'Column 8',
    'dataColName': 'col5',
    'sortable': true,
    'tooltipText': 'Column 8',
    'colWidth': '114px'
  },
  {
    'displayColName': 'Column 9',
    'dataColName': 'col4',
    'sortable': true,
    'tooltipText': 'Column 9',
    'colWidth': '114px'
  },
  {
    'displayColName': 'Column 10',
    'dataColName': 'col5',
    'sortable': true,
    'tooltipText': 'Column 10',
    'colWidth': '135px'
  }];
  tableHeadersChkbox = [{
    'displayColName': 'Column headers can be long or short if you need',
    'dataColName': 'col1',
    'sortable': true,
    'tooltipText': 'Column headers can be long or short if you need',
    'colWidth': 'col-3',
    'displayColumn': true
  },
  {
    'displayColName': 'Column 2',
    'dataColName': 'col2',
    'sortable': true,
    'tooltipText': 'Column 2',
    'colWidth': 'col-2',
    'displayColumn': false
  },
  {
    'displayColName': 'Column 3',
    'dataColName': 'col3',
    'sortable': true,
    'tooltipText': 'Column 3',
    'colWidth': 'col-2',
    'displayColumn': true
  },
  {
    'displayColName': 'Column 4',
    'dataColName': 'col4',
    'sortable': true,
    'tooltipText': 'Column 4',
    'colWidth': 'col-2',
    'displayColumn': false
  },
  {
    'displayColName': 'Column 5',
    'dataColName': 'col5',
    'sortable': true,
    'tooltipText': 'Column 5',
    'colWidth': 'col-2',
    'displayColumn': true
  }];
  testData: Array<any> = [
    {
      'col1': 'dBody copy can go here..........', 'col2': 'vBody', 'col3': '1Body', 'col4': 'Body', 'col5': 'tBody', 'id': '123'
    },
    {
      'col1': 'fBody copy can go here..........', 'col2': 'fBody', 'col3': '2Body', 'col4': 'hBody', 'col5': 'fgBody', 'id': '123'
    },
    {
      'col1': 'hBody copy can go here..........', 'col2': 'hBody', 'col3': 'wBody', 'col4': 'gBody', 'col5': 'fgBody', 'id': '123'
    },
    {
      'col1': 'yBody copy can go here..........', 'col2': 'aBody', 'col3': 'ytBody', 'col4': 'hBody', 'col5': 'hBody', 'id': '123'
    },
    {
      'col1': 'uiBody copy can go here..........', 'col2': 'tBody', 'col3': 'yBody', 'col4': 'hBody', 'col5': 'AaBody', 'id': '123'
    },
    {
      'col1': '7Body copy can go here..........', 'col2': 'iBody', 'col3': 'iuBody', 'col4': 'Body', 'col5': 'Body', 'id': '123'
    },
    {
      'col1': 'iolBody copy can go here..........', 'col2': 'Body', 'col3': 'iBody', 'col4': 'Body', 'col5': 'Body', 'id': '123'
    },
    {
      'col1': 'tyBody copy can go here..........', 'col2': 'Body', 'col3': 'Body', 'col4': 'Body', 'col5': 'ytBody', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'dfBody', 'col3': 'kBody', 'col4': 'mBody', 'col5': 'yBody', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'Body', 'col3': 'Body', 'col4': 'kBody', 'col5': 'eBody', 'id': '123'
    },
    {
      'col1': 'dfBody copy can go here..........', 'col2': 'gBody', 'col3': 'hBody', 'col4': 'kBody', 'col5': 'Body', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'Body', 'col3': 'Body', 'col4': 'sBody', 'col5': 'eBody', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'Body', 'col3': 'ujhBody', 'col4': 'Body', 'col5': 'yBody', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'Body', 'col3': 'Body', 'col4': 'seBody', 'col5': 'lBody', 'id': '123'
    },
    {
      'col1': 'dfgBody copy can go here..........', 'col2': 'Body', 'col3': 'Body', 'col4': 'Body', 'col5': 'Body', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'Body', 'col3': 'Body', 'col4': 'saBody', 'col5': 'oBody', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'Body', 'col3': 'yuBody', 'col4': 'fdBody', 'col5': 'pBody', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'lBody', 'col3': 'Body', 'col4': 'dBody', 'col5': 'jBody', 'id': '123'
    },
    {
      'col1': 'fBody copy can go here..........', 'col2': 'Body', 'col3': 'uBody', 'col4': 'nBody', 'col5': 'lBody', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'Body', 'col3': 'Body', 'col4': 'mBody', 'col5': 'Body', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'Body', 'col3': 'Body', 'col4': 'mBody', 'col5': 'lBody', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'Body', 'col3': 'Body', 'col4': 'Body', 'col5': 'asBody', 'id': '123'
    },
    {
      'col1': 'fBody copy can go here..........', 'col2': 'Body', 'col3': 'uyBody', 'col4': 'utBody', 'col5': 'sdBody', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'jBody', 'col3': 'Body', 'col4': 'Body', 'col5': 'Body', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'rBody', 'col3': 'trBody', 'col4': 'Body', 'col5': 'Body', 'id': '123'
    },
    {
      'col1': 'gBody copy can go here..........', 'col2': 'tBody', 'col3': 'Body', 'col4': 'yuBody', 'col5': 'cBody', 'id': '123'
    },
    {
      'col1': 'Body copy can go here..........', 'col2': 'Body', 'col3': 'erBody', 'col4': 'Body', 'col5': 'bBody', 'id': '123'
    },
    {
      'col1': 'fBody copy can go here..........', 'col2': 'bBody', 'col3': 'Body', 'col4': 'tyBody', 'col5': 'Body', 'id': '123'
    }
  ]
  // @ViewChild(PaginationbladeComponent) child: PaginationbladeComponent;
  showLoader: boolean;
  constructor() { }
  ngOnInit() { }

}

