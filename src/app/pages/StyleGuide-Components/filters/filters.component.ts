import { Component, OnInit, Input, Output, HostBinding, HostListener, ElementRef, EventEmitter } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import { FilterPipe } from './filters.pipe';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
  providers: [FilterPipe]
})

export class FiltersComponent implements OnInit {

  itemsHide: boolean;
  booleanVal = false;
  filtersData: Array<any>;
  filterKeys: Array<any>;
  sortKeys: Array<any>;
  filterMainData: Array<any>;
  itemsVisible: Array<any> = [];
  showFilterValues: Array<any> = [];
  filterTitleArray: Array<any> = [];
  filtersDataPath = 'assets/json/FiltersData.json';

  constructor(private commonService: CommonService, private filterPipe: FilterPipe) {
    this.getFilterData(this.filtersDataPath);
  }
  ngOnInit() {
  }
  private getFilterData(urlPath) {
    this.commonService.getJsonData(urlPath)
      .subscribe(responseJson => {
        this.filtersData = responseJson;
        this.filterData(this.filtersData);
      });
  }

  public filterData(filterData: Array<any>) {
    if (filterData !== null) {
      this.filterKeys = filterData['filterkeys'];
      this.filterMainData = filterData['data'];
      this.sortKeys = filterData['sortkeys'];
      for (let j = 0; j < this.filterKeys.length; j++) {
        this.showFilterValues.push(false);
     }
     for (let i = 0; i < this.filterKeys.length; i++) {
      this.filterTitleArray.push(this.filterKeys[i]);
    }
    }

  }
  itemsDivEmit = function (event) {
    this.itemsVisible = event;
  };

  visibleDiv = function (id) {
    if (this.itemsVisible.length === 0) {
      return true;
    } else {
      for (let i = 0; i < this.itemsVisible.length; i++) {
        if (id === this.itemsVisible[i]) {
          return true;
        }
      }
    }
  };
}
