import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
    name: 'filterUnique',
    pure: true
})

export class FilterPipe implements PipeTransform {
    transform(value: any, itemName): any {
        if (value !== undefined && value !== null) {
                    value = _.orderBy(value, itemName);
                    return _.uniqBy(value, itemName);
        }
    }
}
