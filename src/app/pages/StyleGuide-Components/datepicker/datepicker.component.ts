import { Component, Input, Output, EventEmitter } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
enum Month { January, February, March, April, May, June, July, August, September, October, November, December }
import { CommonService } from '../../../services/common.service';

@Component({
    selector: 'app-datepicker',
    templateUrl: './datepicker.component.html',
    styleUrls: ['./datepicker.component.scss']
})
export class DatepickerComponent {
    @Input() selectedDateFormat = '';
    @Input('defaultPicker') defaultPicker: boolean = true;
    @Input('visible') visible: boolean = false;

    //DATA FROM PARENT STARTS
    @Input('parentTitle') title: string = 'Headline here';
    @Input('parentSubTitle') subTitle: string = 'This is a flexible content area that could be used for more copy as needed';
    @Input('parentShowSubTitle') showSubTitle: boolean = true;
    @Input('parentColumn') column: boolean = true;
    @Input('parentColumn1') column1: boolean = true;
    @Input('parentColumn2') column2: boolean = false;
    @Input('parentIndicator') indicator: boolean;
    @Input('isInputDisabled') isInputDisabled: boolean = true;
    //DATA FROM PARENT ENDS
    @Output('callBackValue') callBackValue = new EventEmitter<string>();

 
    month = ['January', 'February', 'March', 'April',
        'May', 'June', 'July', 'August', 'September',
        'October', 'November', 'December'];
    weekDay = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
    date: Date;
    dueDate: number = 0;
    currentMonth: number = 0;
    currentMonthTitle: string = '';
    monthTitle: string = '';
    nextMonth: number = 0;
    currentDate: number = 0;
    year: number = 0;
    currentYear: number = 0;
    selectedYear: number = 0;
    //maxPastYear = 0;
    //maxFutureYear = 0;
    monthArray: Array<any>;
    selected: number;
    selectedMonth = '';
    firstDayOfAMonth: number;
    lastDayOfAMonth: number;
    firstDayOfCurrentMonth: number;
    lastDayOfCurrentMonth: number;
    payment = 0;
    
    constructor(private commonService: CommonService) {
        this.date = new Date();
        this.currentDate = this.date.getDate();
        this.currentMonth = this.date.getMonth();
        this.currentMonthTitle = this.month[this.currentMonth];
        this.currentYear = this.date.getFullYear();
        this.dueDate = this.currentDate + 11;
        //Disable prev or next buttons based on years
        //this.maxPastYear = this.currentYear - 2;
        // this.maxFutureYear = this.currentYear + 2;
    }
    ngOnInit() {
        this.monthArray = this.getCurrentMonth(this.currentYear, this.currentMonth);
    }
    showCalendar() {
        this.visible = !this.visible;
        this.commonService.showScroll(false);
    }

    getFirstDayOfAMonth(yearValue, monthValue): number {
        return new Date(yearValue, monthValue, 1).getDay();
    }
    getLastDayOfAMonth(yearValue, monthValue): number {
        return new Date(yearValue, monthValue + 1, 0).getDate();;
    }
    getCurrentMonth(yearValue, monthValue): Array<any> {
        this.firstDayOfAMonth = this.getFirstDayOfAMonth(yearValue, monthValue);
        this.lastDayOfAMonth = this.getLastDayOfAMonth(yearValue, monthValue);
        this.monthTitle = this.currentMonthTitle;
        this.year = this.currentYear;
        const numberOfDaysOfAMonth = this.getMonthDateEntries(this.firstDayOfAMonth, this.lastDayOfAMonth);
        let monthArrayData: Array<any>;
        const fifthWeek = numberOfDaysOfAMonth.slice(28, 35);
        const sixthWeek = numberOfDaysOfAMonth.slice(35, 42);
        if (fifthWeek[0] !== '' && sixthWeek[0] !== '') {
            monthArrayData = [
                { 'week': numberOfDaysOfAMonth.slice(0, 7) },
                { 'week': numberOfDaysOfAMonth.slice(7, 14) },
                { 'week': numberOfDaysOfAMonth.slice(14, 21) },
                { 'week': numberOfDaysOfAMonth.slice(21, 28) },
                { 'week': numberOfDaysOfAMonth.slice(28, 35) },
                { 'week': numberOfDaysOfAMonth.slice(35, 42) }
            ];
        }
        else if (fifthWeek[0] !== '') {
            monthArrayData = [
                { 'week': numberOfDaysOfAMonth.slice(0, 7) },
                { 'week': numberOfDaysOfAMonth.slice(7, 14) },
                { 'week': numberOfDaysOfAMonth.slice(14, 21) },
                { 'week': numberOfDaysOfAMonth.slice(21, 28) },
                { 'week': numberOfDaysOfAMonth.slice(28, 35) }
            ];
        }
        return monthArrayData;
    }
    select(item, month, year) {
        if (item && month) {
            this.selected = item;
            this.selectedMonth = month;
            this.selectedYear = year;
        }
    }
    isToday(item, month, year) {
        if (item && month) {
            if (this.currentDate === item && this.currentMonthTitle === month && this.currentYear === year) {
                return true;
            }
        }
    }
    isSelected(item, month, year) {
        if (item && month) {
            if (this.selected === item && month === this.selectedMonth && this.selectedYear === year) {
                if (item >= this.currentDate || item <= (this.dueDate + 2)) {
                    return this.selected === item;
                }
            }
        }
    }
    isDue(item, month, year) {
        if (item) {
            if (this.dueDate === item && this.currentMonthTitle === month && this.currentYear === year) {
                return true;
            }
            if (this.currentMonthTitle !== month) {
                if (this.nextMonth === this.currentMonth && this.currentYear === year) {
                    const dueNewDate = this.dueDate - this.lastDayOfAMonth;
                    if (dueNewDate === item) {
                        return true;
                    }
                }
            }
        }
    }
    isDisabled(item, month, year) {
        if (item) {
            if (month === this.currentMonthTitle && this.currentYear === year) {
                if (item < this.currentDate || item > this.dueDate) {
                    return true;
                }
            }
            else if (month !== this.currentMonthTitle) {
                if (this.nextMonth === this.currentMonth && this.currentYear === year) {
                    const dueNewDate = this.dueDate - this.lastDayOfAMonth;
                    if (item <= dueNewDate) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                else {
                    return true;
                }
            }
            else if (month === this.currentMonthTitle && this.currentYear !== year) {
                return true;
            }
        }
    }
    isPayment(item, month, year) {
        if (this.indicator) {
            if (item && month) {
                this.payment = this.currentDate + 4;
                if (this.payment === item && this.currentMonthTitle === month && this.currentYear === year) {
                    return true;
                }
            }

        }
    }
    getPrevMonth() {
        if (this.currentMonth === 0) {
            this.year = this.year - 1;
            this.currentMonth = 11; //Past Year, and set month as December
        } else {
            this.currentMonth = this.currentMonth - 1;
        }
        this.monthTitle = this.month[this.currentMonth];
        this.firstDayOfCurrentMonth = this.getFirstDayOfAMonth(this.year, this.currentMonth);
        this.lastDayOfCurrentMonth = this.getLastDayOfAMonth(this.year, this.currentMonth);
        const currentMonthNumberOfDays = this.getMonthDateEntries(this.firstDayOfCurrentMonth, this.lastDayOfCurrentMonth);
        const fifthWeek = currentMonthNumberOfDays.slice(28, 35);
        const sixthWeek = currentMonthNumberOfDays.slice(35, 42);
        if (fifthWeek[0] !== '' && sixthWeek[0] !== '') {
            this.monthArray = [{ 'week': currentMonthNumberOfDays.slice(0, 7) },
            { 'week': currentMonthNumberOfDays.slice(7, 14) },
            { 'week': currentMonthNumberOfDays.slice(14, 21) },
            { 'week': currentMonthNumberOfDays.slice(21, 28) },
            { 'week': currentMonthNumberOfDays.slice(28, 35) },
            { 'week': currentMonthNumberOfDays.slice(35, 42) }
            ];
        }
        else if (fifthWeek[0] !== '') {
            this.monthArray = [{ 'week': currentMonthNumberOfDays.slice(0, 7) },
            { 'week': currentMonthNumberOfDays.slice(7, 14) },
            { 'week': currentMonthNumberOfDays.slice(14, 21) },
            { 'week': currentMonthNumberOfDays.slice(21, 28) },
            { 'week': currentMonthNumberOfDays.slice(28, 35) }
            ];
        }
    }
    getNextMonth() {
        if (this.currentMonth === 11) {
            this.year = this.year + 1;
            this.currentMonth = 0; //restart of month from January
        } else {
            this.currentMonth = this.currentMonth + 1;
        }
        this.nextMonth = this.date.getMonth() + 1;
        this.monthTitle = this.month[this.currentMonth];
        this.firstDayOfCurrentMonth = this.getFirstDayOfAMonth(this.year, this.currentMonth);
        this.lastDayOfCurrentMonth = this.getLastDayOfAMonth(this.year, this.currentMonth);
        const currentMonthNumberOfDays = this.getMonthDateEntries(this.firstDayOfCurrentMonth, this.lastDayOfCurrentMonth);
        const fifthWeek = currentMonthNumberOfDays.slice(28, 35);
        const sixthWeek = currentMonthNumberOfDays.slice(35, 42);
        if (fifthWeek[0] !== '' && sixthWeek[0] !== '') {
            this.monthArray = [{ 'week': currentMonthNumberOfDays.slice(0, 7) },
            { 'week': currentMonthNumberOfDays.slice(7, 14) },
            { 'week': currentMonthNumberOfDays.slice(14, 21) },
            { 'week': currentMonthNumberOfDays.slice(21, 28) },
            { 'week': currentMonthNumberOfDays.slice(28, 35) },
            { 'week': currentMonthNumberOfDays.slice(35, 42) }
            ];
        }
        else if (fifthWeek[0] !== '') {
            this.monthArray = [{ 'week': currentMonthNumberOfDays.slice(0, 7) },
            { 'week': currentMonthNumberOfDays.slice(7, 14) },
            { 'week': currentMonthNumberOfDays.slice(14, 21) },
            { 'week': currentMonthNumberOfDays.slice(21, 28) },
            { 'week': currentMonthNumberOfDays.slice(28, 35) }
            ];
        }
    }
    private getMonthDateEntries(firstDayOfMonth, lastDayOfMonth): Array<any> {
        let dateEntry = 1;
        let i = 0;
        const index = 0;
        const dateEntries = Array<any>(42).fill('');
        while (i < dateEntries.length) {
            if (i >= firstDayOfMonth && i < (firstDayOfMonth + lastDayOfMonth)) {
                dateEntries[i] = dateEntry;
                dateEntry++;
            }
            i++;
        }
        return dateEntries;
    }
    selectedBtn() {
        if (this.selected) {
            this.selectedDateFormat = this.selectedMonth + ' ' + this.selected + ', ' + this.selectedYear;
        }
        this.callBackValue.emit(this.selectedDateFormat);
        this.visible = false;
        this.commonService.showScroll(true);
    }
    close() {
        this.visible = false;
        this.commonService.showScroll(true);
        this.callBackValue.emit(this.selectedDateFormat);
    }
}
