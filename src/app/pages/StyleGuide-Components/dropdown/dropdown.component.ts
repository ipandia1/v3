import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {
  showClr1Code = false;
  showClr1Code1 = false;
  showClr1Code2 = false;
  showClr1Code3 = false;
  showClr1Code4 = false;
  constructor() { }

  ngOnInit() {
  }

  selectedVal1: string="Select State"
  categoriesValues: Array<any>=[{state:'AL',disabled:true},{state:'AK',disabled:false},{state:'AZ',disabled:false},{state:'AR',disabled:false},{state:'CA',disabled:false},{state:'CO',disabled:false},{state:'CT',disabled:false},{state:'DE',disabled:false},{state:'EL',disabled:false},{state:'GA',disabled:false},{state:'HI',disabled:false},{state:'ID',disabled:false},{state:'VI',disabled:false},{state:'WA',disabled:false},{state:'WV',disabled:false},{state:'WI',disabled:true}];
  select1(value: string): void {
   this.selectedVal1 = value;
  }
}
