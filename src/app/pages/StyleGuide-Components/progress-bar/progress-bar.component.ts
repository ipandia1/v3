import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress-style-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarstyleComponent implements OnInit {

  completedPercentage: Number = 70;
  constructor() { }
  ngOnInit() {
  }

}
