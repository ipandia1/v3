import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-dynamic',
  templateUrl: './list-dynamic.component.html',
  styleUrls: ['./list-dynamic.component.scss']
})
export class ListDynamicComponent {

  showListEx1Code= false;
  showListEx2Code= false;
  showListEx3Code= false;
  showListEx4Code= false;
  showListEx5Code= false;
  showListEx6Code= false;
  showListEx7Code= false;
  showListEx8Code= false;
  showListEx9Code= false;
  showListEx10Code= false;
  showListEx11Code= false;
  showListEx12Code= false;
  showListEx13Code= false;
  showListEx14Code= false;
  showListEx15Code= false;
  showListEx16Code= false;
  showListEx17Code= false;
  showListEx18Code= false;
  showListEx19Code= false;


  isCollapsed = false;
  isCollapsed1 = false;
  isCollapsed2 = false;
  isCollapsed3 = false;
  isCollapsed4 = false;
  isCollapsed5 = false;
  isCollapsed6 = false;
  isCollapsed7 = false;
  isCollapsed8 = false;
  isCollapsed9 = false;
  isCollapsed10 = false;
  isCollapsed11 = false;
  isCollapsed12= false;
  isCollapsed13 = false;
  isCollapsed14 = false;
  isCollapsed15 = false;
  isCollapsed16 = false;
  isCollapsed17 = false;
  isCollapsed18 = false;
  isCollapsed19 = false;
  isCollapsed20 = false;
  isCollapsed21 = false;
  isCollapsed22 = false;
  isCollapsed23 = false;
  isCollapsed24 = false;
  isCollapsed25 = false;
  isCollapsed26 = false;
  isCollapsed27 = false;
  isCollapsed28 = false;
  isCollapsed29 = false;


  constructor() {
    window.scrollTo(0, 0);
  }
  clickToggle = function () {
    this.isCollapsed = !this.isCollapsed;
  }
  clickToggle1 = function () {
    this.isCollapsed1 = !this.isCollapsed1;
  }
  clickToggle2 = function () {
    this.isCollapsed2 = !this.isCollapsed2;
  }
  clickToggle3 = function () {
    this.isCollapsed3 = !this.isCollapsed3;
  }
  clickToggle4 = function () {
    this.isCollapsed4 = !this.isCollapsed4;
  }
  clickToggle5 = function () {
    this.isCollapsed5 = !this.isCollapsed5;
  }
  clickToggle6 = function () {
    this.isCollapsed6 = !this.isCollapsed6;
  }
  clickToggle7 = function () {
    this.isCollapsed7 = !this.isCollapsed7;
  }
  clickToggle8 = function () {
    this.isCollapsed8 = !this.isCollapsed8;
  }
  clickToggle9 = function () {
    this.isCollapsed9 = !this.isCollapsed9;
  }
  clickToggle10 = function () {
    this.isCollapsed10 = !this.isCollapsed10;
  }
  clickToggle11 = function () {
    this.isCollapsed11 = !this.isCollapsed11;
  }
  clickToggle12 = function () {
    this.isCollapsed12 = !this.isCollapsed12;
  }
  clickToggle13 = function () {
    this.isCollapsed13 = !this.isCollapsed13;
  }
  clickToggle14 = function () {
    this.isCollapsed14 = !this.isCollapsed14;
  }
  clickToggle15 = function () {
    this.isCollapsed15 = !this.isCollapsed15;
  }
  clickToggle16 = function () {
    this.isCollapsed16 = !this.isCollapsed16;
  }
  clickToggle17 = function () {
    this.isCollapsed17 = !this.isCollapsed17;
  }
  clickToggle18 = function () {
    this.isCollapsed18 = !this.isCollapsed18;
  }
  clickToggle19 = function () {
    this.isCollapsed19 = !this.isCollapsed19;
  }
  clickToggle20 = function () {
    this.isCollapsed20 = !this.isCollapsed20;
  }
  clickToggle21 = function () {
    this.isCollapsed21 = !this.isCollapsed21;
  }
  clickToggle22 = function () {
    this.isCollapsed22 = !this.isCollapsed22;
  }
  clickToggle23 = function () {
    this.isCollapsed23 = !this.isCollapsed23;
  }
  clickToggle24 = function () {
    this.isCollapsed24 = !this.isCollapsed24;
  }
  clickToggle25 = function () {
    this.isCollapsed25 = !this.isCollapsed25;
  }
  clickToggle26 = function () {
    this.isCollapsed26 = !this.isCollapsed26;
  }
  clickToggle27 = function () {
    this.isCollapsed27 = !this.isCollapsed27;
  }
  clickToggle28 = function () {
    this.isCollapsed28 = !this.isCollapsed28;
  }
  clickToggle29 = function () {
    this.isCollapsed29 = !this.isCollapsed29;
  }

}
