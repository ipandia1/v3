import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-colors',
  templateUrl: './colors.component.html'
})
export class ColorsComponent implements OnInit {

  showClr1Code = false;
  showClr2Code = false;
  showClr3Code = false;
  showClr4Code = false;
  showClr5Code = false;
  showClr6Code = false;
  showClr7Code = false;
  showClr8Code = false;
  showClr9Code = false;
  showClr10Code = false;
  showClr11Code = false;
  showClr12Code = false;
  showClr13Code = false;
  showClr14Code = false;
  showClr15Code = false;
  showClr16Code = false;
  showClr17Code = false;
  showClr18Code = false;
  showClr19Code = false;
  showClr20Code = false;
  showClr21Code = false;
  showClr22Code = false;
  showClr23Code = false;
  constructor() { }

  ngOnInit() {
  }

}
