import { Component, ViewEncapsulation } from '@angular/core';


@Component({
  selector: 'app-styleguidehome-root',
  templateUrl: './styleguide-home.component.html',
  encapsulation: ViewEncapsulation.None
})
export class StyleGuideHomeComponent {

  constructor() {
    window.scrollTo(0, 0);
  }
}
