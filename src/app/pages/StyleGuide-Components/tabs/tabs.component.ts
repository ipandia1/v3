import { Component, OnInit, EventEmitter } from '@angular/core';
import { TargetLocator } from 'selenium-webdriver';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {
  activeTrue = false;
  public dataArray: Array<any> = [
    { tabTitle: 'Item One long', dataTabCheck: true, content: 'Tab Content 1' },
    { tabTitle: 'Item Two', dataTabCheck: false, content: 'Tab Content 2' },
    { tabTitle: 'Item Three', dataTabCheck: false, content: 'Tab Content 3' },
    { tabTitle: 'Item Four', dataTabCheck: false, content: 'Tab Content 4' },
    { tabTitle: 'Item Five', dataTabCheck: false, content: 'Tab Content 5' },
    { tabTitle: 'Item Six', dataTabCheck: false, content: 'Tab Content 6' }
  ]
  public iconArray: Array<any> = [
    { iconTabTitle: 'fa fa-envelope', iconTabCheck: true, iconContent: 'Tab Content 1' },
    { iconTabTitle: 'fa fa-heart', iconTabCheck: false, iconContent: 'Tab Content 2' },
    { iconTabTitle: 'fa fa-smile-o', iconTabCheck: false, iconContent: 'Tab Content 3' },
    { iconTabTitle: 'fa fa-question-circle', iconTabCheck: false, iconContent: 'Tab Content 4' },
    { iconTabTitle: 'fa fa-lock', iconTabCheck: false, iconContent: 'Tab Content 5' },
    { iconTabTitle: 'fa fa-trash', iconTabCheck: false, iconContent: 'Tab Content 6' }
  ]
  public iconTextArray: Array<any> = [
    { iconTabTitle: 'fa fa-envelope', title: 'Item One long', iconTabCheck: true, iconContent: 'Tab Content 1' },
    { iconTabTitle: 'fa fa-heart', title: 'Item Two', iconTabCheck: false, iconContent: 'Tab Content 2' },
    { iconTabTitle: 'fa fa-smile-o', title: 'Item Three', iconTabCheck: false, iconContent: 'Tab Content 3' },
    { iconTabTitle: 'fa fa-question-circle', title: 'Item Four', iconTabCheck: false, iconContent: 'Tab Content 4' },
    { iconTabTitle: 'fa fa-lock', title: 'Item Five', iconTabCheck: false, iconContent: 'Tab Content 5' },
    { iconTabTitle: 'fa fa-trash', title: 'Item Six', iconTabCheck: false, iconContent: 'Tab Content 6' }
  ]
  public scrollDataArray: Array<any> = [
    { tabTitle: 'Item One long', dataTabCheck: true, content: 'Tab Content 1' },
    { tabTitle: 'Item Two', dataTabCheck: false, content: 'Tab Content 2' },
    { tabTitle: 'Item Three', dataTabCheck: false, content: 'Tab Content 3' },
    { tabTitle: 'Item Four', dataTabCheck: false, content: 'Tab Content 4' },
    { tabTitle: 'Item Five', dataTabCheck: false, content: 'Tab Content 5' },
    { tabTitle: 'Item Six', dataTabCheck: false, content: 'Tab Content 6' },
    { tabTitle: 'Item Seven', dataTabCheck: false, content: 'Tab Content 7' },
    { tabTitle: 'Item Eight', dataTabCheck: false, content: 'Tab Content 8' }
  ]
  public scrollIconTextArray: Array<any> = [
    { iconTabTitle: 'fa fa-envelope', title: 'Item One long', iconTabCheck: true, iconContent: 'Tab Content 1' },
    { iconTabTitle: 'fa fa-heart', title: 'Item Two', iconTabCheck: false, iconContent: 'Tab Content 2' },
    { iconTabTitle: 'fa fa-smile-o', title: 'Item Three', iconTabCheck: false, iconContent: 'Tab Content 3' },
    { iconTabTitle: 'fa fa-question-circle', title: 'Item Four', iconTabCheck: false, iconContent: 'Tab Content 4' },
    { iconTabTitle: 'fa fa-lock', title: 'Item Five', iconTabCheck: false, iconContent: 'Tab Content 5' },
    { iconTabTitle: 'fa fa-trash', title: 'Item Six', iconTabCheck: false, iconContent: 'Tab Content 6' },
    { iconTabTitle: 'fa fa-heart', title: 'Item Seven', iconTabCheck: false, iconContent: 'Tab Content 7' },
    { iconTabTitle: 'fa fa-lock', title: 'Item Eight', iconTabCheck: false, iconContent: 'Tab Content 8' }
  ]
  scrollDiv: any;
  leftArrow: any;
  constructor() { }

  ngOnInit() {
  }
  clickFun(val, tabName) {

    if (tabName === 'firstTab') {
      for (let i = 0; i < this.dataArray.length; i++) {
        this.dataArray[i].dataTabCheck = false;
      }
      this.dataArray[val].dataTabCheck = true;
    } else if (tabName === 'secondTab') {
      for (let i = 0; i < this.iconArray.length; i++) {
        this.iconArray[i].iconTabCheck = false;
      }
      this.iconArray[val].iconTabCheck = true;
    } else if (tabName === 'thirdTab') {
      for (let i = 0; i < this.iconTextArray.length; i++) {
        this.iconTextArray[i].iconTabCheck = false;
      }
      this.iconTextArray[val].iconTabCheck = true;
    } else if (tabName === 'fourthTab') {
      for (let i = 0; i < this.scrollDataArray.length; i++) {
        this.scrollDataArray[i].dataTabCheck = false;
      }
      this.scrollDataArray[val].dataTabCheck = true;
    } else if (tabName === 'fifthTab') {
      for (let i = 0; i < this.scrollIconTextArray.length; i++) {
        this.scrollIconTextArray[i].iconTabCheck = false;
      }
      this.scrollIconTextArray[val].iconTabCheck = true;
    }
  }

  rightArrowFunc(event) {
    this.scrollDiv = event.target.previousSibling.previousElementSibling;
    this.scrollDiv.scrollBy(100, 0);
    this.leftArrow = this.scrollDiv.previousSibling.previousElementSibling;
    if (this.scrollDiv.scrollLeft === 0) {
      this.leftArrow.classList.remove('d-flex');
      this.leftArrow.classList.add('hidden');
    } else {
      this.leftArrow.classList.remove('hidden');
      this.leftArrow.classList.add('d-flex');
    }
  }
  leftArrowFunc(event) {
    this.scrollDiv = event.target.nextSibling.nextElementSibling;
    this.scrollDiv.scrollBy(-100, 0);
    this.leftArrow = this.scrollDiv.previousSibling.previousElementSibling;
    if (this.scrollDiv.scrollLeft === 0) {
      this.leftArrow.classList.remove('d-flex');
      this.leftArrow.classList.add('hidden');
    } else {
      this.leftArrow.classList.remove('hidden');
      this.leftArrow.classList.add('d-flex');
    }
  }
}
