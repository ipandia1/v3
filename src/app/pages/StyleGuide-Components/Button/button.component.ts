import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ButtonComponent implements OnInit {

  showDisplay1Code = false;
  showDisplay2Code = false;
  showDisplay3Code = false;
  showDisplay4Code = false;
  showDisplay5Code = false;
  showDisplay6Code = false;
  showDisplay7Code = false;
  showDisplay8Code = false;
  showDisplay9Code = false;
  showDisplay10Code = false;
  showDisplay11Code = false;
  showDisplay12Code = false;
  constructor() { }

  ngOnInit() {
  }

}
