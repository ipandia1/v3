import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-spacers',
  templateUrl: './spacers.component.html'
})
export class SpacersComponent implements OnInit {
  showBaseCode = false;
  showXsmallCode = false;
  showSmallCode = false;
  showMediumCode = false;
  showLargeCode = false;
  showXLCode = false;
  showXXLCode = false;
  showXXXLCode = false;
  constructor() { }

  ngOnInit() {
  }

}
