import { Component, Input, Output } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({ transform: 'scale(.6)' }),
        animate(300)
      ]),
     transition('* => void', [
        animate(100, style({ transform: 'scale(0.6)' }))
      ])
    ])
  ]
})
export class ModalComponent {
 visible: boolean;
 constructor() { }
 close() {
    this.visible = false;
  }

}
