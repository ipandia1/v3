import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-typography',
  templateUrl: './typography.component.html'
})
export class TypographyComponent implements OnInit {
  showDisplay1Code = false;
  showDisplay2Code = false;
  showDisplay3Code = false;
  showDisplay4Code = false;
  showDisplay5Code = false;
  showDisplay6Code = false;
  showH1Code = false;
  showH2Code = false;
  showH3Code = false;
  showH4Code = false;
  showH5Code = false;
  showH6Code = false;
  showEyebrowCode = false;
  showbodyCode = false;
  showbodyBoldCode = false;
  showbodyLinkCode = false;
  showbodyBLCode = false;
  showLegalCode = false;
  showLegalBoldCode = false;
  showLegalLinkCode = false;
  showLegalBLCode = false;
  constructor() { }

  ngOnInit() {
  }

}
