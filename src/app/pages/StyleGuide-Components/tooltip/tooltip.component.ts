import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent implements OnInit {
  showTooltipBottomCode:boolean=false;
  showTooltipTopCode:boolean=false;
  showTooltipRightCode:boolean=false;
  showTooltipTopLeftCode:boolean=false;
  showTooltipBottomRightCode:boolean=false;
  showTooltipBottomLeftCode:boolean=false;
  showTooltipLeftCode:boolean=false;
  showTooltipTopRightCode:boolean=false;
  constructor() { }

  ngOnInit() {
     }

}
