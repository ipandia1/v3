import { Component, OnInit } from '@angular/core';

import { SpinnerService } from '../../../services/spinner.service';

@Component({
  selector: 'app-spinner-page',
  templateUrl: './spinner-page.component.html',
  styleUrls: ['./spinner-page.component.scss']
})
export class SpinnerPageComponent implements OnInit {
  showClr2Code;
  showspinner: Boolean = false;
  noOverlay: Boolean = true;
  constructor(private spinnerservice: SpinnerService) {
  }

  showsSpinner() {
    this.spinnerservice.showspinner(true);
    this.spinnerservice.showoverlay(true);
  }

  hidesSpinner() {
    this.spinnerservice.showspinner(false);
    this.spinnerservice.showoverlay(false);
  }

  delayedSpinner() {
    this.showsSpinner();
    setTimeout(() => {
      this.hidesSpinner();
    }, 5000)
  }

  ngOnInit() {

  }
}
