import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-my-wallet',
  templateUrl: './my-wallet.component.html',
  styleUrls: ['./my-wallet.component.scss']
})
export class MyWalletComponent implements OnInit, AfterViewInit {
  currentOpenModalId: any;
  deleteModalVisible: boolean;
  walletPageData: any;
  walletData: any;
  walletDataPath = 'assets/json/wallet/wallet.json';
  walletPageIndex = 3; // to change the screen give 1 to 6 numbers
  visible = true;

  parentSubject: Subject<any> = new Subject();

  constructor(private commonService: CommonService) {
    this.commonService.getJsonData(this.walletDataPath)
      .subscribe(responseJson => {
        this.walletData = responseJson;
        this.walletPageData = this.walletData['wallet-' + this.walletPageIndex][0];
      });
  }
  ngOnInit() {
    
  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    // if (this.visible) {
      setTimeout(() => {
      const body = document.getElementsByTagName('body')[0];
      body.classList.add('noScroll');
      const dialog = document.getElementById('goToWalletModal');
      dialog.focus();
     }, 1000);
    // }
  }
  openGoToWallet() {
    this.visible = true;
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('noScroll');
    this.walletModalFocus();
  }
  walletModalFocus() {
      const dialog = document.getElementById('goToWalletModal');
      dialog.focus();
  }
  deleteModalOpen(ev) {
    this.currentOpenModalId = ev;
    this.deleteModalVisible = true;
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('noScroll');
    this.deleteModalFocus();
  }
  deleteModalFocus() {
    const dialog = document.getElementById('deleteModal');
    dialog.focus();
  }
  close() {
    this.visible = false;
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('noScroll');
    const head = document.getElementById('walletHead');
    head.focus();
  }
  closeChildModal() {
    this.deleteModalVisible = false;
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('noScroll');
    this.parentSubject.next(this.currentOpenModalId);
  }
  backTabEvent(event, mainId,lastId) {
    const allElements = document.getElementById(mainId).querySelectorAll
    ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
    let a: any;
      if (document.activeElement === allElements[0]) {
        if (event.keyCode === 9) {
          if (event.shiftKey) {
            event.preventDefault();
            setTimeout(function() {
              //  a = allElements[allElements.length - 1];
              // a.focus();
              document.getElementById(lastId).focus();
            }, 0);
          }
        }
      }
      if (document.activeElement === document.getElementById(mainId)) {
        if (event.keyCode === 9) {
          if (event.shiftKey) {
            event.preventDefault();
            setTimeout(function() {
              // a = allElements[allElements.length - 1];
              // a.focus();
              document.getElementById(lastId).focus();

            }, 0);
          }
        }
      }
      if (document.activeElement === allElements[allElements.length - 1]) {
        if (event.keyCode === 9) {
          if (event.shiftKey) {}
          else {
          event.preventDefault();
          setTimeout(function() {
            document.getElementById(mainId).focus();
          }, 0);
        }
      }
      }
  }

}
