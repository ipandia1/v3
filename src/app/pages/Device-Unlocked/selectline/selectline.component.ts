import { CommonService } from '../../../services/common.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-selectline',
  templateUrl: './selectline.component.html',
  styleUrls: ['./selectline.component.scss']
})

export class SelectlineComponent implements OnInit {
  LineSelectorPath = 'assets/json/deviceUnlock/selectLine.json';
  LineSelectorData: any;
  constructor(private commonService: CommonService) {
    this.commonService.getJsonData(this.LineSelectorPath)
     .subscribe(responseJson => {
       this.LineSelectorData = responseJson;
     });
   }
  ngOnInit() {
    }

}
