import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-unlock-your-device',
  templateUrl: './unlock-your-device.component.html',
  styleUrls: ['./unlock-your-device.component.scss']
})
export class UnlockYourDeviceComponent implements OnInit {
  errorMessageValid: string;
  errorMessage: string;
  minlength: string;
  maxl: string;
  imeiValid = true;
  inputType = {
    inputText: ''
  };
  constructor() { }
  ngOnInit() {
  }
  checkImei(inputText: string) {
    if (inputText != null && inputText.length > 0) {
      this.imeiValid = true;
    } else {
      this.imeiValid = false;
    }
  }
  onKey(event: any) {
    this.imeiValid = true;
  }
}


