import { Component, OnInit } from "@angular/core";

@Component({
  selector: 'app-status-check',
  templateUrl: './status-check.component.html',
  styleUrls: ['./status-check.component.scss']
})
export class StatusCheckComponent implements OnInit {
  // device unlocked
  device = {
    Status: "Device unlocked", deviceLockedStage1: false,
    message: "Please wait up to 10 minutes to restart your device. Don’t forget to backup your device via iCloud.",
    policy: "T-Mobile's unlock policy", btnMessage1: "CTA",
    btnMessage2: "Check a device not on your account", Name: "iPhone 6s - Gold - 16GB",
    IMEI: "359157074493510", OwnerDetails: "John, (307) 220-3553"
  };

  // device locked
  // device = {
  //   Status: "Device locked", deviceLockedStage1: false,
  //   message: "Your device is eligible to be unlocked.",
  //   policy: "T-Mobile's unlock policy", btnMessage1: "CTA",
  //   btnMessage2: "Check a device not on your account", Name: "iPhone 6s - Gold - 16GB",
  //   IMEI: "359157074493510", OwnerDetails: "John, (307) 220-3553"
  // };

   // device locked API failure
  //  device = {
  //   Status: "Device locked", deviceLockedStage1: false,
  //   message: "Something went wrong with your request. Please try again. Message us for immediate assistance.",
  //   policy: "T-Mobile's unlock policy", btnMessage1: "CTA",
  //   btnMessage2: "Check a device not on your account", Name: "iPhone 6s - Gold - 16GB",
  //   IMEI: "359157074493510", OwnerDetails: "John, (307) 220-3553"
  // };

    // device locked - steps
  //  device = {
  //   Status: "Device locked", deviceLockedStage1: true,
  //   message: "Something went wrong with your request. Please try again. Message us for immediate assistance.",
  //   policy: "T-Mobile's unlock policy", btnMessage1: "CTA",
  //   btnMessage2: "Check a device not on your account", Name: "iPhone 6s - Gold - 16GB",
  //   IMEI: "359157074493510", OwnerDetails: "John, (307) 220-3553"
  // };

  // status unknown
  // device = {
  //   Status: "Status unknown", deviceLockedStage1: false,
  //   message: "Something went wrong with your request. Please try again later or message us for immediate assistance.",
  //   policy: "T-Mobile's unlock policy", btnMessage1: "CTA",
  //   btnMessage2: "Check a device not on your account", Name: "iPhone 6s - Gold - 16GB",
  //   IMEI: "359157074493510", OwnerDetails: "John, (307) 220-3553"
  // };

  constructor() { }

  ngOnInit() {
  }

  message1Click() {
    // console.log("button clicked");
  }
  message2Click() {
    // console.log("button clicked");
  }
}
