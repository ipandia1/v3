import { Component, OnInit, AfterViewInit, ElementRef, HostListener } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-enter-card-information',
  templateUrl: './enter-card-information.component.html',
  styleUrls: ['./enter-card-information.component.scss']
})
export class EnterCardInformationComponent implements OnInit {
  deleteVisible: boolean;
  visible: boolean;
  pciCard: PciCard;
  setDefault = false;
  saveToMyWallet = true;
  saveToWalletCheck = true; // true/false for save to wallet check box
  newCardInfo = false; // true/false for new card and edit card pages
  cardIcon = 'palinCardImg';
  constructor(private commonService: CommonService, private elementref: ElementRef) { }
  ngOnInit() {
    this.pciCard = {
      cardName: '',
      creditCardNumber: '',
      expirationDate: '',
      cvvNumber: '',
      zipCode: '',
      nickName: ''
    };
  }

  saveToWalletToggle() {
    this.saveToMyWallet = !this.saveToMyWallet;
  }
  setDefaultToggle() {
    this.setDefault = !this.setDefault;
  }
  moveToTop(id) {
    this.elementref.nativeElement.querySelector('#' + id).focus();
  }
  closeModal() {
    this.visible = false;
    this.commonService.showScroll(true);
    this.elementref.nativeElement.querySelector('#whatsDis').focus();
  }
  showModal() {
    this.visible = true;
    this.commonService.showScroll(false);
    this.elementref.nativeElement.querySelector('#visaImgInModal').focus();
  }
  @HostListener('window:resize', ['$event'])
  onResize() {
    if (this.visible === true) {
      this.commonService.showScroll(false);
      this.elementref.nativeElement.querySelector('#visaImgInModal').focus();
    }
    if (this.deleteVisible === true) {
      this.commonService.showScroll(false);
      this.elementref.nativeElement.querySelector('#deleteModal').focus();
    }
  }
  onRemove(flag) {
    if (!flag) {
      this.deleteVisible = true;
      this.commonService.showScroll(false);
      this.elementref.nativeElement.querySelector('#deleteModal').focus();
    }
  }
  closeDeleteModal() {
    this.deleteVisible = false;
    this.commonService.showScroll(true);
    this.elementref.nativeElement.querySelector('#removeLink').focus();
  }
  cardChange(event) {
    const target = event.target || event.srcElement;
    const val = target.value.split('')[0];

    if (val === '4') {
      this.cardIcon = 'visaImg';
    } else if (val === '5') {
      this.cardIcon = 'masterImg';
    } else if (val === '1') {
      this.cardIcon = 'expressImg';
    } else {
      this.cardIcon = 'palinCardImg';
    }
  }
  backTabEvent(event, mainId, lastId) {
    const allElements = document.getElementById(mainId).querySelectorAll
      ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
    if (document.activeElement === allElements[0]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          const that = this;
          setTimeout(function () {
            this.elementref.nativeElement.querySelector('#' + lastId).focus();
          }, 0);
        }
      }
    }
    if (document.activeElement === document.getElementById(mainId)) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          const that = this;
          setTimeout(function () {
            that.elementref.nativeElement.querySelector('#' + lastId).focus();
          }, 0);
        }
      }
    }
    if (document.activeElement === allElements[allElements.length - 1]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) { } else {
          event.preventDefault();
          const that = this;
          setTimeout(function () {
            that.elementref.nativeElement.querySelector('#' + mainId).focus();
          }, 0);
        }
      }
    }
  }
}
export class PciCard {
  cardName: string;
  creditCardNumber: string;
  expirationDate: string;
  cvvNumber: string;
  zipCode: string;
  nickName: string;
}
