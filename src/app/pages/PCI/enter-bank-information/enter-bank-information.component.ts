import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-enter-bank-information',
  templateUrl: './enter-bank-information.component.html',
  styleUrls: ['./enter-bank-information.component.scss']
})
export class EnterBankInformationComponent implements OnInit {
  deleteVisible: boolean;
  visible: boolean;
  bankinfo: BankInfo;
  saveToWalletCheck = true; // true/false for save to wallet check box
  newBankInfo = true; // true/false for new bank and edit card pages
  setDefault = false;
  saveToMyWallet = true;

  constructor(private commonService: CommonService, private elementref: ElementRef) { }

  ngOnInit() {
    this.bankinfo = {
      nameOnaAccount: '',
      rountingNumber: '',
      accountNumber: '',
      nickName: ''
    };
  }

  moveToTop(id) {
    this.elementref.nativeElement.querySelector('#' + id).focus();
  }
  closeModal() {
    this.visible = false;
    this.commonService.showScroll(true);
    this.elementref.nativeElement.querySelector('#acNumberModalLink').focus();
  }
  showModal() {
    this.visible = true;
    this.commonService.showScroll(false);
    this.elementref.nativeElement.querySelector('#acNumberModal').focus();

  }
  @HostListener('window:resize', ['$event'])
  onResize() {
    if (this.visible === true) {
      this.commonService.showScroll(false);
    this.elementref.nativeElement.querySelector('#acNumberModal').focus();
    }
    if (this.deleteVisible === true) {
      this.commonService.showScroll(false);
      this.elementref.nativeElement.querySelector('#deleteModal').focus();
    }
  }
  saveToWalletToggle() {
    this.saveToMyWallet = !this.saveToMyWallet;
  }
  setDefaultToggle() {
    this.setDefault = !this.setDefault;
  }
  onSubmit(flag) {
    if (!flag) {
      this.deleteVisible = true;
      this.commonService.showScroll(false);
      this.elementref.nativeElement.querySelector('#deleteModal').focus();
    }
  }
  closeDeleteModal() {
    this.deleteVisible = false;
    this.commonService.showScroll(true);
    this.elementref.nativeElement.querySelector('#removeLink').focus();
  }
  backTabEvent(event, mainId, lastId) {
    const allElements = document.getElementById(mainId).querySelectorAll
      ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
    if (document.activeElement === allElements[0]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          const that = this;
          setTimeout(function () {
            that.elementref.nativeElement.querySelector('#' + lastId).focus();
          }, 0);
        }
      }
    }
    if (document.activeElement === document.getElementById(mainId)) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          const that = this;
          setTimeout(function () {
            that.elementref.nativeElement.querySelector('#' + lastId).focus();
          }, 0);
        }
      }
    }
    if (document.activeElement === allElements[allElements.length - 1]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) { }
        else {
          event.preventDefault();
          const that = this;
          setTimeout(function () {
            that.elementref.nativeElement.querySelector('#' + mainId).focus();
          }, 0);
        }
      }
    }
  }
}

export class BankInfo {
  nameOnaAccount: string;
  rountingNumber: string;
  accountNumber: String;
  nickName: String;
}
