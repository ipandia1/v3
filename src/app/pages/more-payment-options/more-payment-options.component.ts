import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-more-payment-options',
  templateUrl: './more-payment-options.component.html',
  styleUrls: ['./more-payment-options.component.scss']
})
export class MorePaymentOptionsComponent implements OnInit {
  title = 'More payment options';
  paymentArray = [{'title': 'Paperless billing', 'content': 'Sign up for paperless billing', 'url': ''},
  {'title': 'Autopay on', 'content': 'You’re saving $20 each month', 'url': ''},
  {'title': 'Payment arrangements', 'content': 'Set up a payment arrangements', 'url': ''},
  {'title': 'Equipment installment plans', 'content': 'You have 1 active installment plan', 'url': ''},
  {'title': 'JUMP! on Demand leases', 'content': 'You have 2 completed leases', 'url': ''},
  {'title': 'Payment methods', 'content': 'Save up to 10 payment methods per line for secure, easy payments.', 'url': ''}];
  constructor() { }

  ngOnInit() {
  }

}
