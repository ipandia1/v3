import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tsconfirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss', '../styles/temporary-suspension.scss']
})
export class TsConfirmationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
