import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tsreactivationconfirmation',
  templateUrl: './reactivationconfirmation.component.html',
  styleUrls: ['./reactivationconfirmation.component.scss', '../styles/temporary-suspension.scss']
})
export class TsReactivationconfirmationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
