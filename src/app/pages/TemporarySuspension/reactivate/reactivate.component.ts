import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tsreactivate',
  templateUrl: './reactivate.component.html',
  styleUrls: ['./reactivate.component.scss', '../styles/temporary-suspension.scss']
})
export class TsReactivateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
