import { CommonService } from '../../../services/common.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tslineselector',
  templateUrl: './lineselector.component.html',
  styleUrls: ['./lineselector.component.scss', '../styles/temporary-suspension.scss']
})
export class TsLineselectorComponent implements OnInit {
  LineSelectorPath = 'assets/json/Temporary suspension/LineSelectorData.json';
  LineSelectorData: any;
  constructor(private commonService: CommonService) {
    this.commonService.getJsonData(this.LineSelectorPath)
     .subscribe(responseJson => {
       this.LineSelectorData = responseJson;
     });
   }
  ngOnInit() {
    }

}
