import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'cardpaymentblade',
  templateUrl: './cardpaymentblade.component.html',
  styleUrls: ['./cardpaymentblade.component.scss']
})
export class CardPaymentbladeComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('title') titleText: string;
  // tslint:disable-next-line:no-input-rename
  @Input('card') cardNumber: string;
  // tslint:disable-next-line:no-input-rename
  @Input('lefticon') iconImage = '';
   // tslint:disable-next-line:no-input-rename
  @Input('url') url = '';
  // tslint:disable-next-line:no-inferrable-types
  // tslint:disable-next-line:no-input-rename
  @Input('showArrow') showArrow = true;
  // tslint:disable-next-line:no-input-rename
//  @Input('expireStatus') expireStatus = '';
   // tslint:disable-next-line:no-input-rename
  @Input('titleClass') titleTextClass = 'body';
  // tslint:disable-next-line:no-input-rename
 // @Input('disableClass') disableClass = 'black';
  // tslint:disable-next-line:no-input-rename
 @Input('cardNumberClass') cardNumberClass = '';
  // New Variables for PA Edit
  // tslint:disable-next-line:no-input-rename
//  @Input('cardStatus') cardStatus = '';
  // tslint:disable-next-line:no-input-rename
 @Input('secondLineDesc') secondLineDesc = '';
  // tslint:disable-next-line:no-input-rename
// @Input('secondLineUrl') secondLineUrl = '';
 // tslint:disable-next-line:no-input-rename
 @Input('disableBlade') disableBlade = null;
  emptyValue = '';
  popUp = true;
  // tslint:disable-next-line:no-output-rename
  @Output('callBackValue') callBackValue = new EventEmitter();
  // tslint:disable-next-line:no-inferrable-types
  // tslint:disable-next-line:no-input-rename
  @Input('action') action = '';
  // tslint:disable-next-line:no-inferrable-types
  // tslint:disable-next-line:no-input-rename
  @Input('expiryDate') expiryDate = '';
  // tslint:disable-next-line:no-input-rename
  @Input('index') index = '';
  bladedisabled = false;
  descWarning = false;
  cardExpiryYear: string;
  cardExpiryMonth: string;
  @Input() parentSubject: Subject<any>;
  constructor() { }

  ngOnInit() {
    if (this.action === 'Confirm/Remove' || this.action === 'Remove') {
      this.iconImage = this.iconImage + '-disabled';
      this.bladedisabled = true;
      this.parentSubject.subscribe(event => {
        document.getElementById(event).focus();
      });
    }

    this.cardExpiryMonth = this.expiryDate.substring(1, 2);
    this.cardExpiryYear = this.expiryDate.substring(3, 5);
    const currentMonth = new Date().getMonth() + 1;
    const currentYear = new Date().getFullYear().toString().slice(-2);
    if (this.expiryDate) {
      if (this.cardExpiryMonth <= currentMonth.toString() && this.cardExpiryYear <= currentYear) {
        this.descWarning = true;
        this.expiryDate = 'Expired ' + this.expiryDate;
        this.iconImage = this.iconImage + '-disabled';
        this.bladedisabled = true;
      } else if (Number(this.cardExpiryMonth) - currentMonth <= 1 &&
        Number(this.cardExpiryYear === currentYear) || (currentMonth === 12 &&
          Number(this.cardExpiryYear) === Number(currentYear) + 1)) {
        this.descWarning = true;
        this.expiryDate = 'Expiring soon ' + this.expiryDate;
      }
    }
  }

  deleteModalOpen(action, index) {
      this.callBackValue.emit(action + '' + index);
  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    if (this.parentSubject) {
      this.parentSubject.unsubscribe();
    }
  }

}
