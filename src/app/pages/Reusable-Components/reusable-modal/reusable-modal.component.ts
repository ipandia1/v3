import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-reusable-modal',
  templateUrl: './reusable-modal.component.html',
  styleUrls: ['./reusable-modal.component.scss']
})
export class ReusableModalComponent implements OnInit {

  @Input('title') title = 'We\'re conflicted';
  @Input('subTitle') subTitle = 'Sub Title';
  @Input('content') content = 'Your selection conflicts with 2 GB High-Speed Data on your account. Select continue to remove 2 GB High-Speed Data and add your new selection.'
  @Input('primaryButton') primaryButton = 'Continue';
  @Input('secondaryButton') secondaryButton = '';
  visible: boolean;
  constructor() { }
  // close() {
  //   this.visible = false;
  // }
  close1 = false;
  @Output('callBackValue') callBackValue = new EventEmitter<boolean>();
  close() {
    this.callBackValue.emit(this.close1);
  }
  ngOnInit() {
  }

}
