import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-account-history-data',
  templateUrl: './account-history-data.component.html',
  styleUrls: ['./account-history-data.component.scss']
})
export class AccountHistoryDataComponent implements OnInit {
  @Input('title') title: Array<string> = ['Date', 'Activity', 'Line', 'Category', 'Amount'];
  @Input('data') data: Array<any>;
  @Input('category') category: 'All Categories';
  isEmptyValue = '';
  constructor() { }

  ngOnInit() {
  }

}
