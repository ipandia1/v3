import { Component, OnInit, Input, Output, EventEmitter,AfterContentChecked } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dropdown-selector',
  templateUrl: './dropdown-selector.component.html',
  styleUrls: ['./dropdown-selector.component.scss']
})
export class DropdownSelectorComponent implements OnInit,AfterContentChecked {
  @Input() subTitle= 'Filter';
  @Input() selectedVal: string;
  @Input() selectNames: Array<any>;
  @Input() dropdownId: string;
  @Input() subTitleClass = 'legal';
  @Input() selectedValClass = 'body';
  @Input() textToRead;
  tabindex = 0;
  DropdownAriaLabelText: string;
  @Output('callBackValue') callBackValue = new EventEmitter<string>();
  constructor(private router: Router) { 

    if (this.selectedVal === '') {
      this.selectedVal = this.selectNames[0].name;
    }

  }

   ngAfterContentChecked()
   {
    if (this.selectedVal === '') {
      this.selectedVal = this.selectNames[0].name;
    }
    
    if (this.selectedVal !== '' && typeof this.selectedVal !== 'undefined')
    {
    // console.log(this.selectedVal.replace(/<\/?[^>]+(>|$)/g, ''));
    this.DropdownAriaLabelText = this.selectedVal.replace(/<\/?[^>]+(>|$)/g, '');
    // console.log(this.DropdownAriaLabelText);
    }
   }
  ngOnInit() {
    for  (const  names  of  this.selectNames) {
      const sepIndex = names.name.indexOf(',');
      const string1 = names.name.split(',');
      if (string1[0].length > 13 && sepIndex !== -1) {
        names.name  =  string1[0].substring(0, 13) + '... , ' + string1[1] ;
      }
    }
    
    
  }
 
  select(value: string): any {
   // alert(value);
      this.selectedVal = value;
     // console.log(this.selectedVal.replace(/<\/?[^>]+(>|$)/g, ''));
     // console.log(document.getElementById('text1'));
      this.callBackValue.emit(this.selectedVal);
      // console.log(document.getElementById('text1').children[0].textContent) ;
  }
}
