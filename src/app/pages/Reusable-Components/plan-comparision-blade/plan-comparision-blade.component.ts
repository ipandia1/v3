import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-plan-comparision-blade',
  templateUrl: './plan-comparision-blade.component.html',
  styleUrls: ['./plan-comparision-blade.component.scss']
})
export class PlanComparisionBladeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input('reviewDetails') reviewDetails: Array<any>;
}
