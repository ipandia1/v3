import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-n-line-blade',
  templateUrl: './n-line-blade.component.html',
  styleUrls: ['./n-line-blade.component.scss']
})
export class NLineBladeComponent implements OnInit {

  @Input('planDetails') planDetails: Array<any>;
  constructor() { }

  ngOnInit() {
  }

}
