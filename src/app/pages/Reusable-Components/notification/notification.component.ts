import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit , HostListener,Renderer } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Router } from '@angular/router';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],

  animations: [
  trigger('alertAnimation', [
    transition('void => *', [
      style({transform: 'translateY(-100%)'}),
      animate(500)
    ]),
    transition('* => void', [
      animate(500, style({transform: 'translateY(-100%)'}))
      ])
  ])
]
})
export class NotificationComponent implements OnInit {
  @Input('alertIcon') alertIcon = 'error_filled_white';
  @Input('alertType') alertType = 'Critical';
  @Input('alertDesc') alertDesc = 'Here\'s a strong alert headline';
  @Input('alertSubDesc') alertSubDesc = 'Here\'s some additional copy explaining how to resolve';
  @Input('alertSubDescMobile') alertSubDescMobile = '';
  @Input('alertNavIcon') alertNavIcon = 'arrow-right-white';
  @Input('alertResolveURL') alertResolveURL = '';
  @Input('alertResolveURLAnchor') alertResolveURLAnchor = '';
  @Input('showNotification') showNotification = true;
  @Output('callBackCloseAlert') callBackCloseAlert = new EventEmitter<boolean>();
  @ViewChild('sticky') sticky: ElementRef;
  isEmpty = '';
  stickTop: boolean;
  alertBarOffset: any;

  ngAfterViewInit() {
    this.alertBarOffset = this.sticky.nativeElement.offsetTop;
  }
  constructor(private router: Router) { }
  /* For sticky-top of alert*/
  @HostListener('window:scroll', []) onScroll() {
    const pageYOffset: number = window.pageYOffset;
    if (pageYOffset >= this.alertBarOffset) {
      this.stickTop = true;
    } else {
      this.stickTop = false;
    }
  }
  ngOnInit() {
  }
  resolveAlert() {
    if ((this.alertResolveURL !== '' && this.alertResolveURLAnchor !== '')) { // Navigate to URl and its anchor tag
      this.router.navigate([this.alertResolveURL], { fragment: this.alertResolveURLAnchor });
    } else if (this.alertResolveURL !== '') { // Navigate to URL
      this.router.navigate([this.alertResolveURL]);
    } else {
      this.showNotification = !this.showNotification;
      this.callBackCloseAlert.emit(this.showNotification);
    }
  }
}
