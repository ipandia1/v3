import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'paginationblade',
  templateUrl: './paginationblade.component.html',
  styleUrls: ['./paginationblade.component.scss']
})
export class PaginationbladeComponent implements OnInit {

  isWhite;
  isUnderline;
  isWhite1;
  isUnderline1;
  totalpages;
  @Input('currentPage') currentPage: number;
  @Input('totalRecords') totalRecords: number;
  @Input('noofrecordsPerpage') noofrecordsPerpage: number;
  @Input('noofPagelink') noofPagelink: number;


  @Output('moveFirstpage') moveFirstpage = new EventEmitter<number>();
  @Output('moveLastpage') moveLastpage = new EventEmitter<number>();
  @Output('moveCurrentPage') moveCurrentPage = new EventEmitter<number>();

  constructor() { }
  ngOnInit() {
  }
  currentpage:number;

  currPage(n: number): void {
    this.moveCurrentPage.emit(n);
    this.currentPage = n ;
  }
  firstPage() {
    //this.moveFirstpage.emit(this.currentPage - 1);
    this.currPage(this.currentPage - 1);
  }
  lastPage() {
    //this.moveLastpage.emit(this.currentPage + 1);
    this.currPage(this.currentPage + 1);
    this.isUnderline = true;
  }
  maxpageno(): boolean {
    this.totalpages = Math.ceil(this.totalRecords / this.noofrecordsPerpage);
    if (this.currentPage === this.totalpages) {
      return true;
    } else {
      return false;
    }
  }
  getPagenumbers(): number[] {
    this.totalpages = Math.ceil(this.totalRecords / this.noofrecordsPerpage);
    if (this.currentPage == null) {
      this.currentPage = 1;
    }
    const presentpage = this.currentPage;
    const pagesToShow = this.noofPagelink;
    const nopages: number[] = [];
    nopages.push(presentpage);
    const max = pagesToShow - 1;
    for (let i = 0; i < max; i++) {
      if (nopages.length < pagesToShow) {
        if (Math.min.apply(null, nopages) > 1) {
          nopages.push(Math.min.apply(null, nopages) - 1);
        }
      }
      if (nopages.length < pagesToShow) {
        if (Math.max.apply(null, nopages) < this.totalpages) {
          nopages.push(Math.max.apply(null, nopages) + 1);
        }
      }
    }

    nopages.sort((a, b) => a - b);
    if (nopages[0] !== 1) {
      nopages.splice(0, 0, 1)
    }
    if (nopages[1] !== 2 && nopages.length > 1) {
      nopages.splice(1, 0, 0)
    }
    if (nopages.indexOf(this.totalpages) === -1) {
      nopages.splice(nopages.length, 0, this.totalpages)
    }
    if (nopages.indexOf(this.totalpages - 1) === -1) {
      nopages.splice(nopages.length - 1, 0, -1)
    }
    return nopages;
  }
  onHover() {
    this.isWhite = true;
    this.isUnderline = false;
  }
  onBlur() {
    this.isWhite = false;
    this.isUnderline = false;
  }
  onHover1() {
    this.isWhite1 = true;
    this.isUnderline1 = false;
  }
  onBlur1() {
    this.isWhite1 = false;
    this.isUnderline1 = false;
  }
}




