import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-plan-comparision-reviews',
  templateUrl: './plan-comparision-reviews.component.html',
  styleUrls: ['./plan-comparision-reviews.component.scss']
})
export class PlanComparisionReviewsComponent implements OnInit {

  @Input('reviewDetails') reviewDetails: Array<any>;
  constructor() { }

  ngOnInit() {
  }

}
