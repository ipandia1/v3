import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'promotions',
  templateUrl: './promotions.component.html',
  styleUrls: ['./promotions.component.scss']
})
export class PromotionsComponent implements OnInit {
  @Input('promotionsData') promotionsData: Array<any>;
  promotionsDataAccordion:boolean[] = [];
  notEnrolledText = "Not Enrolled";
  manualEnrollText = "Manual";
  isEmptyValue = "";
  constructor() { }

  ngOnInit() {
    /*Logic to handle accordion content*/
    for (var i in this.promotionsData) {
      this.promotionsDataAccordion.push(false);
    }
  }

}
