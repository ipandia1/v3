import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'paymentblade',
  templateUrl: './paymentblade.component.html',
  styleUrls: ['./paymentblade.component.scss']
})
export class PaymentbladeComponent implements OnInit {
  @Input('title') titleText: string;
  @Input('icon') iconImage: string = '';
  @Input('card') cardNumber: string;
  @Input('url') url: any = '';
  @Input('showArrow') showArrow: boolean = true;
  @Input('invertColor') invertColor: boolean = false;
  @Input('topDivider') topDivider: boolean = true;
  @Input('topDividerClass') topDividerClass: string = 'inset-divider-1px';
  @Input('bottomDivider') bottomDivider: boolean = true;
  @Input('bottomDividerClass') bottomDividerClass: string = 'inset-divider-1px';
  @Input('titleClass') titleTextClass: string='body';
  @Input('disableClass') disableClass: string='black';
  @Input('cardNumberClass') cardNumberClass: string='';
  // New Variables for PA Edit
  @Input('displaySecondLine') displaySecondLine: boolean = false;
  @Input('secondLineDesc') secondLineDesc: string = '';
  @Input('secondLineUrl') secondLineUrl: string = '';
  emptyValue = '';
  popUp:boolean=true;
  @Output('callBackValue') callBackValue = new EventEmitter<boolean>();
  OpenPopUp() {
     this.callBackValue.emit(this.popUp); 
      }
  constructor(private router: Router) { }

  ngOnInit() {
  }
  goToUrl(event) {
    if (this.url == '') {
     event.stopPropagation();
    } else {     
      this.router.navigate([this.url]);
    }
  }

}
