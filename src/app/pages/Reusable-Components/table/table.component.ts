import { Component, OnInit, Input, OnChanges, AfterViewInit, ChangeDetectorRef, HostListener } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnChanges, AfterViewInit {
  smDown = true;
  isDesktop: boolean;
  colName: any[];
  remLeftArr = false;
  remRightArr = false;
  user: number;
  noOfCols: any;
  screensize: any;
  asc = true;
  showClr1Code;
  pagedItemsChild: Array<string>;
  totalrecords: number;
  isChecked = false;
  path: string[] = ['col1'];
  order = -1; // -1 asc, 1 desc;
  @Input('currentpage') currentpage: number;
  @Input('noofrecordsperpage') noofrecordsperpage: number;
  @Input('TestData') TestData;
  @Input('TableHeaders') TableHeaders;
  @Input('withCheckbox') withCheckbox = false;
  @Input('stripped') stripped = false;
  @Input('rightscroll') rightscroll = false;
  @Input('ids') ids;
  showLoader: boolean;
  moveblewidth: any;
  totalColsWidth = 0;
  removableColIndex: any;
  constructor(private ref: ChangeDetectorRef) { }
  ngOnInit() {
    this.remLeftArr = true;
    this.isDesktop = true;
    this.screensize = document.documentElement.clientWidth;
    this.totalrecords = this.TestData.length;
    this.pagedItemsChild = this.TestData.slice(0, this.noofrecordsperpage);
    let cols = [];
    let x = 0;
    let self = this;
    this.TableHeaders.forEach(function (obj, i) {
      cols[i] = obj.displayColName;
      if (i !== 0) {
        x += self.removePx(obj.colWidth);
      }
    });
    this.totalColsWidth = x;
    this.colName = cols;
    this.myFunction();
    this.removableColIndex = 1;
    this.checkResize();   // 08/07 - Ankit Jain 
  }

  // 08/07 - Ankit Jain 
  checkResize() {
    if (window.innerWidth > 767) {
      this.isDesktop = true;
    } else {
      this.isDesktop = false;
    }
  }

    // 08/07 - Ankit Jain 
  @HostListener('window:resize', ['$event'])
  onResize(event) {
   this.checkResize();
  }

  removePx(str) {
    return +str.split('px').join('');
  }
  myFunction() {
    const screensize = document.documentElement.clientWidth;
    let cols = this.colName;
    this.TableHeaders.forEach(function (obj, i) {
      obj.displayColName = cols[i];
      if (screensize < 575 && screensize >= 475) {
        obj.displayColName = obj.displayColName.length > 45 ? obj.displayColName.substr(0, 45) + '...' : obj.displayColName;
      } else if (screensize < 425 && screensize >= 375) {
        obj.displayColName = obj.displayColName.length > 35 ? obj.displayColName.substr(0, 35) + '...' : obj.displayColName;
      } else if (screensize < 475 && screensize >= 425) {
        obj.displayColName = obj.displayColName.length > 35 ? obj.displayColName.substr(0, 40) + '...' : obj.displayColName;
      } else if (screensize < 375) {
        obj.displayColName = obj.displayColName.length > 35 ? obj.displayColName.substr(0, 35) + '...' : obj.displayColName;
      }
    });
  }
  ngOnChanges() {  }
  scrollTableResize() {
    this.screensize = window.innerWidth;
    if (this.screensize > 767) {
      this.smDown = false;
      if (this.rightscroll) {
        const table_div = document.getElementById(this.ids).querySelectorAll('.tablediv') as unknown as HTMLCollectionOf<HTMLElement>;
        const fixed_div = document.getElementById(this.ids).querySelectorAll('.fixeddiv') as unknown as HTMLCollectionOf<HTMLElement>;
        const moveble_div = document.getElementById(this.ids).querySelectorAll('.moveble') as unknown as HTMLCollectionOf<HTMLElement>;
        let fxWidth = fixed_div[0].clientWidth + 'px';
        if (this.screensize > 767 && this.screensize <= 1370) {
          fixed_div[0].classList.remove('w-100');
          fixed_div[0].style.width = (table_div[0].clientWidth - 68) + 'px';
          fxWidth = fixed_div[0].style.width;
        }
        else {
          fixed_div[0].classList.add('w-100');
        }
        if (!this.withCheckbox) {
          moveble_div[0].style.width = (this.removePx(fxWidth) - this.removePx(this.TableHeaders[0].colWidth)) + 'px';
        } else {
          moveble_div[0].style.width = (this.removePx(fxWidth) - this.removePx(this.TableHeaders[0].colWidth) - 34) + 'px';
        }
        this.moveblewidth = moveble_div[0].style.width;

        const first_columns = document.getElementById(this.ids).querySelectorAll('.first_column') as unknown as HTMLCollectionOf<HTMLElement>;
        let second_columns: any;
        if (this.withCheckbox) {
          second_columns = document.getElementById(this.ids).querySelectorAll('.second_column') as unknown as HTMLCollectionOf<HTMLElement>;
          for (let i = 0; i <= second_columns.length - 1; i++) {
            second_columns[i].style.left = (first_columns[0].clientWidth) + 'px';
          }
        }
        const scrolldiv = document.getElementById(this.ids).querySelectorAll('.moveble') as unknown as HTMLCollectionOf<HTMLElement>;
        if (this.withCheckbox) {
          scrolldiv[0].style.marginLeft = (second_columns[0].clientWidth) + (first_columns[0].clientWidth) + 'px';
        } else {
          scrolldiv[0].style.marginLeft = (first_columns[0].clientWidth) + 'px';
        }
        this.ref.detectChanges();
      }
    } else if (this.screensize <= 767) {
      this.smDown = true;
      const scrolldiv = document.getElementById(this.ids).querySelectorAll('.moveble') as unknown as HTMLCollectionOf<HTMLElement>;
      scrolldiv[0].style.marginLeft = '0px';
    }
    this.ref.detectChanges();
  }
  ngAfterViewInit() {
    this.screensize = window.innerWidth;
    if (this.screensize > 767) {
      this.smDown = false;
      if (this.rightscroll) {
        const fixed_div = document.getElementById(this.ids).querySelectorAll('.fixeddiv') as unknown as HTMLCollectionOf<HTMLElement>;
        const moveble_div = document.getElementById(this.ids).querySelectorAll('.moveble') as unknown as HTMLCollectionOf<HTMLElement>;
        if (!this.withCheckbox) {
          moveble_div[0].style.width = (fixed_div[0].clientWidth - 68 - this.removePx(this.TableHeaders[0].colWidth)) + 'px';
        } else {
          moveble_div[0].style.width = (fixed_div[0].clientWidth - 68 - this.removePx(this.TableHeaders[0].colWidth) - 34) + 'px';
        }
        this.moveblewidth = moveble_div[0].style.width;

        const first_columns = document.getElementById(this.ids).querySelectorAll('.first_column') as unknown as HTMLCollectionOf<HTMLElement>;
        let second_columns: any;
        if (this.withCheckbox) {
          second_columns = document.getElementById(this.ids).querySelectorAll('.second_column') as unknown as HTMLCollectionOf<HTMLElement>;
          for (let i = 0; i <= second_columns.length - 1; i++) {
            second_columns[i].style.left = (first_columns[0].clientWidth) + 'px';
          }
        }
        const scrolldiv = document.getElementById(this.ids).querySelectorAll('.moveble') as unknown as HTMLCollectionOf<HTMLElement>;
        if (this.withCheckbox) {
          scrolldiv[0].style.marginLeft = (second_columns[0].clientWidth) + (first_columns[0].clientWidth) + 'px';
        } else {
          scrolldiv[0].style.marginLeft = (first_columns[0].clientWidth) + 'px';
        }
      }
    } else if (this.screensize <= 767) {
      this.smDown = true;
      const scrolldiv = document.getElementById(this.ids).querySelectorAll('.moveble') as unknown as HTMLCollectionOf<HTMLElement>;
      scrolldiv[0].style.marginLeft = '0px';
    }
    this.ref.detectChanges();
  }
  movenext() {
    this.remLeftArr = false;
    const scrolldiv = document.getElementById(this.ids).querySelectorAll('.fixeddiv') as unknown as HTMLCollectionOf<HTMLElement>
    const moveblediv = document.getElementById(this.ids).querySelectorAll('.moveble') as unknown as HTMLCollectionOf<HTMLElement>;
    let scrollcontent = scrolldiv[0].children[0];
    if (this.totalColsWidth >= this.removePx(this.moveblewidth) + Number(moveblediv[0].scrollLeft)
      + this.removePx(this.TableHeaders[this.removableColIndex].colWidth)) {
      scrollcontent.scrollLeft = scrollcontent.scrollLeft + this.removePx(this.TableHeaders[this.removableColIndex].colWidth);
      this.removableColIndex++;
    }
    else {
      scrollcontent.scrollLeft = scrollcontent.scrollLeft
        + (this.totalColsWidth - Number(moveblediv[0].scrollLeft) - this.removePx(this.moveblewidth));
      this.remRightArr = true;
      this.removableColIndex = this.TableHeaders.length;
    }

    this.user = navigator.userAgent.search(/(?:MSIE|Trident\/.*; rv:)/);
    let x = Number(moveblediv[0].scrollWidth);
    if (this.user >= 0) {
      x -= 107
    }
    if (Number(moveblediv[0].scrollLeft) + this.removePx(this.moveblewidth) >= x) {
      this.remRightArr = true;
      this.removableColIndex = this.TableHeaders.length;
    } else {
    }
  }

  moveprevious() {
    this.remRightArr = false;
    const scrolldiv = document.getElementById(this.ids).querySelectorAll('.fixeddiv') as unknown as HTMLCollectionOf<HTMLElement>;
    let scrollcontent = scrolldiv[0].children[0];
    scrollcontent.scrollLeft = scrollcontent.scrollLeft - this.removePx(this.TableHeaders[this.removableColIndex - 1].colWidth);
    if (scrollcontent.scrollLeft === 0) {
      this.remLeftArr = true;
      this.removableColIndex = 1;
    } else {
      this.removableColIndex--;
    }
  }

  checkall() {
    this.isChecked = !this.isChecked;
  }
  sortBy(prop: string) {
    this.path = prop.split('.')
    this.order = this.order * (-1); // change order
    this.asc = !this.asc;

    if (!this.TestData || !this.path || !this.order) { this.TestData = this.TestData; }

    this.TestData.sort((a: any, b: any) => {
      this.path.forEach(property => {
        a = a[property].toLowerCase();
        b = b[property].toLowerCase();
      })
      return a > b ? this.order : this.order * (- 1);
    })
    this.moveCurrentPage(this.currentpage);
    return false;
  }

  moveCurrentPage(event) {
    this.currentpage = event;
    this.pagedItemsChild = this.TestData.slice(this.noofrecordsperpage * (event - 1), event * this.noofrecordsperpage);
  }
}
