import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lineselector',
  templateUrl: './lineselector.component.html',
  styleUrls: ['./lineselector.component.scss']
})
export class LineselectorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    for  (const  names  of  this.selectNames) {
      const string1 = names.name.split(',');
      if (string1[0].length > 13) {
        names.name  =  string1[0].substring(0, 13) + '... , ' + string1[1] ;
      }
    }
    this.selectedVal = this.selectNames[0].name;
  }

  selectedVal: string = "";
  selectNames: Array<any> = [
    { name: 'Matt, (425) 111-1111', disabled: true },
    { name: 'Tom, (425) 111-1111', disabled: false },
    { name: 'Sam, (425) 111-1111', disabled: false }
  ];
  select(value: string): void {
    this.selectedVal = value;
  }
}
