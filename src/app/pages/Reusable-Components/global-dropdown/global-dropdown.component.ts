import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
// import { EventEmitter } from 'events';


@Component({
  selector: 'app-global-dropdown',
  templateUrl: './global-dropdown.component.html',
  styleUrls: ['./global-dropdown.component.scss']
})
export class GlobalDropdownComponent implements OnInit {

@Input() selectedVal = 'Select State';
@Input() dropdownidval;
@Input() dropdownArray;
@Input() dropdownText;
@Input() tabindex;
@Input() disabled;
@Input() textToRead;
// tslint:disable-next-line:no-output-rename
@Output('callBackValue') callBackValue = new EventEmitter<string>();
select(value: string): void {
  this.selectedVal = value;
  this.callBackValue.emit(value);
}
  constructor() {
   }
   ngOnInit() {
    this.tabindex = 0;
  }


 
}



