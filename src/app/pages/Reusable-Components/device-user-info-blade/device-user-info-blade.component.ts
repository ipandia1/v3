import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-device-user-info-blade',
  templateUrl: './device-user-info-blade.component.html',
  styleUrls: ['./device-user-info-blade.component.scss']
})
export class DeviceUserInfoBladeComponent implements OnInit {

  @Input('deviceDetails') deviceDetails;
  constructor() { }

  ngOnInit() {
  }

}
