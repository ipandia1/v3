import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-scheduled-activity',
  templateUrl: './scheduled-activity.component.html',
  styleUrls: ['./scheduled-activity.component.scss']
})
export class ScheduledActivityComponent implements OnInit {
  @Input('scheduledData') scheduledData: Array<any>;
  constructor() { }

  ngOnInit() {
  }

}
