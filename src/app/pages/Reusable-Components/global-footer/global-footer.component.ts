import { Component, OnInit, HostListener } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-global-footer',
  templateUrl: './global-footer.component.html',
  styleUrls: ['./global-footer.component.scss']
})
export class GlobalFooterComponent implements OnInit {

  isDesktop = true;
  linkArray = [
    { 'title': 'SUPPORT', 'image': 'gf-footer-tooltip', 'magentaImage': 'gf-footer-tooltip-magenta' },
    { 'title': 'CONTACT US', 'image': 'gf-phone', 'magentaImage': 'gf-phone-magenta' },
    { 'title': 'STORE LOCATOR', 'image': 'gf-business', 'magentaImage': 'gf-business-magenta' },
    { 'title': 'COVERAGE', 'image': 'gf-hotspot', 'magentaImage': 'gf-hotspot-magenta' },
    { 'title': 'T-MOBILE.COM', 'image': 'gf-tmobileicon', 'magentaImage': 'gf-tmobileicon-magenta' }];
  footerArray: any;
  showIcons = {};

  checkResize() {
    if (window.innerWidth > 767) {
      this.isDesktop = true;
      this.footerArray = [
        { 'link': 'About T-Mobile USA' },
        { 'link': 'Terms & Conditions' },
        { 'link': 'Return Policy' },
        { 'link': 'Terms Of Use' },
        { 'link': 'Privacy Policy' },
        { 'link': 'Interest-Based Ads' },
        { 'link': 'Privacy Resources' },
        { 'link': 'Open Internet Policy' }];
    } else {
      this.isDesktop = false;
      this.footerArray = [
        { 'link': 'About T-Mobile USA' },
        { 'link': 'Return Policy' },
        { 'link': 'Terms & Conditions' },
        { 'link': 'Terms Of Use' },
        { 'link': 'Privacy Policy' },
        { 'link': 'Privacy Resources' },
        { 'link': 'Interest-Based Ads' },
        { 'link': 'Open Internet Policy' }];
    }
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.checkResize();
  }

  constructor(public commonService: CommonService) {
    this.checkResize();
   }

  ngOnInit() {
    for (let i = 0; i < this.linkArray.length; i++) {
      this.showIcons[i] = true;
    }
  }
  changeIcons(index) {
    this.showIcons[index] = false;
  }

  changeMagentaIcons(index) {
    this.showIcons[index] = true;
  }

}
