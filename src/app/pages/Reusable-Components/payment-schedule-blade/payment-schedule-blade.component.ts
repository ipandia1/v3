import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-payment-schedule-blade',
  templateUrl: './payment-schedule-blade.component.html',
  styleUrls: ['./payment-schedule-blade.component.scss']
})
export class PaymentScheduleBladeComponent implements OnInit {
  @Input('paymentInfo') paymentInfo: Array<any>;
  @Input('topDivider') topDivider = false;
  @Input('bottomDivider') bottomDivider = false;
  @Input('topDividerClass') topDividerClass = 'inset-divider-1px';
  @Input('bottomDividerClass') bottomDividerClass = 'inset-divider-1px';
  @Input('textClass') textClass = 'body-bold';
  @Input('amountClass') amountClass = 'body';
  @Input('iconClass') iconClass = 'fa fa-check-circle green';
  @Input('arrowClass') arrowClass = 'arrow-right d-inline-block';
  @Input('url') url = '';
  @Input('numberBox') numberBox = false;
  @Input('total') total: string;
  isVisible = true;
  constructor() { }

  ngOnInit() {
  }

}
