import { Component, OnInit, Input, Output, HostBinding, HostListener, ElementRef, EventEmitter } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-filters-reusable',
  templateUrl: './filters-reusable.component.html',
  styleUrls: ['./filters-reusable.component.scss']
})

export class FiltersReusableComponent implements OnInit {

  @Input() filterKeys: Array<any>;
  @Input() sortKeys: Array<any>;
  @Input() filterMainData: Array<any>;
  @Input() showFilterValues: Array<any>;
  @Input() filterTitleArray: Array<any>;

  reviewNumberArray: Array<any> = [];

  @Output() itemsDivEmit = new EventEmitter<string[]>();

  showFilterDiv1: boolean;
  showFilterDiv2: boolean;
  showFilterDiv3: boolean;
  showFilterDiv4: boolean;
  showMobileDiv = false;
  showMoreVar = false;
  mobileView: boolean;
  clickedInside: boolean;
  selectedLine = 'Select';
  count = 0;
  clickFunCount = 0;
  filterKeys1 = [];
  idList = [];

  selectedValArray: Array<any> = [];
  itemsDivArray: Array<any> = [];
  starsArray: Array<any> = [];
  countedVal: any = 0;
  uniqueArray = [];
  titleArray: Array<any> = [];
  errorMsg = false;

  constructor() {
  }

  ngOnInit() {
    this.viewFunc();
  }

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement) {
    if (!this.mobileView) {
      if (targetElement.id === this.filterTitleArray[0] + 'TitleDiv' || targetElement.id === this.filterTitleArray[1] + 'TitleDiv' ||
        targetElement.id === this.filterTitleArray[2] + 'TitleDiv' || targetElement.id === this.filterTitleArray[3] + 'TitleDiv' ||
        targetElement.id === this.filterTitleArray[0] + 'showMoreDiv' ||
        document.getElementById('filterDiv').contains(targetElement) === true) {
        this.clickedInside = true;
      } else {
        this.clickedInside = false;
      }
    }
    if (!this.clickedInside) {
      this.closingDivsFunc();
    }
  }

  @HostListener('window:load', [])
  onWindowLoad() {
    this.viewFunc();
  }

  @HostListener('window:resize', [])
  onResize() {
    this.viewFunc();
  }

  closeDiv = function (val) {
    for (let i = 0; i < this.showFilterValues.length; i++) {
      if (i === val) {
        this.showFilterValues[i] = true;
        const titleDiv = document.getElementById(this.filterTitleArray[i] + 'TitleDiv');
        const mainDiv = document.getElementById('mainDiv');
        if (window.innerWidth > 992 && window.innerWidth < 1200) {
          document.getElementById(this.filterTitleArray[i] + 'Div').style.left = titleDiv.offsetLeft / mainDiv.offsetWidth * 100 + 2 + '%';
        } else {
        document.getElementById(this.filterTitleArray[i] + 'Div').style.left = titleDiv.offsetLeft / mainDiv.offsetWidth * 100 + 2.2 + '%';
        }
      } else {
        this.showFilterValues[i] = false;
      }
    }
  };

  arrayFunc = function (returnVal) {
    const array = new Object();
    for (let i = 0; i < this.filterMainData.length; i++) {
      const number1 = parseInt(this.filterMainData[i].Review, 10);
      if (this.reviewNumberArray.indexOf(number1) === -1) {
        this.reviewNumberArray.push(number1);
      }
    }
    this.reviewNumberArray.sort(function (a, b) {
      return b - a;
    });
    for (let i = 0; i <= this.reviewNumberArray.length; i++) {
      array[i] = new Array(this.reviewNumberArray[i]).fill('<i class="star_magenta d-inline-block"></i>');
    }
    return array;
  };

  showMobileDivFunc = function () {
    this.showMobileDiv = !this.showMobileDiv;
  };

  showMoreFunc = function () {
    this.showMoreVar = !this.showMoreVar;
  };

  /*Reused function*/
  viewFunc = function () {
    if (window.innerWidth < 992) {
      this.mobileView = true;
    } else {
      this.mobileView = false;
    }
  };

  /*Reused function*/
  countVal = function () {
    if (this.countedVal === 0 && window.innerWidth < 768) {
      this.countedVal = 'None';
    } else if (window.innerWidth < 768) {
      this.countedVal = this.countedVal;
    }
    return this.countedVal;
  };

  /*Reused function*/
  btnCreate = function () {
    const dynamicDiv = document.getElementsByClassName('dynamicDiv')[0];
    if (this.selectedValArray.indexOf(this.selectedVal) === -1) {
      this.selectedValArray.push(this.selectedVal);
      const dynamicSubDiv = document.createElement('div');
      dynamicSubDiv.classList.add('d-flex', 'align-items-center');
      dynamicSubDiv.id = 'dynamicSubDivId' + this.selectedVal;
      dynamicSubDiv.classList.add('dynamicSelectDiv');
      dynamicSubDiv.classList.add(this.selectedValClass);
      dynamicDiv.appendChild(dynamicSubDiv);
      const btn = document.createElement('span');
      btn.classList.add('ml-2');
      btn.id = 'btnId' + this.selectedVal;

      const crossSpan = document.createElement('span');
      const crossIcon = document.createElement('i');
      crossIcon.id = 'crossIcon';
      crossIcon.addEventListener('click', (e) => this.clearValue(e));
      crossSpan.classList.add('crossIconClass');
      crossIcon.className = 'clear-icon1';
      crossIcon.id = 'crossSpan' + this.selectedVal;
      crossSpan.appendChild(crossIcon);

      btn.innerHTML = this.selectedVal;
      for (let i = 0; i < this.filterMainData.length; i++) {
        if (this.selectedVal === this.filterMainData[i].Review) {
          this.reviewNumber1 = parseInt(this.filterMainData[i].Review, 10);
          btn.innerHTML = '';
          for (let j = 0; j < this.reviewNumber1; j++) {
            btn.innerHTML = btn.innerHTML + '<i class="star_magenta d-inline-block"></i>';
          }
        }
      }
      dynamicSubDiv.appendChild(btn);
      dynamicSubDiv.appendChild(crossSpan);
    }
  };

  /*Reused function*/
  closingDivsFunc = function () {
    for (let i = 0; i < this.showFilterValues.length; i++) {
      this.showFilterValues[i] = false;
    }
    return false;
  };

  checkFunc = function (event, val) {
    this.countedVal += 1;
    this.countVal();
    const colorDiv1 = event.target.parentElement;
    if (colorDiv1.id === this.filterKeys[3] + 'Div') {
      this.selectedVal = event.target.style.backgroundColor;
      this.selectedValClass = 'Color';
      this.countedVal += 1;
      this.countVal();
      this.btnCreate();
      this.titleArray.push('Color');
      document.getElementById('filterDiv').style.top = -52 + 'px';
    } else {
      if (event.target.checked) {
        this.indexVal = event.target.id;
        this.selectedVal = this.indexVal;
        this.selectedValClass = event.target.className;
        this.countedVal += 1;
        this.countVal();
        this.btnCreate();
        this.div1 = document.getElementById(this.selectedVal).className;
        this.titleArray.push(this.div1);
        const inputElement = document.getElementsByTagName('input');
        for (let i = 0; i < inputElement.length; i++) {
          if (inputElement[i].id === this.selectedVal) {
            inputElement[i].checked = true;
          }
        }
        document.getElementById('filterDiv').style.top = -52 + 'px';
      } else {
        this.indexVal = event.target.id;
        this.selectedVal = this.indexVal;
        this.selectedValClass = event.target.className;
        this.countedVal = this.selectedValArray.length;
        this.countedVal -= 1;
        this.countVal();
        this.selectedDiv = document.getElementById('dynamicSubDivId' + this.selectedVal);
        this.dynamicDiv = document.getElementsByClassName('dynamicDiv')[0];
        this.dynamicDiv.removeChild(this.selectedDiv);
        if (this.selectedValArray.indexOf(this.selectedVal) !== -1) {
          this.selectedValArray.splice(this.selectedValArray.indexOf(this.selectedVal), 1);
        }
        this.uniqueFunc(this.selectedValArray, 'clear');
        const inputElement = document.getElementsByTagName('input');
        for (let i = 0; i < inputElement.length; i++) {
          if (inputElement[i].id === this.selectedVal) {
            inputElement[i].checked = false;
          }
        }
        if (!this.dynamicDiv.hasChildNodes()) {
          this.selectedValArray = [];
          this.titleArray = [];
          document.getElementById('filterDiv').style.top = 0 + 'px';
        }
      }
    }
  };
  uniqueFunc = function (array, token) {
    const itemsDivArray = [];
    const uniqueArray = [];
    for (let j = 0; j < array.length; j++) {
      for (let i = 0; i < this.filterMainData.length; i++) {
        for (let k = 0; k < this.filterKeys.length; k++) {
          if (array[j] === this.filterMainData[i][this.filterKeys[k]]) {
            itemsDivArray.push(this.filterMainData[i].id);
          }
        }
      }
    }
    for (let l = 0; l < itemsDivArray.length; l++) {
      for (let m = l + 1; m < itemsDivArray.length; m++) {
        if (itemsDivArray[l] === itemsDivArray[m]) {
          uniqueArray.push(itemsDivArray[l]);
        }
      }
    }
    if (token === 'apply') {
      if (uniqueArray.length === 0) {
        return itemsDivArray;
      } else {
        return uniqueArray;
      }
    } else {
      if (uniqueArray.length === 0) {
        return itemsDivArray;
      } else {
        return uniqueArray;
      }
    }
  };

  applyFunc = function (event) {
    this.countedVal = this.selectedValArray.length;
    this.countVal();
    this.closingDivsFunc();
    if (this.mobileView) {
      this.showMobileDiv = !this.showMobileDiv;
    }
    this.itemsDivEmit.emit(this.uniqueFunc(this.selectedValArray, 'apply'));
  };

  clearValue = function (event) {
    this.countedVal = this.selectedValArray.length;
    this.countedVal -= 1;
    this.countVal();
    const targetEle = event.target;
    const targetEleParent = targetEle.parentElement.parentElement;
    this.selectedDiv = targetEleParent;
    this.selectedVal = this.selectedDiv.id.substring(15, this.selectedDiv.id.length);
    if (this.selectedValArray.indexOf(this.selectedVal) !== -1) {
      this.selectedValArray.splice(this.selectedValArray.indexOf(this.selectedVal), 1);
    }
    this.itemsDivEmit.emit(this.uniqueFunc(this.selectedValArray, 'clear'));
    const dynamicDiv = document.getElementsByClassName('dynamicDiv')[0];
    dynamicDiv.removeChild(this.selectedDiv);
    if (!dynamicDiv.hasChildNodes()) {
      this.selectedValArray = [];
      this.titleArray = [];
      this.uniqueArray = [];
      document.getElementById('filterDiv').style.top = 0 + 'px';
      this.showMoreVar = false;
    }
    const inputElement = document.getElementsByTagName('input');
    for (let i = 0; i < inputElement.length; i++) {
      if (inputElement[i].id === this.selectedVal) {
        inputElement[i].checked = false;
      }
    }
  };

  clearAllFunc = function (event) {
    this.clearDiv = event.target.parentElement.parentElement;
    this.countVal1 = 0;
    this.subDivCount = 0;
    for (let j = 0; j < this.filterKeys.length; j++) {
      if (this.clearDiv.id === this.filterKeys[j] + 'Div') {
        this.inputEle1 = document.getElementById(this.filterKeys[j] + 'Div').getElementsByTagName('input');
        for (let i = 0; i < this.inputEle1.length; i++) {
          if (this.inputEle1[i].checked === true) {
            this.inputEle1[i].checked = false;
          }
          this.dynamicDiv = document.getElementsByClassName('dynamicDiv')[0];
          const clearDivId = this.clearDiv.id.substring(0, this.clearDiv.id.length - 3);
          const dynamicSubDivArray = this.dynamicDiv.children;
          for (let m = 0; m < dynamicSubDivArray.length; m++) {
            if (dynamicSubDivArray[m].classList.contains(clearDivId)) {
              this.dynamicId = dynamicSubDivArray[m].id.substring(15, dynamicSubDivArray[m].id.length);
              for (let k = 0; k < this.selectedValArray.length; k++) {
                if (this.selectedValArray.indexOf(this.dynamicId) !== -1) {
                  this.selectedValArray.splice(this.selectedValArray.indexOf(this.dynamicId), 1);
                }
              }
              this.dynamicDiv.removeChild(document.getElementById(dynamicSubDivArray[m].id));
              this.itemsDivEmit.emit(this.uniqueFunc(this.selectedValArray, 'apply'));
            }
          }
        }
      }
    }
    this.countedVal = this.countVal1;
    this.closingDivsFunc();
    if (!this.dynamicDiv.hasChildNodes()) {
      document.getElementById('filterDiv').style.top = 0 + 'px';
    }
    this.showMoreVar = false;
  };

  resetFunc = function () {
    // this.clearAllFunc();
    this.closingDivsFunc();
    this.selectedValArray = [];
    this.titleArray = [];
    const inputElement = document.getElementsByTagName('input');

    for (let i = 0; i < inputElement.length; i++) {
      inputElement[i].checked = false;
    }
    if (this.showMobileDiv) {
      this.showMobileDiv = false;
    }
    if (this.count === 0) {
      this.filterMainData.sort(function (a, b) {
        const manufactureA = a.Manufacture.toUpperCase();
        const manufactureB = b.Manufacture.toUpperCase();
        let comparison = 0;
        if (manufactureA > manufactureB) {
          comparison = 1;
        } else if (manufactureA < manufactureB) {
          comparison = -1;
        }
        return comparison;
      });
      this.count = 1;
    }
    this.selectedLine = 'Select';
    this.itemsDivEmit.emit(this.uniqueFunc(this.selectedValArray, 'clear'));
    this.dynamicDiv = document.getElementsByClassName('dynamicDiv')[0];
    while (this.dynamicDiv.hasChildNodes()) {
      this.dynamicDiv.removeChild(this.dynamicDiv.lastChild);
      document.getElementById('filterDiv').style.top = 0 + 'px';
    }
    this.showMoreVar = false;
  };

  select(value: string): void {
    this.selectedLine = value;
    if (this.selectedLine === 'Price Low to High') {
      this.filterMainData.sort(function (a, b) {
        const aPriceInNum = parseInt(a.Price.slice(1, a.Price.length), 10);
        const bPriceInNum = parseInt(b.Price.slice(1, b.Price.length), 10);
        return aPriceInNum - bPriceInNum;
      });
    } else if (this.selectedLine === 'Price High to Low') {
      this.filterMainData.sort(function (a, b) {
        const aPriceInNum = parseInt(a.Price.slice(1, a.Price.length), 10);
        const bPriceInNum = parseInt(b.Price.slice(1, b.Price.length), 10);
        return bPriceInNum - aPriceInNum;
      });
    } else if (this.selectedLine === 'Popularity') {
      this.filterMainData.sort(function (a, b) {
        const aPopularityNum = parseInt(a.Popularity, 10);
        const bPopularityNum = parseInt(b.Popularity, 10);
        return bPopularityNum - aPopularityNum;
      });
    } else if (this.selectedLine === 'Relevance') {
      this.filterMainData.sort(function (a, b) {
        const aReview = parseInt(a.Review, 10);
        const bReview = parseInt(b.Review, 10);
        return bReview - aReview;
      });
    }
  }

  public handleLine(callBackValue: string) {
    if (callBackValue !== 'this.isEmpty') {
      this.selectedLine = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }
}
