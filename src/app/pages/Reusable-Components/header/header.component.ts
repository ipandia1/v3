import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private _location: Location) { }

  ngOnInit() {
  }

  gotopreviouspage() {
    window.history.back();
  }
  contactUs() {
    // logic on click of phone icon
  }
}
