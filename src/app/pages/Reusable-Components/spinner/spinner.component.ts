import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
  @Input('showLoader') showLoader: Boolean = false;
  @Input('noOverlay') noOverlay: Boolean = false;

  ngOnInit() { }
  constructor(private _router: Router) {
    const currentAbsoluteUrl = window.location.href;
    const currentRelativeUrl = this._router.url;
    const index = currentAbsoluteUrl.indexOf(currentRelativeUrl);
    const baseUrl = currentAbsoluteUrl.substring(0, index);
  }
}
