import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-eip-jod-blade',
  templateUrl: './eip-jod-blade.component.html',
  styleUrls: ['./eip-jod-blade.component.scss']
})
export class EipJodBladeComponent implements OnInit {

  constructor() { }
  jodAccordion = [];
  // tslint:disable-next-line:no-input-rename
  @Input('JOD_Array_Details') JOD_Array_Details;
  // tslint:disable-next-line:no-input-rename
  @Input('badge_status') badge_status;
  ngOnInit() {
    /*Logic to handle toggle content*/
    // tslint:disable-next-line:forin
    for (const m in this.JOD_Array_Details) {
      this.jodAccordion[m] = false;
     }
  }
  action = function (i) {
    //('function called');
  };

}
