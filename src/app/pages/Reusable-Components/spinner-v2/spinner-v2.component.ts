import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-spinner-v2',
  templateUrl: './spinner-v2.component.html',
  styleUrls: ['./spinner-v2.component.scss']
})
export class SpinnerV2Component implements OnInit {
  @Input('showLoaderV2') showLoaderV2: Boolean = false;
  @Input('noOverlayV2') noOverlayV2: Boolean = false;

  ngOnInit() { }
  constructor(private _router: Router) {
    // const currentAbsoluteUrl = window.location.href;
    // const currentRelativeUrl = this._router.url;
    // const index = currentAbsoluteUrl.indexOf(currentRelativeUrl);
    // const baseUrl = currentAbsoluteUrl.substring(0, index);
  }
}

