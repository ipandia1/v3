import { Component, OnInit, Output, HostListener, ElementRef, AfterViewInit, EventEmitter, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  @Input('disabled') disabled = false;
  @Input('type') type = 'Continuous';
  continuous = false;
  slider = document.getElementsByClassName('myRange');
  slider2 = document.getElementsByClassName('myRange2');
  rangeValue1 = document.getElementsByClassName('rangeVal');
  rangeIndicator = document.getElementsByClassName('rangeIndicator');
  rangeValue = 0;
  rangeValuePicked = 0;
  value = 0;
  left = 0 + 'px';
  marginLeft = 0 + '%';
  bgcolor = true;
  linear_gradient1 = 'linear-gradient(#d6d6d6, #d6d6d6)';
  linear_gradient2 = 'linear-gradient(#e20074, #e20074)';
  sliderDisabled = true;
  constructor() {

  }
  @HostListener('window:resize', []) onResize() {
    const val = <HTMLInputElement>document.getElementById('range');
    this.rangeValue = parseInt(val.value, 10);
    this.slideChange1();
  }
  ngOnInit() {
    this.continuous = (this.type === 'Continuous') ? true : false;
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    this.slideChange1();
  }

  slideChange = function (this) {
    if (this.rangeValuePicked === 0) {
      this.bgcolor = true;
    } else {
      this.bgcolor = false;
    }
    this.rangeBelow = this.rangeValuePicked / 100;
    this.slider[0].setAttribute('style',
      'background-image: -webkit-gradient(linear, left top, right top,' + 'color-stop(' + this.rangeBelow + ', #e20074), '
      + 'color-stop(' + this.rangeBelow + ', #C5C5C5)' + ') !important');

    this.user = navigator.userAgent.search(/(?:MSIE|Trident\/.*; rv:)/);
    if (this.user >= 0) {
      this.slider[0].setAttribute('style', 'background-image:none !important');
      this.slider[0].setAttribute('style', 'height:130px !important;');
    }
    this.userEdge = navigator.userAgent.search(/(?:Edge|Trident\/.*; rv:)/);
    if (this.userEdge >= 0) {
      this.slider[0].setAttribute('style', 'background-image:none !important');
    }

  }

  slideChange1 = function () {
    if (this.rangeValue === 0) {
      this.bgcolor = true;
    } else {
      this.bgcolor = false;
    }
    const currentelement = (<HTMLInputElement>document.getElementById('range1'));
    const Minval = currentelement.attributes['min'].value;
    const Maxval = currentelement.attributes['max'].value;
    const currentVal: any = this.rangeValue;
    let ThumbWidthsize: any = currentelement.dataset.thumbwidth;
    const totalrange = Maxval - Minval;
    const position = ((currentVal - Minval) / totalrange) * 100;
    let positionOffset;
    let popover;

    function postionOfset() {
      positionOffset = Math.round(ThumbWidthsize * position / 100) - (ThumbWidthsize / 2);
      popover = document.getElementById('range1output');
    //  console.log('calc(' + position + '% - ' + positionOffset + ' + px');
      popover.style.left = 'calc(' + position + '% - ' + positionOffset + 'px)';
    }
    postionOfset();
    const newPoint = (this.rangeValue - Minval) / (Maxval - Minval);
    this.userIE = navigator.userAgent.search(/(?:MSIE|Trident\/.*; rv:)/);
    if (this.userIE >= 0) {
      ThumbWidthsize = 23;
      postionOfset();
    }
    this.userEdge = navigator.userAgent.search(/(?:Edge|Trident\/.*; rv:)/);

    this.slider2[0].setAttribute('style',
      'background-image: -webkit-gradient(linear, left top, right top,' + 'color-stop(' + newPoint + ', #e20074), '
      + 'color-stop(' + newPoint + ', #C5C5C5)' + ') !important');
    if (this.userEdge >= 0) {
      ThumbWidthsize = 20;
      if (this.rangeValue === 0) {
        positionOffset = ((ThumbWidthsize * position / 100) - (ThumbWidthsize / 2)) - 2;
      } else if (this.rangeValue >= 50) {
        positionOffset = ((ThumbWidthsize * position / 100) - (ThumbWidthsize / 2)) + 1;
      }
    
      popover = document.getElementById('range1output');
      popover.style.left = 'calc(' + position + '% - ' + positionOffset + 'px)';
      this.slider2[0].setAttribute('style', 'background-image:none !important');
    }
  }

}
