import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profile-blade',
  templateUrl: './profile-blade.component.html',
  styleUrls: ['./profile-blade.component.scss']
})
export class ProfileBladeComponent implements OnInit {
  @Input() titleText: string;
  @Input() titleDesc: string;
  @Input() titleDesc2: string;
  @Input() url = '';
  @Input() topDivider = true;
  @Input() topDividerClass = 'inset-divider-1px';
  @Input() bottomDivider = false;
  @Input() bottomDividerClass = 'inset-divider-1px';
  @Input() titleTextClass = 'Display6';
  @Input('titleDescClass') titleDescClass = 'body d-inline-block';
  @Input('titleDesc2Class') titleDesc2Class = 'body d-inline-block';
  @Input('icon') icon = 'arrow-right d-inline-block';
  @Input('switch') switch = false;
  @Input('checkOpt') checkOpt = false;
  @Input('moreLessDiv') moreLessDiv = false;
  @Input('showMore') showMore = false;
  @Input('ShowMoreText') ShowMoreText = 'Show more';
  @Input('ShowLessText') ShowLessText = 'Show less';
  @Input('ShowTextClass') ShowTextClass = 'body brand-magenta';
  emptyValue = '';
  show: boolean;
  constructor() { }

  ngOnInit() {
  }
  showFunc() {
    if (this.titleDesc2 === '') {
    this.titleDescClass = 'body d-inline-block';
    this.showMore = false;
    } else {
    this.titleDesc2Class = 'body d-inline-block';
    this.showMore = false;
    }
  }
  hideFunc() {
    if (this.titleDesc2 === '') {
    this.titleDescClass = 'd-none';
    this.showMore = true;
    } else {
      this.titleDesc2Class = 'd-none';
    this.showMore = true;
    }
  }
}
