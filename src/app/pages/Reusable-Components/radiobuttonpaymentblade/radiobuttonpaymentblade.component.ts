import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'radiobuttonpaymentblade',
  templateUrl: './radiobuttonpaymentblade.component.html',
  styleUrls: ['./radiobuttonpaymentblade.component.scss']
})
export class RadiobuttonpaymentbladeComponent implements OnInit {

  @Input('cardStatus') cardStatusReceived: string;
  @Input('icon') iconImage: string = '';
  @Input('card') cardNumber: string;
  @Input('url') url: string = '';
  @Input('topDivider') topDivider: boolean = true;
  @Input('topDividerClass') topDividerClass: string = 'inset-divider-1px';
  @Input('bottomDivider') bottomDivider: boolean = true;
  @Input('bottomDividerClass') bottomDividerClass: string = 'inset-divider-1px';
  @Input('radioButtonId') radioButtonId: number;
  @Input('radioButtonName') radioButtonName: string='radiobutton';
  @Input('selected') selected: number=1;
  @Input('childClass') customClass: string='body';
  @Input('showErrorIcon') showErrorIcon: boolean;
  @Input('disableClass') disableClass: string='black';
  @Input('disableState') disableState: boolean=false;
  
  //newly added for editPaymentAlt
  @Input('altText') altText: boolean=false;
  @Input('descData') data: string='';
  @Input('descDetails') details: string='';
  emptyValue = '';
  popUp:boolean=true;
  @Input('showLegal') showLegal: Boolean = false;
  // added for error2
  @Input('note') note: string;
  @Output('callBackValue') callBackValue = new EventEmitter<boolean>();
  BreakDown() {
    this.callBackValue.emit(this.popUp);
  }
  constructor() { }

  ngOnInit() {
  }
  radioChecked(id) {
    if(this.disableState === false) {
    const ele = document.getElementById(id) as HTMLInputElement;
          ele.checked = true;
    }
  }
  radioButtonFocused(id) {
    const ids = 'blade' + id;
    const ele = document.getElementById(ids) as HTMLInputElement;
    ele.focus();
  }
}
