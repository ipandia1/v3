import { Component, OnInit, Input, Output, SimpleChanges, OnChanges, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-usage-details-blade',
  templateUrl: './usage-details-blade.component.html',
  styleUrls: ['./usage-details-blade.component.scss']
})
export class UsageDetailsBladeComponent implements OnInit {
  tMobilePurchasesAdd = 0;
  mobileHotspotAdd = 0;
  dataPassAdd = 0;
  calAdd = 0;
  dataStashAdd = 0;
  msgAdd = 0;
  thirdPartyAdd = 0;
  @Input('UsageData') UsageData: Array<any>;
  @Input('category') category: string;
  @Input('measure') measure: string;
  @Input('unit') unit = '';
  @Output('movedetailspage') movedetailspage = new EventEmitter<object>();
  public dataAdd = 0;
  smallDevice: Boolean;
  public showCategoryDetails: Boolean = false;
  public dataArray: Array<Object> = [
    { name: 'View by Category' },
    { name: 'View by Line' }
  ]
  constructor() { }
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges() {
    if (this.UsageData) {
      this.dataAdd = 0;
      this.msgAdd = 0;
      this.calAdd = 0;
      this.dataPassAdd = 0;
      this.dataStashAdd = 0;
      this.mobileHotspotAdd = 0;
      this.thirdPartyAdd = 0;
      this.tMobilePurchasesAdd = 0;
      this.UsageData.forEach((e: any) => {
        this.dataAdd = this.dataAdd + parseFloat(e['data-usage']);
        this.dataPassAdd = this.dataPassAdd + parseFloat(e['datapass-usage']);
        this.dataStashAdd = this.dataStashAdd + parseFloat(e['datastash-usage']);
        this.mobileHotspotAdd = this.mobileHotspotAdd + parseFloat(e['mobileHotspot-usage']);
        this.thirdPartyAdd = this.thirdPartyAdd + parseFloat(e['thirdParty-usage']);
        this.tMobilePurchasesAdd = this.tMobilePurchasesAdd + parseFloat(e['tMobliePurchases-usage']);
        this.msgAdd = this.msgAdd + parseFloat(e.messages);
        this.calAdd = this.calAdd + parseFloat(e.calls);
      }
      );
    }
  }
  ngOnInit() {
    const deviceHeight = window.innerHeight;
    if (deviceHeight < 565) {
      this.smallDevice = true;
    } else {
      this.smallDevice = false;
    }
  }
  detailspage(name: string, category: string) {
    this.movedetailspage.emit({ name: name, category: category });
  }
  detailsPayment = function () {
    this.showCategoryDetails = !this.showCategoryDetails;
  }
}
