import { Component, OnInit, HostListener} from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-download',
  templateUrl: './tmo-app-download.component.html',
  styleUrls: ['./tmo-app-download.component.scss']
})
export class TmoappdownloadComponent implements OnInit {
  isTmoHeader = false;
  count = 0;

  constructor(public commonService: CommonService) { }

  ngOnInit() {
    if (window.innerWidth < 576) {
      this.isTmoHeader = true;
    } else {
      this.isTmoHeader = false;
    }
  }

  @HostListener('window : resize', [])
  onresize() {
    if (window.innerWidth < 576) {
      if (this.count === 1) {
        this.isTmoHeader = false;
      } else {
        this.isTmoHeader = true;
      }
    } else {
      this.isTmoHeader = false;
    }
  }

  closeHeader() {
    this.count = 1;
    this.isTmoHeader = false;
    this.commonService.visibleTmoHeader = false;
  }

  getBtn() {
    //console.log('clicked on Get button');
  }
}
