import { Component, OnInit, Input, Output, SimpleChanges, OnChanges, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-usage-details-by-line-blade',
  templateUrl: './usage-details-by-line-blade.component.html',
  styleUrls: ['./usage-details-by-line-blade.component.scss']
})
export class UsageDetailsByLineBladeComponent implements OnInit {
  @Input('UsageData') UsageData: Array<any>;
  @Output('movedetailspage') movedetailspage = new EventEmitter<object>();
  smallDevice: Boolean;
  public showViewLineDetails = [];
  constructor() { }
   ngOnInit() {
    const deviceHeight = window.innerHeight;
    if (deviceHeight < 565) {
      this.smallDevice = true;
    } else {
      this.smallDevice = false;
    }
  }
  detailsPayment = function (index) {
    this.showViewLineDetails[index] = !this.showViewLineDetails[index];
  }

  detailspage(name: string, category: string) {
    this.movedetailspage.emit({ name: name, category: category });
  }
}
