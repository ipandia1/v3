import { Component, OnInit } from '@angular/core';
import { Router, Event,NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  breadcrumbvalue = [
    { path: 'landingPage', url: '/landingPage', description: 'Account' },
    { path: 'profileNewHome', url: '/profileNewHome', description: 'Home' },
    { path: 'privacyNotificationNew', url: '/privacyNotificationNew', description: 'Home/Privacy & Notifications New' },
    { path: 'billingAndPaymentsNew', url: '/billingAndPaymentsNew', description: 'Home/Billing & Payments New' },
    { path: 'billingAddress', url: '/billingAddress', description: 'Home/Billing & Payments New/Billing Address' },
    {
      path: 'billingAndPaymentsPaperlessBillingNew', url: '/billingAndPaymentsPaperlessBillingNew',
      description: 'Home/Billing & Payments New/Paperless Billing New'
    },
    {
      path: 'privacyNotificationInterestBasedAdsNew', url: '/privacyNotificationInterestBasedAdsNew',
      description: 'Home/Information on Interest-Based Ads'
    },
    // tslint:disable-next-line:max-line-length
    { path: 'privacyNotificationAdvertisingNew', url: '/privacyNotificationAdvertisingNew', description: 'Home/Privacy & Notifications New/Advertising & Insights' },
    {
      path: 'privacyNotificationMarketingCommunicationsNew', url: '/privacyNotificationMarketingCommunicationsNew',
      description: 'Home/Privacy & Notifications New/Marketing Communications New'
    },
    {
      path: 'privacyNotificationNotificationsNew', url: '/privacyNotificationNotificationsNew',
      description: 'Home/Privacy & Notifications New/Notifications'
    },
    {
      path: 'employeeDesignationCurrentLinesNew', url: '/employeeDesignationCurrentLinesNew',
      description: 'Home/Employee Line Designation'
    },
    {
      path: 'employeeDesignationEditNew', url: '/employeeDesignationEditNew',
      description: 'Home/Employee Line Designation'
    },
    {
      path: 'billingAndPaymentsOnNew', url: '/billingAndPaymentsOnNew',
      description: 'Home/Billing & Payments'
    },
    {
      path: 'billingAndPaymentsPaperlessBillingNew', url: '/billingAndPaymentsPaperlessBillingNew',
      description: 'Home/Paperless Billing'
    },
    {
      path: 'lineSettingsNew', url: '/lineSettingsNew',
      description: 'Home/Line Settings'
    },

    {
      path: 'lineSettingsLineChangedNew', url: '/lineSettingsLineChangedNew',
      description: 'Home/Line Settings/Permissions'
    },
    {
      path: 'lineSettingsSelectLineNew', url: '/lineSettingsSelectLineNew',
      description: 'Home/Line Settings/Select Line'
    },
    {
      path: 'blockingNew', url: '/blockingNew',
      description: 'Home/Blocking'
    },
    {
      path: 'coverageDeviceNew', url: '/coverageDeviceNew',
      description: 'Home/Coverage Device'
    },
    {
      path: 'coverageDeviceDeviceAddressNew', url: '/coverageDeviceDeviceAddressNew',
      description: 'Home/Coverage Device/Device Address'
    },
    {
      path: 'coverageDeviceAssignedNumberNew', url: '/coverageDeviceAssignedNumberNew',
      description: 'Home/Coverage Device/Assigned Mobile Number'
    },
    {
      path: 'coverageDeviceVerifyAddressModalNew', url: '/coverageDeviceVerifyAddressModalNew',
      description: 'Home/Coverage DeviceVerify-Address'
    },
    {
      path: 'multipleSettingsNew', url: '/multipleSettingsNew',
      description: 'Home/Multiple Devices'
    },
    {
      path: 'mediaSettingsNew', url: '/mediaSettingsNew',
      description: 'Home/Media Settings'
    },
    {
      path: 'familyControlNew', url: '/familyControlNew',
      description: 'Home/Family Controls'
    },
    {
      path: 'familyControlWebGaurdNew', url: '/familyControlWebGaurdNew',
      description: 'Home/Family Controls/Web Guard'
    },
    {
      path: 'familyControlFamilyAllowancesNew', url: '/familyControlFamilyAllowancesNew',
      description: 'Home/Family Controls/Family Allowances'
    },
    {
      path: 'TmobileIdNew', url: '/TmobileIdNew',
      description: 'Home/T-Mobile ID'
    },
    {
      path: 'TmobileIdNicknameNew', url: '/TmobileIdNicknameNew',
      description: 'Home/T-Mobile ID'
    },
    {
      path: 'E911Address', url: '/E911Address',
      description: 'Home/Line Settings/e911 Address'
    },
    {
      path: 'billingAndPaymentsPaperlessUAT', url: '/billingAndPaymentsPaperlessUAT',
      description: 'Home/Paperless Billing'
    },
    {
      path: 'billingAndPaymentsPaperlessUATUpdated', url: '/billingAndPaymentsPaperlessUATUpdated',
      description: 'Home/Paperless Billing'
    },
    {
      path: 'PaperlessTermsAndCondition', url: '/PaperlessTermsAndCondition',
      description: 'Home/Paperless Billing Terms & Conditions'
    },
    {
      path: 'lineSettingsNicknameNew', url: '/lineSettingsNicknameNew',
      description: 'Home/Line Settings/Nickname'
    },
    {
      path: 'lineSettingsCallerIdNew', url: '/lineSettingsCallerIdNew',
      description: 'Home/Line Settings/Caller ID'
    },
    {
      path: 'billingAndPaymentsConfirmation', url: '/billingAndPaymentsConfirmation',
      description: 'Home/Billing And Payments Confirmation'
    },
    {
      path: 'militaryStatusDetail', url: '/militaryStatusDetail',
      description: 'Home/Military Status Details'
    },
    {
      path: 'registrationVeteran', url: '/registrationVeteran',
      description: 'Home/Military Veterans Registration'
    },
    {
      path: 'registrationVeteranDisabled', url: '/registrationVeteranDisabled',
      description: 'Home/Military Veterans Registration Disabled'
    },
    {
      path: 'confirmInfo', url: '/confirmInfo',
      description: 'Home/Military verification confirmation'
    },
    {
      path: 'successVerified', url: '/successVerified',
      description: 'Home/Military verified'
    },
    {
      path: 'alertMessage', url: '/alertMessage',
      description: 'Home/Military verification alert'
    },
    {
      path: 'uploadDoc', url: '/uploadDoc',
      description: 'Home/Military verification upload documentation'
    },
    {
      path: 'uploadDocDefault', url: '/uploadDocDefault',
      description: 'Home/Military verification upload default documentation'
    },
    {
      path: 'successUploadReceived', url: '/successUploadReceived',
      description: 'Home/Military verification upload complete'
    },
    {
      path: 'fidTermsAndCondition', url: '/fidTermsAndCondition',
      description: 'Home/Terms and Conditions'
    },
    {
      path: 'fidPrivacyPolicy', url: '/fidPrivacyPolicy',
      description: 'Home/Privacy Policy'
    },
    {
      path: 'alertMessage/TooManyFailed', url: '/alertMessage/TooManyFailed',
      description: 'Home/Alert: Too many failed attempts'
    },
    {
      path: 'alertMessage/DocNotAccepted', url: '/alertMessage/DocNotAccepted',
      description: 'Home/Alert: Documents not accepted'
    },
    {
      path: 'alertMessage/InfoPrevious', url: '/alertMessage/InfoPrevious',
      description: 'Home/Alert: Information previously submitted'
    },
    {
      path: 'alertMessage/Generic', url: '/alertMessage/Generic',
      description: 'Home/Alert: Generic'
    },
    {
      path: 'statusUpload/DocNotAccepted', url: '/statusUpload/DocNotAccepted',
      description: 'Home/Status blade:Documents not accepted'
    },
    {
      path: 'statusUpload/FinishVerification', url: '/statusUpload/FinishVerification',
      description: 'Home/Status blade: Finish Verification'
    },
    {
      path: 'statusUpload/verified', url: '/statusUpload/verified',
      description: 'Home/Status blade: Verified'
    },
    {
      path: 'statusUpload/InfoPrevious', url: '/statusUpload/InfoPrevious',
      description: 'Home/Status blade: Information previously submitted'
    },
    {
      path: 'statusUpload/upload', url: '/statusUpload/upload',
      description: 'Home/Status blade: Upload'
    },
    {
      path: 'statusUpload/TooManyFailed', url: '/statusUpload/TooManyFailed',
      description: 'Home/Status blade: Too many failed attempts'
    },
    {
      path: 'aucPermissions', url: '/aucPermissions',
      description: 'Home/AUC Permissions'
    },
    {
      path: 'aucMessages', url: '/aucMessages',
      description: 'Home/AUC Messages'
    },
    {
      path: 'aucMessagesConfirmation', url: '/aucMessagesConfirmation',
      description: 'Home/AUC Messages - Confirmation'
    },
    {
      path: 'aucTermsAndCondition', url: '/aucTermsAndCondition',
      description: 'Home/AUC Terms And Condition'
    },
    { path: 'lineDetailNew', url: '/lineDetailNew', description: 'Account/line Detail' },
    { path: 'languageSettings', url: './language-settings', description: 'Home/Language Settings' },
    { path: 'lineSettingsCallerId', url: './lineSettingsCallerId', description: 'Home/Line Settings/Caller ID' },
    { path: 'lineSettingsSelectCalleridConfirmation', url: './lineSettingsSelectCalleridConfirmation', description: 'Home/Line Settings/Confirmation' }
  ];

  currenturl: string;
  currentbreadcrumb;
  currentpathdata = [];
  currentbreadcrumbpath = [];
  currentpath;
  constructor(private router: Router) { }
  ngOnInit(): void {
    // tslint:disable-next-line:prefer-const
    let currentpath;
    let currentdesc;
    const subscription = this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.currenturl = event.url;
        this.currentpathdata = [];
        this.currentbreadcrumbpath = [];
        if (this.currenturl !== '/' || this.currenturl.length === 0) {
          this.currentbreadcrumb = this.breadcrumbvalue.find(x => x.path === this.currenturl.replace(/\//g, ''));
          if (this.currentbreadcrumb != null) {
            this.currentpath = this.currentbreadcrumb['description'].split('/');
            let currentdata = this.currentbreadcrumb['description'].toString();
            while (currentdata.length !== 0) {
              currentdesc = this.breadcrumbvalue.find(x => x.description === currentdata.toString());
              this.currentpathdata.push(currentdesc);
              if (currentdata.lastIndexOf('/') !== -1) {
                currentdata = currentdata.substring(0, currentdata.lastIndexOf('/'));
              } else {
                currentdata = '';
              }
            }
        //    console.log(this.currentpathdata);
            this.currentbreadcrumbpath = this.currentpathdata;
            this.currentbreadcrumbpath = this.currentbreadcrumbpath.reverse();
          }
        }
      }
    });

  }
}


