import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-in-session-radio-button-payment-blade',
  templateUrl: './in-session-radio-button-payment-blade.component.html',
  styleUrls: ['./in-session-radio-button-payment-blade.component.scss']
})
export class InSessionRadioButtonPaymentBladeComponent implements OnInit {
  descWarning = false;
  cardExpiryYear: string;
  cardExpiryMonth: string;

  @Input('cardNumber') cardNumber: string;
  @Input('icon') iconImage: string = '';
  @Input('cardTitle') cardTitle: string;
  @Input('expiryDate') expiryDate: string = '';
  @Input('disable') disable: boolean = false;
  //  @Input('url') url: string = '';
  //  @Input('topDivider') topDivider: boolean = true;
  //  @Input('topDividerClass') topDividerClass: string = 'inset-divider-1px';
  @Input('bottomDivider') bottomDivider: boolean = true;
  @Input('bottomDividerClass') bottomDividerClass: string = 'inset-divider-1px';
  @Input('radioButtonId') radioButtonId: number;
  @Input('radioButtonName') radioButtonName: string = 'radiobutton';
  @Input('selected') selected: number = 1;
  //  @Input('childClass') customClass: string='body';
  //  @Input('showErrorIcon') showErrorIcon: boolean;
  @Input('disableClass') disableClass: string = 'black';
  @Input('disableState') disableState: boolean = false;

  //newly added for editPaymentAlt
  //  @Input('altText') altText: boolean=false;
  @Input('descData') data: string = '';
  @Input('action') action: string = '';
  emptyValue = '';
  popUp: boolean = true;
  @Input('showLegal') showLegal: Boolean = false;
  selectBtnDisabled = false;
  @Input('radioBtnChecked') radioBtnChecked;
  radioBtnChildChecked = false;
  // added for error2
  //  @Input('note') note: string;
  @Output('callBackValue') callBackValue = new EventEmitter();
  @Output('selectButtonDisabled') selectButtonDisabled = new EventEmitter<boolean>();
  @Output('radioButtonChecked') radioButtonChecked = new EventEmitter<boolean>();
  bladedisabled: boolean = null;
  @Input() parentSubject: Subject<any>;
  

  constructor() { }

  ngOnInit() {
    if (this.action !== 'Edit') {
      this.iconImage = this.iconImage + '-disabled';
      this.bladedisabled = true;
      this.selectBtnDisabled = true;
      this.selectButtonDisabled.emit(this.selectBtnDisabled);
    }

    this.cardExpiryMonth = this.expiryDate.substring(1, 2);
    this.cardExpiryYear = this.expiryDate.substring(3, 5);
    const currentMonth = new Date().getMonth() + 1;
    const currentYear = new Date().getFullYear().toString().slice(-2);
    if (this.expiryDate) {
      if (this.cardExpiryMonth <= currentMonth.toString() && this.cardExpiryYear <= currentYear) {
        this.descWarning = true;
        this.expiryDate = 'Expired ' + this.expiryDate;
        this.iconImage = this.iconImage + '-disabled';
        this.bladedisabled = true;
        this.selectBtnDisabled = true;
        this.selectButtonDisabled.emit(this.selectBtnDisabled);
      } else if (Number(this.cardExpiryMonth) - currentMonth <= 1 &&
        Number(this.cardExpiryYear === currentYear) || (currentMonth === 12 &&
          Number(this.cardExpiryYear) === Number(currentYear) + 1)) {
        this.descWarning = true;
        this.expiryDate = 'Expiring soon ' + this.expiryDate;
        this.selectBtnDisabled = false;
        this.selectButtonDisabled.emit(this.selectBtnDisabled);
        if (!this.radioBtnChecked) {
          this.radioBtnChildChecked = !this.radioBtnChildChecked;
          this.radioButtonChecked.emit(true);
        }
      } else {
        if (!this.radioBtnChecked) {
          this.radioBtnChildChecked = !this.radioBtnChildChecked;
          this.radioButtonChecked.emit(true);
        }
      }
    }
    this.parentSubject.subscribe(event => {
      document.getElementById(event).focus();
    });
  }
  deleteModalOpen(action, radioButtonId) {
    if (this.action !== 'Edit') {
      this.callBackValue.emit(action + '' + radioButtonId);
    }
  }
  checkRadio() {
    this.radioBtnChildChecked = true;
  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    this.parentSubject.unsubscribe();
  }
}
