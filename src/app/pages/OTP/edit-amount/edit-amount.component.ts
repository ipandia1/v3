import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-amount',
  templateUrl: './edit-amount.component.html',
  styleUrls: ['./edit-amount.component.scss']
})
export class EditAmountComponent implements OnInit {
  enterAmount: EditAmount;
  displayClass = 'Display6';
  minvalue: boolean;
 checkOpt: boolean;
 public paybillvalues: Array<any> = [
  { descTitle: 'Pay in full', value: '$100.00', id: 'radio1', name: 'sel'},
  { descTitle: 'Other amount', value: this.enterAmount, id: 'radio2', name: 'sel'}
];
 constructor() { }

  ngOnInit() {
    this.enterAmount = {
      enterAmount: ''
    }
  }
}

export class EditAmount {
  enterAmount;
}

