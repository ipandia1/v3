import { Component } from '@angular/core';

@Component({
  selector: 'app-cvv-modal-all-others',
  templateUrl: './cvv-modal-all-others.component.html',
  styleUrls: ['./cvv-modal-all-others.component.scss']
})
export class CvvModalAllOthersComponent{
  visible: boolean;
  constructor() { }
  close() {
    this.visible = false;
  }
}
