import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pay-your-bill',
  templateUrl: './pay-your-bill.component.html',
  styleUrls: ['./pay-your-bill.component.scss']
})
export class PayYourBillComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  paymentmethod:string="Payment method";

  paymentdue:string="$100.00";
  duedate:string="Dec 19";
  Paymentdate:string="Nov 17";
  paymentamount:string="$100.00"
  iconClass = "visa-icon";
 
  

  public paybillvalues: Array<any> = [
    { descTitle: "Amount", value:this.paymentdue,image:"",bottomDivider:false},
    { descTitle: "Payment method", value: "****1234",image:this.iconClass,bottomDivider:false},
    { descTitle: "Date", value: "Dec 17, 2017",image:"",bottomDivider:true}
  ];
}
