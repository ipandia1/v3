import { Component, OnInit, Input, Output, HostBinding, HostListener } from '@angular/core';
import { CommonService } from '../../../services/common.service';
@Component({
  selector: 'app-credit-card-updated',
  templateUrl: './credit-card-updated.component.html',
  styleUrls: ['./credit-card-updated.component.scss']
})
export class CreditCardUpdatedComponent implements OnInit {

  creditcard: creditCard;
  visible: boolean;
  checkOpt: boolean;
  cvvlength:string = "3";
  cardIcon:string = "plainImg";
  visible1: Boolean = false;
  visible2: Boolean = false;
  constructor(private commonService: CommonService) { }
 
  ngOnInit() {
       this.creditcard = {
      cardName: '',
      creditCardNumber: '',
      expirationDate: '',
      cvvNumber: '',
      zipCode: ''
    }
    this.checkOpt = false;
  }
  closeModal() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  showModal() {
    this.visible = true;
    this.commonService.showScroll(false);
  }
  cardChange(event){
    
    var target = event.target || event.srcElement;
    var val = target.value.split('')[0];
    
    if(val == "4"){
        this.cardIcon="visaImg";
    }
    else if(val=="5"){
        this.cardIcon="masterImg";
    }
    else if(val=="1"){
        this.cardIcon="expressImg";
        this.cvvlength="4";
    }
    else{
        this.cardIcon="plainImg";
    }
  }
}


export class creditCard
{
  cardName:string;
  creditCardNumber:string;
  expirationDate:string;
  cvvNumber:string;
  zipCode:string;
}
