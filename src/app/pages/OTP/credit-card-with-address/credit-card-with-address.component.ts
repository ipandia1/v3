import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-credit-card-with-address',
  templateUrl: './credit-card-with-address.component.html',
  styleUrls: ['./credit-card-with-address.component.scss']
})
export class CreditCardWithAddressComponent implements OnInit {

  creditcard: creditCard;
  billingAddress: billingAddress;
  newBillingAddress: newBillingAddress;
  visible: boolean;
  checkOpt: boolean;
  // tslint:disable-next-line:no-inferrable-types
  useBillAddress: boolean = true;
  // tslint:disable-next-line:no-inferrable-types
  stateErr: boolean = false;
  // tslint:disable-next-line:no-inferrable-types
  cvvlength: string = '3';
  cardIcon: string = 'plainImg';
  selectedLine = 'Select';
  selectlineError: boolean;
  subTitle = 'State';
  selectLine: Array<any> = [
    { name: 'AL', disabled: false },
    { name: 'AK', disabled: false },
    { name: 'AZ', disabled: false },
    { name: 'AR', disabled: false }
  ];
  constructor(private commonService: CommonService) { }
  // tslint:disable-next-line:member-ordering
  ngOnInit() {
    this.creditcard = {
      cardName: '',
      creditCardNumber: '',
      expirationDate: '',
      cvvNumber: '',
      zipCode: ''
    };
    this.billingAddress = {
      address1: '1234 Main St',
      address2: 'Suite 001',
      city: 'Bellevue',
      state: 'WA',
      zipcode: '98111'
    }
    this.newBillingAddress = {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: ''
    }
    this.checkOpt = false;
   }
   select(value: string): void {
    if (!value) {
      this.selectedLine = 'Select';
      this.stateErr = true;
    }
    else {
      this.selectedLine = value;
      this.stateErr = false;
     }
   }
   select1() {
     this.selectedLine = 'Select';
     this.stateErr = true;
   }
  closeModal() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  showModal() {
    this.visible = true;
    this.commonService.showScroll(false);
  }

  cardChange(event) {
    var target = event.target || event.srcElement;
    var val = target.value.split('')[0];

    if (val === '4') {
        this.cardIcon = "visaImg";
    }
    else if(val === '5'){
        this.cardIcon="masterImg";
    }
    else if (val === '1') {
        this.cardIcon="expressImg expressImgs";
        this.cvvlength="4";
    }
    else{
        this.cardIcon="plainImg";
    }
  }

}

export class creditCard {
  cardName: string;
  creditCardNumber: string;
  expirationDate: string;
  cvvNumber: string;
  zipCode: string;
}

// tslint:disable-next-line:class-name
export class billingAddress {
  address1: string;
  address2: string;
  city: string;
  state: string;
  zipcode: string;
}
// tslint:disable-next-line:class-name
export class newBillingAddress {
  address1: string;
  address2: string;
  city: string;
  state: string;
  zipcode: string;
}
