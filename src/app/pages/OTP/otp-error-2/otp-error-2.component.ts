import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-otp-error-2',
  templateUrl: './otp-error-2.component.html',
  styleUrls: ['./otp-error-2.component.scss']
})
export class OtpError2Component implements OnInit {
  paymentMethodEnable: boolean = false;
  constructor() { }

  ngOnInit() {
  }
// tslint:disable-next-line:member-ordering
paymentMethods: Array<any> = [
  {cardIcon: 'visa-icon', cardNumber: '****1234', cardStatus: 'In use', add: ''},
  {cardIcon: '', cardNumber: '****1234', cardStatus: 'Delete', add: ''},
   {cardIcon: '', cardNumber: '', cardStatus: '', add: 'Add a bank'},
   {cardIcon: '', cardNumber: '', cardStatus: '', add: 'Add a card'}
];
// tslint:disable-next-line:member-ordering
paymentMethodsReusable: Array<any> = [
  {iconClass: 'visa-icon', cardNumber: '****1234', cardStatus: 'In use', add: '',
   note: 'We can\'t accept bank payments from this account.'},
  {iconClass: 'credit_card_Bank', cardNumber: '****1234', cardStatus: 'Delete', add: '',
   note: 'We can\'t accept bank payments from this account.'},
   {iconClass: '', cardNumber: '', cardStatus: '', add: 'Add a bank'},
   {iconClass: '', cardNumber: '', cardStatus: '', add: 'Add a card'}
]
}
