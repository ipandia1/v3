import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-otp-modal',
  templateUrl: './otp-modal.component.html',
  styleUrls: ['./otp-modal.component.scss']
})
export class OtpModalComponent implements OnInit {

  visible: boolean;
  constructor() { }

  ngOnInit() {
  }

  close() {
    this.visible = false;
  }

}
