import { Component, OnInit, Output } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-edit-amount-alt',
  templateUrl: './edit-amount-alt.component.html',
  styleUrls: ['./edit-amount-alt.component.scss']
})
export class EditAmountAltComponent implements OnInit {
  visible = false;
  enterAmount: EditAmount;
  displayClass = 'Display6';
   public BreakDownValues: Array<any> = [
    { desc: 'Current balance', amount: '$7.24' },
    { desc: 'Past due balance', amount: '$92.76' },
    { desc: 'Restoration fee', amount: '$20.00' },
    { desc: 'Restoration fee tax', amount: '$3.26' },
  ];
  public paybillvalues: Array<any> = [
    { descTitle: "Pay in full", descData: "Restore my account and pay my past-due amount.", descDetails: "View details", value: "$250.00", id: "radio1", name: "sel" },
    { descTitle: "Pay past-due amount", descData: "Restore my account, pay my past due amount.", descDetails: "View details", value: "$124.00", id: "radio3", name: "sel" },
    { descTitle: "Other amount", value: this.enterAmount, id: "radio2", name: "sel" }
  ];
  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.enterAmount = {
      enterAmount: ''
    }
  }
  close() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  select() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  change(popUp) {
    this.commonService.showScroll(false);
    this.visible = popUp;
  }
 


}

export class EditAmount {
  enterAmount;
}
