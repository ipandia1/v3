import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {
    public smallDevice:boolean=false;
    public name: string = "Tom";
    public showDetails: boolean = false;
  constructor() { }
  ngOnInit() { 
  var deviceHeight=window.innerHeight;
  if(deviceHeight<565){
    this.smallDevice=true;
    }
    else{
    this.smallDevice=false;
    }
  }

  public dataPassInfo: Array<Object> = [
    { descTitle: 'Name on account', value: 'John Doe' },
    { descTitle: 'T-Mobile account no.', value: '12346789' },
    { descTitle: 'Reference no.', value: '0123456789' },
    { descTitle: 'Authorization code', value: '0123456789' },
    { descTitle: 'Name on card', value: 'John Doe' },
    { descTitle: 'Payment method', value: 'Visa ****1234' },
    { descTitle: 'Zip', value: '98115' },
    { descTitle: 'Transaction type', value: 'Bill_Pay' }
  ];

  public orderDetails: Array<Object> = [
    { descTitle: 'Name ID', value: '4.00', tax: '+ tax' },
    { descTitle: 'Scam Block', value: '0.00' }
  ];

  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  }

}
