import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-payment-method-disable',
  templateUrl: './edit-payment-method-disable.component.html',
  styleUrls: ['./edit-payment-method-disable.component.scss']
})
export class EditPaymentMethodDisableComponent implements OnInit {

 // tslint:disable-next-line:no-inferrable-types
 paymentMethodEnable: boolean = false;
 disableClass: string = 'disable-div';
  constructor() { }

  ngOnInit() {
  }
// tslint:disable-next-line:member-ordering
paymentMethods: Array<any> = [
  {cardIcon: 'visa-icon', cardNumber: '****1234', cardStatus: 'In use', add: ''},
  {cardIcon: '', cardNumber: '****1234', cardStatus: 'Delete', add: ''},
   {cardIcon: '', cardNumber: '', cardStatus: '', add: 'Add a bank'},
   {cardIcon: '', cardNumber: '', cardStatus: '', add: 'Add a card'}
];
// tslint:disable-next-line:member-ordering
paymentMethodsReusable: Array<any> = [
  
  {iconClass: 'visa-icon-disabled', cardNumber: '****1234', cardStatus: 'In use', add: ''},
  {iconClass: 'bank_disabled', cardNumber: '****1234', cardStatus: 'Delete', add: ''},
   {iconClass: '', cardNumber: '', cardStatus: '', add: 'Add a bank'},
   {iconClass: '', cardNumber: '', cardStatus: '', add: 'Add a card'}
]
}
