import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-edit-payment-method',
  templateUrl: './edit-payment-method.component.html',
  styleUrls: ['./edit-payment-method.component.scss']
})
export class EditPaymentMethodComponent implements OnInit {

  visible: Boolean = false;
  constructor(private commonService: CommonService) { }
  ngOnInit() {
  }
paymentMethods: Array<any>=[
  {cardIcon:"visa-icon",cardNumber:"****1234", cardStatus:"In use",add:""},
  {cardIcon:"",cardNumber:"****1234", cardStatus:"Delete",add:""},
   {cardIcon:"",cardNumber:"", cardStatus:"",add:"Add a bank"},
   {cardIcon:"",cardNumber:"", cardStatus:"",add:"Add a card"}
]
paymentMethodsReusable: Array<any>=[
  {iconClass:"credit_card_Bank",cardNumber:"****1234", cardStatus:"Delete",add:""},
  {iconClass:"visa-icon",cardNumber:"****1234", cardStatus:"In use",add:""},
  {iconClass:"credit_card_Bank",cardNumber:"****1234", cardStatus:"Delete",add:""},
   {iconClass:"",cardNumber:"", cardStatus:"",add:"Add a bank"},
   {iconClass:"",cardNumber:"", cardStatus:"",add:"Add a card"}
]

close() {
  this.visible = false;
  this.commonService.showScroll(true);
  const label = document.getElementsByClassName('labelClass');
  for (let i = 0; i < label.length; i++) {
   label[i].removeAttribute('style');
  }
  }
// select() {
//   this.visible = false;
//   this.commonService.showScroll(true);
//    }
change(popUp) {
  this.commonService.showScroll(false);
  this.visible = popUp;
   const label = document.getElementsByClassName('labelClass');
   for (let i = 0; i < label.length; i++) {
    label[i].setAttribute('style', 'position:unset');
   }
  }

}
