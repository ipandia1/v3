import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-bank-info',
  templateUrl: './bank-info.component.html',
  styleUrls: ['./bank-info.component.scss']
})
export class BankInfoComponent implements OnInit {

  bankinfo: bankInfo;

  visible: boolean;
  checkOpt: boolean;
  constructor(private commonService: CommonService) { }
  ngOnInit() {
       this.bankinfo = {
      nameOnaAccount: '',
      rountingNumber: '',
      accountNumber: '',
    }
    this.checkOpt = true;
  }
  closeModal() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  showModal() {
    this.visible = true;
    this.commonService.showScroll(false);
  }
}


export class bankInfo
{
  nameOnaAccount:string;
  rountingNumber:string;
  accountNumber:String;
}
