import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blade-02',
  templateUrl: './blade-02.component.html',
  styleUrls: ['./blade-02.component.scss']
})
export class Blade02Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public paybillvalues: Array<any> = [
    { descTitle: 'Payment method', value:"Add a payment method",font:'Display6'},
  ];
  
}
