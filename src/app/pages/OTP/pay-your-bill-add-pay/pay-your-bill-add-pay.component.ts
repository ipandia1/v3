import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pay-your-bill-add-pay',
  templateUrl: './pay-your-bill-add-pay.component.html',
  styleUrls: ['./pay-your-bill-add-pay.component.scss']
})
export class PayYourBillAddPayComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  paymentdue:string="$100.00";
  duedate:string="Dec 19";
  Paymentdate:string="Nov 17";
  paymentamount:string="$100.00"

  public paybillvalues: Array<any> = [
    { descTitle: 'Amount', value:"Enter amount",bottomDivider:false},
    { descTitle: 'Payment method', value: "Add a payment method",bottomDivider:false},
    { descTitle: 'Date', value: "Dec 17, 2017",bottomDivider:true}
  ];

}
