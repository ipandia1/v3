import { Component } from '@angular/core';

@Component({
  selector: 'app-replacecard-modal',
  templateUrl: './replacecard-modal.component.html',
  styleUrls: ['./replacecard-modal.component.scss']
})
export class ReplacecardModalComponent {
  visible: boolean;
  constructor() { }
    close() {
    this.visible = false;
  }
}
