import { Component, Input, Output } from '@angular/core';

@Component({
  selector: 'app-delete-bank-modal',
  templateUrl: './delete-bank-modal.component.html',
  styleUrls: ['./delete-bank-modal.component.scss']
})
export class DeleteBankModalcomponent {
visible: boolean;
constructor() { }
  close() {
    this.visible = false;
  }
}
