import { Component, OnInit, HostBinding, HostListener, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-confirmationpitchpage',
  templateUrl: './confirmationpitchpage.component.html',
  styleUrls: ['./confirmationpitchpage.component.scss']
})
export class ConfirmationpitchpageComponent implements OnInit, AfterViewInit {
    public smallDevice:boolean=false;
    public name: string = "Tom";
    public showDetails: boolean = false;
  constructor() { }
  ngOnInit() { 
  var deviceHeight=window.innerHeight;
  if(deviceHeight<565){
    this.smallDevice=true;
    }
    else{
    this.smallDevice=false;
    }
  }

  iconClass = "visa-icon";
   descTitle= "Using my";
    value= "****1234";
    image=this.iconClass;
    bottomDivider=true;

  public dataPassInfo: Array<Object> = [
    { descTitle: 'Name on account', value: 'John Doe' },
    { descTitle: 'T-Mobile account no.', value: '12346789' },
    { descTitle: 'Reference no.', value: '0123456789' },
    { descTitle: 'Authorization code', value: '0123456789' },
    { descTitle: 'Name on card', value: 'John Doe' },
    { descTitle: 'Payment method', value: 'Visa ****1234' },
    { descTitle: 'Zip', value: '98115' },
    { descTitle: 'Transaction type', value: 'Bill_Pay' }
  ];

  public orderDetails: Array<Object> = [
    { descTitle: 'Name ID', value: '4.00', tax: '+ tax' },
    { descTitle: 'Scam Block', value: '0.00' }
  ];

  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  }
  totalGraydivHeight:'0px';
  ngAfterViewInit() {
    this.divHeight();
 }

  @HostListener('window:load', [])
  onWindowLoad() {
    this.divHeight();
  }

  @HostListener('window:resize', [])
  onResize() {
    this.divHeight();
   }

   divHeight = function() {
    var windowHeight = window.innerHeight;
    var whitedivHeight = document.getElementsByClassName('whitediv')[0];
    var whiteareaHeight = whitedivHeight.getBoundingClientRect().height;
    return this.totalGraydivHeight = windowHeight-(whiteareaHeight)+'px';
   }

}
