import { Component, Input, Output } from '@angular/core';

@Component({
  selector: 'app-delete-card-modal',
  templateUrl: './delete-card-modal.component.html',
  styleUrls: ['./delete-card-modal.component.scss']
})
export class DeleteCardModalcomponent {
visible: boolean;
constructor() { }
  close() {
    this.visible = false;
  }
}
