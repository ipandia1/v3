import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.scss']
})
export class CreditCardComponent implements OnInit {

  creditcard: creditCard;
  test:string = 'Srinivas';
  visible: boolean;
  checkOpt: boolean;
  constructor(private commonService: CommonService) { }

  ngOnInit() {
       this.creditcard = {
      cardName: '',
      creditCardNumber: '',
      expirationDate: '',
      cvvNumber: '',
      zipCode: ''
    }
    this.checkOpt = true;
  }
  closeModal() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  showModal() {
    this.visible = true;
    this.commonService.showScroll(false);
  }
}


export class creditCard
{
  cardName:string;
  creditCardNumber:string;
  expirationDate:string;
  cvvNumber:string;
  zipCode:string;
}
