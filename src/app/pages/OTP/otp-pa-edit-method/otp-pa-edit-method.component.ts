import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-otp-pa-edit-method',
  templateUrl: './otp-pa-edit-method.component.html',
  styleUrls: ['./otp-pa-edit-method.component.scss']
})
export class OtpPaEditMethodComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }
// tslint:disable-next-line:member-ordering
paymentMethods: Array<any> = [
  {cardIcon: 'visa-icon', cardNumber: '****1234', cardStatus: 'In use', add: ''},
  {cardIcon: '', cardNumber: '****1234', cardStatus: 'Delete', add: ''},
   {cardIcon: '', cardNumber: '', cardStatus: '', add: 'Add a bank'},
   {cardIcon: '', cardNumber: '', cardStatus: '', add: 'Add a card'}
];
// tslint:disable-next-line:member-ordering
paymentMethodsReusable: Array<any> = [
  {iconClass: 'visa-icon', cardNumber: '****1234', cardStatus: 'In use', add: '', note: ''},
  {iconClass: '', cardNumber: 'Remove existing payment method', add: '',
   note: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas molestie dui et sagittis fermentum.'},
   {iconClass: '', cardNumber: '', cardStatus: '', add: 'Add a bank'},
   {iconClass: '', cardNumber: '', cardStatus: '', add: 'Add a card'}
];
}
