import { Component } from '@angular/core';

@Component({
  selector: 'app-bank-modal',
  templateUrl: './bank-modal.component.html',
  styleUrls: ['./bank-modal.component.scss']
})
export class BankModalComponent {
  visible: boolean;
  constructor() { }
    close() {
    this.visible = false;
  }
}
