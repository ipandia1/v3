import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-otp-breakdown-modal',
  templateUrl: './otp-breakdown-modal.component.html',
  styleUrls: ['./otp-breakdown-modal.component.scss']
})

export class OtpBreakdownModal implements OnInit {
visible: boolean;
  close() {
    this.visible = false;
  }

  otpbreakdown : [{
          chargename : string,
          amount:string;
      },{
        chargename : string,
        amount:string;
    },{
        chargename : string,
        amount:string;
    },{
        chargename : string,
        amount:string;
    }];
  constructor() {
                this.otpbreakdown = [{
                        chargename : "Current balance",
                        amount: "7.24"
                    },
                    {
                        chargename : "Past-due balance",
                        amount: "92.76"
                    },
                    {
                        chargename : "Restoration fee",
                        amount: "20.00"
                    },
                    {
                        chargename : "Restoration fee tax",
                        amount: "3.26"
                    }
                ];       

}
  public add:number=0;
  public total:string;
  ngOnInit() {
        this.otpbreakdown.forEach((e:any) => {
            this.add = this.add + parseFloat(e.amount);
        });
          this.total=this.add.toFixed(2);
  }
}
