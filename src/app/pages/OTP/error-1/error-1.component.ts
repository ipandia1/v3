import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error-1',
  templateUrl: './error-1.component.html',
  styleUrls: ['./error-1.component.scss']
})
export class Error1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  paymentmethod:string="Payment method";

  paymentdue:string="$100.00";
  duedate:string="Dec 19";
  Paymentdate:string="Nov 17";
  paymentamount:string="$100.00"
  iconClass = "''";
 
  

  public paybillvalues: Array<any> = [
    { descTitle: "Amount", value:this.paymentdue,image:"",bottomDivider:false},
    { descTitle: "Payment method", value: "****1234",image:this.iconClass,bottomDivider:false},
    { descTitle: "Date", value: "Dec 17, 2017",image:"",bottomDivider:true}
  ];

  errormessages : Array<any> = [
    {"message":"You must pay your past-due balance of $XX.XX to avoid service interruption and a $20.00/line restore fee."},
    {"message":"Your next AutoPay payment is scheduled for [MM/DD/YYYY]."},
    {"message":"You must pay your past-due balance of $XX.XX to avoid service interruption and a $20.00/line restore fee."}
  ];
}
