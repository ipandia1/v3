import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blade',
  templateUrl: './blade.component.html',
  styleUrls: ['./blade.component.scss']
})
export class BladeComponent implements OnInit {

  constructor() { }
  paymentMethod = 'Payment method';
  cardNumber = '****1234';
  iconClass = 'visa-icon';
  titleClass = 'Display6';
  ngOnInit() {
  }
}
