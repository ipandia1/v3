import { Directive, Input } from '@angular/core';
import { AbstractControl, FormControl, ValidatorFn, NG_VALIDATORS, Validator } from '@angular/forms';

@Directive({
  selector: '[appRangevalidator][ngModel]',
  providers: [{provide: NG_VALIDATORS, useExisting: RangevalidatorDirective, multi: true}]
})
export class RangevalidatorDirective implements Validator  {
@Input()
  appRangevalidator: string;
  validate(controldata: FormControl) {
    let controlValue;
    if ( controldata.value != null) {
    const decimaldata = controldata.value;
    controlValue = Number(decimaldata.replace(/[^0-9\.-]+/g, ''));
    const values = this.appRangevalidator.split('-');
   return (controlValue > values[0] &&  controlValue <= values[1]) ? { 'appRangevalidator': true} : null;
}


}
}
