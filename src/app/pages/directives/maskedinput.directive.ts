import { Directive, Input, HostListener, ElementRef, OnInit,Output,EventEmitter } from '@angular/core';
import { NgControl } from '@angular/forms';
@Directive({
  selector: '[appMaskedinput]',
 })
export class MaskedinputDirective implements OnInit {
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  @Input() maskinputid: string;
   numberofdigts: number;
   label: string;
   mask: string;
  constructor(private el: ElementRef, private control: NgControl) { }
   ngOnInit() {
    if (this.maskinputid === 'cardnumber') {
      this.numberofdigts = 4
      this.label = 'creditCardNumberLabel';
      this.mask = 'creditCardNumberMask';
    } else if (this.maskinputid === 'banknumber') {
      this.numberofdigts = 3;
      this.label = 'bankNumberLabel';
      this.mask = 'bankNumberMask';
    }
  }

  @HostListener('focus', ['$event'])
  onfocus(event: KeyboardEvent) {
    document.getElementById(this.label).classList.add('labelfocusClass');
    document.getElementById(this.mask).classList.remove('placeHolderDisplay')
  }

  @HostListener('blur', ['$event'])
  onBlur(target: any) {

    // alert(this.numberofdigts)
    let cardnumber = this.el.nativeElement.value.replace(/[^\d|\-+|\.+]/g, '');
    if (this.numberofdigts === 4) {
      cardnumber = cardnumber.replace(/(\d{4})/g, '$1 ').replace(/(^\s+|\s+$)/, '');
    } else {
      cardnumber = cardnumber.replace(/(\d{3})/g, '$1 ').replace(/(^\s+|\s+$)/, '');
    }
    this.el.nativeElement.value = cardnumber;
    const htmele = this.el.nativeElement as HTMLInputElement;
    const placeholder = htmele.getAttribute('data-placeholder');
    const ivalue = this.el.nativeElement.value;
    document.getElementById(this.mask).innerHTML = '<i>' + ivalue + '</i>' + placeholder.substring(this.el.nativeElement.value.length);
    if (this.el.nativeElement.value === '') {
      // this.el.nativeElement.labels[0].classList.remove("redClass");
      document.getElementById(this.label).classList.remove('labelfocusClass');
      document.getElementById(this.mask).classList.add('placeHolderDisplay')
    } else {
      document.getElementById(this.mask).classList.remove('placeHolderDisplay')
    }
  }

 @HostListener('keydown', ['$event']) onkeydown(e): boolean {
   if (e.keyCode === 8 || (e.which >= 48 && e.which <= 57) ||
   (e.which >= 96 && e.which <= 105) || [8, 9, 13, 27, 37, 38, 39, 40].indexOf(e.which) > -1) {
       return true;
     } else { e.preventDefault();
       return false;
     }
   }

   @HostListener('keyup', ['$event'])
  onkeyuphandler(event) {
    // alert("srinivas");
     if ([9, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {
     return true;
    }
    let str: string ;
    let arr: any = [];
    str = event.target.value + '';
    arr = str.split('');
    const maxlength = this.el.nativeElement.getAttribute('maxlength');
    let cursorposition = this.el.nativeElement.selectionStart;
     let cardnumber = this.el.nativeElement.value.replace(/[^\d|\-+|\.+]/g, '');
      if (this.numberofdigts === 4) {
        cardnumber = cardnumber.replace(/(\d{4})/g, '$1 ').replace(/(^\s+|\s+$)/, '');
      } else {
        cardnumber = cardnumber.replace(/(\d{3})/g, '$1 ').replace(/(^\s+|\s+$)/, '');
      }
      // this.control.viewToModelUpdate((cardnumber === '') ? null : cardnumber);

      const previousvalue = this.el.nativeElement.value;
      this.el.nativeElement.value = cardnumber;
      const htmele = this.el.nativeElement as HTMLInputElement;
      const placeholder = htmele.getAttribute('data-placeholder');
      document.getElementById(this.mask).innerHTML = '<i>' + cardnumber + '</i>' + placeholder.substring(cardnumber.length);
      if (htmele.setSelectionRange) {
        htmele.focus();
        cursorposition = cursorposition + (this.el.nativeElement.value.length - previousvalue.length);
        setTimeout(() => { htmele.setSelectionRange(cursorposition, cursorposition);
        }, 0);

      }
      if (event.target.value.length > maxlength) {
        event.target.value = event.target.value.substring(0, maxlength);
        this.ngModelChange.emit((event.target.value.length <= maxlength) ? event.target.value : event.target.value.substring(0, maxlength));
      } else {
        this.ngModelChange.emit((event.target.value === 0 ) ? null : event.target.value);
       // this.control.viewToModelUpdate((event.target.value.length<= maxlength) ?
       // event.target.value : event.target.value.substring(0,maxlength));
      }
    }
  }

