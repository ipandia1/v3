import { Directive, HostListener, ElementRef, Output, EventEmitter} from '@angular/core';

@Directive({
  selector: '[appCurrencyformat]'
})
export class CurrencyformatDirective {
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  constructor(private el: ElementRef) { }


  @HostListener('keydown', ['$event']) onKeyDown(event) {
    const e = <KeyboardEvent>event;
    if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+C
      (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+V
      (e.keyCode === 86 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+X
      (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  }

  @HostListener('keyup', ['$event']) onKeyup(event) {
    let cursorposition = this.el.nativeElement.selectionStart;
    const previousvalue = this.el.nativeElement.value;
    while (this.el.nativeElement.value.charAt(0) === '0' &&
     this.el.nativeElement.value.length > 1 && this.el.nativeElement.value.indexOf('.') === -1  ) {
      this.el.nativeElement.value = this.el.nativeElement.value.substr(1);
    }
    if (this.el.nativeElement.value.indexOf('.') >= 0) {
      this.el.nativeElement.value = this.el.nativeElement.value.slice(0, this.el.nativeElement.value.indexOf('.') + 3);
  }
    const decimalSplit = this.el.nativeElement.value.split('.');
    let intPart = decimalSplit[0];
    let decPart = decimalSplit[1];
    intPart = intPart.replace(/[^0-9\.-]+/g, '');
    intPart = intPart.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    if (decPart === undefined) {
      decPart = '';
    } else {
      decPart = '.' + decPart;
    }
    this.el.nativeElement.value = '$' + intPart + decPart;
    if (this.el.nativeElement.setSelectionRange) {
      this.el.nativeElement.focus();
      cursorposition = cursorposition + (this.el.nativeElement.value.length - previousvalue.length);
      setTimeout(() => { this.el.nativeElement.setSelectionRange(cursorposition, cursorposition);
      }, 0);
    // alert(this.el.nativeElement.value);
  }
}

  @HostListener('blur', ['$event']) onblur(event) {
    if (this.el.nativeElement.value !== '' ) {
      const decimalSplit = this.el.nativeElement.value.split('.');
      const intPart = decimalSplit[0];
      let decPart = decimalSplit[1];
      if (decPart === undefined) {
        decPart = '.00';
      } else {
        decPart = '.' + decPart;
      }
  if (this.el.nativeElement.value.indexOf('$') !== -1) {
        event.target.value =  intPart + decPart;
      } else {
        event.target.value = '$' + intPart + decPart;
      }

      if (event.target.value.endsWith('.') === true) {
        event.target.value = event.target.value + '00';
      }

     this.ngModelChange.emit((event.target.value === '$.00') ? null : event.target.value);
    }
  }

  @HostListener('focus', ['$event'])
  public onfocus(event) {
    this.ngModelChange.emit((event.target.value === '') ? '$' : event.target.value);
    setTimeout(() => { this.el.nativeElement.setSelectionRange(event.target.value.length, 1);
    }, 0);
  }

  @HostListener('click', ['$event'])
  public onclick(event) {
    this.ngModelChange.emit((event.target.value === '') ? '$' : event.target.value);
    setTimeout(() => { this.el.nativeElement.setSelectionRange(event.target.value.length, 1);
    }, 0);
  }
}


