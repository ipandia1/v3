import { Directive, ElementRef, HostListener } from '@angular/core';
import { posix } from 'path';

@Directive({
  selector: '[appButtonripple]'
})
export class ButtonrippleDirective {

  constructor(private _elementRef: ElementRef) { };

  @HostListener('click', ['$event', '$event.currentTarget'])
  click(event, element) {
    if (element.querySelector('.ripple-animate') == null) {
      var ripple = document.createElement('span');
      var currentelementpositions = element.getBoundingClientRect();
      var radius;
      if (currentelementpositions.height > currentelementpositions.width) {
        radius = currentelementpositions.height;
      }
      else {
        radius = currentelementpositions.width;
      }

      var scrolltop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
      var scrollleft = (document.documentElement && document.documentElement.scrollLeft) || document.body.scrollLeft;
      var rippleleft = event.pageX - currentelementpositions.left - radius / 2 - scrollleft;
      var rippletop = event.pageY - currentelementpositions.top - radius / 2 - scrolltop;
      ripple.className = 'ripple-animate';
      ripple.style.width = ripple.style.height = radius + 'px';
      ripple.style.left = rippleleft + 'px';
      ripple.style.top = rippletop + 'px';
      ripple.addEventListener('webkitAnimationEnd', () => {
        element.removeChild(ripple);
      });

      element.appendChild(ripple);
    }
  }

}
