import { Directive, ElementRef, HostListener, Input, Output, EventEmitter, Renderer } from '@angular/core';

@Directive({
  selector: '[appClearInput]'
})
export class ClearInputDirective  {


  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  constructor(private renderer: Renderer, private el: ElementRef) {
  }
   @HostListener('keyup', ['$event']) onkeyup(e) {
     if (this.el.nativeElement.value) {
            this.showCrossElement();
        }
        if (!this.el.nativeElement.value) {
            this.hideCrossElement();
        }
    }
    OnInit(): void {
        const currentelement = this.el.nativeElement as HTMLInputElement;
        const crossSpan = document.createElement('span') as HTMLSpanElement;
        const crossIcon = document.createElement('i') as HTMLElement;
        crossIcon.id = 'crossIcon';
        crossIcon.addEventListener('click', (e) => this.clearValue());
        crossSpan.setAttribute('style', 'margin-left: -23px;cursor:pointer;top: 10px;pointer-events:visible;');
        crossIcon.setAttribute('style', 'display:none');
        crossIcon.className = 'clear-icon';
        crossSpan.appendChild(crossIcon);
        crossSpan.id = 'crossSpan';
        currentelement.insertAdjacentElement('afterend', crossSpan);
    }
    showCrossElement() {
        const crossIcon = document.getElementById('crossIcon');
        if (crossIcon) {
            crossIcon.removeAttribute('style');
        }
    }
    hideCrossElement() {
        const crossIcon = document.getElementById('crossIcon');
        if (crossIcon) {
            crossIcon.setAttribute('style', 'display:none');
        }
    }
     clearValue() {
        this.el.nativeElement.value = '';
        this.ngModelChange.emit(this.el.nativeElement.value);
        const crossIcon = document.getElementById('crossIcon');
        if (crossIcon) {
          crossIcon.setAttribute('style', 'display:none');
      }
      const cardValue = this.el.nativeElement.value;
      const htmele = this.el.nativeElement as HTMLInputElement;
      const dataplaceholder = htmele.getAttribute('data-placeholder');
       
      if (dataplaceholder) {
        htmele.parentElement.children[0].innerHTML = '<i>' + cardValue + '</i>' + dataplaceholder ;
      }
    this.el.nativeElement.focus();
}
  }

