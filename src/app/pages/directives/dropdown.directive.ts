import { Directive, ElementRef, HostListener, EventEmitter, Input, Output, ViewChild } from '@angular/core';

import { element } from 'protractor';
@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  @Input() dropdownid: string;

  private Inside = false;
  constructor(private _elementRef: ElementRef) { };
  @HostListener('click', ['$event.target'])
  public onclick(target) {
    const selecteddropdown = document.getElementById(this.dropdownid).querySelectorAll('.dropdown-menu') as unknown as HTMLCollectionOf<HTMLElement>;
    document.getElementById(this.dropdownid).focus();
    const dropdownvalue = document.getElementById(this.dropdownid).querySelectorAll('.dropdown-toggle') as unknown as HTMLCollectionOf<HTMLElement>;
    const bodyrect = selecteddropdown[0].getBoundingClientRect();
    const activeelement = document.getElementById(this.dropdownid).querySelectorAll('.active') as unknown as HTMLCollectionOf<HTMLElement>;
    if (selecteddropdown[0].classList.toggle('show')) {
      selecteddropdown[0].style.marginTop = -(this._elementRef.nativeElement.clientHeight - 3) + 'px';
    } else {
      selecteddropdown[0].style.marginTop = 0 + 'px';
    }
    if (activeelement.length !== 0) {
      selecteddropdown[0].scrollTop = activeelement[0].offsetTop;
    }

  }

  @HostListener('keydown', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    const kcode = event.keyCode;
    if (kcode === 40 || kcode === 38 || kcode === 13) {
      const dropdownvalue = document.getElementById(this.dropdownid).querySelectorAll('.dropdown-toggle') as unknown as HTMLCollectionOf<HTMLElement>;
      const selecteddropdown = document.getElementById(this.dropdownid).querySelectorAll('.dropdown-menu') as unknown as HTMLCollectionOf<HTMLElement>;
      let hoverelement = document.getElementById(this.dropdownid).querySelectorAll('.dropdownclass') as unknown as HTMLCollectionOf<HTMLElement>;
      if (hoverelement.length === 0) {
        hoverelement = document.getElementById(this.dropdownid).querySelectorAll('.active') as unknown as HTMLCollectionOf<HTMLElement>;
      }
      if (hoverelement.length === 0) {
        hoverelement = document.getElementById(this.dropdownid).querySelectorAll('.dropdown-option') as unknown as HTMLCollectionOf<HTMLElement>;
      }
      hoverelement[0].classList.remove('dropdownclass');
      let menuitemtop;
      if (kcode === 40) {
        if (hoverelement[0].nextElementSibling === null) {
          menuitemtop = hoverelement[0].getBoundingClientRect();
          hoverelement[0].classList.add('dropdownclass');
        } else {
          hoverelement[0].nextElementSibling.classList.add('dropdownclass');
          const activeelement = document.getElementById(this.dropdownid).querySelectorAll('.dropdownclass') as unknown as HTMLCollectionOf<HTMLElement>;
          menuitemtop = hoverelement[0].nextElementSibling.getBoundingClientRect();
          selecteddropdown[0].scrollTop = activeelement[0].offsetTop;
        }
        if (menuitemtop.top + 25 >= window.innerHeight) {


        } else {
          event.preventDefault();
          event.stopPropagation();
        }

      } else if (kcode === 38) {
        if (hoverelement[0].previousElementSibling === null || hoverelement[0].previousElementSibling.children.length === 0) {
          hoverelement[0].classList.add('dropdownclass');
        } else {
          hoverelement[0].previousElementSibling.classList.add('dropdownclass');
          const activeelement = document.getElementById(this.dropdownid).querySelectorAll('.dropdownclass') as unknown as HTMLCollectionOf<HTMLElement>;
          selecteddropdown[0].scrollTop = activeelement[0].offsetTop;
        }
        event.preventDefault();
        event.stopPropagation();
      } else if (kcode === 13) {
        const dropdownvalue1 = hoverelement[0] as HTMLElement;
        if (selecteddropdown[0].classList.contains('show')) {
          dropdownvalue1.classList.add('dropdown-option-active');
          dropdownvalue1.children[0].classList.add('dropdown-option-active-a');
        }

        dropdownvalue1.click();
        event.preventDefault();
        event.stopPropagation();

      }
    }

  }

  @HostListener('keyup', ['$event'])
  onKeyupHandler(event: KeyboardEvent) {
    const kcode = event.keyCode;
    if (kcode === 13) {
      // const dropdownvalue = document.getElementById(this.dropdownid).querySelectorAll('.dropdown-toggle') as HTMLCollectionOf<HTMLElement>;
      const selecteddropdown = document.getElementById(this.dropdownid).querySelectorAll('.dropdown-menu') as unknown as HTMLCollectionOf<HTMLElement>;
      let hoverelement = document.getElementById(this.dropdownid).querySelectorAll('.dropdownclass') as unknown as HTMLCollectionOf<HTMLElement>;
      if (hoverelement.length === 0) {
        hoverelement = document.getElementById(this.dropdownid).querySelectorAll('.active') as unknown as HTMLCollectionOf<HTMLElement>;
      }
      if (hoverelement.length === 0) {
        hoverelement = document.getElementById(this.dropdownid).querySelectorAll('.dropdown-option') as unknown as HTMLCollectionOf<HTMLElement>;
      }
      const dropdownvalue1 = hoverelement[0] as HTMLElement;
      dropdownvalue1.classList.remove('dropdown-option-active');
      dropdownvalue1.children[0].classList.remove('dropdown-option-active-a');
    }
    event.preventDefault();
    event.stopPropagation();
  }

  @HostListener('focus', ['$event.target'])
  public onfocus(target) {
  }
  @HostListener('blur', ['$event.target'])
  public onblur(target) {
    const Alldropdowns = document.querySelectorAll('.dropdown-menu');
    const hoverelement = document.getElementById(this.dropdownid).querySelectorAll('.dropdownclass') as unknown as HTMLCollectionOf<HTMLElement>;
    if (hoverelement.length !== 0) {
      hoverelement[0].classList.remove('dropdownclass');
    }
    for (let count = 0; count < Alldropdowns.length; count++) {
      const dropdowncontrol = Alldropdowns[count];
      if (dropdowncontrol.classList.contains('show')) {
        dropdowncontrol.classList.remove('show');
      }
    }
  }
}
