import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appGlobalDropdown]'
})
export class GlobalDropdownDirective {
  @Input() dropdownid: string;

  private Inside = false;
  kcode = 0;
  private accessibility = 'true';
  constructor(private _elementRef: ElementRef, private renderer: Renderer2) { }

  @HostListener('document:click', ['$event'])
  public outsideClick(event) {
    const selecteddropdown = this._elementRef.nativeElement.children[1];
    if (!this._elementRef.nativeElement.contains(event.target)) {
      const Alldropdowns = selecteddropdown.classList.contains('gd-dropdown-menu');
      const showDropdown = selecteddropdown.classList.contains('show');
      const hoverelement = selecteddropdown.querySelectorAll('.gd-dropdown-hover');
      if (hoverelement.length !== 0) {
        hoverelement[0].classList.remove('gd-dropdown-hover');
      }
      if (Alldropdowns && showDropdown) {
        selecteddropdown.classList.remove('show');
        this.renderer.setAttribute(selecteddropdown, 'aria-hidden', 'true');
      }
    } else {
      this._elementRef.nativeElement.focus();
      const dropdownvalue = this._elementRef.nativeElement.children[0];
      const bodyrect = selecteddropdown.getBoundingClientRect();
      const activeelement = selecteddropdown.querySelectorAll('.gd-active');
      if (selecteddropdown.classList.toggle('show')) {
        this.renderer.setStyle(selecteddropdown, 'marginTop', 0 + 'px');
        this.renderer.setStyle(selecteddropdown, 'top', 0 + 'px');
        const hoverelement = selecteddropdown.querySelectorAll('.gd-dropdown-hover');
        if (hoverelement.length !== 0) {
          hoverelement[0].classList.remove('gd-dropdown-hover');
        }
      }
      if (activeelement.length !== 0) {
        (<HTMLElement>selecteddropdown).scrollTop = (<HTMLElement>activeelement[0]).offsetTop;
        this.accessibility === 'true' ? this.accessibility = 'false' : this.accessibility = 'true';
        this.renderer.setAttribute(selecteddropdown, 'aria-hidden', this.accessibility);
      }
    }
  }

  @HostListener('keydown', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    this.kcode = event.keyCode;
    const selecteddropdown = this._elementRef.nativeElement.children[1];
    const showMenu = selecteddropdown.classList.contains('show');
    if (this.kcode === 40 || this.kcode === 38 || this.kcode === 13 || (this.kcode === 9 && showMenu)) {
      const dropdownvalue = this._elementRef.nativeElement.children[0];
      let hoverelement = selecteddropdown.querySelectorAll('.gd-dropdown-hover');
      if (hoverelement.length === 0) {
        hoverelement = selecteddropdown.querySelectorAll('.gd-active');
      }
      if (hoverelement.length === 0) {
        hoverelement = selecteddropdown.querySelectorAll('.gd-dropdown-option');
      }
      hoverelement[0].classList.remove('gd-dropdown-hover');
      let menuitemtop;
      if (this.kcode === 40 || this.kcode === 9) {
        if (hoverelement[0].nextElementSibling === null) {
          if (this.kcode === 9) {
            selecteddropdown.classList.remove('show');
          } else {
            hoverelement[0].classList.add('gd-dropdown-hover');
            menuitemtop = hoverelement[0].getBoundingClientRect();
          }
        } else {
          hoverelement[0].nextElementSibling.classList.add('gd-dropdown-hover');
          if (this.kcode === 40) {
            hoverelement[0].nextElementSibling.children[0].focus();
          }
          const activeelement = selecteddropdown.querySelectorAll('.gd-dropdown-hover');
          menuitemtop = hoverelement[0].nextElementSibling.getBoundingClientRect();
          (<HTMLElement>selecteddropdown).scrollTop = (<HTMLElement>activeelement[0]).offsetTop;
        }
        if (this.kcode === 40) {
          if (menuitemtop.top + 25 >= window.innerHeight) {
          } else {
            event.preventDefault();
            event.stopPropagation();
          }
        }
      } else if (this.kcode === 38) {
        if (hoverelement[0].previousElementSibling === null || hoverelement[0].previousElementSibling.children.length === 0) {
          hoverelement[0].classList.add('gd-dropdown-hover');
        } else {
          hoverelement[0].previousElementSibling.classList.add('gd-dropdown-hover');
          const activeelement = selecteddropdown.querySelectorAll('.gd-dropdown-hover');
          (<HTMLElement>selecteddropdown).scrollTop = (<HTMLElement>activeelement[0]).offsetTop;
        }
        event.preventDefault();
        event.stopPropagation();
      } else if (this.kcode === 13) {
        const dropdownvalue1 = hoverelement[0] as HTMLElement;
        if (selecteddropdown.classList.contains('show')) {
          this.renderer.setAttribute(selecteddropdown, 'aria-hidden', 'false');
        }
        dropdownvalue1.click();
        event.preventDefault();
        event.stopPropagation();

      }
    }
  }

  @HostListener('keyup', ['$event'])
  onKeyupHandler(event: KeyboardEvent) {
    this.kcode = event.keyCode;
    if (this.kcode === 13) {
      const selecteddropdown = this._elementRef.nativeElement.children[1];
      let hoverelement = selecteddropdown.querySelectorAll('.gd-dropdown-hover');
      if (hoverelement.length === 0) {
        hoverelement = selecteddropdown.querySelectorAll('.gd-active');
      }
      const dropdownvalue1 = hoverelement[0] as HTMLElement;
      dropdownvalue1.children[0].classList.remove('gd-dropdown-option-active-a');
    }
    event.preventDefault();
    event.stopPropagation();
  }

  @HostListener('focus', ['$event.target'])
  public onfocus(target) {
  }
  @HostListener('blur', ['$event.target'])
  public onblur(target) {
    const Alldropdowns = this._elementRef.nativeElement.querySelectorAll('.gd-dropdown-menu');
    const selecteddropdown = this._elementRef.nativeElement.children[1];
    const hoverelement = selecteddropdown.querySelectorAll('.gd-dropdown-hover');
    if (hoverelement.length !== 0) {
      hoverelement[0].classList.remove('gd-dropdown-hover');
    }
    for (let count = 0; count < Alldropdowns.length; count++) {
      const dropdowncontrol = Alldropdowns[count];
      if (dropdowncontrol.classList.contains('show')) {
        dropdowncontrol.classList.remove('show');
        this.renderer.setAttribute(selecteddropdown , 'aria-hidden', 'true');
      }
    }
  }

}
