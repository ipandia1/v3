import { Component, Input, Output } from '@angular/core';

@Component({
  selector: 'app-netflix-modal',
  templateUrl: './netflix-modal.component.html',
  styleUrls: ['./netflix-modal.component.scss']
})
export class NetflixModalComponent {
visible: boolean;
constructor() { }
  close() {
    this.visible = false;
  }
}
