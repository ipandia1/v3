import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirmation-order',
  templateUrl: './confirmation-order.component.html',
  styleUrls: ['./confirmation-order.component.scss']
})
export class ConfirmationOrderComponent implements OnInit {

  constructor() { }
  public oderdetails: Array<Object> = [
    { descTitle: 'Order no.', value: 'John Doe' },
    { descTitle: 'Payment type', value: 'Visa ****1234' },
    { descTitle: 'Name on account', value: '123467890' },
    { descTitle: 'T-Mobile account no.', value: '123467890' }
  ];

   ngOnInit() {
  }

}
