import { CommonService } from '../../../services/common.service';
import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-line-selector',
  templateUrl: './line-selector.component.html',
  styleUrls: ['./line-selector.component.scss']
})
export class LineSelectorComponent implements OnInit {
  LineSelectorPath = 'assets/json/LineSelectorData.json';
  isDesktop: boolean;
  lineSelectorDetails: true;
  LineSelectorData: any;
  public showDetails: Boolean = false;
  constructor(private commonService: CommonService, private elementref: ElementRef) {
    this.commonService.getJsonData(this.LineSelectorPath)
     .subscribe(responseJson => {
       this.LineSelectorData = responseJson;
     });
   }
  ngOnInit() {
    if (window.innerWidth >= 768) {
      this.isDesktop = true;
  } else {
      this.isDesktop = false;
  }
  }
@HostListener('window:resize', ['$event'])
onResize(event) {
  if (window.innerWidth > 767) {
    this.isDesktop = true;
  } else {
    this.isDesktop = false;
  }
}
}
