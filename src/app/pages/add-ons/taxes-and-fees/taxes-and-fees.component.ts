import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-taxes-and-fees',
  templateUrl: './taxes-and-fees.component.html',
  styleUrls: ['./taxes-and-fees.component.scss']
})
export class TaxesAndFeesComponent implements OnInit {

  constructor() { }

  public taxItems: Array<Object> = [
    { descTitle: 'Federal universal service fund', value: '.10' },
    { descTitle: 'Utility use', value: '.15' },
    { descTitle: 'Regulatory programs & telco recovery fee', value: '.22' },
    { descTitle: 'State & local sales tax', value: '.35' },
    { descTitle: 'State 911', value: '.08' }
  ];

  ngOnInit() {
  }

}
