import { CommonService } from '../../../services/common.service';
import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-value-added-service',
  templateUrl: './value-added-service.component.html',
  styleUrls: ['./value-added-service.component.scss']
})
export class ValueSelectorComponent implements OnInit {
  LineSelectorPath = 'assets/json/ValueSelectorData.json';
  isDesktop: boolean;
  lineSelectorDetails: true;
  ValueSelectorData: any;
  public showDetails: Boolean = false;
  constructor(private commonService: CommonService, private elementref: ElementRef) {
    this.commonService.getJsonData(this.LineSelectorPath)
     .subscribe(responseJson => {
       this.ValueSelectorData = responseJson;
     });
   }
  ngOnInit() {
    if (window.innerWidth >= 768) {
      this.isDesktop = true;
  } else {
      this.isDesktop = false;
  }
  }
@HostListener('window:resize', ['$event'])
onResize(event) {
  if (window.innerWidth > 767) {
    this.isDesktop = true;
  } else {
    this.isDesktop = false;
  }
}
}
