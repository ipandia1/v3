import { Component, OnInit,ElementRef } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-p360-scary',
  templateUrl: './p360-scary.component.html',
  styleUrls: ['./p360-scary.component.scss']
})
export class P360ScaryComponent implements OnInit {
  visible: boolean;
  constructor(private elementref: ElementRef,private commonService: CommonService) { }

  ngOnInit() {
  }
  close() {
    this.visible = false;
    this.commonService.showScroll(true);
    this.elementref.nativeElement.querySelectorAll('.SecondaryCTA')[0].focus();
  }
  openEditModal(){
    this.visible = true;
    this.commonService.showScroll(false); 
  }
  
  backTabEvent(event, mainId, lastId) {
    const me = this;
    const allElements = document.getElementById(mainId).querySelectorAll
      ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
    if (document.activeElement === allElements[0]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          setTimeout(function () {
            // document.getElementById(lastId).focus();
            me.elementref.nativeElement.querySelector('#' + lastId).focus();

          }, 0);
        }
      }
    }
    if (document.activeElement === document.getElementById(mainId)) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          setTimeout(function () {
            // document.getElementById(lastId).focus();
            me.elementref.nativeElement.querySelector('#' + lastId).focus();

          }, 0);
        }
      }
    }
    if (document.activeElement === allElements[allElements.length - 1]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
        } else {
          event.preventDefault();
          setTimeout(function () {
            // document.getElementById(mainId).focus();
            me.elementref.nativeElement.querySelector('#' + mainId).focus();
          }, 0);
        }
      }
    }
  }
}
