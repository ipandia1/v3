import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P360ScaryComponent } from './p360-scary.component';

describe('P360ScaryComponent', () => {
  let component: P360ScaryComponent;
  let fixture: ComponentFixture<P360ScaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P360ScaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P360ScaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
