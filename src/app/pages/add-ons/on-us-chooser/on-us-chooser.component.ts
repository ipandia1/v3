import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
@Component({
  selector: 'app-on-us-chooser',
  templateUrl: './on-us-chooser.component.html',
  styleUrls: ['./on-us-chooser.component.scss'],
})
export class OnUsChooserComponent implements OnInit {
  onUsOptions;
  selctedOption;

  constructor(public router: Router, private commonService: CommonService) {}
  ngOnInit() {
    this.loadJson();
  }
  loadJson() {
    if (
      localStorage.getItem('variant') === null ||
      localStorage.getItem('variant') === ''
    ) {
      localStorage.setItem('variant', '0'); // 0 or 1
      this.onUsOptions = require('../../../../assets/json/add-ons/on-us-chooser.json');
    } else {
      const variant = Number(localStorage.getItem('variant'));
      switch (variant) {
        case 0: {
          this.onUsOptions = require('../../../../assets/json/add-ons/on-us-chooser.json');
          break;
        }
        case 1: {
          this.onUsOptions = require('../../../../assets/json/add-ons/on-us-chooser1.json');
          break;
        }
        case 2: {
          this.onUsOptions = require('../../../../assets/json/add-ons/on-us-chooser2.json');
          break;
        }
        case 3: {
          this.onUsOptions = require('../../../../assets/json/add-ons/on-us-chooser3.json');
          break;
        }
        case 4: {
          this.onUsOptions = require('../../../../assets/json/add-ons/on-us-business1.json');
          break;
        }
        case 5: {
          this.onUsOptions = require('../../../../assets/json/add-ons/on-us-business2.json');
          break;
        }
        default: {
          this.onUsOptions = require('../../../../assets/json/add-ons/on-us-chooser.json');
          break;
        }
      }
    }
  }
}
