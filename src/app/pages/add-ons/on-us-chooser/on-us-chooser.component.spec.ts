import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnUsChooserComponent } from './on-us-chooser.component';

describe('OnUsChooserComponent', () => {
  let component: OnUsChooserComponent;
  let fixture: ComponentFixture<OnUsChooserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnUsChooserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnUsChooserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
