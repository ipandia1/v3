import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-p360-removal',
  templateUrl: './p360-removal.component.html',
  styleUrls: ['./p360-removal.component.scss']
})
export class P360RemovalComponent implements OnInit {

  constructor() { }
  
  body = document.getElementsByTagName('body')[0];
  visible = false;
  ngOnInit() {
  }
  showScroll() {
    this.body.classList.remove('noScroll');
  }
  hideScroll() {
    this.body.classList.add('noScroll');
  }
    close() {
      this.visible = false;
      this.showScroll();
    }
  modalOpen() {
    this.visible = true;
    this.hideScroll();
  }
  backTabEvent(event, mainId,lastId) {
    const allElements = document.getElementById(mainId).querySelectorAll
    ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
      if (document.activeElement === allElements[0]) {
        if (event.keyCode === 9) {
          if (event.shiftKey) {
            event.preventDefault();
            setTimeout(function() {
              document.getElementById(lastId).focus();
            }, 0);
          }
        }
      }
      if (document.activeElement === document.getElementById(mainId)) {
        if (event.keyCode === 9) {
          if (event.shiftKey) {
            event.preventDefault();
            setTimeout(function() {
              document.getElementById(lastId).focus();
            }, 0);
          }
        }
      }
      if (document.activeElement === allElements[allElements.length - 1]) {
        if (event.keyCode === 9) {
          if (event.shiftKey) {}
          else {
          event.preventDefault();
          setTimeout(function() {
            document.getElementById(mainId).focus();
          }, 0);
        }
  
      }
      }
  }

}
