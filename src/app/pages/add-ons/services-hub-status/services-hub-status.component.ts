import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-services-hub-status',
  templateUrl: './services-hub-status.component.html',
  styleUrls: ['./services-hub-status.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ServicesHubStatusComponent implements OnInit {
  hubData = [
    {
      'descTitle': 'Quibi for T-Mobile',
      'font': 'Display6 gray85',
      'class': 'title-float', // remove class for blade without icons
      'icon': 'check-full-green', // remove icon for blade without icons
      'url': '/'
    },
    {
      'descTitle': 'TES Live TV',
      'font': 'Display6 gray85',
      'class': 'title-float', // remove class for blade without icons
      'icon': 'check-full-gray', // remove icon for blade without icons
      'url': '/'
    },
    {
      'descTitle': 'ScamShield',
      'font': 'Display6 gray85',
      'class': 'title-float', // remove class for blade without icons
      'icon': 'check-full-gray', // remove icon for blade without icons
      'url': '/'
    }
  ];

  constructor() { }
  ngOnInit() { }
}
