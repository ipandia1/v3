import { Component,ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-manage-data',
  templateUrl: './manage-data.component.html',
  styleUrls: ['./manage-data.component.scss']
})
export class ManageDataComponent {

enabled = true;
radioBtnChecked = false;
constructor(private cdRef: ChangeDetectorRef) { }
public UserInfo:Array<object> = [
  { name: 'Elton', value: '(206)-555-1111' }
];

}

