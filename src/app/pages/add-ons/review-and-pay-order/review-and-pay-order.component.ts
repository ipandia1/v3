import { Component, OnInit, ElementRef } from '@angular/core';

@Component({
  selector: 'app-review-and-pay-order',
  templateUrl: './review-and-pay-order.component.html',
  styleUrls: ['./review-and-pay-order.component.scss']
})
export class ReviewAndPayOrderComponent implements OnInit {

  showDetails = false;
  enabled = true;

  constructor(private elref: ElementRef) { }

  ngOnInit() {
  }
  public reviewordervalues: Array<any> = [
   
    { descTitle: 'Payment method', value:"Add a payment method",font:'Display6',font1:'body gray60'}
  ];
  public reviewordervalues1: Array<any> = [
   
    { descTitle1: 'Taxes & fees', value:"$1.00",font:'Display6',font1:'body gray60'}
  ];
  detailsPayment() {
     this.showDetails = !this.showDetails;
  }

}
