import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-up-verification',
  templateUrl: './sign-up-verification.component.html',
  styleUrls: ['./sign-up-verification.component.scss']
})
export class SignUpVerificationComponent {
    public smallDevice:boolean=false;
    public name: string = "Matt";
    public showDetails: boolean = false;
  constructor() { }
  public dataPassInfo: Array<Object> = [
    { descTitle: 'Order no.', value: '0123456789' },
    { descTitle: 'Transaction total', value: '$16.00' },
    { descTitle: 'Payment type', value: 'Visa ****1234' },
    { descTitle: 'Name on account', value: 'John Doe' },
    { descTitle: 'T-Mobile account no.', value: '12346789' }
  ];

  public giftArray: Array<Object> = [{
    id: '1', imageLink: 'http://placehold.it/608x304?text=FPO', giftTitle:'We have Gift for you!' , giftmessage: 'Get your Circle Home on us',
  },
  ];


  public addItems: Array<Object> = [
    { descTitle: 'Single line name service', value: '15.99' },
    { descTitle: 'Single line name service', value: '15.99' }
  ];
  public removeItems: Array<Object> = [
    { descTitle: 'Single line name service', value: '15.99' }
  ];
  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  }
}
