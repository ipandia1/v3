import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pa-landing-no-payment',
  templateUrl: './pa-landing-no-payment.component.html',
  styleUrls: ['./pa-landing-no-payment.component.scss']
})
export class paLandingNoPaymentComponent implements OnInit {
  public add = 0;
  public total: string;
  paymentMethod = 'Using my';
  icon = '';
  cardNumber = 'Add a payment method';
  bottomDividerClass = '';
  arrowClass = 'arrow-right d-inline-block';
  topDivider = true;
  paymentInfo: Array<any> = [{
    title: 'Payment blade Info',
   paymentData: [
   { title: 'Payment schedule',
    total: '210.00',
    status: false,
    paymentDetails: [
      {id: 1, date: 'Jul 13, 2017', status: '', amount: '105.00'},
      {id: 2, date: 'Jul 27, 2017', status: '', amount: '105.00'}],
 }],
}];
constructor(private _location: Location) { }
  ngOnInit() {
  }
  gotopreviouspage() {
    this._location.back();
  }
}