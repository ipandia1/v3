import { Component, OnInit, Output } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pa-edit-v3-2',
  templateUrl: './pa-edit-v3-2.component.html',
  styleUrls: ['./pa-edit-v3-2.component.scss']
})
export class PaEditV32Component implements OnInit {

 payment = '$210.00';
 visible: boolean;
  breakdown: [{
    chargename: string,
    amount: string;
  },{
    chargename: string,
    amount: string;
  },{
    chargename: string,
    amount: string;
  },{
    chargename: string,
    amount: string;
  }];
  public add = 0;
  public total: string;
  public paybillvalues: Array<any> = [
    { descTitle: 'Total balance', value: this.payment,
    image: '', bottomDivider: false,
    secondLineDesc: 'Lorem ipsum dolor sit amet, adipiscing elit.',
    secondLineUrl: 'View details', displaySecondLine: true},
  ];
  startDateTitle = 'Select a payment date';
  constructor(private commonService: CommonService, private _location: Location) {
      this.breakdown = [{
      chargename: 'Current balance',
      amount: '109.00'
    },
    {
      chargename: 'Past due balance',
      amount: '109.00'
    },
    {
      chargename: 'Restoration fee',
      amount: '20.00'
    },
    {
      chargename: 'Restoration fee tax',
      amount: '2.00'
    }
    ];
   }

  ngOnInit() {
    this.breakdown.forEach((e: any) => {
      this.add = this.add + parseFloat(e.amount);
    });
    this.total = this.add.toFixed(2);
  }
  close() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
   change(popUp) {
     this.visible = true;
     this.commonService.showScroll(false);
   }
   gotopreviouspage() {
    this._location.back();
  }
}
