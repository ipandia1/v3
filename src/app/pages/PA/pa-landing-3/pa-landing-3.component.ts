import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pa-landing-3',
  templateUrl: './pa-landing-3.component.html',
  styleUrls: ['./pa-landing-3.component.scss']
})
export class paLanding3Component implements OnInit {
  public add = 0;
  public total: string;
  paymentMethod = 'Using my';
  icon = 'visa-icon-disabled';
  cardNumber = '****1234';
  topDivider = true;
  bottomDividerClass = '';
  arrowClass = 'arrow-right d-inline-block';
  paymentMethodEnable = false;
  disableClass = 'gray30';
  paymentInfo: Array<any> = [{
    title: 'Payment blade Info',
   paymentData: [
   { title: 'Payment schedule',
    total: '210.00',
    status: false,
    paymentDetails: [
      {id: 1, date: 'Jul 13, 2017', status: 'Scheduled', amount: '105.00'},
      {id: 2, date: 'Jul 27, 2017', status: 'Scheduled', amount: '105.00'}],
 }],
}];
constructor(private _location: Location) { }
  ngOnInit() {
}
gotopreviouspage() {
  this._location.back();
}

}
