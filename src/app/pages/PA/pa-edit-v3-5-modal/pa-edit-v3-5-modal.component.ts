import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pa-edit-v3-5-modal',
  templateUrl: './pa-edit-v3-5-modal.component.html',
  styleUrls: ['./pa-edit-v3-5-modal.component.scss']
})
export class PaEditV35ModalComponent implements OnInit {
  title: String = 'Select payment date 2';
  showSubTitle: Boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
