import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pa-otp-edit-method',
  templateUrl: './pa-otp-edit-method.component.html',
  styleUrls: ['./pa-otp-edit-method.component.scss']
})
export class PaOtpEditMethodComponent implements OnInit {

  enterAmount = '';
  displayClass = 'Display6';
  iconwh = 'iconClass';
  public paybillvalues: Array<any> = [
    { descData: '', iconClass: 'visa-icon', cardNumber: '****1234', cardStatus: 'In use'},
    { descData: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas molestie dui et sagittis fermentum.', iconClass: '',
    cardNumber: 'Remove existing payment method', cardStatus: ''}
  ];
  public Addcard: Array<any> = [
    { add: 'Add a bank'},
    { add: 'Add a card'}
  ];
  constructor(private _location: Location) {
  }
  ngOnInit() {
  }
  gotopreviouspage() {
    this._location.back();
  }

}
