import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pa-edit-v3-6-modal',
  templateUrl: './pa-edit-v3-6-modal.component.html',
  styleUrls: ['./pa-edit-v3-6-modal.component.scss']
})
export class PaEditV36ModalComponent implements OnInit {

  title: String = 'Select a payment date';
  showSubTitle: Boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
