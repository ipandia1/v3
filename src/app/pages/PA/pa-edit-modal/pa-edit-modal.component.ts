import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pa-edit-modal',
  templateUrl: './pa-edit-modal.component.html',
  styleUrls: ['./pa-edit-modal.component.scss']
})
export class PaEditModalComponent implements OnInit {
  visible: boolean;
  breakdown: [{
    chargename: string,
    amount: string;
  },{
    chargename: string,
    amount: string;
  },{
    chargename: string,
    amount: string;
  },{
    chargename: string,
    amount: string;
  }];
  public add = 0;
  public total: string;
  constructor() {
    this.breakdown = [{
      chargename: 'Current balance',
      amount: '109.00'
    },
    {
      chargename: 'Past due balance',
      amount: '109.00'
    },
    {
      chargename: 'Restoration fee',
      amount: '20.00'
    },
    {
      chargename: 'Restoration fee tax',
      amount: '2.00'
    }
    ];

  }
  ngOnInit() {
    this.breakdown.forEach((e: any) => {
      this.add = this.add + parseFloat(e.amount);
    });
    this.total = this.add.toFixed(2);
  }
  close() {
    this.visible = false;
  }
}
