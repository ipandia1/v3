import { Component } from '@angular/core';

@Component({
  selector: 'app-pa-otp-modal',
  templateUrl: './pa-otp-modal.component.html',
  styleUrls: ['./pa-otp-modal.component.scss']
})
export class PaOtpModalComponent {
  visible: boolean;
  constructor() { }
  close() {
    this.visible = false;
  }
}
