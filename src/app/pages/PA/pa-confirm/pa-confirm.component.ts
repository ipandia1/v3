import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pa-confirm',
  templateUrl: './pa-confirm.component.html',
  styleUrls: ['./pa-confirm.component.scss']
})
export class PaConfirmComponent implements OnInit {
  public showDetails: Boolean = false;
  public dataPassInfo: Array<Object> = [
    { descTitle: 'Names on account', value: 'John Doe' },
    { descTitle: 'T-Mobile account no.', value: '0123456789' },
    { descTitle: 'Reference on.', value: '0123456789' },
    { descTitle: 'Authorization code', value: '0123456789' },
    { descTitle: 'Name on card', value: 'John Doe' },
    { descTitle: 'Payment method', value: 'Visa ****1234' },
    { descTitle: 'Zip', value: '98115' },
    { descTitle: 'Transaction type', value: 'PA' }
  ];
  arrowClass =  '';
  paymentInfo:  Array<any> =  [{
  title:  'Payment blade Info',
  paymentData:  [
  { title:  'Payment schedule',
  total:  '210.00',
  status:  true,
  paymentDetails:  [
  {id:  1,  date:  'Jul 13, 2017',  status:  '',  amount:  '105.00'},
  {id:  2,  date:  'Jul 27, 2017',  status:  '',  amount:  '105.00'}],
  }],
  }];
  constructor(private _location: Location) { }

  ngOnInit() {
  }
  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  }
  gotopreviouspage() {
    this._location.back();
  }
}
