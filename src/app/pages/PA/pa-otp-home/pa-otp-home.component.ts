import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pa-otp-home',
  templateUrl: './pa-otp-home.component.html',
  styleUrls: ['./pa-otp-home.component.scss']
})
export class PaOtpHomeComponent implements OnInit {

  paymentmethod: String = 'Payment method';
  paymentdue: String = '$100.00';
  duedate: String = 'Dec 19';
  Paymentdate: String = 'Nov 17';
  paymentamount: String = '$100.00';
  iconClass = 'visa-icon';
  public paybillvalues: Array<any> = [
    { descTitle: 'Amount', value: this.paymentdue, image: '', bottomDivider: false },
    { descTitle: 'Payment method', value: '****1234', image: this.iconClass, bottomDivider: false },
    { descTitle: 'Date', value: 'Dec 17, 2017', image: '', bottomDivider: true }
  ];
  errormessages: Array<any> = [
    { 'message': 'This payment will NOT cancel your scheduled Payment Arrangement. Review' }
  ];
  constructor(private _location: Location) { }

  ngOnInit() {
  }
  gotopreviouspage() {
    this._location.back();
  }
}
