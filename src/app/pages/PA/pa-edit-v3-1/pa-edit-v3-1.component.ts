import { Component, OnInit, Output } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pa-edit-v3-1',
  templateUrl: './pa-edit-v3-1.component.html',
  styleUrls: ['./pa-edit-v3-1.component.scss']
})
export class PaEditV31Component implements OnInit {
  visible = false;
  displayClass = 'Display6';
  title: String = 'Select payment date 1';
  title2: String = 'Select payment date 2';
  showSubTitle: Boolean = false;
  showPayment2: Boolean = false;
  payment: String = '$210.00';
  startDateTitle = 'Select a payment date';
 // placeholderText = 'Select date';
  checkOpt: Boolean;
  public BreakDownValues: Array<any> = [
    { desc: 'Current balance', amount: '$119.00' },
    { desc: 'Past due balance', amount: '$110.00' },
    { desc: 'Restoration fee', amount: '$2.00' },
    { desc: 'Restoration fee tax', amount: '$2.00' },
  ];

  public paybillvalues: Array<any> = [
    { descTitle: 'Total balance', descData: '', descDetails: 'View details', value: '$210.00', id: 'radio1', name: 'sel' },
    { descTitle: 'Past due', descData: '', descDetails: 'View details', value: '$110.00', id: 'radio2', name: 'sel' },

  ];
  constructor(private commonService: CommonService, private _location: Location) { }

  ngOnInit() {
    this.checkOpt = false;
  }
  close() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  select() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  change(popUp) {
    this.commonService.showScroll(false);
    this.visible = popUp;
  }
  gotopreviouspage() {
    this._location.back();
  }
}
