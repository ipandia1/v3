import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-line-settings-nickname',
  templateUrl: './line-settings-nickname.component.html',
  styleUrls: ['./line-settings-nickname.component.scss']
})
export class LineSettingsNicknameComponent implements OnInit {

  subTitle = 'Select Line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';
  name: Name;
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];

  constructor() { }

  ngOnInit() {
    this.name = {
      firstName : '',
      lastName : ''
    }
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue;
    }
  }

}

export class Name {
  firstName: string;
  lastName: string;
}
