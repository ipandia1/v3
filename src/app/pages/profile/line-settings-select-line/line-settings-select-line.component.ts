import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-line-settings-select-line',
  templateUrl: './line-settings-select-line.component.html',
  styleUrls: ['./line-settings-select-line.component.scss']
})
export class LineSettingsSelectLineComponent implements OnInit {

   subTitle = 'Select Line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';
  topDividerClass = '';

  bladeInfo: Array<any> = [
    {title: 'Nickname', desc: 'Jay L', desc2: '', url: '/lineSettingsNickname'},
    {title: 'e911Address', desc: '1 Ravinia Dr Suite 1000', desc2: 'Atlanta, GA 30346', url: ''},
    {title: 'Usage Address', desc: '1 Ravinia Dr Suite 1000', desc2: 'Atlanta, GA 30346', url: ''},
    {title: 'Permissions', desc: 'Primary Account Holder', desc2: '', url: '/lineSettingsLineChanged'},
  ];

  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];

  constructor() { }

  ngOnInit() {
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue;
    }
  }
}
