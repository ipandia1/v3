import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-designation-current-lines',
  templateUrl: './employee-designation-current-lines.component.html',
  styleUrls: ['./employee-designation-current-lines.component.scss']
})
export class EmployeeDesignationCurrentLinesComponent implements OnInit {
  bladeInfo: Array<any> = [
    { title: 'Employee name', desc: '(123) 456-7890', desc2: 'Non-dependent', url: '' },
    { title: 'Employee name', desc: '(123) 456-7890', desc2: 'Non-dependent', url: '' },
    { title: 'Employee name', desc: '(123) 456-7890', desc2: 'Non-dependent', url: '' },
    { title: 'Employee name', desc: '(123) 456-7890', desc2: 'Non-dependent', url: '' },
    { title: 'Employee name', desc: '(123) 456-7890', desc2: 'Non-dependent', url: '' }
  ];
  constructor() { }

  ngOnInit() {
  }

}
