import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-media-settings',
  templateUrl: './media-settings.component.html',
  styleUrls: ['./media-settings.component.scss']
})
export class MediaSettingsComponent implements OnInit {
  subTitle = 'Select line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';

  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  bladeInfo: Array<any> = [
    {title: 'Binge On<sup>TM</sup>',
    desc: 'Worry-free streaming with mobile-optimized video.',
    desc2: '<a class="body-link cursor black" href="#">Learn more about Binge On</a>',
    url: '', icon: '', switch: true, checkOpt : false},
    {title: 'HD video resolution', desc: '', desc2: '', url: '', icon: '', switch: true, checkOpt: true}
  ];
  constructor() { }

  ngOnInit() {
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }

}
