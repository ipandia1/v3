import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-privacy-notifications-marketing-communications',
  templateUrl: './privacy-notifications-marketing-communications.component.html',
  styleUrls: ['./privacy-notifications-marketing-communications.component.scss']
})
export class PrivacyNotificationsMarketingCommunicationsComponent implements OnInit {
  subTitle = 'Select line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  subTitle1 = 'Language preference';
  dropdownId1 = 'id2';
  selectedLanguage = 'English';
  selectLanguages: Array<any> = [
    { name: 'English', disabled: false },
    { name: 'Spanish', disabled: false },
    { name: 'German', disabled: false }
  ];
  toggleSwitch: boolean[] = [];
  marketingOptions: Array<any> = [
    { optionTitle: 'Text my phone', desc: '', desc2: '', url: '', icon: '', switch: true, checkOpt : false },
    { optionTitle: 'Send me email', desc: '', desc2: '', url: '', icon: '', switch: true, checkOpt : false },
    { optionTitle: 'Receive monthly T-Mobile newsletter', desc: '', desc2: '', url: '', icon: '', switch: true, checkOpt : false },
    { optionTitle: 'Mail to my billing address', desc: '', desc2: '', url: '', icon: '', switch: true, checkOpt : false }
  ];
  constructor() { }

  ngOnInit() {
    for (const i of this.marketingOptions) {
      if (i.optionTitle !== 'Receive Monthly T-Mobile Newsletter') {
        this.toggleSwitch.push(true);
      } else {
        this.toggleSwitch.push(false);
      }
    }
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }
  public handleLine1(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLanguage = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }

}
