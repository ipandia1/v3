import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-privacy-notifications-notifications',
  templateUrl: './privacy-notifications-notifications.component.html',
  styleUrls: ['./privacy-notifications-notifications.component.scss']
})
export class PrivacyNotificationsNotificationsComponent implements OnInit {

  subTitle = 'Select Line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  toggleSwitch: boolean[] = [];
  notificationOptions: Array<any> = [
    { optionTitle: 'T-Mobile will always send notifications when you are approaching the talk, text, or data limits on your own line. ',
    desc: '', desc2: '', url: '', icon: '', switch: true, checkOpt : false },
    { optionTitle: 'Receive alerts when other lines on this account are approaching their talk, text, or data limits.',
    desc: '', desc2: '', url: '', icon: '', switch: true, checkOpt : false }
  ];
  constructor() { }

  ngOnInit() {
    for (let i of this.notificationOptions) {
      if (i.optionTitle !== 'Receive alerts when other lines on this account are approaching their talk, text, or data limits.') {
        this.toggleSwitch.push(true);
      }  else {
        this.toggleSwitch.push(false);
      }
    }
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }

}
