import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tmobile-id',
  templateUrl: './tmobile-id.component.html',
  styleUrls: ['./tmobile-id.component.scss']
})
export class TmobileIdComponent implements OnInit {
  subTitle = 'Select line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  bladeInfo: Array<any> = [
    {title: 'Name', desc: 'John Legere', desc2: '', url: '/tMobileIDNickname'},
    {title: 'Email', desc: 'JayL@t-mobile.com', desc2: '', url: ''},
    {title: 'Password', desc: 'Last updated on 11/15/2015 08:23:55 PT', desc2: '', url: ''},
      {title: 'Security questions', desc: 'Last updated on 11/15/2015 08:23:55 PT', desc2: '', url: ''},
    {title: 'Phone numbers', desc: '(206)-555-1212', desc2: '', url: ''},
  ];
  constructor() { }

  ngOnInit() {
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue;
    }
  }
}
