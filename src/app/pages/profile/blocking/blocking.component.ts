import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blocking',
  templateUrl: './blocking.component.html',
  styleUrls: ['./blocking.component.scss']
})
export class BlockingComponent implements OnInit {
  subTitle = 'Select Line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';
  topDividerClass= '';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  bladeInfo: Array<any> = [
    {title: 'Block international roaming ',
    desc: '<a class="body-link cursor black" href="#">Show more</a>', desc2: '',
    url: '', icon: '', switch: true, checkOpt : false},
    {title: 'Block charge international roaming', desc: '<a class="body-link cursor black" href="#">Show more</a>', desc2: '',
    url: '', icon: '', switch: true, checkOpt: true}
  ];
  constructor() { }

  ngOnInit() {
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== 'this.isEmpty') {
      this.selectedLine = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }

}
