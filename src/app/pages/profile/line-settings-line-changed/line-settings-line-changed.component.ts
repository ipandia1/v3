import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-line-settings-line-changed',
  templateUrl: './line-settings-line-changed.component.html',
  styleUrls: ['./line-settings-line-changed.component.scss']
})
export class LineSettingsLineChangedComponent implements OnInit {
  subTitle = 'Select Line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  showPermissions = false;
  radioArray: string[] = ['Full access', 'Standard access', 'Restricted access', 'No access'];
  titleArray: Array<any> = [
    { mainTitle: 'What are permissions?',
    content: 'Permissions control activities such as viewing billing information, changing services, or making purchases.',
    title: 'Permission Levels'}
  ];
  accessArray: Array<any> = [
    { title: 'Primary Account Holder',
    content: 'You can manage the account and all lines on the account. You are able to use stored payment methods. You can also set permissions for other people on the account.'},
    { title: 'Full access',
    content: 'You can manage the account and all lines on the account. You are able to use stored payment methods'},
    { title: 'Standard access',
    content: 'You can see basic account information and details about your own line. You are not able to use stored payment methods. You cannot make account changes or changes to any line (including your own).'},
    {title: 'Restricted access',
    content: 'You can see basic account information and a usage summary for your own line. You are not able to use stored payment methods. You cannot make account changes or changes to any line (including your own).'},
    { title: 'No access',
    content: 'You don’t have access to My T-Mobile.'}
  ];
  constructor() { }

  ngOnInit() {
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue;
    }
  }
}
