import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-family-control-web-gaurd',
  templateUrl: './family-control-web-gaurd.component.html',
  styleUrls: ['./family-control-web-gaurd.component.scss']
})
export class FamilyControlWebGaurdComponent implements OnInit {
  public showDetails: Boolean = false;
  subTitle = 'Select Line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  radioArray: string[] = [ 'Child' , 'Teen', 'Young adult', 'No restrictions' ];
  familyArray: Array<any> = [
    { mainTitle: 'What are permissions?',
    content: 'Permissions control activities such as viewing billing information, changing services, or making purchases.',
    title: 'Permission Levels'}
  ];
  levelArray: Array<any> = [
    { title: 'Primary Account Holder',
    content: 'You can manage the account and all lines on the account. You are able to use stored payment methods. You can also set permissions for other people on the account.'},
    { title: 'Full Access',
    content: 'You can manage the account and all lines on the account. You are able to use stored payment methods.'},
    { title: 'Standard Access',
    content: 'You can see basic account information and details about your own line. You are not able to use stored payment methods. You cannot make account changes or changes to any line (including your own).'},
    {title: 'Restricted Access',
    content: 'You can see basic account information and a usage summary for your own line. You are not able to use stored payment methods. You cannot make account changes or changes to any line (including your own).'},
    { title: 'No Access',
    content: 'You don\'t have access to My T-Mobile.'}
  ];
  constructor() { }

  ngOnInit() {
  }

  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }
}
