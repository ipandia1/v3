import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-billing-and-payments-paperless-billing',
  templateUrl: './billing-and-payments-paperless-billing.component.html',
  styleUrls: ['./billing-and-payments-paperless-billing.component.scss']
})
export class BillingAndPaymentsPaperlessBillingComponent implements OnInit {
  services: Array<any> = [
    { 'title' : 'Select the type of bill you would like to receive:',
        id: 1,
      'data' : [
      {id: 1,  'desc': 'Paperless bill', 'desc2': 'Save trees and get a friendly reminder when your bill is ready.', 'address': ''},
      {id: 2,  'desc': 'Paper bill', 'desc2': '', 'addressdet':'Use this billing address:', 'address': '1234 Main St','address2': 'Suite 001', 'address3': 'Bellevue, WA 98111'}
    ]},
    { 'title' : 'Select the level of detail you would like on your bill:',
      id: 2,
      'data' : [
      {id: 3, 'desc': 'Summary bill', 'desc2': '', 'price': 'Free', 'address': ''},
     { id: 4, 'desc': 'Detailed bill', 'desc2': 'Includes a log of all your calls, text and data', 'price' : '$1.99/month', 'address': ''}
    ]}
];
  constructor() { }

  ngOnInit() {
  }

}
