import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-home',
  templateUrl: './profile-home.component.html',
  styleUrls: ['./profile-home.component.scss']
})
export class ProfileHomeComponent implements OnInit {
  subTitle = 'Select Line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  bladeInfo: Array<any> = [
    {title: 'Billing and payments', desc: 'Manage billing address, AutoPay, and paperless billing.', desc2: '', url: '/billingAndPayments'},
    {title: 'T-Mobile ID', desc: 'Edit your name, email, password, security questions, and linked phone numbers.',
    desc2: '', url: '/tMobileID'},
    {title: 'Media settings', desc: 'Manage Binge On<sup>TM</sup>', desc2: '', url: '/mediaSettings'},
    {title: 'Line settings',
      desc: 'Manage KickBack<sup>TM</sup> and edit your nickname, e911 address, usage address, and site permissions.',
      desc2: '', url: '/lineSettings'},
    {title: 'Blocking', desc: 'Prevent device from receiving specific communications and downloads.',  desc2: '', url: '/blocking'},
    {title: 'Privacy and notifications',
      desc: 'Manage service alerts, marketing and third-party communications, and apps that use T-Mobile ID.', url: '/privacyNotification'},
    {title: 'Family controls', desc: 'Restrict access to adult content.', desc2: '', url: '/familyControl'},
    {title: 'Multiple devices',
      desc: 'Allows you to extend your number\'s voice calling and messaging to multiple devices.', desc2: '', url: '/multipleDevices'}
  ];
  constructor() { }

  ngOnInit() {
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== 'this.isEmpty') {
      this.selectedLine = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }
}
