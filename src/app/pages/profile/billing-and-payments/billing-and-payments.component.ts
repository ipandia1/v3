import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-billing-and-payments',
  templateUrl: './billing-and-payments.component.html',
  styleUrls: ['./billing-and-payments.component.scss']
})
export class BillingAndPaymentsComponent implements OnInit {
  bladeInfo: Array<any> = [
    {title: 'Billing address', desc: '1 Ravinia Dr Suite 1000 Atlanta, GA 30346', desc2: '', url: ''},
    {title: 'Paperless billing', desc: 'Sign up for paperless billing.', desc2: '', url: '/billingAndPaymentsPaperlessBilling'},
    {title: 'AutoPay OFF', desc: 'Save up to $00.00 each month by signing up for AutoPay', desc2: '', url: ''}
  ];
  constructor() { }

  ngOnInit() {
  }

}
