import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-billing-and-payments-on',
  templateUrl: './billing-and-payments-on.component.html',
  styleUrls: ['./billing-and-payments-on.component.scss']
})
export class BillingAndPaymentsOnComponent implements OnInit {
   bladeInfo: Array<any> = [
    {title: 'Billing address', desc: '1 Ravinia Dr Suite 1000 Atlanta, GA 30346', desc2: '', url: ''},
    {title: 'Paperless billing', desc: 'You\'re signed up for paperless billing.', desc2: '', url: '/billingAndPaymentsPaperlessBilling'},
    {title: 'AutoPay ON', desc: 'You\'re saving up to $10.00 per line each month with AutoPay! ', desc2: '', url: ''}
  ];
  constructor() { }

  ngOnInit() {
  }

}
