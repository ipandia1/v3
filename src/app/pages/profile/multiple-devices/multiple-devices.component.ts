import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-multiple-devices',
  templateUrl: './multiple-devices.component.html',
  styleUrls: ['./multiple-devices.component.scss']
})
export class MultipleDevicesComponent implements OnInit {

   subTitle = 'Select line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';
  toggleData = false;
  moreLessDiv = true;
  showMore = true;
  titleDesc2Class = 'body d-none';
  topDividerClass = '';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  toggleSwitch: boolean[] = [];
  notificationOptions: Array<any> = [
    { optionTitle: 'DIGITS<sup>®</sup>', desc: 'Allows you to use DIGITS® voice and messaging services on multiple devices.',
    desc2: '<a class="brand-magenta" href="#">Manage settings</a>', url: '', icon: '', switch: false, checkOpt : false },
    { optionTitle: 'Devices status', desc: '0 devices are using your DIGITS® number in addition to your primary phone.',
    desc2:'It may take up to 2 hours for changes to take effect.It is recommended to restart any device using DIGITS® after changing this setting.',
    url: '', icon: '', switch: true, checkOpt : false }
  ];
  constructor() { }

  ngOnInit() {
    for (let i of this.notificationOptions) {
      if (i.optionTitle !== 'Receive alerts when other lines on this account are approaching their talk, text, or data limits.') {
        this.toggleSwitch.push(true);
      } else {
        this.toggleSwitch.push(false);
      }
    }
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }


}
