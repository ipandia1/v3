import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-family-control-family-allowances',
  templateUrl: './family-control-family-allowances.component.html',
  styleUrls: ['./family-control-family-allowances.component.scss']
})
export class FamilyControlFamilyAllowancesComponent implements OnInit {
  subTitle = 'Select Line';
  dropdownId = 'id1';
  selectedLine = 'Marky Mark, (206)-861-9327';
  selectLine: Array<any> = [
    { name: 'Marky Mark, (206)-861-9327', disabled: false },
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  constructor() { }

  ngOnInit() {
  }

  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }

}
