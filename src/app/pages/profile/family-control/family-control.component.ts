import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-family-control',
  templateUrl: './family-control.component.html',
  styleUrls: ['./family-control.component.scss']
})
export class FamilyControlComponent implements OnInit {

  subTitle = 'Select line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  bladeInfo: Array<any> = [
    {title: 'Web Guard', desc: 'Prevent access to adult web content.<br>No restrictions.', desc2: '', url: '/familyControlWebGaurd'},
    {title: 'Family Allowances', desc: 'Set allowances on minutes, messages and downloads', desc2: '',
    url: '/familyControlFamilyAllowances'},
    {title: 'FamilyWhere', desc: 'Locate your family\'s devices on a map.', desc2: '', url: ''}
  ];
  constructor() { }
  ngOnInit() {
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }

}
