import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-privacy-notifications-advertising',
  templateUrl: './privacy-notifications-advertising.component.html',
  styleUrls: ['./privacy-notifications-advertising.component.scss']
})
export class PrivacyNotificationsAdvertisingComponent implements OnInit {

  subTitle = 'Select line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';
  titleDescClass= 'body d-none';
  moreLessDiv = true;
  showMore = true;
  ShowTextClass= 'body-link pt-md-3';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  toggleSwitch: boolean[] = [];
  toggleData: boolean[] = [];
  advertisingOptions: Array<any> = [
    { optionTitle: 'Insights',
     desc: 'Your device can receive ads that are tailored to your interests from T-Mobile and our ad providers.These interest-based ads are selected based on your use of T-Mobile\'s services and other information accessed by our ad providers. If you turn off interest-based ads on this device, you will see just as many ads, but the ads will not be based on your interests and may be less relevant to you.',
     desc2: '', url: '', icon: '', switch: true, checkOpt : false},
    { optionTitle: 'Interest-based ads',
     desc:'Your device can receive ads that are tailored to your interests from T-Mobile and our ad providers.These interest-based ads are selected based on your use of T-Mobile\'s services and other information accessed by our ad providers. If you turn off interest-based ads on this device, you will see just as many ads, but the ads will not be based on your interests and may be less relevant to you.',
     desc2: '', url: '', icon: '', switch: true, checkOpt : false},
    { optionTitle: 'Web browsing and app information',
    desc2: '', desc:'Your device can receive ads that are tailored to your interests from T-Mobile and our ad providers.These interest-based ads are selected based on your use of T-Mobile\'s services and other information accessed by our ad providers. If you turn off interest-based ads on this device, you will see just as many ads, but the ads will not be based on your interests and may be less relevant to you.',
     url: '', icon: '', switch: true, checkOpt : false},
    { optionTitle: 'Device location',
     desc:'Your device can receive ads that are tailored to your interests from T-Mobile and our ad providers.These interest-based ads are selected based on your use of T-Mobile\'s services and other information accessed by our ad providers. If you turn off interest-based ads on this device, you will see just as many ads, but the ads will not be based on your interests and may be less relevant to you.',
     desc2: '', url: '', icon: '', switch: true, checkOpt : false}
  ];
  constructor() { }

  ngOnInit() {
    for (let i of this.advertisingOptions) {
        this.toggleSwitch.push(false);
    }
    for (let i of this.advertisingOptions) {
        this.toggleData.push(false);
    }
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }

}
