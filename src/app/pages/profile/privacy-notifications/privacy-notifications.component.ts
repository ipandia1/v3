import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-privacy-notifications',
  templateUrl: './privacy-notifications.component.html',
  styleUrls: ['./privacy-notifications.component.scss']
})
export class PrivacyNotificationsComponent implements OnInit {

  subTitle = 'Select Line';
  dropdownId = 'id1';
  selectedLine = 'John Legere, (206)-555-1212';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  bladeInfo: Array<any> = [
    {title: 'Marketing communications', desc: 'Control how T-Mobile sends you information about products and services.', desc2: '',
    url: '/privacyNotificationMarketingCommunications'},
    {title: 'Notifications', desc: 'Manage service alerts and policy updates.', desc2: '', url: '/privacyNotificationNotifications'},
    {title: 'Advertising and insights', desc: 'Programs that use non-personally identifiable information related to data and device usage.',
    desc2: '', url: '/privacyNotificationAdvertising'}
  ];
  constructor() { }

  ngOnInit() {
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }

}
