import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-coverage-device-device-address',
  templateUrl: './coverage-device-device-address.component.html',
  styleUrls: ['./coverage-device-device-address.component.scss']
})
export class CoverageDeviceDeviceAddressComponent implements OnInit {
   newBillingAddress: BillingAddress;
   stateErr = false;
  selectedVal1 = 'State';
  categoriesValues: Array<any> = [
    { state: 'AL', disabled: true }, { state: 'AK', disabled: false }, { state: 'AZ', disabled: false }, { state: 'AR', disabled: false },
    { state: 'CA', disabled: false }, { state: 'CO', disabled: false }, { state: 'CT', disabled: false }, { state: 'DE', disabled: false },
    { state: 'EL', disabled: false }, { state: 'GA', disabled: false }, { state: 'HI', disabled: false }, { state: 'ID', disabled: false },
    { state: 'VI', disabled: false }, { state: 'WA', disabled: false }, { state: 'WV', disabled: false }, { state: 'WI', disabled: true }];

  constructor() { }

  ngOnInit() {
    this.newBillingAddress = {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: ''
    }
  }select1(value: string): void {
   this.selectedVal1 = value;
  }
  stateValidation() {
    if (this.selectedVal1 === 'State') {
      this.stateErr = true;
     } else {
      this.stateErr = false;
     }
  }
}
export class BillingAddress {
  address1: string;
  address2: string;
  city: string;
  state: string;
  zipcode: string;
}
