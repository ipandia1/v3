import { Component, Input, Output } from '@angular/core';

@Component({
  selector: 'app-coverage-device-verify-address-modal',
  templateUrl: './coverage-device-verify-address-modal.component.html',
  styleUrls: ['./coverage-device-verify-address-modal.component.scss']
})
export class CoverageDeviceVerifyAddressModalComponent {
visible: boolean;
constructor() { }
  close() {
    this.visible = false;
  }
}
