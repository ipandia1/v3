import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-coverage-device',
  templateUrl: './coverage-device.component.html',
  styleUrls: ['./coverage-device.component.scss']
})
export class CoverageDeviceComponent implements OnInit {

  bladeInfo: Array<any> = [
    {
      title: 'Device address', desc: '2 RAVINIA DR', desc2: 'DUNWOODY, GA 30346',
      url: '/coverageDeviceDeviceAddress'
    },
    { title: 'Assigned mobile number', desc: 'Marky Mark, (206) 861-9327', desc2: '', url: '/coverageDeviceAssignedNumber' }
  ];
  constructor() { }

  ngOnInit() {
  }


}
