import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-systemerror',
  templateUrl: './systemerror.component.html',
  styleUrls: ['./systemerror.component.scss']
})

export class SystemErrorComponent implements OnInit {
    public smallDevice:boolean=false;
    public Errorname: string = "Oops! Something went wrong.";
    public Errormessage: string = "We ran into a technical problem while processing your payment. Please try again, or message us for help with this transaction."
    public showDetails: boolean = false;
  constructor() { }
  ngOnInit() { 
  var deviceHeight=window.innerHeight;
  if(deviceHeight<565){
    this.smallDevice=true;
    }
    else{
    this.smallDevice=false;
    }
  }


  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  }

}
