import { Component } from '@angular/core';

@Component({
  selector: 'app-errorstate-modal',
  templateUrl: './errorstate-modal.component.html',
  styleUrls: ['./errorstate-modal.component.scss']
})
export class ErrorStateModalComponent {
visible: boolean;
constructor() { }
  close() {
    this.visible = false;
  }
}
