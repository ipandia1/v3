import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-paymentdeclined',
  templateUrl: './paymentdeclined.component.html',
  styleUrls: ['./paymentdeclined.component.scss']
})

export class PaymentDeclinedComponent implements OnInit {
    public smallDevice:boolean=false;
    public Errorname: string = "Payment declined.";
    public Errormessage: string = "Please try another payment method, or contact your financial institution to resolve the issue."
    public Troublemessage: string = "Want help?";
    public showDetails: boolean = false;
  constructor() { }
  ngOnInit() { 
  var deviceHeight=window.innerHeight;
  if(deviceHeight<565){
    this.smallDevice=true;
    }
    else{
    this.smallDevice=false;
    }
  }


  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  }

}
