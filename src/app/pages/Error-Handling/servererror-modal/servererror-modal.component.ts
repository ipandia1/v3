import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-servererror-modal',
  templateUrl: './servererror-modal.component.html',
  styleUrls: ['./servererror-modal.component.scss']
})


export class ServerErrorModalComponent {
  visible: boolean;
  constructor() { }
    close() {
      this.visible = false;
    }
  }