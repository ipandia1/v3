import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { ChangeDetectorRef } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-in-session',
  templateUrl: './in-session.component.html',
  styleUrls: ['./in-session.component.scss']
})
export class InSessionComponent implements OnInit {
  currentOpenModalId: any;
  deleteModalVisible: boolean;
  inSessionPageData: any;
  inSessionData: any;
  inSessionDataPath = 'assets/json/inSession/in-session.json';
  inSessionPageIndex = 3; // to change the screen give 1 to 6 numbers
  visible: boolean;
  selectBtnDisabled;
  radioBtnChecked = false;
  parentSubject: Subject<any> = new Subject();

  constructor(private commonService: CommonService, private cdRef: ChangeDetectorRef) {
    this.commonService.getJsonData(this.inSessionDataPath)
      .subscribe(responseJson => {
        this.inSessionData = responseJson;
        this.inSessionPageData = this.inSessionData['in-session-' + this.inSessionPageIndex][0];
      });
  }

  ngOnInit() {
  }
  openGoToWallet() {
    this.visible = true;
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('noScroll');
    this.walletModalFocus();
  }
  walletModalFocus() {
    const dialog = document.getElementById('goToWalletModal');
    dialog.focus();
  }
  deleteModalOpen(ev) {
    this.currentOpenModalId = ev;
    this.deleteModalVisible = true;
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('noScroll');
    this.deleteModalFocus();
  }
  deleteModalFocus() {
    const dialog = document.getElementById('deleteModal');
    dialog.focus();
  }
  close() {
    this.visible = false;
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('noScroll');
    document.getElementById('moreInfoLink').focus();
  }
  closeChildModal() {
    this.deleteModalVisible = false;
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('noScroll');
    this.parentSubject.next(this.currentOpenModalId);
  }
  selectButtonDisabled(value) {
    this.selectBtnDisabled = value;
  }
  radioButtonChecked(value) {
    this.radioBtnChecked = value;
    this.cdRef.detectChanges();
  }
  backTabEvent(event, mainId,lastId) {
    const allElements = document.getElementById(mainId).querySelectorAll
    ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
      if (document.activeElement === allElements[0]) {
        if (event.keyCode === 9) {
          if (event.shiftKey) {
            event.preventDefault();
            setTimeout(function() {
              document.getElementById(lastId).focus();
            }, 0);
          }
        }
      }
      if (document.activeElement === document.getElementById(mainId)) {
        if (event.keyCode === 9) {
          if (event.shiftKey) {
            event.preventDefault();
            setTimeout(function() {
              document.getElementById(lastId).focus();
            }, 0);
          }
        }
      }
      if (document.activeElement === allElements[allElements.length - 1]) {
        if (event.keyCode === 9) {
          if (event.shiftKey) {}
          else {
          event.preventDefault();
          setTimeout(function() {
            document.getElementById(mainId).focus();
          }, 0);
        }
  
      }
      }
  }
}
