import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-problem-identifier',
  templateUrl: './problem-identifier.component.html',
  styleUrls: ['./problem-identifier.component.scss']
})

export class ProblemIdentifierComponent implements OnInit {
  errorMessageValid: string;
  errorMessage: string;
  minlength: string;
  maxl: string;
  inputType = {
    accountNumber: '',
    zipcode: '',
    pincode: '',
    showPincode : true,
    showZipcode : true,
    showAccountno : true
  };
  savePayment = '';
   constructor() { }
    ngOnInit() {
    }
}
