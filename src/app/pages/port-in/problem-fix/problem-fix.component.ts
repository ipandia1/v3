import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-problem-fix',
  templateUrl: './problem-fix.component.html',
  styleUrls: ['./problem-fix.component.scss']
})

export class ProblemFixComponent implements OnInit {
  details = {
    title: 'Thanks for the help!'
  };
  showDetails = true;
  constructor() { }
  ngOnInit() {
  }
  clickFun = function () {
    this.showDetails = !this.showDetails;
  };
}
