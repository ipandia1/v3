import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-success-status',
  templateUrl: './success-status.component.html',
  styleUrls: ['./success-status.component.scss']
})

export class SuccessStatusComponent implements OnInit {
  detailsArray = [{
    'title': 'Success headline',
    'content': 'Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. Copy goes here. '
  }];
  successFromResubmit = true;
  constructor() { }
  ngOnInit() {
  }

}
