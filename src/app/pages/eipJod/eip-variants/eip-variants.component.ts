import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-eip-variants',
  templateUrl: './eip-variants.component.html',
  styleUrls: ['./eip-variants.component.scss']
})
export class EipVariantsComponent implements OnInit {
  JOD_Array = [
    {
      'heading': 'Completed EIP',
      'details': [
        {
          'name': 'Napoleon', 'phone_number': '(555) 555- 1111', 'type': 'LG G7 ThinQ™',
          'image': './assets/img/phone-image@2x.png', 'variant': 'EIP',
          'status': 'Completed', 'payments_remaining': '0', 'original_price': '$750.00',
          'down_payment': '$30.00', 'plan_start_date': '08/08/2017', 'plan_status': 'Complete - Closed',
          'EIP_plan_ID': '12345678912345', 'IMEI': '123456789123456'
        },
        {
          'name': 'Napoleon', 'phone_number': '(555) 555- 1111', 'type': 'LG G7 ThinQ™',
          'image': './assets/img/phone-image@2x.png', 'variant': 'EIP',
          'status': 'Completed', 'payments_remaining': '0', 'original_price': '$750.00',
          'down_payment': '$30.00', 'plan_start_date': '08/08/2017', 'plan_status': 'Complete - Closed',
          'EIP_plan_ID': '12345678912345', 'IMEI': '123456789123456'
        }
      ]
    },
    {
      'heading': 'Active EIP',
      'details': [
        {
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'LG G7 ThinQ™',
          'payment': '$345.98',
          'duration': '$30/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
          'purchase_status': false,
          // tslint:disable-next-line:max-line-length
          'description': 'Use the estimator to see how making extra payments will affect your plan.',
          'monthly_payment': '$30.00', 'total_lease_amount': '', 'EIP_status': true,
          'purchase_price': '', 'lease_start_date': '', 'lease_status': '',
          'payments_remaining': '4', 'plan_id': '', 'IMEI': '123456789123456',
          'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment': '$30.00', 'final_payment': '$28.99',
          'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped', 'EIP_plan_ID': '12345678912345', 'variant': 'ActiveEIP'
        },
        {
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Samsung Galaxy Note8',
          'payment': '$143.54', 'purchase_status': false,
          'duration': '$30/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
          // tslint:disable-next-line:max-line-length
          'description': 'Use the estimator to see how making extra payments will affect your plan.',
          'monthly_payment': '$30.00', 'total_lease_amount': '', 'EIP_status': true,
          'purchase_price': '', 'lease_start_date': '', 'lease_status': '',
          'payments_remaining': '4', 'plan_id': '', 'IMEI': '123456789123456',
          'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment': '$30.00', 'final_payment': '$28.99',
          'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped', 'EIP_plan_ID': '12345678912345', 'variant': 'ActiveEIP'
        }
      ]
    },
    {
      'heading': 'Active EIP + promotion info ',
      'details': [
        {
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Samsung Galaxy Note8',
          'payment': '$345.98',
          'duration': '$30/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
          'purchase_status': false,
          // tslint:disable-next-line:max-line-length
          'description': 'Use the estimator to see how making extra payments will affect your plan.',
          'monthly_payment': '$30.00', 'total_lease_amount': '', 'EIP_status': true,
          'purchase_price': '', 'lease_start_date': '', 'lease_status': '',
          'payments_remaining': '4', 'plan_id': '', 'IMEI': '123456789123456',
          'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment': '$30.00', 'final_payment': '$28.99',
          'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped',
          'EIP_plan_ID': '12345678912345', 'promotion': 'TM87 - 2017 2018 Samsung GS9 Launch',
          'monthlyCredits': '24', 'amt_monthly_credits': '$8.34', 'variant': 'EIPpromotion'
        }
      ]
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
