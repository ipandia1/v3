import {Component, Input, Output, OnInit, HostBinding, HostListener, AfterViewInit} from '@angular/core';

@Component({
  selector: 'app-extra_device_payment_variants',
  templateUrl: './extra_device_payment_variants.component.html',
  styleUrls: ['./extra_device_payment_variants.component.scss']
})
export class ExtraDevicePaymentVariantsComponent implements OnInit, AfterViewInit {
  totalDocSignHeight:'0px';
  ngOnInit() {
  }

  ngAfterViewInit() {
    this.bgHeight();
 }

 @HostListener('window:load', [])
 onWindowLoad() {
   this.bgHeight();

 }
 @HostListener('window:resize', [])
 onResize() {
   this.bgHeight();
  }
  bgHeight = function() {
    var windowHeight = window.innerHeight-1 ;
    return this.totalBgHeight = windowHeight + 'px';
   }

  name: string="Napoleon"; 
  public paymentValues: Array<Object> = [
    { descTitle: '(425) 456-2344', value: 'Samsung Galaxy Note8' },
    { descTitle: 'EIP (Installment Plan) ID:', value: '12345678912345' },
    { descTitle: 'Device balance:', value: '111.11'}
  ];
  JOD_Array = [
      {'heading': 'Customers have ONE installment plan'},
      {'heading': 'Customers have 1+ installment plans'}   
      ];
}
