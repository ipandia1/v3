import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jod-lease-variants',
  templateUrl: './jod-lease-variants.component.html',
  styleUrls: ['./jod-lease-variants.component.scss']
})
export class JodLeaseVariantsComponent implements OnInit {
  /** m_lease **/
  /** JOD_Array = [
     {
       'heading': 'Completed leases or plans',
       'details': [
         {
           'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone 6s',
           'image': './assets/img/phone-image@2x.png', 'variant': 'EIP',
           'status': 'Completed', 'payments_remaining': '0', 'original_price': '$750.00',
           'down_payment': '$30.00', 'plan_start_date': '08/08/2017', 'plan_status': 'Complete - Closed',
           'EIP_plan_ID': '12345678912345', 'IMEI': '123456789123456'
         }
       ]
     },
     {
       'heading': 'Active JOD lease',
       'details': [
         {
           'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone 6S',
           'payment': '8 payments',
           'duration': '$24/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
           'purchase_status': false,
           // tslint:disable-next-line:max-line-length
           'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': false,
           'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Completed',
           'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
           'variant': 'ActiveEIP'
         },
         {
           'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone 6S',
           'payment': '8 payments',
           'duration': '$24/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
           'purchase_status': false,
           // tslint:disable-next-line:max-line-length
           'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': false,
           'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Completed',
           'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
           'variant': 'ActiveEIP'
         }
       ]
     },
     {
       'heading': 'JOD  turns POIP',
       'details': [
         {
           'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone 6S',
           'payment': '8 payments',
           'duration': '$24/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
           'purchase_status': false,
           // tslint:disable-next-line:max-line-length
           'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': false,
           'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Completed',
           'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
           'variant': 'ActiveEIP'
         }
       ]
     }
   ]; **/


  /** JOD lease cards lease ending soon **/

  /**  JOD_Array = [
     {
       'heading': 'JOD lease ending soon',
       'badge_status': true,
       'details': [
         {
         'lease_badge_status': true,
         'badge_text': 'Ending soon',
           'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone X - Silver',
           'payment': '8 payments',
           'duration': '$24/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Take action',
           'purchase_status': false,
           // tslint:disable-next-line:max-line-length
           'description': 'If you don’t take action by 08/08/18, the purchase price of your device ($124.00 + taxes) will be added to your bill.',
           'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': false,
           'purchase_price': '$124.00', 'lease_start_date': '08/08/18', 'lease_status': 'Active',
           'payments_remaining': '2', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
           'variant': 'leaseEndingSoon'
         }
       ]
     },
     {
       'heading': 'JOD lease ending - customer not PAH',
       'badge_status': true,
       'details': [
         {
         'lease_badge_status': true,
         'badge_text': 'Ending soon',
           'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone 6s',
             'image': './assets/img/ssu/samsung.png', 'btnText': 'Take action',
           'purchase_status': false,
           'msg':'Please contact the primary account holder to take action',
           // tslint:disable-next-line:max-line-length
           'description': 'If you don’t take action by 08/08/18, the purchase price of your device ($124.00 + taxes) will be added to your bill.',
           'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': false,
           'purchase_price': '$124.00', 'lease_start_date': '08/08/18', 'lease_status': 'Active',
           'payments_remaining': '2', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
           'variant': 'leaseEndingPAH', 'leasePAHDisable': true
         }
         ]
     }
    ]; **/

  /** JOD lease Ended - Act now **/

  JOD_Array = [
    {
      'heading': 'JOD lease ended',
      'badge_status': true,
      'details': [
        {
          'lease_badge_status': true,
          'badge_text': 'Ended - Act now',
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone 6s',
          'payment': '8 payments',
          'duration': '$24/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Take action',
          'purchase_status': false,
          // tslint:disable-next-line:max-line-length
          'msg': 'The remaining cost of your device is due now.',
          'description': 'It’s not too late to sign up and continue making payments. You’ve been billed for the full purchase option price. Sign up before your bill is due.',
          'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': false,
          'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Active - Shipped',
          'payments_remaining': '2', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
          'variant': 'leaseEnded'
        }
      ]
    },
    {
      'heading': 'JOD lease ended - Past due',
      'badge_status': true,
      'details': [
        {
          'lease_badge_status': true,
          'badge_text': 'Ended - Act now',
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone 6s',
          'image': './assets/img/ssu/samsung.png', 'btnText': 'Pay your bill',
          'btnText2': 'See more options',
          'purchase_status': false,
          'msg': 'Your bill is past due, please pay your bill to continue.',
          // tslint:disable-next-line:max-line-length
          'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': false,
          'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Active - Shipped',
          'payments_remaining': '0', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
          'variant': 'leaseDue'
        }
      ]
    },
    {
      'heading': 'JOD lease ended - Past due - customer not PAH',
      'badge_status': true,
      'details': [
        {
          'lease_badge_status': true,
          'badge_text': 'Ended - Act now',
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone 6s',
          'image': './assets/img/ssu/samsung.png', 'btnText': 'Pay your bill',
          'btnText2': 'See more options',
          'purchase_status': false,
          'msg': 'Your bill is past due and you are not the primary account holder. Please contact the account holder to take action.',
          // tslint:disable-next-line:max-line-length
          'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': false,
          'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Active - Shipped',
          'payments_remaining': '0', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
          'variant': 'leaseDuePAH', 'leaseDuePAHDisable': true
        }
      ]
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
