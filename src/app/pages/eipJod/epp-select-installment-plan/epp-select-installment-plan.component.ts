import {Component, Input, Output} from '@angular/core';

@Component({
  selector: 'app-epp-select-installment-plan',
  templateUrl: './epp-select-installment-plan.component.html',
  styleUrls: ['./epp-select-installment-plan.component.scss']
})
export class EppSelectInstallmentPlanComponent  {
  visible: boolean;
  constructor() { } 
  close() {
    this.visible = false;
  }
  
  public paymentValues: Array<Object> = [
    { name:'Napoleon', descTitle: '(425) 456-2344', value: 'Samsung Galaxy Note8' ,
     eipID: 'EIP (Installment Plan) ID:', eipvalue: '12345678912345' ,
     deviceBalance: 'Device balance:', deviceBalancevalue: '111.11'},
     { name:'Jane', descTitle: '(425) 456-2344', value: 'Apple iPhone 8' ,
     eipID: 'EIP (Installment Plan) ID:', eipvalue: '12345678912345' ,
     deviceBalance: 'Device balance:', deviceBalancevalue: '111.11'},
     { name:'Long nick name', descTitle: '(425) 456-2344', value: 'LG G7 ThinQ™' ,
     eipID: 'EIP (Installment Plan) ID:', eipvalue: '12345678912345' ,
     deviceBalance: 'Device balance:', deviceBalancevalue: '111.11'}
  ];

}
