import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-jod-ending-soon',
  templateUrl: './jod-ending-soon.component.html',
  styleUrls: ['./jod-ending-soon.component.scss']
})
export class JodEndingSoonComponent implements OnInit {
  paymentTitle = 'Monthly payments';
  paymentAmt = '$90.00';
  balanceArray = [{ 'title': 'Lease balance', 'amount': '$192.00', 'lease': '1 active lease', 'perMonth': '$24/month' },
  { 'title': 'Plan balance', 'amount': '$469.74', 'lease': '2 active installment plans', 'perMonth': '$30/month' }];
  // Uncomment below balanceArray & comment above array to view 'No Active Plans & Leases' page
  //balanceArray = [];
  statusArray = [{ 'tabTitle': 'Active', 'statusTabCheck': true, 'content': 'Active content here' },
  { 'tabTitle': 'Completed', 'statusTabCheck': false, 'content': 'Completed content here' }];
  docTitle = 'Documents & receipts';
  docDetails = 'To view billing, payment history, and electronic agreements related to your account, go to ';
  docLink = 'account activities';
  /**  joid array **/
  // No Active Plan and lease
  // JOD_Array = [];
  /** jod_endingsoon  **/
  JOD_Array = [
    {
      'heading': 'JUMP! on Demand Leases',
      'badge_status': true,
      'badge_text': 'Ending soon',
      'details': [
        {
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone 6S', 'payment': '2 payments',
          'duration': '$24/month', 'image': './assets/img/phone-image.png', 'btnText': 'Take action', 'purchase_status': false,
          // tslint:disable-next-line:max-line-length
          'description': 'If you don’t take action by 08/08/18, the purchase price of your device ($124.00 + taxes) will be added to your bill.',
          'completedPercentage': '70', 'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': false, 'variant': 'onDemand',
          'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Active - Shipped',
          'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
          'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment': '$30.00', 'final_payment': '$28.99',
          'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped', 'EIP_plan_ID': '12345678912345'
        }
       
      ]
    },
    {
      'heading': ' Equipment Installment Plans',
      'badge_status': false,
      'badge_text': 'Ending soon',
      'details': [
        {
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Samsung Galaxy Note8', 'payment': '$345.98',
          'duration': '$30/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
          'purchase_status': false,
          // tslint:disable-next-line:max-line-length
          'description': 'Use the estimator to see how making extra payments will affect your plan.',
          'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': true, 'variant': '',
          'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Active - Shipped',
          'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
          'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment': '$30.00', 'final_payment': '$28.99',
          'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped', 'EIP_plan_ID': '12345678912345'
        },
        {
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Samsung Galazy S9', 'payment': '$257.37',
          'purchase_status': false, 'variant': '',
          'duration': '$30/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
          // tslint:disable-next-line:max-line-length
          'description': 'Use the estimator to see how making extra payments will affect your plan.',
          'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': true,
          'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Active - Shipped',
          'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
          'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment': '$30.00', 'final_payment': '$28.99',
          'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped', 'EIP_plan_ID': '12345678912345'
        }
      ]
    }
  ];

  /** Jod_turns_poid array **/

  /*JOD_Array = [
    {
      'heading': 'JUMP! on Demand Leases',
      'badge_status': false,
      'badge_text': 'Ending soon',
      'details': [
        {
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone 6S',
          'payment': '', 'purchase_status': true, 'btnText': 'Download your agreement', 'jod_status': 'JOD',
          'duration': '', 'image': './assets/img/phone-image.png',
          'description': 'This device has been purchased with an equipment installment plan',
          'completedPercentage': '', 'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': false,
          'purchase_price': '$124.00', 'lease_start_date': '08/08/18', 'lease_status': 'Active',
          'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
          'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment': '$30.00', 'final_payment': '$28.99',
          'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped', 'EIP_plan_ID': '12345678912345'
        }
      ]
    },
    {
      'heading': ' Equipment Installment Plans',
      'badge_status': false,
      'badge_text': 'Ending soon',
      'details': [{
        'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone X - Silver',
        'payment': '$113.34', 'purchase_status': false,
        'duration': '$20.23/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
        // tslint:disable-next-line:max-line-length
        'description': 'Use the estimator to see how making extra payments will affect your plan.',
        'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': true, 'jod_status': 'EIP',
        'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Active - Shipped',
        'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
        'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment': '$30.00', 'final_payment': '$28.99',
        'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped', 'EIP_plan_ID': '12345678912345'
      },
      {
        'name': 'Maggie', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone X - Silver',
        'payment': '$345.98', 'purchase_status': false,
        'duration': '$30/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
        // tslint:disable-next-line:max-line-length
        'description': 'Use the estimator to see how making extra payments will affect your plan.',
        'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': true, 'jod_status': 'EIP',
        'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Active - Shipped',
        'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
        'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment': '$30.00', 'final_payment': '$28.99',
        'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped', 'EIP_plan_ID': '12345678912345'
      },
      {
        'name': 'Zack', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone X - Silver',
        'payment': '$123.76', 'purchase_status': false,
        'duration': '$30/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
        // tslint:disable-next-line:max-line-length
        'description': 'Use the estimator to see how making extra payments will affect your plan.',
        'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': true, 'jod_status': 'EIP',
        'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Active - Shipped',
        'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
        'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment': '$30.00', 'final_payment': '$28.99',
        'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped', 'EIP_plan_ID': '12345678912345'
      }
      ]
    }
  ];*/


  /**  Active_closed **/
  /**  JOD_Array = [
    {
      'heading': 'JUMP! on Demand Leases',
        'badge_status': false,
        'badge_text': 'Ending soon',
      // EIP_status is true then payment estimator will enable
      'details': [
        {
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone 6S', 'payment': '2 payments',
          'duration': '$24/month', 'image': './assets/img/phone-image.png', 'btnText': 'Make a payment', 'purchase_status': false,
          // tslint:disable-next-line:max-line-length
          'description': 'Use the estimator to see how making extra payments will affect your plan.',
          'completedPercentage': '70', 'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': false,
          'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Completed',
          'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
          'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment':  '$30.00', 'final_payment': '$28.99',
          'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped',  'EIP_plan_ID': '12345678912345', 'variant': 'JOD'
        }
      ]
    },
    {
      'heading': ' Equipment Installment Plans',
      'badge_status': false,
      'badge_text': 'Ending soon',
      'details': [
        {
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Samsung Galaxy Note8', 'payment': '5 payments',
          'duration': '$30/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
          'purchase_status': false,
          // tslint:disable-next-line:max-line-length
          'description': 'Use the estimator to see how making extra payments will affect your plan.',
          'monthly_payment': '$30.00', 'total_lease_amount': '', 'EIP_status': true,
          'purchase_price': '', 'lease_start_date': '', 'lease_status': '',
          'payments_remaining': '8', 'plan_id': '', 'IMEI': '123456789123456',
          'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment':  '$30.00', 'final_payment': '$28.99',
          'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped',  'EIP_plan_ID': '12345678912345', 'variant': ''
        },
        {
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Samsung Galazy S9', 'payment': '8 payments', 
          'purchase_status': false, 'duration': '$30/month', 'completedPercentage': '60', 
          'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
          // tslint:disable-next-line:max-line-length
          'description': 'Use the estimator to see how making extra payments will affect your plan.',
          'monthly_payment': '$30.00', 'total_lease_amount': '', 'EIP_status': true,
          'purchase_price': '', 'lease_start_date': '', 'lease_status': '',
          'payments_remaining': '8', 'plan_id': '', 'IMEI': '123456789123456',
          'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment':  '$30.00', 'final_payment': '$28.99',
          'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped',  'EIP_plan_ID': '12345678912345', 'variant': ''
        }
      ]
    }
  ];
  **/
  //  completed status array
  JOD_Array_Completed = [
    {
      'heading': 'JUMP! on Demand Leases',
      'badge_status': false,
      'badge_text': 'Ending soon',
      // EIP_status is true then payment estimator will enable
      'details': [
        {
          'name': 'Jane', 'phone_number': '(555) 555- 1111', 'type': 'Apple iPhone 6S', 'payment': '2 payments',
          'duration': '$24/month', 'image': './assets/img/phone-image.png', 'btnText': 'Make a payment', 'purchase_status': false,
          // tslint:disable-next-line:max-line-length
          'description': 'Use the estimator to see how making extra payments will affect your plan.',
          'completedPercentage': '70', 'monthly_payment': '$27.00', 'total_lease_amount': '$750.00', 'EIP_status': false,
          'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Completed', 'status': 'Completed',
          'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
          'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment': '$30.00', 'final_payment': '$28.99',
          'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped', 'EIP_plan_ID': '12345678912345', 'variant': 'jodCompleted'
        }
      ]
    },
    {
      'heading': ' Equipment Installment Plans',
      'badge_status': false,
      'badge_text': 'Ending soon',
      'details': [
        {
          'name': 'Diane Lane', 'phone_number': '(555) 555- 1111', 'type': 'Samsung Galaxy Note8', 'payment': '5 payments',
          'duration': '$30/month', 'completedPercentage': '60', 'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
          'purchase_status': false, 'status': 'Completed',
          // tslint:disable-next-line:max-line-length
          'description': 'Use the estimator to see how making extra payments will affect your plan.',
          'monthly_payment': '$30.00', 'total_lease_amount': '', 'EIP_status': true,
          'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Completed',
          'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
          'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment': '$30.00', 'final_payment': '$28.99',
          'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped', 'EIP_plan_ID': '12345678912345', 'variant': 'jodCompleted'
        },
        {
          'name': 'John', 'phone_number': '(555) 555- 1111', 'type': 'Samsung Galazy S9', 'payment': '8 payments',
          'purchase_status': false, 'duration': '$30/month', 'completedPercentage': '60', 'status': 'Completed',
          'image': './assets/img/ssu/samsung.png', 'btnText': 'Make a payment',
          // tslint:disable-next-line:max-line-length
          'description': 'Use the estimator to see how making extra payments will affect your plan.',
          'monthly_payment': '$30.00', 'total_lease_amount': '', 'EIP_status': true,
          'purchase_price': '$0.00', 'lease_start_date': '08/08/18', 'lease_status': 'Completed',
          'payments_remaining': '8', 'plan_id': '12345678912345', 'IMEI': '123456789123456',
          'plan_start_date': '08/08/2017', 'original_price': '$750.00', 'down_payment': '$30.00', 'final_payment': '$28.99',
          'installment_plan_type': 'EIP', 'plan_status': 'Active - Shipped', 'EIP_plan_ID': '12345678912345', 'variant': 'jodCompleted'
        }
      ]
    }
  ];

  constructor(private commonService: CommonService) { }

  ngOnInit() {
  }
  activeTab(index) {
    for (let i = 0; i < this.statusArray.length; i++) {
      this.statusArray[i].statusTabCheck = false;
    }
    this.statusArray[index].statusTabCheck = true;
  }
  goTo(val){
    var el = document.getElementById(val);
    el.scrollIntoView( { behavior: 'smooth', block: 'start', inline: 'start' });
  }
}
