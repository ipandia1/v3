import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment-system-error',
  templateUrl: './payment-system-error.component.html',
  styleUrls: ['./payment-system-error.component.scss']
})
export class PaymentSystemErrorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
