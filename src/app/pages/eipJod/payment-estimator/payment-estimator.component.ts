import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment-estimator',
  templateUrl: './payment-estimator.component.html',
  styleUrls: ['./payment-estimator.component.scss']
})
export class PaymentEstimatorComponent implements OnInit {
  btnEnable = false;
  btnEnable1 = false;
  details = {
    name: 'Napoleon', phoneNo: '(555) 555- 1111', phone: 'Apple iPhone 6s', EIPID: '12345678912345', balance: '111.11',
    extraPayment: '', currentBal: '234.87', estBal: '209.87', currentPay: '8', estPay: '8'
  };
  constructor() { }
  valChange = function(event) {
    this.val = event.target.value;
    if (this.val === '' || this.val < 1 || this.val === '.') {
      this.btnEnable = false;
      this.btnEnable1 = false;
    } else {
      this.btnEnable = true;
    }
    this.crossIcon1 = document.getElementById('crossSpan');
    if (this.crossIcon1) {
      this.crossIcon1.addEventListener('click', (e) => this.clearValue1());
    }
  };

  clearValue1 = function() {
    this.btnEnable = false;
    this.btnEnable1 = false;
  };

  btnClick = function() {
      if (this.btnEnable === true) {
        this.btnEnable1 = true;
      } else {
        this.btnEnable1 = false;
      }
    };
  ngOnInit() {
  }

}
