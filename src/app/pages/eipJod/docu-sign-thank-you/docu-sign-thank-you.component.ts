import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-docu-sign-thank-you',
  templateUrl: './docu-sign-thank-you.component.html',
  styleUrls: ['./docu-sign-thank-you.component.scss']
})
export class DocuSignThankYouComponent implements OnInit {
  details = {
      title: 'THANKS', cardNo: '8080', amount: '13.34', month: 'Jul', date: '04', year: '2018',
      orderNo: 'S0000035668', orderDate: '08/08/2018', orderTime: '10:21:33', emailId: 'jane@email.com'
    };

  showDetails = true;
  constructor() { }

  ngOnInit() {
  }
  clickFun = function() {
      this.showDetails = !this.showDetails;
  };

}
