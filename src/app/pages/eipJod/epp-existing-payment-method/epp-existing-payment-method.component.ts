import {Component, Input, Output} from '@angular/core';

@Component({
  selector: 'app-epp-existing-payment-method',
  templateUrl: './epp-existing-payment-method.component.html',
  styleUrls: ['./epp-existing-payment-method.component.scss']
})
export class EppExistingPaymentMethodComponent  {
  visible: boolean;
  constructor() { } 
  close() {
    this.visible = false;
  }
  
  paymentmethod:string="Payment method";

  paymentdue:string="$100.00";
  duedate:string="Dec 19";
  name: string="Napoleon";
  Paymentdate:string="Nov 17";
  paymentamount:string="$100.00"
  iconClass = "visa-icon";
 
  public paymentValues: Array<Object> = [
    { descTitle: '(425) 456-2344', value: 'Samsung Galaxy Note8' },
    { descTitle: 'EIP (Installment Plan) ID:', value: '12345678912345' },
    { descTitle: 'Device balance:', value: '111.11'}
  ];

  public paybillvalues: Array<any> = [
    { descTitle: "Amount", value:this.paymentdue,image:"",bottomDivider:false},
    { descTitle: "Payment Method", value: "****1234",image:this.iconClass,bottomDivider:true}  ];
}
