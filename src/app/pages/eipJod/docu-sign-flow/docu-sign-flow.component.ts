import { Component, OnInit, HostBinding, HostListener, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-docu-sign-flow',
  templateUrl: './docu-sign-flow.component.html',
  styleUrls: ['./docu-sign-flow.component.scss']
})
export class DocuSignFlowComponent implements OnInit, AfterViewInit  {

  totalDocSignHeight:'0px';
  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.banerHeight();
 }

  @HostListener('window:load', [])
  onWindowLoad() {
    this.banerHeight();
  }

  @HostListener('window:resize', [])
  onResize() {
    this.banerHeight();
   }

   banerHeight = function() {
    var windowHeight = window.innerHeight;
    var planTitleDiv = document.getElementsByClassName('planheight')[0];
    var planTitleHeight = planTitleDiv.getBoundingClientRect().height;
    //console.log("planTitleHeight"+planTitleHeight);
    return this.totalDocSignHeight = windowHeight-(planTitleHeight+24)+'px';
   }


}
