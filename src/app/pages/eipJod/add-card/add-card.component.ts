import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.scss']
})
export class AddCardComponent implements OnInit {
  creditcard: CreditCard;
  savePayment = true;
  visible: boolean;
  setDefault: boolean;
    constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.creditcard = {
    cardName: '',
    creditCardNumber: '',
    expirationDate: '',
    cvvNumber: '',
    zipCode: '',
    nickName: ''
  };
  }
  closeModal() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  showModal() {
    this.visible = true;
    this.commonService.showScroll(false);
  }

}
export class CreditCard {
  cardName: string;
  creditCardNumber: string;
  expirationDate: string;
  cvvNumber: string;
  zipCode: string;
  nickName: string;
}
