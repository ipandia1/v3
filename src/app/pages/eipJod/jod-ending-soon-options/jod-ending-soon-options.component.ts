import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jod-ending-soon-options',
  templateUrl: './jod-ending-soon-options.component.html',
  styleUrls: ['./jod-ending-soon-options.component.scss']
})
export class JodEndingSoonOptionsComponent implements OnInit {
  monthlyBill = '120.00';
  details = {
    name: 'Thomas', phoneNo: '(555) 555- 1111', phone: 'Apple iPhone X - Silver', dueMonth: '13.34', due9Months: '20.23'
  };
  constructor() { }

  ngOnInit() {
  }

}
