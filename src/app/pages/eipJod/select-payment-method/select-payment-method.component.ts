import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-select-payment-method',
  templateUrl: './select-payment-method.component.html',
  styleUrls: ['./select-payment-method.component.scss']
})
export class SelectPaymentMethodComponent implements OnInit {
  disableClass = 'body black';
  paymentMethod: Array<any> = [
    { iconClass: 'visa-icon', cardName: 'Visa Credit Card', cardNumber: '****9775', add: '' },
    { iconClass: 'amex-icon', cardName: 'American Express', cardNumber: '****4412', add: '' },
    { iconClass: '', cardName: '', cardNumber: '', add: 'Add a card', url: '' }
  ];
  constructor() { }

  ngOnInit() {
  }

}
