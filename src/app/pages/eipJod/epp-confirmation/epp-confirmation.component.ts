import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-epp-confirmation',
  templateUrl: './epp-confirmation.component.html',
  styleUrls: ['./epp-confirmation.component.scss']
})
export class EppConfirmationComponent implements OnInit {
  details = {
      title: 'THANKS', 
      cardNo: '8080', 
      amount: '25.00', 
      month: 'Jul', 
      date: '04', 
      year: '2018',
      referenceNumb: '1960337049', 
      authorizationCode: '06489I', 
      zipCode: '98298', 
      transType: 'EIP Balance Payment',
      tmoAcctNum: '917633439', 
      newBal: '$86.11', 
      paymentRemain: '4', 
      eipPlanIDNnum: '20170329967087',
      IMEI:'357754080138226'
  };
  showDetails = true;
  constructor() { }

  ngOnInit() {
  }
  clickFun = function() {
    this.showDetails = !this.showDetails;
};


}
