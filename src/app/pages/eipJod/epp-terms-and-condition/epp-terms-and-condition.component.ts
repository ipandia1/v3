import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-epp-terms-and-condition',
  templateUrl: './epp-terms-and-condition.component.html',
  styleUrls: ['./epp-terms-and-condition.component.scss']
})
export class EppTermsAndConditionComponent implements OnInit {

  constructor(private _location: Location) { }

  ngOnInit() {
  }
  gotopreviouspage() {
    this._location.back();
  }

}
