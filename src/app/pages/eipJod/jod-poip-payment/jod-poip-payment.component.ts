import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jod-poip-payment',
  templateUrl: './jod-poip-payment.component.html',
  styleUrls: ['./jod-poip-payment.component.scss']
})
export class JodPoipPaymentComponent implements OnInit {

  details = {
    name: 'Thomas', phoneNo: '(555) 555- 1111', phone: 'Apple iPhone X - Silver', dueMonth: '13.34', due9Months: '20.23'
  };
  constructor() { }

  ngOnInit() {
  }

}
