import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-balance-summary-variants',
  templateUrl: './balance-summary-variants.component.html',
  styleUrls: ['./balance-summary-variants.component.scss']
})
export class BalanceSummaryVariantsComponent implements OnInit {

  balanceArray1 = [{
    'Heading': 'When customers have lease +', 'Heading1': 'installment plans', 'paymentTitle': 'Monthly payments',
    'paymentAmt': '$90.00',
    'obj1': [{ 'title': 'Lease balance', 'amount': '$192.00', 'lease': '1 active lease', 'perMonth': '$24/month' },
    { 'title': 'Plan balance', 'amount': '$469.74', 'lease': '2 active installment plans', 'perMonth': '$30/month' }],
    'showNothing': ''
  },
  {
    'Heading': 'When customers only have lease', 'Heading1': '', 'paymentTitle': 'Monthly payments',
    'paymentAmt': '$90.00',
    'obj1': [{ 'title': 'Lease balance', 'amount': '$192.00', 'lease': '1 active lease', 'perMonth': '$24/month' }],
    'showNothing': ''
  },
  {
    'Heading': 'When customers only have installment plan', 'Heading1': '', 'paymentTitle': 'Monthly payments',
    'paymentAmt': '$90.00',
    'obj1': [{ 'title': 'Plan balance', 'amount': '$192.00', 'lease': '1 active lease', 'perMonth': '$230/month' }],
    'showNothing': ''
  },
  {
    'Heading': 'When customers do not have active lease/ plan', 'Heading1': '', 'paymentTitle': '',
    'paymentAmt': '',
    'obj1': [],
    'showNothing': 'Show nothing'
  }];
  constructor() { }

  ngOnInit() {
  }
  goTo(id1, id2) {
    var id = '0';
    if (id1 === id2) {
      id = id1 + 1;
      var el = document.getElementById(id);
      el.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'start' })

    }
    if (id1 === 0 && id2 === 1) {
      id = id2 + 1;
      var el = document.getElementById(id);
      el.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'start' })

    }
  }
}
