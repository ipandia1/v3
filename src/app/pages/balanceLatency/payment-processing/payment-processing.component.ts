import { Component, OnInit } from '@angular/core';
import { SpinnerService } from '../../../services/spinner.service';
@Component({
  selector: 'app-payment-processing',
  templateUrl: './payment-processing.component.html',
  styleUrls: ['./payment-processing.component.scss']
})
export class PaymentProcessingComponent implements OnInit {

showLoaderV2: boolean;
  noOverlayV2: boolean;
  showButton =  true;
  showContent = false;
  showBalanceUpdated = false;
  showBalanceNotUpdated = false;
  constructor(private spinnerservice: SpinnerService) {
  }

  showsSpinner() {
    this.showButton = false;
    this.spinnerservice.showspinnerV2(true);
    this.spinnerservice.showoverlayV2(false);
   // this.delayedSpinner();
  }

  hidesSpinner() {
    this.spinnerservice.showspinnerV2(false);
    this.spinnerservice.showoverlayV2(false);
  }

  delayedSpinner() {
    this.showsSpinner();
    this.delayedContent();
    setTimeout(() => {
      this.hidesSpinner();
      this.showContent = false;
      this.showBalanceUpdated = true; // to show the balance update make it true otherwise false
      this.showBalanceNotUpdated = false; // to show the balance not update make it true otherwise false
    }, 5000);
  }


  delayedContent() {
    setTimeout(() => {
    this.showContent = true;
    }, 2000);

  }

  ngOnInit() {
  }

}
