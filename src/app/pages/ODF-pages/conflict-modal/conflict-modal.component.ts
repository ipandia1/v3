import { Component } from '@angular/core';

@Component({
  selector: 'app-conflict-modal',
  templateUrl: './conflict-modal.component.html',
  styleUrls: ['./conflict-modal.component.scss']
})
export class ConflictModalComponent {
visible: boolean;
constructor() { }
  close() {
    this.visible = false;
  }
}
