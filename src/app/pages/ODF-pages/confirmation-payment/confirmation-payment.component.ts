import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-confirmation-payment',
  templateUrl: './confirmation-payment.component.html',
  styleUrls: ['./confirmation-payment.component.scss']
})

export class ConfirmationPaymentComponent implements OnInit {
    public smallDevice:boolean=false;
    public name: string = "Tom";
    public showDetails: boolean = false;
  constructor() { }
  ngOnInit() { 
  var deviceHeight=window.innerHeight;
  if(deviceHeight<565){
    this.smallDevice=true;
    }
    else{
    this.smallDevice=false;
    }
  }

  public dataPassInfo: Array<Object> = [
    { descTitle: 'Order no.', value: '0123456789' },
    { descTitle: 'Transaction total', value: '$16.00' },
    { descTitle: 'Payment type', value: 'Visa ****1234' },
    { descTitle: 'Name on account', value: 'John Doe' },
    { descTitle: 'T-Mobile account no.', value: '12346789' }
  ];

  public orderDetails: Array<Object> = [
    { descTitle: 'Name ID', value: '4.00', tax: '+ tax' },
    { descTitle: 'Scam Block', value: '0.00' }
  ];

  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  }

}
