import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-electronic-signature-terms',
  templateUrl: './electronic-signature-terms.component.html',
  styleUrls: ['./electronic-signature-terms.component.scss']
})
export class ElectronicSignatureTermsComponent implements OnInit {

  constructor(private _location: Location) { }

  ngOnInit() {
  }
  gotopreviouspage(){
    this._location.back();
  }
}
