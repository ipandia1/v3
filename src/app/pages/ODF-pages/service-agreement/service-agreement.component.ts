import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-service-agreement',
  templateUrl: './service-agreement.component.html',
  styleUrls: ['./service-agreement.component.scss']
})
export class ServiceAgreementComponent implements OnInit {

  constructor(private _location: Location) { }

  ngOnInit() {
  }
  gotopreviouspage(){
    this._location.back();
  }
}
