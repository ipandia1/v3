import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-tax-modal',
  templateUrl: './tax-modal.component.html',
  styleUrls: ['./tax-modal.component.scss']
})

export class TaxModalComponent implements OnInit {
visible: boolean;
  close() {
    this.visible = false;
  }
  Tmobilefees:[{
      chargename: string,
      amount: string
  }]
  govfees : [{
          chargename: string,
          amount:string;
      },{
        chargename: string,
        amount:string;
    },{
        chargename: string,
        amount:string;
    }];
  constructor() {
                this.govfees = [{
                        chargename : "Federal universal service fund",
                        amount: "0.15"
                    },
                    {
                        chargename : "Utility use",
                        amount: "0.15"
                    },
                    {
                        chargename : "Regulatory programs and Telco fees",
                        amount: "2.50"
                    }];

                this.Tmobilefees = [{
                        chargename : "e911 Fee",
                        amount: "0.15"
                    },];

   }
  public add:number=0;
  public total:string;
  ngOnInit() {
        this.govfees.forEach((e:any) => {
            this.add = this.add + parseFloat(e.amount);
        });
          this.Tmobilefees.forEach((e:any) => {
            this.add = this.add + parseFloat(e.amount);
        });

        this.total=this.add.toFixed(2);
  }
}
