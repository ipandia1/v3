import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-review-payment',
  templateUrl: './review-payment.component.html',
  styleUrls: ['./review-payment.component.scss']
})
export class ReviewPaymentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  public paymentValues: Array<Object> = [
    { descTitle: 'Monthly recurring charges', value: '4.00' },
    { descTitle: 'Taxes and fees', value: '1.00' },
    { descTitle: 'Order total', value: '5.00'}
  ];
  paymentMethod = "Payment method";
  cardNumber = "****1234";
  iconClass = "visa-icon";

}
