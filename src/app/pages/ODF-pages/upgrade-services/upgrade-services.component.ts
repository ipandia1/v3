import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-upgrade-services',
  templateUrl: './upgrade-services.component.html',
  styleUrls: ['./upgrade-services.component.scss']
})
export class UpgradeServicesComponent implements OnInit {

  upgradeServices: Array<any> = [
    { "title" : "Data plan",
      "data":[
      {id:1,  "name": "T-Mobile ONE","price":"$20 /mo","description1":"T-Mobile ONE includes unlimited data on our network with no annual service contracts, or data overages. Plus, you get unlimited data with up to 5GB of 4G LTE in Mexico and Canada at no extra charge! Even better, you'll get an hour of complimentary data at 30K feet with Gogo and unlimited data almost everywhere else in the world. Charges will include taxes and fees for customers on tax inclusive rate plans.", "description2":"On all T-Mobile plans, during congestion, the small fraction of customers using >50GB/mo. may notice reduced speeds until next bill cycle due to data prioritization.", "description3":"Changes made in the middle of a billing cycle will result in full monthly charges of the new feature."}
     ]},
    { "title" : "Data pass",
      "data":[
      {id:2, "name":"HD Video", "price":0,"description":"Enable HD"},
     { id:3, "name": "International Data 10 Day - 1 GB","price":"$20 /mo","description1":"Provides up to 1 GB of high-speed data for 10 days while roaming internationally in over 140 countries. Smartphone Mobile HotSpot included. Additional passes can be purchased as needed once this single use pass expires.", "description2":"Check to see if 3G data (or above) is vailable in the country you're travelling to."},
     { id:4, "name": "Mobile HotSpot 1 Week - 1GB","price":"$10","description1":"T-Mobile ONE includes unlimited data on our network with no annual service contracts, or data overages. Plus, you get unlimited data with up to 5GB of 4G LTE in Mexico and Canada at no extra charge! Even better, you'll get an hour of complimentary data at 30K feet with Gogo and unlimited data almost everywhere else in the world. Charges will include taxes and fees for customers on tax inclusive rate plans.", "description2":"On all T-Mobile plans, during congestion, the small fraction of customers using >50GB/mo. may notice reduced speeds until next bill cycle due to data prioritization.", "description3":"Changes made in the middle of a billing cycle will result in full monthly charges of the new feature."}
    ]},
    { "title" : "Services",
      "data":[
     {id:5,  "name": "Voicemail to Text","price":"$4/mo","description1":"T-Mobile ONE includes unlimited data on our network with no annual service contracts, or data overages. Plus, you get unlimited data with up to 5GB of 4G LTE in Mexico and Canada at no extra charge! Even better, you'll get an hour of complimentary data at 30K feet with Gogo and unlimited data almost everywhere else in the world. Charges will include taxes and fees for customers on tax inclusive rate plans.", "description2":"On all T-Mobile plans, during congestion, the small fraction of customers using >50GB/mo. may notice reduced speeds until next bill cycle due to data prioritization.", "description3":"Changes made in the middle of a billing cycle will result in full monthly charges of the new feature."}
    ]},
];
  upgradeServiceAccordion:boolean[][] = [];
  constructor() { }

  ngOnInit() {
    /*Logic to handle toggle content*/
    for(var i in this.upgradeServices) {
      this.upgradeServiceAccordion.push([]);
      for (var k in this.upgradeServices[i].data){
        this.upgradeServiceAccordion[i].push(false);
        
      }
   }
  }
    isChecked: boolean = false;
  count: number = 0;
  checkFunc(event) {
    if (event.target.checked)
      this.count = this.count + 1;
    else
      this.count = this.count - 1;

    if (this.count > 0)
      this.isChecked = true;
    else
      this.isChecked = false;
  }

}
