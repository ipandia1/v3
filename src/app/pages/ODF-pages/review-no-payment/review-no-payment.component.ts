import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-review-no-payment',
  templateUrl: './review-no-payment.component.html',
  styleUrls: ['./review-no-payment.component.scss']
})
export class ReviewNoPaymentComponent implements OnInit {
  public noPaymentValues: Array<Object> = [
    { descTitle: 'Monthly recurring charges', value: '$4.00', perMonth: '$4.00/mo', tax: '+tax' },
    { descTitle: 'Monthly recurring charges - removed', value: '-$4.00', perMonth: '$0.00/mo', tax: '+tax' }
  ];
  public incVal: string = "$2.00/mo + tax";

  constructor() { }

  ngOnInit() {
  }
}
