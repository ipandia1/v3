import { Component, OnInit, Input } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import {Location} from '@angular/common';

@Component({
  selector: 'app-terms-and-condition',
  templateUrl: './terms-and-condition.component.html',
  styleUrls: ['./terms-and-condition.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({ transform: 'scale(.6)' }),
        animate(300)
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'scale(0.6)' }))
      ])
    ])
  ]
})
export class TermsAndConditionComponent implements OnInit {
  @Input() visible: boolean;
  constructor(private _location: Location) { }

  ngOnInit() {
  }
  close() {
    this.visible = false;
  }
 

  gotopreviouspage(){
      this._location.back();
  }
}
