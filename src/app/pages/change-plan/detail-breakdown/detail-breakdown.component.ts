import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-detail-breakdown',
  templateUrl: './detail-breakdown.component.html',
  styleUrls: ['./detail-breakdown.component.scss']
})
export class DetailBreakdownComponent implements OnInit  {
  detailBreakdownDataPath = 'assets/json/changePlan/detailBreakdownData.json';
  breakdownDetails: any;
  buttonText = 'Back to summary';
  number: any;
  constructor(private commonService: CommonService, private router: Router, private _Activatedroute: ActivatedRoute) {
    this.commonService.getJsonData(this.detailBreakdownDataPath)
     .subscribe(responseJson => {
       this.breakdownDetails = responseJson;
     });
     this._Activatedroute.queryParams
      .subscribe(params => {
          if (params['fromPlanConfirmation']) {
            this.buttonText = 'Close';
          }
      });
   }


  ngOnInit() {
  }
   isNumber(data) {
    this.number = Number(data);
    return this.number ;
  }
  checkChangeplan(ischangeplan) {
   // alert('changeplanYes' + document.getElementById('changeplanYes'));
 //   alert('changeplanNo' + document.getElementById('changeplanNo'));

 alert('ischangeplan');
    if (ischangeplan === 'Yes') {
      if (document.getElementById('changeplanYes') == null) {
        return true;
      } else {
        return false;
      }
    }
    if (ischangeplan === 'No') {
      {
      if (document.getElementById('changeplanNo') !== null) {
        return '1';
      } else {
        return '2';
      }
    }
  }
}
}
