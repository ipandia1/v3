import { Component, OnInit, ElementRef } from '@angular/core';

@Component({
  selector: 'app-review-order',
  templateUrl: './review-order.component.html',
  styleUrls: ['./review-order.component.scss']
})
export class ReviewOrderComponent implements OnInit {
  datePicker = false;
  showDetails = false;
  constructor(private elref: ElementRef) { }

  ngOnInit() {
  }
  viewDatePicker() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('noScroll');
    this.datePicker = true;
    const datepick = this.elref.nativeElement.querySelector('.date-picker');
    datepick.setAttribute('aria-hidden', false);
  }
  handleCallBack(callBackValue: string) {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('noScroll');
    this.datePicker = false;
    const datepick = this.elref.nativeElement.querySelector('.date-picker');
    datepick.setAttribute('aria-hidden', true);
  }
  detailsPayment() {
    this.showDetails = !this.showDetails;
    const accordion = this.elref.nativeElement.querySelector('.accordion-content');
    accordion.setAttribute('aria-hidden', !this.showDetails);
  }
}
