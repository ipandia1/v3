import { CommonService } from '../../../services/common.service';
import { Component, OnInit } from '@angular/core';
import { ElementRef } from '@angular/core';

@Component({
  selector: 'app-plan-confirmation',
  templateUrl: './plan-confirmation.component.html',
  styleUrls: ['./plan-confirmation.component.scss']
})
export class PlanConfirmationComponent implements OnInit {

  PlanConfirmationPath = 'assets/json/changePlan/planConfirmationData.json';

  planConfirmationData: any;
  public showDetails: Boolean = false;
  constructor(private commonService: CommonService, private elementref: ElementRef) {
    this.commonService.getJsonData(this.PlanConfirmationPath)
     .subscribe(responseJson => {
       this.planConfirmationData = responseJson;
     });
   }

  ngOnInit() {
  }

  detailsPayment = function () {
    this.showDetails = !this.showDetails;
    const accordion = this.elementref.nativeElement.querySelector('.accordion-content');
    accordion.setAttribute('aria-hidden', !this.showDetails);
  };
}
