import { Component, OnInit, Renderer2 } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-review-changes',
  templateUrl: './review-changes.component.html',
  styleUrls: ['./review-changes.component.scss']
})
export class ReviewChangesComponent implements OnInit {
  checkOpt: Boolean;
  changePlanJson = 'assets/json/changePlan/changePlanData.json';
  changePlanData: Array<Object>;
  plans:  Array<Object>;
  paymentData;
  selectLine: Array<any> = [{'name': 'Current bill cycle – MM/DD/YYYY'},
                            {'name': 'Next bill cycle – MM/DD/YYYY'}];
  selectedLine: string;
  constructor(private commonService: CommonService, private renderer: Renderer2) {
   }
  ngOnInit() {
    this.checkOpt = true;
    this.commonService.getJsonData(this.changePlanJson)
    .subscribe(responseJson => {
      this.changePlanData = responseJson;
      this.toHandleData(this.changePlanData);
    });
    this.selectedLine = this.selectLine[0].name;
  }
  toHandleData(data) {
    this.plans = data['data'];
    this.paymentData = this.plans[0];
     }
     toggle() {
      const btnState = this.renderer.selectRootElement('.slider-btn');
      if (btnState.checked) {
      btnState.checked =  false;
    } else {
        btnState.checked =  true;
      }
    }
    public handleLine(callBackValue: string) {
      if (callBackValue !== '') {
        this.selectedLine = callBackValue;
      }
    }
}
