import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef , AfterViewInit, AfterViewChecked, HostListener } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-plan-comparison',
  templateUrl: './plan-comparison.component.html',
  styleUrls: ['./plan-comparison.component.scss']
})
export class PlanComparisonComponent implements OnInit, AfterViewChecked {
  stickTop: boolean;
  // sticky: any;
  visible: boolean;
  visible1: boolean;
  selectedLine1: string;
  selectedLine2: string;
  subTitle1 = 'Current Plan';
  subTitle2 = 'Choose this plan';
  dropdownId1 = 'id1';
  dropdownId2 = 'id2';
  dynamicplaceholder = 'Find a line';
  changePlanData: Array<Object>;
  data: any;
 changeHeight1 = false;
  retreiveData1: Array<any> = [
    {
      'name': 'Simple Choice', 'amountWithTaxes': '160.00', 'tcText':'Monthly with Autopay + $11.00 in estimated Taxes & Fees',
      'dataPlan': '10GB of 4G LTE per line', 'hotspot': 'Hotspot data included', 'talkText': 'Unlimited'
    }
  ];
  additionalData1: Array<any> = [{
    'freeVideoStream': 'With 3GB or higher data plan',
    'freeMusicStream': 'On included services',
    'netflixOnus': false,
    'textingDataAbroad': 'Included',
    'overseasVoice': false,
    'useDeviceInMexico': ['Talk', 'Text & Data'],
    'taxInclusive': false,
    'autoPayDiscount': 'No Discount',
    'gogoInFlightBenfits': 'In-flight texting & one hour of free data',
    'familyAllowance': false,
    'tMobFamilyMode': true
  }];
  height1;
  divHeight1Prev;
  @ViewChild('sticky') sticky: ElementRef;
  @ViewChild('stickyTwo') stickyTwo: ElementRef;
  retreiveData2: Array<any> = [
    {
      'name': 'T-Mobile One', 'amountWithTaxes': '193.00', 'tcText': 'Monthly with autopay, taxes & fees included',
      'dataPlan': 'Unlimited', 'hotspot': 'Unlimited', 'talkText': 'Unlimited'
    }
  ];

  additionalData2: Array<any> = [{
    'freeVideoStream': 'Unlimited',
    'freeMusicStream': 'Unlimited',
    'netflixOnus': true,
    'textingDataAbroad': 'Included',
    'overseasVoice': false,
    'useDeviceInMexico': ['Talk', 'Text & Data'],
    'taxInclusive': true,
    'autoPayDiscount': '$5 discount per line with Autopay Enabled',
    'gogoInFlightBenfits': 'In-flight texting & one hour of free data',
    'familyAllowance': true,
    'tMobFamilyMode': true
  }];

  selectLine: Array<any> = [];
  mobileView: boolean;
  changePlanDataPath = 'assets/json/changePlan/planComparision.json';

  alertBarOffset: any;
  alertBarOffsetTwo: any;
  dynamicHeight: any;
  dynamicHeight1 = 'auto';
  leftDropdown = false;
  rightDropdown = false;
  isTmoHeaderVisible = false;
  isTmoHeader = false;
  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    this.alertBarOffset = this.sticky.nativeElement.offsetTop;
    this.alertBarOffsetTwo = this.stickyTwo.nativeElement.offsetTop;
    this.heightFunc();
  }
  /* For sticky-top of alert*/
  @HostListener('window:scroll', []) onScroll() {
    this.enableSticky();
    this.heightFunc();
  }

  public enableSticky() {

    if (window.innerWidth < 576) {
      this.isTmoHeader = true;
    } else {
      this.isTmoHeader = false;
    }

    this.isTmoHeaderVisible = this.commonService.isTmoHeaderVisible();
    const pageYOffset: number = window.pageYOffset;
    if(this.isTmoHeader) {
      if (this.isTmoHeaderVisible) {
        if ((pageYOffset > this.alertBarOffset && pageYOffset >= 273) || (pageYOffset > this.alertBarOffsetTwo && pageYOffset >= 273)) {
          this.stickTop = true;
        } else {
          this.stickTop = false;
        }
      } else {
        if ((pageYOffset > this.alertBarOffset && pageYOffset >= 187) || (pageYOffset > this.alertBarOffsetTwo && pageYOffset >= 187)) {
          this.stickTop = true;
        } else {
          this.stickTop = false;
        }
      }
    } else {
      if ((pageYOffset > this.alertBarOffset && pageYOffset >= 187) || (pageYOffset > this.alertBarOffsetTwo && pageYOffset >= 187)) {
        this.stickTop = true;
      } else {
        this.stickTop = false;
      }
    }
  }

  constructor(private cdr: ChangeDetectorRef, private commonService: CommonService, private router: Router) {
    this.getChangePlanData(this.changePlanDataPath);
  }
  private getChangePlanData(urlPath) {
    this.commonService.getJsonData(urlPath)
      .subscribe(responseJson => {
        this.changePlanData = responseJson;
        this.planData(this.changePlanData);
      });
  }
  public planData(fullData: Array<any>) {
    if (fullData !== null) {
      this.data = this.changePlanData['data'];
      for (let i = 0; i <= this.data.length - 1; i++) {
        if (this.data[i].currentPlan) {
          this.selectLine.push({ 'name': '<span class="black">' + this.data[i].planName + '</span>' });
        } else {
          if (this.data[i].addOn) {
            //console.log(this.data[i].addOn);
            this.selectLine.push({
              'name': '<span class="black">T-Mobile ' + this.data[i].planName + ' ' + this.data[i].addOn + '</span>'
            });
          } else {
            this.selectLine.push({
              'name': '<span class="black">T-Mobile ' + this.data[i].planName + '</span>'
            });
          }
        }
      }
      this.selectedLine1 = this.selectLine[0].name;
      this.selectedLine2 = this.selectLine[1].name;
    }
  }
  public handleLine1(callBackValue: string) {
    if (callBackValue !== '') {
      this.retreiveData1 = [];
      this.additionalData1 = [];
      for (let i = 0; i <= this.selectLine.length - 1; i++) {
        if (this.selectLine[i]['name'] === callBackValue) {
          this.retreiveData1.push(this.data[i]);
          this.additionalData1.push(this.data[i].AdditionalDetails);
        }
      }
      this.selectedLine1 = callBackValue;
    }
  }
  public handleLine2(callBackValue: string) {
    if (callBackValue !== '') {
      this.retreiveData2 = [];
      this.additionalData2 = [];
      for (let i = 0; i <= this.selectLine.length - 1; i++) {
        if (this.selectLine[i]['name'] === callBackValue) {
          this.retreiveData2.push(this.data[i]);
          this.additionalData2.push(this.data[i].AdditionalDetails);
        }
      }
      this.selectedLine2 = callBackValue;
    }
  }
  modalInvoke() {
    this.visible = !this.visible;
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('noScroll');
    this.modelTab();
  }
  modalInvoke1() {
    this.visible1 = !this.visible1;
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('noScroll');
    this.modelTabVideoStream();
  }
  modelTab() {
    const dialog = document.getElementById('dialog');
    dialog.focus();
  }
  modelTabVideoStream() {
    const dialog = document.getElementById('dialogVideoStream');
    dialog.focus();
  }
  close() {
    this.visible = false;
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('noScroll');
  }
  close1() {
    this.visible1 = false;
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('noScroll');
  }

  @HostListener('window:load', [])
  onWindowLoad() {
    this.viewFunc();
    this.enableSticky();
  }

  public heightFunc() {
      const divHeight = document.getElementById('taxesDiv').offsetHeight;
      const divHeight1 = document.getElementById('taxesDiv1').offsetHeight;

      let text1 = '';
      let text2 = '';
      if ( this.retreiveData1[0]['tcText'] != null && this.retreiveData1[0]['tcText'] !== undefined) {
        text1 = this.retreiveData1[0]['tcText'];
      }
      if ( this.retreiveData2[0]['tcText'] != null && this.retreiveData2[0]['tcText'] !== undefined) {
        text2 = this.retreiveData2[0]['tcText'];
      }

      if ( text1.length >= text2.length ) {
          this.dynamicHeight1 = divHeight + 'px';
          this.dynamicHeight = 'auto';
      } else {
          this.dynamicHeight1 = 'auto' ;
          this.dynamicHeight = divHeight1 + 'px';
      }
  }

  public getheight() {
    this.heightFunc();
    return this.dynamicHeight;
  }

  public getheight1() {
    this.heightFunc();
    return this.dynamicHeight1;
  }

  @HostListener('window:resize', [])
  onResize() {
    this.viewFunc();
    this.heightFunc();
  }
  ngOnInit() {
    this.viewFunc();
  }

  ngAfterViewChecked() {
    this.heightFunc();
    this.cdr.detectChanges();
    //console.log("changessss");
  }

  viewFunc = function () {
    if (window.innerWidth < 768) {
      this.mobileView = true;
    } else {
      this.mobileView = false;
    }
  };
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.visible1 = false;
    this.visible = false;
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('noScroll');
}
  @HostListener('document:click', ['$event']) onDropdownclick(event) {
    this.leftDropdown = this.sticky.nativeElement.getElementsByTagName('ul')[0].classList.contains('show');
    this.rightDropdown = this.stickyTwo.nativeElement.getElementsByTagName('ul')[0].classList.contains('show');
  }
  @HostListener('window:blur', ['$event'])
  onBlur(event: any): void {
      this.leftDropdown = this.sticky.nativeElement.getElementsByTagName('ul')[0].classList.contains('show');
      this.rightDropdown = this.stickyTwo.nativeElement.getElementsByTagName('ul')[0].classList.contains('show');
  }
  @HostListener('keydown', ['$event'])
  onKeydown(event: KeyboardEvent) {
    if (event.keyCode === 9 ) {
     this.leftDropdown = false;
     this.rightDropdown = false;
    }
  }
}
