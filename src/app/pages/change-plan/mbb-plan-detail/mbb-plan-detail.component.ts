import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mbb-plan-detail',
  templateUrl: './mbb-plan-detail.component.html',
  styleUrls: ['./mbb-plan-detail.component.scss']
})
export class MBBPlanDetailComponent implements OnInit {

  PaymentHistoryPath = 'assets/json/changePlanData.json';
  planDetails = [
       {
        planTitle: 'New Data Plan 03',
        price: '$25.00',
        // tslint:disable-next-line:max-line-length
        description: 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vitae lorem leo. Ut nec est vitae tortor consequat venenatis sit amet ac ipsum.',
        subtitle: 'Here\'s what you get with ONE Plus in addition to all your T-Mobile ONE benefits:',
        subtitleMobile: 'Sed et congue dui, sagittis suscipit mi. Praesent euismod nisi nec:',
        features: [
          {feature: 'Nulla congue arcu eu felis blandit. '},
          {feature: 'Maecenas turpis eros, ornare et justo non, feugiat efficitur dolor. Tristique et turpis at erat. '},
          {feature: 'Curabitur molestie erat ac erat vulputatr. '},
          {feature: 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. '},
          {feature: 'Vestibulum ornare, ex eu facilisis tristique, nibh augue nec accumsan augue mauris non sem.'},
          {feature: 'Nulla pellentesque mi libero, ac pharetra lectus.'},
          {feature: 'Vestibulum ante ipsum primis in faucibus.'},
        ]}
    ];
  constructor() { }

  ngOnInit() {
  }

}
