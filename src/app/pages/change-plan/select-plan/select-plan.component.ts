import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-select-plan',
  templateUrl: './select-plan.component.html',
  styleUrls: ['./select-plan.component.scss']
})
export class SelectPlanComponent implements OnInit {

  SelectPlanPath='assets/json/changePlan/changePlanData.json';

  selectPlanData: any;
  currentPlanData: any;
  otherPlanData: any=[];
  showMore = false;
  constructor(private commonService: CommonService) { 
    this.commonService.getJsonData(this.SelectPlanPath)
     .subscribe(responseJson => {
       this.selectPlanData = responseJson["data"];
       this.getCurrentPlan(this.selectPlanData);
       //console.log(this.currentPlanData);
       //console.log(this.otherPlanData);

     });
  }

  ngOnInit() {
  }

  getCurrentPlan(plans){
    for (let plan of plans){
      if(plan.currentPlan){
        this.currentPlanData=plan;
      }
      else{
        this.otherPlanData.push(plan);
      }
    }
  }

}
