import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-historical-bills-modal',
  templateUrl: './historical-bills-modal.component.html',
  styleUrls: ['./historical-bills-modal.component.scss']
})

export class HistoricalBillsModalComponent implements OnInit {
  showValueBill: any;
  radioValue: any;
  visible: boolean;
  selected = 0;
  // tslint:disable-next-line:member-ordering
  historicalBills: [{
    billingPeriod: string,
  },{
    billingPeriod: string,
  },{
    billingPeriod: string,
  },{
    billingPeriod: string,
  },{
    billingPeriod: string,
  },{
    billingPeriod: string,
  },{
    billingPeriod: string,
  },{
    billingPeriod: string,
  },{
    billingPeriod: string,
  },{
    billingPeriod: string,
  },{
    billingPeriod: string,
  },{
    billingPeriod: string,
  }];
  constructor() {
   
    this.historicalBills = [{
      billingPeriod: 'Feb. 27 - Mar. 26, 2018'
    },
    {
      billingPeriod: 'Jan. 27 - Feb. 26, 2018'
    },
    {
      billingPeriod: 'Dec. 27 - Jan. 26, 2018'
    },
    {
      billingPeriod: 'Nov. 27 - Dec. 26, 2017'
    },
    {
      billingPeriod: 'Oct. 27 - Nov. 26, 2017'
    },
    {
      billingPeriod: 'Sep. 27 - Oct. 26, 2017'
    },
    {
      billingPeriod: 'Aug. 27 - Sep. 26, 2017'
    },
    {
      billingPeriod: 'Jul. 27 - Aug. 26, 2017'
    },
    {
      billingPeriod: 'Jun. 27 - Jul. 26, 2017'
    },
    {
      billingPeriod: 'May. 27 - Jun. 26, 2017'
    },
    {
      billingPeriod: 'Apr. 27 - May. 26, 2017'
    },
    {
      billingPeriod: 'Mar. 27 - Apr. 26, 2017'
    }
       ];

  }
  selectedValue(value) {
    this.radioValue = value;
  }
  close() {
    this.visible = false;
    this.showValueBill = this.radioValue;
  }
  ngOnInit() {
    if (this.selected === 0) {
      this.radioValue = 'Feb. 27 - Mar. 26, 2018';
    }
  }
}
