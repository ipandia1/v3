import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-payment-history',
  templateUrl: './payment-history.component.html',
  styleUrls: ['./payment-history.component.scss']
})
export class PaymentHistoryComponent implements OnInit {
 //PaymentHistoryPath = 'assets/json/accountHistory/paymentHistory.json';
 PaymentHistoryPath = 'assets/json/accountHistory/paymentHistory-more-data.json';
 
 paymentHistoryData: Array<Object>;
 tempData: Array<Object>;
 sliceStart = 0;
 sliceEnd = 26;
 next = true;
 dataLength;
 constructor(private commonService: CommonService) { }

 ngOnInit() {
   this.commonService.getJsonData(this.PaymentHistoryPath)
     .subscribe(responseJson => {
       this.paymentHistoryData = responseJson;
       this.toHandleData(this.paymentHistoryData);
     });

 }
 toHandleData(data) {
   this.tempData = data['data'];
   this.dataLength = this.tempData.length
 }
 nextTable() {
   this.next = false;
   this.sliceStart = this.sliceEnd;
   this.sliceEnd = this.tempData.length;
 }

}
