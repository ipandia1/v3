import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { CommonService } from '../../../services/common.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { filterQueryId } from '@angular/core/src/view/util';

@Component({
  selector: 'app-usage-overview',
  templateUrl: './usage-overview.component.html',
  styleUrls: ['./usage-overview.component.scss']
})
export class UsageOverviewComponent implements OnInit {
  datapasscount = 0;
  dataStashCount = 0;
  add: any;
  usageData: any;
  usageData1 = [];
  fromDate;
  toDate;
  private date;
  d = new Date();
  callmins = 0;
  messagecount = 0;
  datacount = 0;
  mobileHotspotCount = 0;
  thirdPartyCount = 0;
  TMobileCount = 0;
  monDetail: string;
  public monthArray: Array<any> = [
    { name: 'Nov', monthTabCheck: true },
    { name: 'Dec', monthTabCheck: false },
    { name: 'Jan', monthTabCheck: false },
    { name: 'Feb', monthTabCheck: false }
  ]

  selectLine: Array<any> = [
    { name: 'Nov 24 - Dec 24', disabled: false },
    { name: 'Dec 25 - Jan 25', disabled: false },
    { name: 'Jan 26 - Feb 26', disabled: false },
    { name: 'Feb 27 - current', disabled: false }
  ];

  public catagoryArray: Array<any> = [
    { category: 'Data', measure: 'Gigabytes', unit: 'GB' },
    { category: 'Data pass', measure: 'Gigabytes', unit: 'GB' },
    { category: 'Data stash', measure: 'Gigabytes', unit: 'GB' },
     { category: 'Mobile Hotspot', measure: 'Gigabytes',  unit: 'GB'},
    { category: 'Messages', measure: 'Messages', unit: '' },
    { category: 'Calls', measure: 'Minutes', unit: 'MIN' },
     { category: 'Third party purchases', measure: '', unit: '.00'},
     { category: 'T-Mobile purchases', measure: '', unit: '.00'},
  ]

  selecteddate: string;
  public dataArray: Array<any> = [
    { tabTitle: 'View by category', dataTabCheck: true },
    { tabTitle: 'View by line', dataTabCheck: false }
  ]
  allCategoriesDataPath = 'assets/json/accountHistory/Usage_Details.json';
  constructor(private _location: Location, private router: Router, private commonService: CommonService) {
    this.selecteddate = this.selectLine[0];
    this.getUsageData(this.allCategoriesDataPath);
  }
  private getUsageData(urlPath) {
    //console.log('url::::', urlPath);
    this.commonService.getJsonData(urlPath)
      .subscribe(responseJson => {
        this.usageData = responseJson.data;
        this.filteringByMonth();
      })
  }

  totalCount(catagory, i, measure) {
    const catagorydetails = this.usageData[i][catagory];
    let totalCounts = 0;
    // tslint:disable-next-line:forin
    for (const j in catagorydetails) {
      let count;
      const calldate = new Date(catagorydetails[j]['date'])
      if (calldate >= this.fromDate && calldate < this.toDate) {
        if (measure !== '') {
          // tslint:disable-next-line:radix
          count = parseInt(catagorydetails[j][measure].split(' ')[0]);
        } else {
          count = 1;
        }
        totalCounts = totalCounts + count;
      }
    }

    return totalCounts;
  }


  filteringByMonth() {
    this.usageData1 = [];
    const n = this.d.getFullYear();
    this.fromDate = this.selecteddate['name'].split('-')[0];
    this.fromDate = new Date(this.selecteddate['name'].split('-')[0] + ' ' + n);
    if (this.d <= this.fromDate) {
      this.fromDate = new Date(this.selecteddate['name'].split('-')[0] + ' ' + (n - 1));
    }
    if (this.selecteddate['name'].split('-')[1].trim() === 'current') {
      this.toDate = this.d;
    } else {
      this.toDate = new Date(this.selecteddate['name'].split('-')[1] + ' ' + n);
      if (this.d <= this.toDate) {
        this.toDate = new Date(this.selecteddate['name'].split('-')[1] + ' ' + (n - 1));
      }
    }
    if (this.selecteddate['name'].split('-')[1].trim() === 'current') {
      this.monDetail = 'There are 10 days left in your billing cycle';
    } else {
      this.monDetail = 'From ' + this.selecteddate['name'].split('-')[0] + 'to' +
        this.selecteddate['name'].split('-')[1] + ', ' + this.toDate.getFullYear();
    }

    // tslint:disable-next-line:forin
    for (const i in this.usageData) {

      this.callmins = this.totalCount('call-details', i, 'duration');
      this.messagecount = this.totalCount('message-details', i, '');
      this.datacount = this.totalCount('data-details', i, 'usage');
      this.datapasscount = this.totalCount('datapass-details', i, 'usage');
      this.dataStashCount = this.totalCount('datastash-details', i, 'usage');
       this.mobileHotspotCount = this.totalCount('mobileHotspot-details', i, 'usage');
       this.thirdPartyCount = this.totalCount('thirdPartyPurchases-details', i, 'usage');
       this.TMobileCount = this.totalCount('T-MobilePurchases-details', i, 'usage');
      this.usageData1.push({
        'name': this.usageData[i].name,
        'number': this.usageData[i].number,
        'calls': this.callmins,
        'messages': this.messagecount,
        'data-usage': this.datacount,
        'datapass-usage': this.datapasscount,
        'datastash-usage': this.dataStashCount,
        'mobileHotspot-usage': this.mobileHotspotCount,
        'thirdParty-usage': this.thirdPartyCount,
        'tMobliePurchases-usage': this.TMobileCount,
        'data-pass-usage': this.usageData[i].dataPassUsage,
        'data-stash-usage': this.usageData[i].dataStashUsage,
        'mobile-hotspot-usage': this.usageData[i].mobileHotspotUsage,
        'third-party-usage': this.usageData[i].thirdPartyPurchasesUsage,
        't-mobile-usage': this.usageData[i].tMobilePurchasesUsage,
        'usage': this.usageData[i].usage
      })
      this.callmins = 0;
      this.messagecount = 0;
      this.datacount = 0;
      this.datapasscount = 0;
      this.dataStashCount = 0;
      this.mobileHotspotCount = 0;
      this.thirdPartyCount = 0;
      this.TMobileCount = 0;
    }
  }

  ngOnInit() {
  }

  movedetailspage(event) {
    // alert("srini");
    const selectedindex = this.selectLine.indexOf(this.selecteddate);
    this.router.navigate(['usageDetails1', event.category, event.name, selectedindex]);
  }
  clickFun(val, tabName) {

    if (tabName === 'firstTab') {
      for (let i = 0; i < this.monthArray.length; i++) {
        this.monthArray[i].monthTabCheck = false;
      }
      this.monthArray[val].monthTabCheck = true;
      this.selecteddate = this.selectLine[val];
    } else {
      for (let i = 0; i < this.dataArray.length; i++) {
        this.dataArray[i].dataTabCheck = false;
      }
      this.dataArray[val].dataTabCheck = true;
    }
    this.filteringByMonth();
  }

}
