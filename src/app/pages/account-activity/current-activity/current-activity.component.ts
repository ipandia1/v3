import { Component, OnInit, EventEmitter } from '@angular/core';
import { CommonService } from '../../../services/common.service';
@Component({
  selector: 'app-current-activity',
  templateUrl: './current-activity.component.html',
  styleUrls: ['./current-activity.component.scss']
})
export class CurrentActivityComponent implements OnInit {
  selectedAccount = 'Account (all lines)';
  selectNames: Array<any> = [
    { name: 'Account (all lines)', disabled: true },
    { name: 'Vlad, (555) 555-5555', disabled: false },
    { name: 'Sam, (555) 555-5555', disabled: false },
    { name: 'Gwen (555) 555-5555', disabled: false },
    { name: 'Jill, (555) 555-5555', disabled: false }
  ];
  alertsInfo: Array<any> = [
    {
      'headingTitle': 'Alerts',
      'data': [{
        'title': 'New',
        'desc': 'Save up to 10 payment methods per line for secure, easy payments. Just re-enter payment info once to save a payment method.',
        'img': 'tooltip-info-white',
        'viewDetailsLink': '#',
        'leftArrowLink': '#'
      },
      {
        'title': 'Zero down eligible',
        'desc': ' You\'ve made 12+ consecutive on-time payments and now qualify for zero down on select devices.',
        'img': 'warning-icon',
        'viewDetailsLink': '#',
        'leftArrowLink': '#'
      },
      {
        'title': 'Zero down eligible',
        'desc': ' You\'ve made 12+ consecutive on-time payments and now qualify for zero down on select devices.',
        'img': 'tooltip-info-gray',
        'viewDetailsLink': '#',
        'leftArrowLink': '#'
      }],
      'showMoreLink': '#'
    }
  ];

  id1 = '1';
  id2 = '2';
  id3 = '3';
  selectedCategory = 'All Categories';
  selectCategory: Array<any> = [
    { name: 'All Categories', disabled: true },
    { name: 'Billing', disabled: false },
    { name: 'Payments', disabled: false },
    { name: 'Services', disabled: false },
    { name: 'Account', disabled: false },
    { name: 'Device', disabled: false },
    { name: 'Documents', disabled: false },
    { name: 'Conversations', disabled: false }
  ];
  selectedTimePeriod = 'Last 30 days';
  selectTimePeriod: Array<any> = [
    { name: 'Last 30 days', disabled: false },
    { name: 'Last 90 days', disabled: false },
    { name: 'Current year', disabled: false },
    { name: 'Previous year', disabled: false },
    { name: 'All (24 months)', disabled: false },
    { name: 'Custom dates', disabled: false }
  ];
  customDates = false;
  startDateTitle =  'Choose start date';
  startDateDesc =  'Choose the start date to filter your view of account history';
  endDateTitle =  'Choose end date';
  endDateDesc =  'Choose the end date to filter your view of account history';
  accountHistoryData: Array<Object>;

  scheduledInfo: Array<any> = [{
    'heading' : 'Scheduled activity',
    'data' : [{
    'scheduledDate': '06/15/17',
    'title': 'Bill payment - AutoPay',
    'description': 'An automatic payment will be processed on approximately this day for any outstanding amount due.',
    'iconClass': 'visa-icon',
    'cardNum': '****9876',
    'paymentTitle': 'Payment with:'
    },{
      'scheduledDate': '06/15/17',
      'title': 'Bill payment - AutoPay',
      'description': 'An automatic payment will be processed on approximately this day for any outstanding amount due.',
      'iconClass': 'visa-icon',
      'cardNum': '****9876',
      'paymentTitle': 'Payment with:'
      }]
}];
  /* Pagination variables*/
  currentpage: number;
  pageHeaders: Array<any>;
  pageData: Array<any>;
  tempData: Array<any>;
  noOfRecordsPerPage: number;
  totalRecords: number;
  allCategoriesDataPath = 'assets/json/accountHistory/allcategories.json';
  conversationsDataPath = 'assets/json/accountHistory/conversations.json';
  constructor(private commonService: CommonService) {
    this.getAccountHistoryData(this.allCategoriesDataPath);
  }

  ngOnInit() {
  }

  private getAccountHistoryData(urlPath) {
    this.commonService.getJsonData(urlPath)
      .subscribe(responseJson => {
        this.accountHistoryData = responseJson;
        this.paginateData(this.accountHistoryData);
      });
  }

  public handleAccount(callBackValue: string) {
    if (callBackValue !== 'this.isEmpty') {
      this.selectedAccount = callBackValue; // can use selectedAccount variable whenever we want to use the selected value
    }
  }

  public handleCategory(callBackValue: string) {
    if (callBackValue !== 'this.isEmpty') {
      this.selectedCategory = callBackValue; // can use selectedCategory variable whenever we want to use the selected value
    }
    if (callBackValue === 'All Categories') {
      this.getAccountHistoryData(this.allCategoriesDataPath);
    } else if (callBackValue === 'Conversations') {
      this.getAccountHistoryData(this.conversationsDataPath);
    }
  }
  public handleTimePeriod(callBackValue: string) {
    if (callBackValue !== 'this.isEmpty') {
      if (callBackValue === 'Custom dates') {
        this.customDates = true;
      } else {
        this.customDates = false;
      }
      this.selectedTimePeriod = callBackValue; // can use selectedTimePeriod variable whenever we want to use the selected value
    }
  }

  public paginateData(historyData: Array<any>) {
    if (historyData !== null) {
      this.pageHeaders = historyData['title'];
      this.tempData = historyData['data'];
      this.currentpage = 1;
      this.noOfRecordsPerPage = 6;
      this.totalRecords = this.tempData.length;
      // console.log(this.pageHeaders);
      this.pageData = this.tempData.slice(0 , this.noOfRecordsPerPage)
    }
  }
  moveCurrentPage(event) {
    this.pageData = this.tempData.slice(this.noOfRecordsPerPage * (event - 1),  event * this.noOfRecordsPerPage);
    this.currentpage = event;
  }
  moveFirstpage() {
    this.pageData = this.tempData.slice(1, this.noOfRecordsPerPage);
    this.currentpage = 1;
  }
  moveLastPage() {
    const lastpage = Math.ceil(this.totalRecords / this.noOfRecordsPerPage);
    this.pageData = this.tempData.slice(this.noOfRecordsPerPage * (lastpage - 1),  lastpage * this.noOfRecordsPerPage);
    this.currentpage = lastpage ;
  }
}
