import { Component, OnInit, HostListener } from '@angular/core';
import { Location } from '@angular/common';
import { CommonService } from '../../../services/common.service';
import {ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-usage-detail1',
  templateUrl: './usage-detail1.component.html',
  styleUrls: ['./usage-detail1.component.scss']
})
export class UsageDetail1Component implements OnInit {
  selectedLine = '';
  subTitle = '';
  dropdownId = 'id1';
  TMOIddetails = 'TmobileID details';
  visible: boolean;
  mobileapp: boolean = false;
  restorefilter: boolean = false;
  selectLine: Array<any> = [
    { name: 'Nov 24 - Dec 24', disabled: false },
    { name: 'Dec 25 - Jan 25', disabled: false },
    { name: 'Jan 26 - Feb 26', disabled: false },
    { name: 'Feb 27 - current', disabled: false }
  ];
  monthVal: Array<any> = [
    {name: 'Jan', value: 1},
    {name: 'Feb', value: 2},
    {name: 'Mar', value: 3},
    {name: 'Apr', value: 4},
    {name: 'May', value: 5},
    {name: 'Jun', value: 6},
    {name: 'Jul', value: 7},
    {name: 'Aug', value: 8},
    {name: 'Sep', value: 9},
    {name: 'Oct', value: 10},
    {name: 'Nov', value: 11},
    {name: 'Dec', value: 12},
  ];
  public TMODetails: Array<any> = [
    { desc: 'T-Mobile ID', value: 'reallyreallylongname@google.com' },
    { desc: 'Category', value: 'Voice' },
    { desc: 'Number', value: '(805)-555-1111' },
    { desc: 'Destination', value: 'to ventura, CA' },
    { desc: 'Type', value: 'Wi-Fi call' },
    { desc: 'Duration', value: '2 Min' },
    { desc: 'Date & Time', value: '1/1/18 12.00 PM' },
    { desc: 'Charge', value: '-' }
  ];
  currentTime = new Date();
  currentMonth = this.currentTime.getMonth() + 1 ;
  currentDay = this.currentTime.getDate();
  currentYear = this.currentTime.getFullYear();
  accountHistoryData: Array<Object>;
  /* Pagination variables*/
  currentpage: number;
  pageHeaders: Array<any>;
  pageData: Array<any>;
  tempData: Array<any>;
  noOfRecordsPerPage: number;
  totalRecords: number;
  name: string;
  category: string;
  totalminutes = 0;
  totalCharges = 0;
  allCategoriesDataPath = 'assets/json/accountHistory/Usage_Details.json';
   // new
   datesArray = [];
   from = [];
   till: Array<any>;
   tempDataArray = [];
   currentTempData = [];
   tempDataMonth: any ;
   tempDataDate: any ;
   tempDataYear: any ;
   currentData: number;
   fromMonth: any;
   fromDate: any;
   tillMonth: any;
   tillDate: any;
   historySize: any;
   dataCal: any;
   filterdata: boolean = false;
   filterjson : any[] = [];
   currentselectdata: any[] = [];
   temporiginal :Array<any>;
  constructor(private route: ActivatedRoute, private _location: Location, private commonService: CommonService) {
    this.getAccountHistoryData(this.allCategoriesDataPath);
   }
  private getAccountHistoryData(urlPath) {
    this.commonService.getJsonData(urlPath)
      .subscribe(responseJson => {
        this.accountHistoryData = responseJson;
        this.paginateData(this.accountHistoryData);
      });
  }
 
  applyfilter()
  {
    if(this.filterjson.length === 0){
    if (this.filterdata === true ){
      this.filterdata = false
      this.filterjson = [];
      this.tempData = this.temporiginal;
      this.restorefilter = false;
      this.funapplyfilter();
    } else {
      this.filterdata = true;
      this.temporiginal = this.tempData;
    }  
  }
  }

  Applyfilterdata(desc, data) {
    if(this.filterdata === true)
     {
      this.filterjson = [];
      this.filterjson.push({"desc": desc , "data": data } );
      this.filterdata = false;
      this.funapplyfilter();

    }
  }

  public paginateData(historyData: Array<any>) {
    if (historyData !== null) {
       for (let i = 0; i < historyData['data'].length ; i++) {
          if (historyData['data'][i]['name'] === this.name ) {
            this.pageHeaders = historyData['data'][i]['name'];
             if (this.category === 'Calls') {
              this.tempData = historyData['data'][i]['call-details'];
             } else if (this.category === 'Messages') {
              this.tempData = historyData['data'][i]['message-details'];
             } else if (this.category === 'Data') {
              this.tempData = historyData['data'][i]['data-details'];
             } else if (this.category === 'Mobile Hotspot') {
               this.tempData = historyData['data'][i]['mobileHotspot-details'];
             } else if (this.category === 'Data stash') {
               this.tempData = historyData['data'][i]['datastash-details'];
             } else if (this.category === 'Data pass') {
              this.tempData = historyData['data'][i]['datapass-details'];
             } else if (this.category === 'T-Mobile purchases') {
              this.tempData = historyData['data'][i]['thirdPartyPurchases-details'];
             } else if (this.category === 'Third party purchases' ) {
              this.tempData = historyData['data'][i]['thirdPartyPurchases-details'];
             }
          }
       }
      let num: any;
      this.currentData = 0;
      this.totalCharges = 0;
      this.datesArray = this.selectedLine.split('-');
      this.from = this.datesArray[0].split(' ');
       for (let j = 0; j < this.monthVal.length; j++) {
          if (this.from[0] === this.monthVal[j].name ) {
            this.fromMonth = this.monthVal[j].value;
          }
       }
       this.fromDate = parseInt(this.from[1], 10);
      this.till = this.datesArray[1].split(' ');
      if (this.till[1] !== 'current') {
        for (let j = 0; j < this.monthVal.length; j++) {
          if (this.till[1] === this.monthVal[j].name ) {
            this.tillMonth = this.monthVal[j].value;
          }
       }
      }
      this.tillDate = this.till[2];
      if ( this.tempData) {
      for (let i = 0; i < this.tempData.length; i++) {
        this.tempDataArray = this.tempData[i]['date'].split('/');
        this.tempDataMonth = parseInt(this.tempDataArray[0] , 10);
        this.tempDataDate = parseInt(this.tempDataArray[1], 10);
        this.tempDataYear = parseInt(this.tempDataArray[2], 10);
        if (this.till[1] === 'current') {
            if ((this.tempDataMonth <= this.currentMonth) && (this.tempDataMonth >= parseInt(this.fromMonth, 10))) {
              if (((this.tempDataDate <= this.currentDay) && (this.tempDataMonth === this.currentMonth))
              || ((this.tempDataDate >= this.fromDate) && (this.tempDataMonth === parseInt(this.fromMonth, 10)))) {
                    this.currentTempData[this.currentData] = this.tempData[i];
                  this.currentData++;
              }
            }
          } else if ( this.from[0] !== 'Dec' && this.till[1] !== 'Jan' ) {
              if ((this.tempDataMonth <= this.tillMonth) && (this.tempDataMonth >= parseInt(this.fromMonth, 10)) ) {
                if (((this.tempDataDate <= this.tillDate) && (this.tempDataMonth === this.tillMonth))
                || ((this.tempDataDate >= this.fromDate) && (this.tempDataMonth === parseInt(this.fromMonth, 10)))) {
                  this.currentTempData[this.currentData] = this.tempData[i];
                  this.currentData++;
                }
            }
          } else {
            if (((this.tempDataDate <= this.tillDate) && (this.tempDataMonth === this.tillMonth))
            || ((this.tempDataDate >= this.fromDate) && (this.tempDataMonth === parseInt(this.fromMonth, 10)))) {
              this.currentTempData[this.currentData] = this.tempData[i];
              this.currentData++;
            }
          }
      }
    }
      this.tempData = this.currentTempData;
      this.currentTempData = [];
      if (this.category === 'Calls') {
        for (let i = 0; i < this.tempData.length; i++) {
          num = this.tempData[i]['duration'].match(/\d+/g).map(Number);
          if (num.length === 2) {
            num = (num[0] * 60) + num[1];
          } else {
            num = num[0] * 60;
          }
          this.totalminutes += num;
        }
        this.totalminutes = Math.round(this.totalminutes / 60 );
        this.totalCharges = 0;
        for (let i = 0; i < this.tempData.length; i++) {
          if (this.tempData[i]['call-charges'] === '-') {
            num = 0;
          } else {
            num = parseFloat(this.tempData[i]['call-charges'].substring(1, ));
          }
          this.totalCharges = this.totalCharges + num ;
        }
      }
      if (this.category === 'Data' || this.category === 'Mobile Hotspot' || this.category === 'Data stash'
      || this.category === 'Data pass' || this.category === 'T-Mobile purchases' || this.category === 'Third party purchases') {
        this.dataCal = 0;
        for (let i = 0; i < this.tempData.length; i++) {
        this.dataCal = Math.ceil(this.dataCal + parseFloat(this.tempData[i]['usage'].substring(0, (this.tempData[i]['usage'].length - 2))));
        }
        if (this.dataCal >= 1024) {
        this.dataCal = (this.dataCal / 1024).toFixed(2) + ' GB';
        } else {
          this.dataCal = (this.dataCal).toFixed(2) + ' MB';
        }
      }
    }
    this.currentpage = 1;
    this.noOfRecordsPerPage = 5;
    this.totalRecords = this.tempData.length;
    this.pageData = this.tempData.slice(0, this.noOfRecordsPerPage);
  }

  moveCurrentPage(event) {
    this.pageData = this.tempData.slice(this.noOfRecordsPerPage * (event - 1), event * this.noOfRecordsPerPage);
    this.currentpage = event;
  }
  moveFirstpage() {
    this.pageData = this.tempData.slice(0, this.noOfRecordsPerPage);
    this.currentpage = 1;
  }
  moveLastPage() {
    const lastpage = Math.ceil(this.totalRecords / this.noOfRecordsPerPage);
    this.pageData = this.tempData.slice(this.noOfRecordsPerPage * (lastpage - 1), lastpage * this.noOfRecordsPerPage);
    this.currentpage = lastpage;
  }

  ngOnInit() {
    this.name  = this.route.snapshot.params['name'];
    this.category  = this.route.snapshot.params['category'];
    this.selectedLine = this.selectLine[this.route.snapshot.params['date']].name;
  
    if (window.innerWidth < 768) {
      this.mobileapp = false;
      } else {
        this.mobileapp = true;
      }
  }
  gotopreviouspage() {
    window.history.back();
  }
  close() {
    this.visible = false;
  }
  modal(i) {
       this.TMODetails = [
      { desc: 'T-Mobile ID', value: 'reallyreallylongname@google.com' },
      { desc: 'Category', value: this.pageData[i]['call-type'] },
      { desc: 'Number', value: this.pageData[i]['call-number']},
      { desc: 'Destination', value: this.pageData[i]['location']},
      { desc: 'Type', value: this.pageData[i]['call-type'] },
      { desc: 'Duration', value: this.pageData[i]['duration'] },
      { desc: 'Date & Time', value: this.pageData[i]['date'] + ' ' + this.pageData[i]['time'] },
      { desc: 'Charge', value: this.pageData[i]['call-charges'] }
    ];
    this.visible = true;
  }
  public handleLine(callBackValue: string) {
    this.filterdata = false;
    this.filterjson = [];
    if (callBackValue !== '') {
      this.selectedLine = callBackValue;
      this.paginateData(this.accountHistoryData);
    }
  }



 public closeFunction(i)
 {

  this.filterjson.splice(i, 1);
  this.filterdata = true;
  this.tempData = this.temporiginal;
  //console.log(this.filterjson)
   this.funapplyfilter();
 }
 
 public funapplyfilter() {
   this.pageData = [];
   
  // this.pageData = this.tempData;
  //if (this.filterjson.length === 0) {
    this.pageData = this.tempData;
  //}
  if(this.mobileapp !== false)
  {
    //console.log(this.filterjson);
   for (const data of this.filterjson) {
    if (data.desc.indexOf(',') !== -1) {
      let desc = data.desc.split(',');
     // console.log(desc);
      this.pageData = this.pageData.filter(e => e[desc[0]] + ',' + e[desc[1]] === data.data);
    }
    else {
    this.pageData = this.pageData.filter(e => e[data.desc] === data.data);
  }
}
 }
 this.currentpage = 1;
 this.totalRecords = this.pageData.length;
 this.noOfRecordsPerPage = 5;
 this.tempData=this.pageData;
 // console.log(this.pageData );
 this.filteredCount(this.category, this.pageData);
 this.pageData = this.pageData.slice(0, this.noOfRecordsPerPage);


 }
 filteredCount = function(category, tempData) {
  if (category === 'Data' || category === 'Mobile Hotspot' || category === 'Data stash'
  || category === 'Data pass' || category === 'T-Mobile purchases' || category === 'Third party purchases') {
    this.dataCal = 0;
    for (let i = 0; i < tempData.length; i++) {
    this.dataCal = Math.ceil(this.dataCal + parseFloat(tempData[i]['usage'].substring(0, (tempData[i]['usage'].length - 2))));
    }
    if (this.dataCal >= 1024) {
    this.dataCal = (this.dataCal / 1024).toFixed(2) + ' GB';
    } else {
      this.dataCal = (this.dataCal).toFixed(2) + ' MB';
    }
  }
  if (category === 'Calls') {
    let num;
    for (let i = 0; i < tempData.length; i++) {
      num = tempData[i]['duration'].match(/\d+/g).map(Number);
      if (num.length === 2) {
        num = (num[0] * 60) + num[1];
      } else {
        num = num[0] * 60;
      }
      this.totalminutes += num;
    }
    this.totalminutes = Math.round(this.totalminutes / 60 );
    this.totalCharges = 0;
    for (let i = 0; i < tempData.length; i++) {
      if (tempData[i]['call-charges'] === '-') {
        num = 0;
      } else {
        num = parseFloat(tempData[i]['call-charges'].substring(1, ));
      }
      this.totalCharges = this.totalCharges + num ;
    }
  }
}

 @HostListener('window:resize', ['$event'])
 onResize(event) {
     
     if (event.target.innerWidth < 768) {
      this.mobileapp = false;
      if(this.filterdata === true)
      {
       this.restorefilter = true;
      }
      
      this.filterdata = false;
      if (this.temporiginal !== undefined) {
      this.tempData = this.temporiginal;
      }
      } else {
        this.mobileapp = true;
        if(this.restorefilter === true)
        {
          this.filterdata = true;
        }
       }
       console.log(this.restorefilter);
      this.funapplyfilter();
 }
}
