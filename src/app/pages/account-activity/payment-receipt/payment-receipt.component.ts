import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment-receipt',
  templateUrl: './payment-receipt.component.html',
  styleUrls: ['./payment-receipt.component.scss']
})
export class PaymentReceiptComponent implements OnInit {

  constructor() {
    window.scrollTo(0, 0);
  }

  ngOnInit() {
  }

}
