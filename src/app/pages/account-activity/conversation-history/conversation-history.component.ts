import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-conversation-history',
  templateUrl: './conversation-history.component.html',
  styleUrls: ['./conversation-history.component.scss']
})
export class ConversationHistoryComponent implements OnInit {
public chatObj: Array<Object> = [
  { user: 'T-Mobile Customer', date: '9:00, Mar 27', message: 'Hi I should be getting a bill credit for my black friday free iphone deal. I got it for a couple of months and then it disappeared.', supportTeam: false },
  { user: 'SarahJ', date: '9:05, Mar 27', message: 'I did the research on your account and I do see that the credit this month is delayed. I see that it is pending, so you will see 2 credits on your next bill. Does that make sense?', supportTeam: true },
  { user: 'T-Mobile Customer', date: '9:00, Mar 27', message: 'Yeah I guess that\'s nice to know.  But why does it take two months?  I should be seeing in on my bill immediately right?', supportTeam: false },
  { user: 'SarahJ', date: '9:05, Mar 27', message: 'Hi Joe, I am back! Just message me if you had any other question ???', supportTeam: true }];
  constructor() { }

  ngOnInit() {
  }
}
