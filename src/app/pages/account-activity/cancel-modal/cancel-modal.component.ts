import { Component } from '@angular/core';

@Component({
  selector: 'app-cancel-modal',
  templateUrl: './cancel-modal.component.html',
  styleUrls: ['./cancel-modal.component.scss']
})
export class CancelModalComponent {
visible: boolean;
constructor() { }
  close() {
    this.visible = false;
  }
}
