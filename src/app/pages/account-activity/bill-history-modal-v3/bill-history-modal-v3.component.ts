import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bill-history-modal-v3',
  templateUrl: './bill-history-modal-v3.component.html',
  styleUrls: ['./bill-history-modal-v3.component.scss']
})

export class BillHistoryModalV3Component implements OnInit {

  constructor() {
    window.scrollTo(0, 0);
   }

  public popupOpen: boolean = false;

  ngOnInit() {
  }

  openDialog() {
    this.popupOpen = true;
    //document.body.style.overflow = "hidden";
    //document.body.style.height = "100%";
  }

  closeDialog() {
    this.popupOpen = false;
  }

  openDialogV2() {
    this.popupOpen = true;
    //document.body.style.overflow = "hidden";
    //document.body.style.height = "100%";
    window.scrollTo(0, 0);
  }

  closeDialogV2() {
    this.popupOpen = false;
    //document.body.style.overflow = "visible";
    //document.body.style.height = "auto";
    window.scrollTo(0, 0);
  }


  onEvent(event) {
    event.stopPropagation();
  }

  onOutsideClickEvent(event) {
    //document.body.style.overflow = "visible";
    //document.body.style.height = "auto";
    window.scrollTo(0, 0);
    event.stopPropagation();
    
  }
}
