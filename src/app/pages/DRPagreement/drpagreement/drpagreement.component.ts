import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-drpagreement',
  templateUrl: './drpagreement.component.html',
  styleUrls: ['./drpagreement.component.scss']
})
export class DrpagreementComponent implements OnInit {
  deviceRecoveryTerms: Array<any> = [
    { "title" : "Device Recovery Terms",
      "data":[ { descTitle: 'Channel', value: 'Web' },
               { descTitle: 'Customer', value: 'Arnold Schwarzenegger' },
               { descTitle: 'Date', value: '11/30/17'},
               { descTitle: 'Account', value: '930204029'},
               { descTitle: 'Billing address', value:["Arnold Schwarzenegger","1234 Something street","Bellevue, WA  98004"]}
              //{ descTitle: 'Billing address', value:[{AddressLine1:"Arnold Schwarzenegger"},{AddressLine2:"1234 Something street"},{AddressLine3:"Bellevue, WA  98004"}]}
              ]}
    ];

    deviceRecoveryProgram: Array<any> = [
      { "title" : "DEVICE RECOVERY PROGRAM - DEVICE DETAILS - ACCEPTED OFFER",
        "data":[ { descTitle: 'T-Mobile #', value: '(123) 456-7890' },
                 { descTitle: 'Make - Model', value: 'Apple iPhone 6 Plus 64GB Grey - T-Mobile - MG23LL' },
                 { descTitle: 'Serial #', value: '2818349092839482918'},
                 { descTitle: 'Offer Price', value: '$109.00'},
                ]}
      ];
  //Below variable to define cols  
  tMobileTitle ="T-Mobile #";
  makeModelTitle = "Make - Model";
  customerTitle = "Customer";
  billAddressTitle = "Billing address";
  constructor() { }

  ngOnInit() {
  }

}
