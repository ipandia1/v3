import { Component, OnInit, HostListener } from '@angular/core';
import { transition } from '@angular/animations';

@Component({
  selector: 'app-order-status',
  templateUrl: './order-status.component.html',
  styleUrls: ['./order-status.component.scss']
})
export class OrderStatusComponent implements OnInit {

  isSmartPhone = false;
  isDevicemd =false;
  showMore: boolean = false;
 
  showMoreIndex = 3;
  constructor() {

   }
  ngOnInit() {
  
    this.isDeviceuptomd();
  }
  public isDeviceSmartPhone() {
    let isDeviceSmartPhone = "false";
    if (window.innerWidth <= 767) {
      this.isSmartPhone = true;
      isDeviceSmartPhone = "true";
    }
    return isDeviceSmartPhone;
  }
  public isDeviceuptomd() {
    let isDeviceuptomd = "false";
    if (window.innerWidth <= 991) {
      this.isDevicemd = true;
      isDeviceuptomd = "true";
    }
    return isDeviceuptomd;
  }

  
  @HostListener('window:resize', ['$event'])
  onResize(event) { //To handle, UI logics for rotation of devices
    
    this.isDeviceSmartPhone();
    this.isDeviceuptomd();
   //console.log()
 }


  accessoriesList: Array<any> = [{ img: "assets/img/ssu/Beats.PNG", value: "Beats Solo3 Wireless On-Ear Headphone- Rose Gold" },
  { img: "assets/img/ssu/Megaboom.PNG", value: "Ultimate Ears MEGABOOOM Wireless Bluetooth&reg; Speaker" },
  { img: "assets/img/ssu/LGwireless.PNG", value: "LG Force Bluetooth Wireless Headset" },
  { img: "assets/img/ssu/samsung.png", value: "Apple iPhone 7 LifeProof&reg; FRE&reg; Case - Black" },
  { img: "assets/img/ssu/Trueboom.PNG", value: "Hasselblad True Zoom" },
  { img: "assets/img/ssu/holder.png", value: "August Smart Lock Pro and Connect, 3rd Generation - Silver" }
  ];


  availableStatus: Array<any> = [
    {
      "title": "Shipped",
     
      "image":  { 
        "imagexs": ('assets/img/SVGs/Logos/shipped.svg'), 
        "imagesm": ('assets/img/SVGs/Logos/shippedxs.svg')},
      "currentStatus": false,
      "estdddate": "N/A",
      "estddday": " ",
      "estddmonth": " ",
      "weight": "2.90 ",
      "data": [
        { "subtitle": "Ship to", value: "David R Smith", value1: "330 Mckinley Ave, Piqua", value2: "OH, 45356" }
      ]
    },
    {
      "title": "Shipped",
      
      "image":  { 
        "imagexs": ('assets/img/SVGs/Logos/shipped.svg'), 
        "imagesm": ('assets/img/SVGs/Logos/shippedxs.svg')},
      "currentStatus": true,
      "estdddate": "14",
      "estddday": "Thursday",
      "estddmonth": "December",
      "weight": "2.90 ",
      "data": [
        { "subtitle": "Ship to", value: "David R Smith", value1: "330 Mckinley Ave, Piqua", value2: "OH, 45356" }
      ]
    },
    {
      "title": "In transit",
      
      "image":  { 
        "imagexs": ('assets/img/SVGs/Logos/Transit.svg'), 
        "imagesm": ('assets/img/SVGs/Logos/Transitxs.svg')},
      "currentStatus": false,
      "estdddate": "14",
      "estddday": "Thursday",
      "estddmonth": "December",
      "weight": "2.90 ",
      "data": [
        { "subtitle": "Ship to", value: "David R Smith", value1: "330 Mckinley Ave, Piqua", value2: "OH, 45356" }
      ]
    },
    {
      "title": "Attempted delivery",
      
      "image":  { 
        "imagexs": ('assets/img/SVGs/Logos/Transit.svg'), 
        "imagesm": ('assets/img/SVGs/Logos/Transitxs.svg')},
      "currentStatus": false,
      "estdddate": "14",
      "estddday": "Thursday",
      "estddmonth": "December",
      "weight": "2.90 ",
      "data": [
        { "subtitle": "Ship to", value: "David R Smith", value1: "330 Mckinley Ave, Piqua", value2: "OH, 45356" }
      ]
    },
    {
      "title": "Return to sender",
   
      "image":  { 
        "imagexs": ('assets/img/SVGs/Logos/Return.svg'), 
        "imagesm": ('assets/img/SVGs/Logos/Returnxs.svg')},
      "currentStatus": false,
      "estdddate": "14",
      "estddday": "Thursday",
      "estddmonth": "December",
      "weight": "2.90 ",
      "data": [
        { "subtitle": "Ship to", value: "David R Smith", value1: "330 Mckinley Ave, Piqua", value2: "OH, 45356" }
      ]
    },
    {
      "title": "Delivered",
      
      "image":  { 
        "imagexs": ('assets/img/SVGs/Logos/Delivered.svg'), 
        "imagesm": ('assets/img/SVGs/Logos/Deliveredxs.svg')},
      "currentStatus": false,
      "estdddate": "14",
      "estddday": "Thursday",
      "estddmonth": "December",
      "weight": "2.90 ",
      "data": [
        { "subtitle": "Ship to", value: "David R Smith", value1: "330 Mckinley Ave, Piqua", value2: "OH, 45356" }
      ]
    },
  ];
  latestActivity: Array<any> = [{ date: " Dec 30", time: "8:39 AM", status: "Billing information received", desc: "" },
  { date: " Dec 29", time: "4:52 PM", status: "Delivered", desc: "Marcellus, NY" },
  { date: " Dec 27", time: "10:29 AM", status: "Destination Scan", desc: "E syracuse, NY" },
  { date: " Dec 27", time: "10:01 AM", status: "Destination Scan", desc: "E syracuse, NY" },
  { date: " Dec 27", time: "10:01 AM", status: "Destination Scan", desc: "E syracuse, NY" }
  ];
  trackingNumber: Array<any> = [{ img: "http://fpoimg.com/40x47", title: "Tracking Number", value: "1ZF1033WYW53397743" }];
  estdddate = "N/A";
  attempteddel = "Attempted delivery";
  delivered = "Delivered";
  returntosender = "Return to sender";
}
