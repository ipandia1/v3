import { Component, Input, Output } from '@angular/core';

@Component({
  selector: 'app-shop-page-modal',
  templateUrl: './shop-page-modal.component.html',
  styleUrls: ['./shop-page-modal.component.scss']
})
export class ShopPageModalComponent {
visible: boolean;
  constructor() { }
    close() {
    this.visible = false;
  }
}
