import { Component, OnInit, Input } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import {Location} from '@angular/common';

@Component({
  selector: 'app-shop-terms-conditions',
  templateUrl: './shop-terms-conditions.component.html',
  styleUrls: ['./shop-terms-conditions.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({ transform: 'scale(.6)' }),
        animate(300)
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'scale(0.6)' }))
      ])
    ])
  ]
})
export class ShopTermsConditionsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
