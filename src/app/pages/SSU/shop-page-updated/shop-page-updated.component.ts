import { Component, OnInit, Input, Output, HostBinding, HostListener } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-shop-page-updated',
  templateUrl: './shop-page-updated.component.html',
  styleUrls: ['./shop-page-updated.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({ transform: 'scale(.6)' }),
        animate(150)
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'scale(0.6)' }))
      ])
    ])
  ]
})
export class ShopPageUpdatedComponent implements OnInit {

  stickShow: boolean;
  stickShow1: boolean;
  visible: boolean;
  visible1: boolean;
  stickNav: boolean;
  stickNavHeader: boolean;
  stickNav1: boolean;
  stickNavHeader1: boolean;
  noStick: boolean;
  alignTotTop: boolean;
  require: any;
  isinverted = true;
  eyebrowImgBlock = 'assets/img/ssu/Artboard.png';
  eyebrowImgInverted = 'assets/img/ssu/Artboard_inverted.png';
  
  heroBackgroundDesktopSrc = "assets/img/ssu/plain-desktop.png";
  heroBackgroundTabletSrc = "assets/img/ssu/plain-tablet.png";
  heroBackgroundMobileSrc = "assets/img/ssu/plain-mobile.png";

  halfbanner_BackgroundDesktopSrc = "assets/img/ssu/plain-tablet.png";
  halfbanner_heroBackgroundTabletSrc = "assets/img/ssu/plain-mobile.png";
  halfbanner_heroBackgroundMobileSrc = "assets/img/ssu/plain-mobile.png";

// Toggle to change theme for text
  darkTheme = true;
// Toggle to change theme for CTA
  darkCTATheme = false; // true for dark theme
  lightCTATheme = false; // true for light theme

// For all styles - eyebrow,subHeadLine,body,legal
  dark_textColor = 'white'; // dark_textColor = '#e20074'; -- for magenta
  light_textColor = 'grey';
  magenta_textColor = '#e20074';
  dark_headLine = 'white'; // can change to magenta for headLine
  light_headLine = 'black';

  // For Banner Position

  portrait = false;
  landscape = false;

  mobileView = false;
  tabletView = false;
  desktopView = false;

  // Default Banner Position Values
  banner_pos_Mobile = 'top';
  banner_pos_Tablet = 'top';
  banner_pos_Desktop = 'top';

  half_banner_pos_Mobile = 'top';
  half_banner_pos_Tablet = 'top';
  half_banner_pos_Desktop = 'top';
 
  bannerPosition = function () {

    var windowHeight = window.innerHeight;
    var windowWidth = window.innerWidth;

    if (windowHeight > windowWidth) {
      this.portrait = true;
      this.landscape = false;
    } else {
      this.portrait = false;
      this.landscape = true;
    }

    if (windowWidth < 768) { // For Mobiles

        this.mobileView = true;
        this.tabletView = false;
        this.desktopView = false;

        if(this.portrait==true)
        {
          this.banner_pos_Mobile = 'top'; // change value here for Mobiles
          this.half_banner_pos_Mobile = 'top';
        } else {
          this.banner_pos_Mobile = 'top'; // change value here for Mobiles
          this.half_banner_pos_Mobile = 'top';
        }

    } else if (windowWidth >= 768 && windowWidth < 992 ) { // For Tablets

        this.mobileView = false;
        this.tabletView = true;
        this.desktopView = false;

        if(this.portrait==true)
        {
          this.banner_pos_Tablet = 'top'; // change value here for Tablets
          this.half_banner_pos_Tablet = 'top';
        } else {
          this.banner_pos_Tablet = 'top'; // change value here for Tablets
          this.half_banner_pos_Tablet = 'top';
        }

    } else if (windowWidth >= 992) { // For Desktops

       this.mobileView = false;
       this.tabletView = false;
       this.desktopView = true;

       this.banner_pos_Desktop = 'top'; // change value here for Desktops
       this.half_banner_pos_Desktop = 'top';
    }
  }

  navBarLegalTextPos = function () {
    var windowHeight = window.innerHeight;
    var windowWidth = window.innerWidth;
    var y = window.pageYOffset;

    /*Sticky for Popular Phones Div */

    var bannerDiv = document.getElementById('bannerDiv');
    var bannerHeight = bannerDiv.getBoundingClientRect().height;
    var holidayDiv = document.getElementById('holidayDiv');
    var holidayHeight = holidayDiv.getBoundingClientRect().height;
    // var legalDiv1 = window.getComputedStyle(document.getElementById('fixedDiv1'));
    // var legalDivPaddingTop1 = parseInt(legalDiv1.getPropertyValue('padding-top').split("px")[0]);
    // var legalDivLineHeight1 = parseInt(legalDiv1.getPropertyValue('line-height').split("px")[0]);
    var legalDiv1 = document.getElementsByClassName('fixedDiv1')[0];
    var legalDivPaddingTop1 = legalDiv1.getBoundingClientRect().height - 12 - 16;
    var navbarDiv = document.getElementsByClassName('nav-bar-styles')[0];
    var navBarHeight = navbarDiv.getBoundingClientRect().height;
    var imgDiv = document.getElementById('imgDiv');
    var imgHeight = imgDiv.getBoundingClientRect().height;
    var priceDiv1 = document.getElementsByClassName('priceDiv1')[0];
    var priceDiv1Height = priceDiv1.getBoundingClientRect().height;
    var popularTitleDiv = document.getElementsByClassName('title2')[0];
    var popularTitleHeight = popularTitleDiv.getBoundingClientRect().height;
    var imgDiv1 = document.getElementsByClassName('img0')[0];
    var imgDiv1Height = imgDiv1.getBoundingClientRect().height;
    var phoneTitle1 = document.getElementsByClassName('phoneTitle00')[0];
    var phoneTitle1Height = phoneTitle1.getBoundingClientRect().height;
    var priceDiv2 = document.getElementsByClassName('priceDiv2')[0];
    var priceDiv2Height = priceDiv2.getBoundingClientRect().height;
    var leftTextDiv1 = document.getElementsByClassName('leftText00')[0];
    var leftTextHeight1 = leftTextDiv1.getBoundingClientRect().height;
    var textHeight1 = leftTextHeight1 - priceDiv2Height;
    var consolidatedHeight1 = bannerHeight + navBarHeight + holidayHeight + popularTitleHeight + textHeight1 - 60 - windowHeight;

    if (windowWidth < 576) {
      var consolidatedHeight1 = bannerHeight + navBarHeight + holidayHeight + popularTitleHeight + textHeight1 - 30 - windowHeight;
    }
    if (windowWidth > 768) {
      var consolidatedHeight1 = bannerHeight + navBarHeight + holidayHeight + popularTitleHeight + textHeight1 - 90 - windowHeight;
    }
    if (windowWidth > 991) {
      var consolidatedHeight1 = bannerHeight + navBarHeight + holidayHeight + popularTitleHeight + textHeight1 - 70 - windowHeight;
    }

    var consolidatedPopularHeight = bannerHeight + navBarHeight + holidayHeight + imgHeight +
      legalDivPaddingTop1 - 15 - 32 - windowHeight;

    if (windowWidth < 576) {
      consolidatedPopularHeight = consolidatedPopularHeight + 65;
    }

    if ((windowWidth > 576) || (y >= consolidatedHeight1)) {
      if (y >= consolidatedHeight1 && y < consolidatedPopularHeight) {
        this.stickShow1 = true;
      } else {
        this.stickShow1 = false;
      }
    } else {
      this.stickShow1 = false;
    }

    /* Sticky for Valentine Div */

    var valentineDiv = document.getElementsByClassName('valentineDiv')[0];
    var valentineDivHeight = valentineDiv.getBoundingClientRect().height;
    var valentineTitle = document.getElementsByClassName('title1')[0];
    var valentineTitleHeight = valentineTitle.getBoundingClientRect().height;
    var promoTitle = document.getElementsByClassName('promoTitle0')[0];
    var promoTitleHeight = promoTitle.getBoundingClientRect().height;
    var phoneTitle = document.getElementsByClassName('phoneTitle0')[0];
    var phoneTitleHeight = promoTitle.getBoundingClientRect().height;
    var priceDiv1 = document.getElementsByClassName('priceDiv1')[0];
    var priceDiv1Height = priceDiv1.getBoundingClientRect().height;
    // var legalDiv2 = window.getComputedStyle(document.getElementById('fixedDiv2'));
    // var legalDivPaddingTop2 = parseInt(legalDiv2.getPropertyValue('padding-top').split("px")[0]);
    // var legalDivLineHeight2 = parseInt(legalDiv2.getPropertyValue('line-height').split("px")[0]);
    // var legalDiv2 = document.getElementsByClassName('fixedDiv2')[0];
    // var legalDivPaddingTop2 = legalDiv2.getBoundingClientRect().height;
    var leftTextDiv = document.getElementsByClassName('leftText0')[0];
    var leftTextHeight = leftTextDiv.getBoundingClientRect().height;
    var textHeight = leftTextHeight - priceDiv1Height;

    var valentineStickyHeight = bannerHeight + navBarHeight + valentineTitleHeight +
      textHeight - 30 - windowHeight;

    if (windowWidth < 576) {
      var valentineStickyHeight = bannerHeight + navBarHeight + valentineTitleHeight +
        textHeight - 25 - windowHeight;
    }

    if (windowWidth > 768) {
      var valentineStickyHeight = bannerHeight + navBarHeight + valentineTitleHeight +
        textHeight - 60 - windowHeight;
    }
    if (windowWidth > 991) {
      var valentineStickyHeight = bannerHeight + navBarHeight + valentineTitleHeight +
        textHeight - 50 - windowHeight;
    }

    var consolidatedValentineHeight = bannerHeight + navBarHeight + valentineDivHeight - 30 - windowHeight;

    if ((windowWidth > 576)) {
      var consolidatedValentineHeight = bannerHeight + navBarHeight + valentineDivHeight - 40 - windowHeight;
    }

    if ((windowWidth > 576) || (y >= valentineStickyHeight)) {
      if (y >= valentineStickyHeight && y < consolidatedValentineHeight) {
        this.stickShow = true;
      } else {
        this.stickShow = false;
      }
    } else {
      this.stickShow = false;
    }


    /*************** Nav Bar code ***************/
    var consolidatedNavHeight = bannerHeight + 48 + 10 - windowHeight;
    var consolidatedNavHeight1 = bannerHeight + navBarHeight - windowHeight;

    if ((windowWidth >= 576) || (y >= consolidatedNavHeight)) {
      if (y <= consolidatedNavHeight) {

        this.stickNav = true;
        this.stickNavHeader = false;
        this.noStick = false;
      } else if (y > (consolidatedNavHeight) && y < (consolidatedNavHeight + windowHeight - 48 - 10)) {
        this.stickNav = false;
        this.stickNavHeader = false;
        this.noStick = true;
      } else {
        this.stickNav = false;
        this.stickNavHeader = true;
        this.noStick = false;
      }

      if (y <= consolidatedNavHeight1) {
        this.stickNav = true;
        this.stickNavHeader = false;
      } else if (y > (consolidatedNavHeight1) && y < (consolidatedNavHeight1 + windowHeight - 48 + 8)) {
        this.stickNav1 = false;
        this.stickNavHeader1 = false;
      } else {
        this.stickNav1 = true;
        this.stickNavHeader1 = true;
      }
    }
    if (windowWidth < 576) {
      this.stickNav = false;
      this.stickNavHeader = false;
    }
    if (windowWidth < 576 && y <= consolidatedNavHeight1) {
      this.stickNav = true;
      this.stickNavHeader = false;
    }
  }
  @HostListener('window:load', [])

  onWindowLoad() {
    this.navBarLegalTextPos();
    this.bannerPosition();
  }

  @HostListener('window:scroll', [])

  onWindowScroll() {
    this.navBarLegalTextPos();
    this.bannerPosition();

  }
  @HostListener('window:resize', [])
  onResize() { this.navBarLegalTextPos();
    this.bannerPosition();
   }

  constructor() { }

  ngOnInit() {
  }
  public navObj: Array<Object> = [
    { id: '1', name: 'Apple' },
    { id: '2', name: "Samsung" },
    { id: '3', name: "All phones" },
    { id: '4', name: "Latest deals" },
    { id: '5', name: "Accessories" },
    { id: '6', name: "Add a line" }];

  navObj1 = this.navObj.slice(2, 6);


  public dataArray: Array<Object> = [
    {
      id: '1', imageLink: 'assets/img/ssu/iPhone8.png', title: 'Samsung Galaxy S8',
      todayMoney: '$279.99', monthlyMoney: '$30.00',
      tax: 'down + tax ', mos: 'x24 mos.', off: '$200', verbiage: "Get $300 off with trade-in via monthly bill credits", promo: "Special offer"
    },
    {
      id: '2', imageLink: 'assets/img/ssu/iPhone8.png', title: 'Apple iPhone 7 Apple iPhone 7',
      todayMoney: '$279.99', monthlyMoney: '$30.00',
      tax: 'down + tax ', mos: 'x24 mos.', off: '$200', verbiage: "", promo: ""
    },
    {
      id: '3', imageLink: 'assets/img/ssu/iPhone8.png', title: 'Apple iPhone 7 Plus',
      todayMoney: '$279.99', monthlyMoney: '$30.00',
      tax: 'down + tax ', mos: 'x24 mos.', off: '$200', verbiage: "", promo: "Special offer"
    },
    {
      id: '4', imageLink: 'assets/img/ssu/iPhone8.png', title: 'Samsung Galaxy S7',
      todayMoney: '$279.99', monthlyMoney: '$30.00',
      tax: 'down + tax ', mos: 'x24 mos.', off: '$200', verbiage: "Get $300 off with trade-in via monthly bill credits", promo: ""
    },
    {
      id: '5', imageLink: 'assets/img/ssu/iPhone8.png', title: 'Title Title Title Title Title Title Title Title',
      todayMoney: '$279.99', monthlyMoney: '$30.00',
      tax: 'down + tax ', mos: 'x24 mos.', off: '$200', verbiage: "Get $300 off with trade-in via monthly bill credits", promo: ""
    },
    {
      id: '6', imageLink: 'assets/img/ssu/iPhone8.png', title: 'Apple iPhone 8',
      todayMoney: '$279.99', monthlyMoney: '$30.00',
      tax: 'down + tax ', mos: 'x24 mos.', off: '$200', verbiage: "", promo: "Special offer"
    },
  ];
  public holArray: Array<Object> = [{
    id: '1', imageLink: 'assets/img/ssu/iPhone8.png', promoTitle: 'Promo title Promo title Promo title', title: 'Samsung Galaxy S8',
    todayMoney: '$279.99', monthlyMoney: '$30.00',
    tax: 'down + tax ', mos: 'x24 mos.'
  },
  {
    id: '2', imageLink: 'assets/img/ssu/iPhone8.png', promoTitle: 'Promo title Second title Second title', title: 'Apple iPhone 7 Apple iPhone 7 Apple iPhone 7',
    todayMoney: '$279.99', monthlyMoney: '$30.00',
    tax: 'down + tax ', mos: 'x24 mos.'
  },
  {
    id: '3', imageLink: 'assets/img/ssu/iPhone8.png', promoTitle: 'Promo title Second title Second title', title: 'Apple iPhone 7 Apple iPhone 7 Apple iPhone 7',
    todayMoney: '$279.99', monthlyMoney: '$30.00',
    tax: 'down + tax ', mos: 'x24 mos.'
  },
  {
    id: '4', imageLink: 'assets/img/ssu/iPhone8.png', promoTitle: 'Promo title Second title Second title', title: 'Apple iPhone 7 Apple iPhone 7 Apple iPhone 7',
    todayMoney: '$279.99', monthlyMoney: '$30.00',
    tax: 'down + tax ', mos: 'x24 mos.'
  }];
  firstDiv = function () {
    var scrollIntoView = require('scroll-into-view');
    this.alignToTop = true;
    var latestDiv = document.getElementById('latestDeals');
    scrollIntoView(latestDiv,
      {
        time: 1000
      });
  }

  normalDiv = function () {
    var scrollIntoView = require('scroll-into-view');
    this.alignToTop = true;
    var latestDiv = document.getElementById('imgDiv');
    scrollIntoView(latestDiv,
      {
        time: 1000
      });
  }
  doNothing = function () {

  }

  modalDiv = function () {
    this.visible = !this.visible;
    let body = document.getElementsByTagName('body')[0];
    body.classList.add('noScroll');

  }
  legalDiv = function () {
    this.visible1 = !this.visible1;
    let body = document.getElementsByTagName('body')[0];
    body.classList.add('noScroll');

  }
  allPhonesDiv = function () {
    //alert("allPhonesDiv");
  }
  secondDiv = function () {
    //alert("secondDIV");
  }
  close() {
    this.visible = false;
    this.visible1 = false;
    let body = document.getElementsByTagName('body')[0];
    body.classList.remove('noScroll');
  }

}
