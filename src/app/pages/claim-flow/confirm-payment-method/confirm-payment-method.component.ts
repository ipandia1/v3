import { Component, OnInit, AfterViewInit, ElementRef, HostListener } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-claim-flow',
  templateUrl: './confirm-payment-method.component.html',
  styleUrls: ['./confirm-payment-method.component.scss']
})
export class confirmPayementMethodComponent implements OnInit {
  creditcard: creditCard;
  deleteModelVisible = false;
  isBank = false;
  cardIcon = this.isBank ? 'bankImg' : 'palinCardImg';

  constructor(private elementRef: ElementRef, private commonService: CommonService) {

  }
  ngOnInit() {
    this.creditcard = {
      creditCardNumber: ''
    };
  }
  deleteModelOpen() {
    this.deleteModelVisible = !this.deleteModelVisible;
    this.commonService.showScroll(false);
    this.elementRef.nativeElement.querySelector('#deleteModal').focus();
  }
  deleteModelClose() {
    this.deleteModelVisible = !this.deleteModelVisible;
    this.commonService.showScroll(true);
    this.elementRef.nativeElement.querySelector('#deleteModelLink').focus();
  }
  @HostListener('window:resize', ['$event'])
  onResize() {
    if (this.deleteModelVisible === true) {
      this.commonService.showScroll(false);
      this.elementRef.nativeElement.querySelector('#deleteModal').focus();
    }
  }
  cardChange(event) {
    let target = event.target || event.srcElement;
    let val = target.value.split('')[0];
    if (this.isBank) {
      this.cardIcon = 'bankImg';
    } else {
      if (val === '4') {
        this.cardIcon = 'visaImg';
      } else if (val === '5') {
        this.cardIcon = 'masterImg';
      } else if (val === '1') {
        this.cardIcon = 'expressImg';
      } else {
        this.cardIcon = 'palinCardImg';
      }
    }
  }
  backTabEvent(event, mainId, lastId) {
    var me = this;
    const allElements = document.getElementById(mainId).querySelectorAll
      ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
    if (document.activeElement === allElements[0]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          setTimeout(function () {
            // document.getElementById(lastId).focus();
            me.elementRef.nativeElement.querySelector('#' + lastId).focus();

          }, 0);
        }
      }
    }
    if (document.activeElement === document.getElementById(mainId)) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          setTimeout(function () {
            // document.getElementById(lastId).focus();
            me.elementRef.nativeElement.querySelector('#' + lastId).focus();

          }, 0);
        }
      }
    }
    if (document.activeElement === allElements[allElements.length - 1]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) { }
        else {
          event.preventDefault();
          setTimeout(function () {
            // document.getElementById(mainId).focus();
            me.elementRef.nativeElement.querySelector('#' + mainId).focus();

          }, 0);
        }
      }
    }
  }
}
export class creditCard {
creditCardNumber;
}
