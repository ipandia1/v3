import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent {
  faqArray: Array<any> = [
    {
      title: 'What is AutoPay?',
      content: 'AutoPay is a service that allows you to set up recurring monthly payments for your bill. We will automatically withdraw your payment each month, using your preferred payment method.'
    },
    {
      title: 'How does AutoPay determine the amount to be processed?',
      content: 'AutoPay processes the total balance on the account. If your account balance is zero for the given billing cycle, a payment will not be processed.'
    },
    {
      title: 'What are the benefits of using AutoPay?',
      content: 'Never worry about missing a payment or incurring late fee. Simply select a payment method of your choice and let AutoPay handle your monthly payments moving forward.'
    },
    {
      title: 'How do I sign up for AutoPay?',
      content: 'On the AutoPay Landing Page: ',
      list1: '1. Add a payment method (Select ‘Edit’ to modify the default selection if applicable)',
      list2: '2. Click AGREE & SUBMIT to accept the terms and conditions and complete setup'
    }
  ];
  constructor(private _location: Location) { }
  gotopreviouspage() {
    this._location.back();
  }
}
