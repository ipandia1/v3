import { Component, OnInit, ElementRef } from '@angular/core';
import { CommonService } from '../../../services/common.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-auto-pay-off3',
  templateUrl: './auto-pay-off3.component.html',
  styleUrls: ['./auto-pay-off3.component.scss']
})
export class AutoPayOff3Component implements OnInit {

  paymentMethod = 'Using my';
  cardNumber = '****1234';
  iconClass = 'visa-icon';
  visible = false;
  visible1 = false;
  constructor(private elementref: ElementRef,private commonService: CommonService, private _location: Location) { }

  ngOnInit() {
  }
  close() {
    this.visible = false;
    this.commonService.showScroll(true);
    document.getElementById('cancelAutopayLink').focus();
  }
  change() {
    this.visible = true;
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('noScroll');
    this.commonService.showScroll(false);
    this.cancelModalFocus();
  }
   cancelModalFocus() {
    const dialog = document.getElementById('CancelAutopayModal');
    dialog.focus();
  }
  gotopreviouspage() {
    this._location.back();
  }
  backTabEvent(event, mainId, lastId) {
    const allElements = document.getElementById(mainId).querySelectorAll
    ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
      if (document.activeElement === allElements[0]) {
        if (event.keyCode === 9) {
          if (event.shiftKey) {
            event.preventDefault();
            setTimeout(function() {
              document.getElementById(lastId).focus();
            }, 0);
          }
        }
      }
      if (document.activeElement === document.getElementById(mainId)) {
        if (event.keyCode === 9) {
          if (event.shiftKey) {
            event.preventDefault();
            setTimeout(function() {
              document.getElementById(lastId).focus();
            }, 0);
          }
        }
      }
      if (document.activeElement === allElements[allElements.length - 1]) {
        if (event.keyCode === 9) {
          if (event.shiftKey) {}
          else {
          event.preventDefault();
          setTimeout(function() {
            document.getElementById(mainId).focus();
          }, 0);
        }
      }
      }
      }
}
 