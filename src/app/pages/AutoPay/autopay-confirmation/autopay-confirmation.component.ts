import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-autopay-confirmation',
  templateUrl: './autopay-confirmation.component.html',
  styleUrls: ['./autopay-confirmation.component.scss']
})
export class AutopayConfirmationComponent implements OnInit {
    public smallDevice:boolean=false;
    public name: string = "Tom";
    public showDetails: boolean = false;

    public errormessage: string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque erat orci, pulvinar id diam.";
      
      
  constructor(private _location: Location) { }
  ngOnInit() { 
  var deviceHeight=window.innerHeight;
  if(deviceHeight<565){
    this.smallDevice=true;
    }
    else{
    this.smallDevice=false;
    }
  }

  public dataPassInfo: Array<Object> = [
    { descTitle: 'Customer', value: 'John Doe' },
    { descTitle: 'Name', value: 'John Doe' },
    { descTitle: 'Date', value: '12/01/2017' },
    { descTitle: 'Payment Method', value: 'Visa ****1234' },
    { descTitle: 'Zip', value: '98115' },
    { descTitle: 'AutoPay Status', value: 'Complete' },
    { descTitle: 'T-Mobile Account', value: '123456789' }
  ];

  public orderDetails: Array<Object> = [
    { descTitle: 'Name ID', value: '4.00', tax: '+ tax' },
    { descTitle: 'Scam Block', value: '0.00' }
  ];

  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  }
  gotopreviouspage() {
    this._location.back();
  }

}
