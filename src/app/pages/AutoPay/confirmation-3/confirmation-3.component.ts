import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
    selector: 'app-confirmation-3',
    templateUrl: './confirmation-3.component.html',
    styleUrls: ['./confirmation-3.component.scss']
  })

  export class Confirmation3Component implements OnInit {
    public smallDevice:boolean=false;
    public name: string = "Tom";
    public showDetails: boolean = false;

    public errormessage: string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque erat orci, pulvinar id diam.";
      
      
  constructor(private _location: Location) { }
  ngOnInit() { 
  var deviceHeight=window.innerHeight;
  if(deviceHeight<565){
    this.smallDevice=true;
    }
    else{
    this.smallDevice=false;
    }
  }

  public dataPassInfo: Array<Object> = [
    { descTitle: 'Name on account', value: 'John Doe' },
    { descTitle: 'T-Mobile account no.', value: '12346789' },
    { descTitle: 'Reference no.', value: '0123456789' },
    { descTitle: 'Authorization code', value: '0123456789' },
    { descTitle: 'Name on card', value: 'John Doe' },
    { descTitle: 'Payment method', value: 'Visa ****1234' },
    { descTitle: 'Zip', value: '98115' },
    { descTitle: 'Transaction type', value: 'Bill_Pay' }
  ];

  public orderDetails: Array<Object> = [
    { descTitle: 'Name ID', value: '4.00', tax: '+ tax' },
    { descTitle: 'Scam Block', value: '0.00' }
  ];

  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  }

  gotopreviouspage() {
    this._location.back();
  }

}
