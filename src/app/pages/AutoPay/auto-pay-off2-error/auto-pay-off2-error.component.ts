import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-auto-pay-off2-error',
  templateUrl: './auto-pay-off2-error.component.html',
  styleUrls: ['./auto-pay-off2-error.component.scss']
})
export class AutoPayOff2ErrorComponent implements OnInit {

  paymentMethod = 'Use my';
  cardNumber = '****1234';
  iconClass = 'visa-icon';
  constructor(private _location: Location) { }

  ngOnInit() {
  }
  gotopreviouspage() {
    this._location.back();
  }

}
