import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {Router} from '@angular/router';
@Component({
  selector: 'app-auto-pay-off2',
  templateUrl: './auto-pay-off2.component.html',
  styleUrls: ['./auto-pay-off2.component.scss']
})
export class AutoPayOff2Component implements OnInit {

  paymentData: Array<any> = [
    {paymentMethod : 'Use my',
    cardNumber : '****1234',
    cardNumberClass: '',
    iconClass : 'visa-icon',
    url: ''
    },
    {
      paymentMethod : 'Payment method',
      cardNumber : '****4567',
      cardNumberClass:'visibility-hidden',
      iconClass : 'apple-icon',
      url: ''
    },
    {
      paymentMethod : 'Payment method',
      cardNumber : '',
      cardNumberClass:'pl-0',
      iconClass : 'apple-icon',
      url: ''
    }
  ];
  paymentMethod: Array<any> = [
    { iconClass: 'apple-icon m-t-4', cardName: 'Apple Pay' },
    { iconClass: 'visa-icon m-t-4', cardName: '****1234' }
  ];
  constructor(private _location: Location, private _router: Router) { }

  ngOnInit() {
  }
  gotopreviouspage() {
    this._location.back();
  }
  faqLink() {
    this._router.navigate(['/faq']);
  }
}
