import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { CommonService } from '../../../services/common.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-device-promo-list',
  templateUrl: './device-promo-list.component.html',
  styleUrls: ['./device-promo-list.component.scss']
})
export class DevicePromoListComponent implements OnInit {


  public dataArray: Array<any> = [
    { tabTitle: 'Device Promo', dataTabCheck: true },
    { tabTitle: 'Service Promo', dataTabCheck: false }
  ];

  devicePromoList = 'assets/json/servicepromo/promolist.json';
  servicePromoList = 'assets/json/servicepromo/servicepromo.json';
  promotionData: any;
  servicePromoData: any;
  constructor(private _location: Location, private router: Router, private commonService: CommonService) {
    this.getPromoList(this.devicePromoList);
    this.getServicePromoList(this.servicePromoList);
  }
  private getPromoList(urlPath) {
    this.commonService.getJsonData(urlPath)
      .subscribe(responseJson => {
        this.promotionData = responseJson.data;
      });
  }
  private getServicePromoList(urlPath) {
    this.commonService.getJsonData(urlPath)
      .subscribe(responseJson => {
        this.servicePromoData = responseJson.data;
      });
  }
  ngOnInit() {
  }
  clickFun(val) {
    for (let i = 0; i < this.dataArray.length; i++) {
      this.dataArray[i].dataTabCheck = false;
    }
    this.dataArray[val].dataTabCheck = true;
  }
}
