import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevicePromoListComponent } from './device-promo-list.component';

describe('DevicePromoListComponent', () => {
  let component: DevicePromoListComponent;
  let fixture: ComponentFixture<DevicePromoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevicePromoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevicePromoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
