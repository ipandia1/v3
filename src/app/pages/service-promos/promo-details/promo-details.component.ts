import { Component, OnInit,HostListener } from '@angular/core';
import { CommonService } from '../../../services/common.service';
@Component({
  selector: 'app-promo-details',
  templateUrl: './promo-details.component.html',
  styleUrls: ['./promo-details.component.scss']
})
export class PromoDetailsComponent implements OnInit {
  mobileView = false;
  contentHeight = 0;
  constructor(private commonService: CommonService) { }
  promoDetailsJson = 'assets/json/servicepromo/promoDetails.json';
  promoDetails: any;
  ngOnInit() {
    this.getpromoDetails(this.promoDetailsJson);
    this.onScreenResize();
    this.setContentHeight();
  }
  private getpromoDetails(urlPath) {
    this.commonService.getJsonData(urlPath)
      .subscribe(responseJson => {
        this.promoDetails = responseJson.data;
      });
  }
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.onScreenResize();
    this.setContentHeight();
  }
  onScreenResize() {
    if (window.innerWidth < 768) {
      this.mobileView = true;
    } else {
      this.mobileView = false;
    }
  }
  setContentHeight() {
    Promise.resolve(null).then(() => {
      if (this.mobileView) {
        const innerHeight = window.innerHeight;
        this.contentHeight = innerHeight - 185;
      } else {
        this.contentHeight = 0;
      }
    });
  }

}
