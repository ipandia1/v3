import { Component, OnInit, TemplateRef,HostListener } from '@angular/core';
import { Location } from '@angular/common';
import { CommonService } from '../../../services/common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-padt-promos',
  templateUrl: './past-promos-list.component.html',
  styleUrls: ['./past-promos-list.component.scss']
})
export class PastPromosListComponent implements OnInit {
  mobileView = false;
  contentHeight = 0;
  public dataArray: Array<any> = [
    { tabTitle: 'Device Promos', dataTabCheck: true },
    { tabTitle: 'Service Promos', dataTabCheck: false }
  ];
  devicePromoList = 'assets/json/servicepromo/pastDevicePromo.json';
  servicePromoList = 'assets/json/servicepromo/pastServicePromo.json';
  promotionData: any;
  servicePromoData: any;
  constructor(private _location: Location, private router: Router, private commonService: CommonService) {
    this.getPromoList(this.devicePromoList);
    this.getServicePromoList(this.servicePromoList);
  }
  private getPromoList(urlPath) {
    this.commonService.getJsonData(urlPath)
      .subscribe(responseJson => {
        this.promotionData = responseJson.data;
      });
  }
  private getServicePromoList(urlPath) {
    this.commonService.getJsonData(urlPath)
      .subscribe(responseJson => {
        this.servicePromoData = responseJson.data;
      });
  }
  ngOnInit() {
    this.onScreenResize();
    this.setContentHeight();
  }
  clickFun(val) {
    for (let i = 0; i < this.dataArray.length; i++) {
      this.dataArray[i].dataTabCheck = false;
    }
    this.dataArray[val].dataTabCheck = true;
  }
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.onScreenResize();
    this.setContentHeight();
  }
  onScreenResize() {
    if (window.innerWidth < 768) {
      this.mobileView = true;
    } else {
      this.mobileView = false;
    }
  }
  setContentHeight() {
    Promise.resolve(null).then(() => {
      if (this.mobileView) {
        const innerHeight = window.innerHeight;
        this.contentHeight = innerHeight - 185;
      } else {
        this.contentHeight = 0;
      }
    });
  }
}
