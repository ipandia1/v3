import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PadtPromosComponent } from './past-promos-list.component';

describe('PadtPromosComponent', () => {
  let component: PadtPromosComponent;
  let fixture: ComponentFixture<PadtPromosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PadtPromosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PadtPromosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
