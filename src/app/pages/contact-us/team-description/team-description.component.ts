import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-team-description',
  templateUrl: './team-description.component.html',
  styleUrls: ['./team-description.component.scss']
})
export class TeamDescriptionComponent implements OnInit {
  showDetails = true;
  isDesktop: boolean;
  lastVal: any = -1;
  topTasks: Array<any> = [
    {
      title: 'Device & Coverage',
      data: [{ name: 'Lost/Stolen device' },
      { name: 'Trade in value' },
      { name: 'Device unlock status' },
      { name: 'File a damage claim' },
      { name: 'Upgrade device' },
      { name: 'Access voicemail' }]
    },
    {
      title: 'Billing & Payments',
      data: [{ name: 'Make a payment' },
      { name: 'Make device payment (EIP)' },
      { name: 'AutoPay settings' },
      { name: 'Paperless billing' },
      { name: 'View bill' },
      { name: 'Payment history' }]
    },
    {
      title: 'Plans & Usage',
      data: [{ name: 'Change service' },
      { name: 'View app purchases' },
      { name: 'View account usage' },
      { name: 'Add family allowances' }]
    },
    {
      title: 'Account & Profile',
      data: [{ name: 'View recent activity' },
      { name: 'Trade-in status' },
      { name: 'Check order status' },
      { name: 'Check rebate status' },
      { name: 'Suspend line' },
      { name: 'View family controls' }]
    }
  ]
  askOnTheGo: Array<any> = [
    { title: 'Account balance', iconClass: 'message_magenta', code: '#BAL# (#225#)'},
    { title: 'Minutes usage', iconClass: 'message_magenta', code: '#MIN# (#646#)'},
    { title: 'Message usage', iconClass: 'message_magenta', code: '#MSG# (#674#)'},
    { title: 'Data usage', iconClass: 'message_magenta', code: '#WEB# (#932#)'}
  ]
  socialSupport: Array<any> = [
    { title: 'Facebook', iconClass: 'fa fa-facebook', url: '#' },
    { title: 'Twitter', iconClass: 'fa fa-twitter', url: '#' },
    { title: 'Google+', iconClass: 'fa fa-google-plus', url: '#' },
    { title: 'Community', iconClass: 'fa fa-users', url: '#' }
  ]
  visible = false;
  inputtextfield: InputTextField;
  callBackVisible = false;
  confrmnVisible = false;
  scheduleConfrmnVisible = false;
  number = '';
  selectedLine1 = 'Mon 00, 0000';
  selectedLine2 = '00:00  AM';
  selectedLine3 = 'Pacific';
  dateDetails = [{ 'date': 'Mon 00, 0000' }, { 'date': 'Tue 00, 0000' }, { 'date': 'Wed 00, 0000' }];
  timeDetails = [{ 'time': '00:00  AM' }, { 'time': '01:00  AM' }, { 'time': '02:00  AM' }];
  timeZone = [{ 'zone': 'Pacific' }, { 'zone': 'India' }, { 'zone': 'Eastern' }];
  //dateTimeDetails = [{ 'time': '00:00 AM', 'date': 'MON 00' }];
  //dateTimeDetails = [{ 'time': '00:00 AM', 'date': 'MON 00' }, { 'time': '00:00 AM', 'date': 'MON 00' }];
  /*dateTimeDetails = [{ 'time': '00:00 AM', 'date': 'MON 00' }, { 'time': '00:00 AM', 'date': 'MON 00' },
  { 'time': '00:00 AM', 'date': 'MON 00' }];*/
  dateTimeDetails = [{ 'time': '00:00 AM', 'date': 'MON 00' },{ 'time': '00:00 AM', 'date': 'MON 00' },{ 'time': '00:00 AM', 'date': 'MON 00' },{ 'time': '00:00 AM', 'date': 'MON 00' }];
  body = document.getElementsByTagName('body')[0];
  constructor(private router: Router) { }

  ngOnInit() {
    if (window.innerWidth > 767) {
      this.isDesktop = true;
    } else {
      this.isDesktop = false;
    }
    this.inputtextfield = {
      inputText: ''
    }
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth > 767) {
      this.isDesktop = true;
    } else {
      this.isDesktop = false;
    }
  }
  showFunc = function (val) {
    if (this.showDetails) {
      this.hideVar = val;
      if (this.lastVal === this.hideVar) {
        this.hideVar = -1;
      }
      this.lastVal = this.hideVar;
    }
  }
// call back starts
showScroll() {
  this.body.classList.remove('noScroll');
}
hideScroll() {
  this.body.classList.add('noScroll');
}
  close() {
    this.visible = false;
    this.callBackVisible = false;
    this.confrmnVisible = false;
    this.scheduleConfrmnVisible = false;
    this.showScroll();
  }
  select(value, type) {
    if (value) {
      if (type === 'date') {
        this.selectedLine1 = value;
      }
      if (type === 'time') {
        this.selectedLine2 = value;
      }
      if (type === 'zone') {
        this.selectedLine3 = value;
      }
    }
  }
  // closeCallBack() {
  //   this.callBackVisible = false;
  //   this.body.classList.remove('noScroll');
  // }
change() {
    this.hideScroll();
    this.visible = !this.visible;
}
scheduleCall() {
  this.visible = false;
  this.scheduleConfrmnVisible = false;
  this.callBackVisible = true;
}
confirm() {
  this.hideScroll();
  this.callBackVisible = false;
  this.confrmnVisible = true;
}
done() {
  this.hideScroll();
  this.confrmnVisible = false;
  this.scheduleConfrmnVisible = true;
}
}
export class InputTextField {
  inputText: string;
}