import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mvp-review-add-ons',
  templateUrl: './mvp-review-add-ons.component.html',
  styleUrls: ['./mvp-review-add-ons.component.scss']
})
export class MvpReviewAddOnsComponent implements OnInit {

  planDetails: Array<any> = [
    {
      'title': { id: 1, 'name': 'Caller Tunes 5', 'price': '$XX.XX/mo', 'applicable': true },
      'data': [
        { 'details': 'Kirby, (206) 617-5567' }
      ]
    },
    {
      'title': { id: 2, 'name': 'Family Allowances', 'price': '$0.00/mo', 'applicable': true },
      'data': [
        { 'details': 'Will, (206) 617-1111' },
        { 'details': 'Matt, (206) 617-5567' },
      ]
    }
  ];
  total: Array<any> = [
    {
      'title': { id: 1, 'name': 'Total', 'price': '$XX.XX/mo', 'applicable': false }
    }
  ];
  applicableArray: Array<any> = [
    { 'title' : 'Not applicable with your new changes',
      'data': [
      {id: 1, 'name': 'Family Allowances', 'price': '$XX.XX/mo', 'applicable': false}
    ]}
];
  constructor() { }

  ngOnInit() {
  }

}
