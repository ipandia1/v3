import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-mvp-select-plan',
  templateUrl: './mvp-select-plan.component.html',
  styleUrls: ['./mvp-select-plan.component.scss']
})
export class MvpSelectPlanComponent implements OnInit {
  currentPlanDetails: any = {
    'title': 'Simple Choice North America',
    'subTitle': '$XXX.XX / 3 Lines',
    'data': [
      { 'details': 'Another attribute to compare' },
      { 'details': 'Another attribute to compare' },
      { 'details': 'Another attribute to compare' },
      { 'details': 'Another attribute to compare' },
      { 'details': 'Another attribute to compare' },
      { 'details': 'Another attribute to compare' },
      { 'details': 'Another attribute to compare' },
      { 'details': 'Another attribute to compare' },
      { 'details': 'Another attribute to compare' },
      { 'details': 'Another attribute to compare' }
    ]
  };
  planDetails: Array<any> = [
    {
      'title': 'with Netflix On Us!',
      'data': [
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Netflix at no extra charge' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' }
      ]
    },
    {
      'title': 'One Plan. All Unlimited.',
      'data': [
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Netflix at no extra charge' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' }
      ]
    },
    {
      'title': 'with Netflix On Us!',
      'data': [
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Netflix at no extra charge' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' }
      ]
    },
    {
      'title': 'One Plan. All Unlimited.',
      'data': [
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Netflix at no extra charge' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' }
      ]
    },
    {
      'title': 'with Netflix On Us!',
      'data': [
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Netflix at no extra charge' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' },
        { 'details': 'Another attribute to compare' }
      ]
    },
  ];
  showItems = false;
  showPlanDetails = false;
  hideOrShowListContent: Array<number> = [];
  currentdiv = 0;
  activeDiv = 2;
  mobileShift = 1;
  mobilewidth;
  mobileleft;
  mobilediv = 0;
  constructor() { }

  ngOnInit() {
    for (let index = 0; index < this.planDetails.length; index++) {
      this.hideOrShowListContent[index] = -1;
    }
    this.caluculatemobileleft();
  }

  caluculatemobileleft() {
    if (window.screen.width >= 768 && window.screen.width <= 992) {
      this.mobileleft = 3;
    } else if (window.screen.width >= 993 && window.screen.width <= 1200) {
      this.mobileleft = 15;
    }
    else {
      this.mobileleft = 24;
    }
  }
  test() {
    const divelement1 = document.querySelectorAll('.slide') as unknown as HTMLCollectionOf<HTMLElement>;
    const divelement = document.querySelectorAll('.Rectangle-1') as unknown as HTMLCollectionOf<HTMLElement>;
    this.mobilewidth = divelement1[0].getBoundingClientRect().width;
    const scrolldivelement = document.querySelectorAll('.swipe-scroll') as unknown as HTMLCollectionOf<HTMLElement>;
    //console.log(scrolldivelement[0].scrollLeft);
    //console.log(scrolldivelement[0].scrollLeft / this.mobilewidth);
    if (scrolldivelement[0].scrollLeft != 0) {
      
      this.currentdiv = Number(Math.round(scrolldivelement[0].scrollLeft / this.mobilewidth));
      this.activeDiv = this.currentdiv + 2;
      // console.log(this.currentdiv);
      this.mobilewidth = divelement1[0].getBoundingClientRect().width;
    }
  }

  @HostListener('window:resize')  onResize() {
    const divelement1 = document.querySelectorAll('.slide') as unknown as HTMLCollectionOf<HTMLElement>;
    const divelement = document.querySelectorAll('.Rectangle-1') as unknown as HTMLCollectionOf<HTMLElement>;
    const scrolldivelement = document.querySelectorAll('.swipe-scroll') as unknown as HTMLCollectionOf<HTMLElement>;
    //alert("test");
    if (window.screen.width >= 768) {
      //console.log("srini");
      //console.log(this.currentdiv);
      divelement1[0].style.transition = '';
      //console.log("widthd"+this.mobilewidth);
      if(this.currentdiv > divelement1.length-2)
      {
        this.currentdiv = divelement1.length-2;
        this.activeDiv =this.currentdiv+2;
      }
      if (this.mobileShift === 1) {
        scrolldivelement[0].scrollLeft = 0;
        this.mobileShift = 0;
      }
            
      if (this.currentdiv === 0) {
        divelement1[0].style.marginLeft = '5px';
      
      } else {
        setTimeout(()=> {
        const currentmargin: number = (this.currentdiv - 1) * (divelement[0].getBoundingClientRect().width) + (Number(window.getComputedStyle(divelement1[2]).marginLeft.replace("px", '')) + this.mobileleft);
        divelement1[0].style.marginLeft = (- currentmargin - divelement[0].getBoundingClientRect().width) + 'px';
        },400);
      }
    } else {
      //alert(this.mobilediv);
      scrolldivelement[0].scrollLeft = ((this.mobilediv) *  divelement[0].getBoundingClientRect().width)+(this.mobilediv * 15);
      divelement1[0].style.marginLeft = (0) + 'px';
      divelement1[0].style.transition = '';
      this.mobileShift = 1;
    }
  }


  show(i) {
    if (this.hideOrShowListContent[i] !== i) {
      this.hideOrShowListContent[i] = i;
    } else {
      this.hideOrShowListContent[i] = -1;
    }
  }

  next() {
    //console.log(this.activeDiv);
    if (this.activeDiv < document.getElementsByClassName('slide').length) {
      const divelement1 = document.querySelectorAll('.slide') as unknown as HTMLCollectionOf<HTMLElement>;
      const divelement = document.querySelectorAll('.Rectangle-1') as unknown as HTMLCollectionOf<HTMLElement>;
      const currentmargin: number = this.currentdiv * (divelement[0].getBoundingClientRect().width) + this.mobileleft + (Number(window.getComputedStyle(divelement1[2]).marginLeft.replace("px", '')));
      divelement1[0].style.marginLeft = (- currentmargin - divelement[0].getBoundingClientRect().width) + 'px';
      divelement1[0].style.transition = '.9s all';
      this.currentdiv = this.currentdiv + 1;
      this.mobilediv = this.mobilediv + 1;
     // alert(this.mobilediv);
      ++this.activeDiv;

    }
  }
  previous() {
    const divelement1 = document.querySelectorAll('.slide') as unknown as HTMLCollectionOf<HTMLElement>;
    // console.log(this.activeDiv);
    if (this.activeDiv > 2) {
      const divelement = document.querySelectorAll('.Rectangle-1') as unknown as HTMLCollectionOf<HTMLElement>;
      const currentmargin: number = -this.currentdiv * (divelement[0].getBoundingClientRect().width);
      if (this.currentdiv - 1 === 0) {
        divelement1[0].style.marginLeft = '5px';
        divelement1[0].style.transition = '.9s all';
      } else {
        divelement1[0].style.marginLeft = (currentmargin + divelement[0].getBoundingClientRect().width - 10) + 'px';
        divelement1[0].style.transition = '.9s all';
      }
      this.currentdiv = this.currentdiv - 1;
      this.mobilediv = this.mobilediv - 1;
     // alert(this.mobilediv);
      --this.activeDiv;
    }
  }
}


