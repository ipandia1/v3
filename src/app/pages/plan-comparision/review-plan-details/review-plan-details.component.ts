import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-review-plan-details',
  templateUrl: './review-plan-details.component.html',
  styleUrls: ['./review-plan-details.component.scss']
})
export class ReviewPlanDetailsComponent implements OnInit {
    planDetails: Array<any> = [
      { 'title' : '3 Voice Lines',
        'data': [
        {id: 1,  'name': 'T-Mobile One Tax Inclusive', 'price': '$XXX.XX/mo', 'applicable1': true, 'applicable2': false},
        {id: 2,  'name': 'with AutoPay Discount', 'price': '$140.00/mo', 'applicable1': true, 'applicable2': true}
      ]},
      { 'title' : 'Additional Lines',
        'data': [
        {id: 3, 'name': 'T-Mobile One Tablet', 'price': '$20.00/mo', 'applicable1': true, 'applicable2': true},
        {id: 4, 'name': 'T-Mobile One Wearable', 'price': '$10.00/mo', 'applicable1': true, 'applicable2': true},
      ]},
      { 'title' : 'Shared Services',
        'data': [
        {id: 5, 'name': 'Family allowances', 'price': '$0.00/mo', 'applicable1': true, 'applicable2': true}
      ]},
      { 'title' : 'Not applicable with your new plan',
        'data': [
        {id: 6, 'name': 'Simple choice North America 16GB', 'price': '$80.00/mo', 'applicable1': false, 'applicable2': false},
        {id: 7, 'name': 'Family allowances', 'price': '$5.00/mo', 'applicable1': false, 'applicable2': false},
      ]},
    ];
  total = { 'title': 'Total', 'price': '$170.00/mo' };
  belowContent = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur neque dolor, nec mattis ante vestibulum in. Curabitur auctor diam enim, vitae feugiat augue venenatis eget. Suspendisse porta feugiat aliquet. Aliquam a risus mi. Vestibulum at viverra massa. Ut in nunc quis lorem sollicitudin mollis vitae vitae nunc. Duis tincidunt convallis tellus, vel mollis tellus aliquam eget. Quisque ac ipsum quis turpis tristique pharetra. Praesent auctor, diam at porttitor vulputate, turpis sapien convallis lectus, non mattis libero elit ac mi. Mauris in sollicitudin sem. Phasellus varius blandit arcu, egestas feugiat arcu efficitur sed. Proin quis dapibus lectus. Phasellus fringilla sodales massa, sed ullamcorper ante luctus non. Maecenas varius congue augue, eget iaculis erat consequat quis. Nulla pellentesque luctus nunc, nec porta lacus pulvinar id. Proin interdum suscipit cursus.';
  constructor() { }
  ngOnInit() {
  }
}
