import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mvp-review-changes',
  templateUrl: './mvp-review-changes.component.html',
  styleUrls: ['./mvp-review-changes.component.scss']
})
export class MvpReviewChangesComponent implements OnInit {
  paymentMethod = 'Payment Method';
  cardNumber = '****1234';
  iconClass = 'visa-icon';
  titleClass = 'Display6';
  disableClass = '';
  url = '';
  checkOpt: boolean;
  reviewDetails: Array<any> = [
      { descTitle: 'Plans', value: '$XX.XX/mo', image: '', url: '/mvpReviewPlans'},
      { descTitle: 'Add-Ons', value: '$XX.XX/mo', image: '', url: '/mvpReviewAddOns'},
      { descTitle: 'Equipment', value: '$XX.XX/mo', image: '', url: '/mvpReviewEquipment'}
    ];
  constructor() { }

  ngOnInit() {
    this.checkOpt = true;
  }
}
