import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mvp-review-plans',
  templateUrl: './mvp-review-plans.component.html',
  styleUrls: ['./mvp-review-plans.component.scss']
})
export class MvpReviewPlansComponent implements OnInit {
  planDetails: Array<any> = [
      {
        'title': { id: 1, 'name': 'T-Mobile ONE', 'price': '$XX.XX/mo', 'applicable': false },
        'subTitle': { 'name': 'with AutoPay', 'price': '$XX.XX/mo', 'applicable': true },
        'data': [
          { 'details': 'Will, (206) 861-9123' },
          { 'details': 'Kirby, (206) 617-5567' },
          { 'details': 'Matt, (206) 617-5567' },
        ]
      },
      {
        'title': { id: 2, 'name': 'T-Mobile ONE for Tablet', 'price': '$XX.XX/mo', 'applicable': false },
        'subTitle': { 'name': 'with AutoPay', 'price': '$XX.XX/mo', 'applicable': true },
        'data': [
          { 'details': 'Will, (206) 617-1111' },
          { 'details': 'Will, (206) 617-1111' },
          { 'details': 'Will, (206) 617-1111' },
        ]
      },
      {
        'title': { id: 3, 'name': 'T-Mobile ONE for Wearable', 'price': '$XX.XX/mo', 'applicable': false },
        'subTitle': { 'name': 'with AutoPay', 'price': '$XX.XX/mo', 'applicable': true },
        'data': [
          { 'details': 'Kirby, (206) 617-5567' }
        ]
      }
    ];
  total: Array<any> = [
      {
        'title': { id: 1, 'name': 'Total', 'price': '$XX.XX/mo', 'applicable': false },
        'subTitle': { 'name': 'with AutoPay', 'price': '$XX.XX/mo', 'applicable': true },
      }
    ];
  applicableArray: Array<any> = [
      { 'title' : 'Not applicable with your new changes',
        'data': [
        {id: 1, 'name': 'Simple choice North America 16GB', 'price': '$XX.XX/mo', 'applicable': false},
        {id: 2, 'name': 'Simple choice MINT Plan A', 'price': '$XX.XX/mo', 'applicable': false},
        {id: 3, 'name': 'Simple choice MINT Plan B', 'price': '$XX.XX/mo', 'applicable': false},
      ]}
  ];
  constructor() { }

  ngOnInit() {
  }

}
