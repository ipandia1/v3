import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-review-auto-pay-upsell',
  templateUrl: './review-auto-pay-upsell.component.html',
  styleUrls: ['./review-auto-pay-upsell.component.scss']
})
export class ReviewAutoPayUpsellComponent implements OnInit {
  paymentMethod = 'Payment Method';
  cardNumber = '****1234';
  iconClass = 'visa-icon';
  titleClass = 'body';
  disableClass = '';
  url = '';
  checkOpt: boolean;
  constructor() { }

  ngOnInit() {
    this.checkOpt = true;
  }

  reviewDetails: Array<any> = [
    { "title" : "Plan",
      "data":[
      {id:1, "name": "T-Mobile ONE", "subTitle":"", "phNo":"","effectiveDate":"Effective May 24, 2018","price":"$170.00/mo","url":"/reviewPlanDetails", "planName":"" }
      ]
    },
    { "title" : "Lines",
      "data":[
        {id:1, "name": "Will","subTitle":"","phNo":"(206) 861-9123","effectiveDate":"","price":"$15.00/mo", "url":"/reviewLineDetails", "planName":"T-Mobile ONE"},
        {id:2,"name": "Kirby","subTitle":"","phNo":"(206) 617-5567","effectiveDate":"","price":"$0.00/mo", "url":"/reviewLineDetails", "planName":"T-Mobile ONE"},
        {id:3,"name": "Matt","subTitle":"","phNo":"(206) 617-5567","effectiveDate":"","price":"$0.00/mo", "url":"/reviewLineDetails", "planName":"T-Mobile ONE (No Data)"}
      ]
    },
    { "title" : "",
      "data":[
        {id:1,"name": "Will","subTitle":"","phNo":"(206) 617-5567","effectiveDate":"","price":"$20.00/mo", "url":"/reviewLineDetails", "planName":"T-Mobile One Tablet"},
        {id:2,"name": "Kirby","subTitle":"","phNo":"(206) 617-5567","effectiveDate":"","price":"$10.00/mo", "url":"/reviewLineDetails", "planName":"T-Mobile One Wearable"}
        ]
    }
    ];
}
