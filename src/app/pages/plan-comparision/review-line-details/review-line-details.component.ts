import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-review-line-details',
  templateUrl: './review-line-details.component.html',
  styleUrls: ['./review-line-details.component.scss']
})
export class ReviewLineDetailsComponent implements OnInit {
  lineDetails: Array<any> = [
    {
      'data': [
        { id: 1, 'name': 'ONE Unlimited', 'price': '$0.00', 'defaultInfo' : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur neque dolor, nec mattis ante vestibulum in. Curabitur auctor diam enim, vitae feugiat augue venenatis eget. Suspendisse porta feugiat aliquet.', 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur neque dolor, nec mattis ante vestibulum in. Curabitur auctor diam enim, vitae feugiat augue venenatis eget. Suspendisse porta feugiat aliquet.' },
        { id: 2, 'name': 'ONE Plus', 'price': '$10.00/mo', 'defaultInfo' : '', 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur neque dolor, nec mattis ante vestibulum in. Curabitur auctor diam enim, vitae feugiat augue venenatis eget. Suspendisse porta feugiat aliquet.' },
        { id: 3, 'name': 'ONE Plus International', 'price': '$25.00/mo', 'defaultInfo' : '', 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur neque dolor, nec mattis ante vestibulum in. Curabitur auctor diam enim, vitae feugiat augue venenatis eget. Suspendisse porta feugiat aliquet.' },
        { id: 4, 'name': 'No Data', 'price': '$0.00/mo', 'defaultInfo' : '', 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur neque dolor, nec mattis ante vestibulum in. Curabitur auctor diam enim, vitae feugiat augue venenatis eget. Suspendisse porta feugiat aliquet.' }
      ]
    }
  ];
  planDetails: Array<any> = [
    {
      'title': 'Keeping',
      'data': [
        { id: 1, 'name': 'T-Mobile Caller Tunes 5', 'price': '$5.00/mo', 'applicable': true },
        { id: 2, 'name': 'Premium Device Protection', 'price': '$15.00/mo', 'applicable': true }
      ]
    },
    {
      'title': 'Not applicable with your new plan',
      'data': [
        { id: 1, 'name': 'Stateside Internation Talk', 'price': '$15.00/mo', 'applicable': false },
        { id: 2, 'name': 'Name ID + VM to Text Bundle', 'price': '$15.00/mo', 'applicable': false },
        { id: 3, 'name': 'Data Stash identifier GSM', 'price': '$15.00/mo', 'applicable': false },
        { id: 4, 'name': 'SC 6GB Data & SMHS', 'price': '$15.00/mo', 'applicable': false }
      ]
    }
  ];
  isEmpty = '';
  lineDetailAccordion: boolean[][] = [];
  total = { 'title': 'Total', 'price': '$35.00/mo' };
  belowContent = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur neque dolor, nec mattis ante vestibulum in. Curabitur auctor diam enim, vitae feugiat augue venenatis eget. Suspendisse porta feugiat aliquet. Aliquam a risus mi. Vestibulum at viverra massa. Ut in nunc quis lorem sollicitudin mollis vitae vitae nunc. Duis tincidunt convallis tellus, vel mollis tellus aliquam eget. Quisque ac ipsum quis turpis tristique pharetra. Praesent auctor, diam at porttitor vulputate, turpis sapien convallis lectus, non mattis libero elit ac mi. Mauris in sollicitudin sem. Phasellus varius blandit arcu, egestas feugiat arcu efficitur sed. Proin quis dapibus lectus. Phasellus fringilla sodales massa, sed ullamcorper ante luctus non. Maecenas varius congue augue, eget iaculis erat consequat quis. Nulla pellentesque luctus nunc, nec porta lacus pulvinar id. Proin interdum suscipit cursus.';

  constructor() { }

  ngOnInit() {
    /*Logic to handle toggle content*/
    for (const i in this.lineDetails) {
      this.lineDetailAccordion.push([]);
      for (const k in this.lineDetails[i].data) {
        this.lineDetailAccordion[i].push(false);
      }
    }
  }
}
