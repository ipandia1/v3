import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mvp-review-order-details-by-line',
  templateUrl: './mvp-review-order-details-by-line.component.html',
  styleUrls: ['./mvp-review-order-details-by-line.component.scss']
})
export class MvpReviewOrderDetailsByLineComponent implements OnInit {
planDetails: Array<any> = [

  {
  'title1': '',
  'subtitle1': 'Account',
  'subtitle1Price': '$133.00',
  'data1': [
    { 'title' : 'Plan',
      'data': [
      {id: 1,  'name': 'T-Mobile ONE', 'price': '130', 'applicable1': true, 'applicable2': true},
      {id: 2,  'name': 'Autopay Discount', 'price': '-10', 'applicable1': true, 'applicable2': true}
    ]},
    { 'title' : 'Add-ons',
      'data': [
      {id: 3, 'name': 'Fam + Netflix On Us', 'price': '13', 'applicable1': true, 'applicable2': true},
      {id: 4, 'name': 'Fam + Netflix On Us Discount', 'price': '-13', 'applicable1': true, 'applicable2': true},
    ]},
    { 'title' : 'No longer applicalbe',
      'data': [
      {id: 6, 'name': 'SC North America 2GB', 'price': '8', 'applicable1': false, 'applicable2': false},
      {id: 7, 'name': 'Family Allowance', 'price': '5', 'applicable1': false, 'applicable2': false},
      {id: 8, 'name': 'Stateside International Talk', 'price': '10', 'applicable1': false, 'applicable2': false, 'highlight':true},
    ]}
  ]
},
{
  'title1': 'Kirby',
  'subtitle1': '(206) 555-1212',
  'subtitle1Price': '$28.00',
  'data1': [
    { 'title' : 'Plan',
      'data': [
      {id: 1,  'name': 'T-Mobile ONE', 'price': 'Included', 'applicable1': true, 'applicable2': true}
    ]},
    { 'title' : 'Add-ons',
      'data': [
      {id: 2, 'name': 'Name ID', 'price': '5', 'applicable1': true, 'applicable2': true},
      {id: 3, 'name': 'Fam + Netflix On Us Discount', 'price': '-13', 'applicable1': true, 'applicable2': true},
    ]},
    { 'title' : 'Equipment',
      'data': [
      {id: 4, 'name': 'Iphone 7+ Installment plan', 'price': '30', 'applicable1': true, 'applicable2': true},
      {id: 5, 'name': '$300 off iphone offer', 'price': '-7', 'applicable1': true, 'applicable2': true}
    ]}
  ]
},
{
  'title1': 'Matt',
  'subtitle1': '(206) 555-2323',
  'subtitle1Price': '$5.00',
  'data1': [
    { 'title' : 'Plan',
      'data': [
      {id: 1,  'name': 'T-Mobile ONE', 'price': 'Included', 'applicable1': true, 'applicable2': true}
    ]},
    { 'title' : 'Add-ons',
      'data': [
      {id: 2, 'name': 'Caller Tunes', 'price': '5', 'applicable1': true, 'applicable2': true}
    ]}
  ]
},
{
  'title1': 'Will',
  'subtitle1': '(666)666-6666',
  'subtitle1Price': '$20.00',
  'data1': [
    { 'title' : 'Plan',
      'data': [
      {id: 1,  'name': 'T-Mobile ONE Additional line', 'price': '75', 'applicable1': true, 'applicable2': true},
      {id: 2,  'name': 'Autopay Discounte', 'price': '-5', 'applicable1': true, 'applicable2': true}
    ]},
    { 'title' : 'No longer applicalbe',
      'data': [
      {id: 3, 'name': 'SC AAL', 'price': '10', 'applicable1': false, 'applicable2': true},
      {id: 4, 'name': 'This one is on us promo', 'price': '-10', 'applicable1': false, 'applicable2': false, 'highlight':true},
      {id: 5, 'name': 'SC 4GB High Speed Data with SMHS', 'price': '5', 'applicable1': false, 'applicable2': true}
    ]}
  ]
},
{
  'title1': 'Will',
  'subtitle1': '(777)777-7777',
  'subtitle1Price': '$20.00',
  'data1': [
    { 'title' : 'Plan',
      'data': [
      {id: 1,  'name': 'T-Mobile ONE Additional line', 'price': '75', 'applicable1': true, 'applicable2': true},
      {id: 2,  'name': 'Autopay Discounte', 'price': '-5', 'applicable1': true, 'applicable2': true},
      {id: 3,  'name': 'AT-Mobile ONE Tablet + Voice bill credit', 'price': '-50', 'applicable1': true, 'applicable2': true}
    ]},
    { 'title' : 'No longer applicalbe',
      'data': [
      {id: 4, 'name': 'SC Mobile Internet 6GB High Speed Data', 'price': '25', 'applicable1': false, 'applicable2': true}
    ]}
  ]
},
];
  constructor() { }

  ngOnInit() {
  }

}
