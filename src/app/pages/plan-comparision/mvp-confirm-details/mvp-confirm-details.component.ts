import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mvp-confirm-details',
  templateUrl: './mvp-confirm-details.component.html',
  styleUrls: ['./mvp-confirm-details.component.scss']
})
export class MvpConfirmDetailsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public smallDevice: boolean = false;
  public name: string = "Kirby!";
  public showDetails: boolean = false;
  public dataPassInfo: Array<Object> = [
    { descTitle: 'Order no.', value: '0123456789' },
    { descTitle: 'Payment type', value: 'Visa ****1234' },
    { descTitle: 'Autopay date', value: 'June 14, 2018' },
    { descTitle: 'Name on account', value: 'Kirby Thorton' },
    { descTitle: 'T-Mobile account no.', value: '12346789' }
  ];

  public addItems: Array<Object> = [
    { descTitle: 'T-Mobile ONE', effectTitle: 'Effective May 24, 2018' },
  ];
  public removeItems: Array<Object> = [
    { descTitle: 'Single line name service', value: '15.99' }
  ];
  public totalItems: Array<Object> = [
    { descTitle: 'New Monthly Total', value: 'xxx.xx'},
  ];
  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  }

}
