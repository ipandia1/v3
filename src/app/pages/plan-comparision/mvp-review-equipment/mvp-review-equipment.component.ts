import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mvp-review-equipment',
  templateUrl: './mvp-review-equipment.component.html',
  styleUrls: ['./mvp-review-equipment.component.scss']
})
export class MvpReviewEquipmentComponent implements OnInit {
  planDetails: Array<any> = [
    {
      'title': { id: 1, 'name': 'Premium Device Protection', 'price': '$XX.XX/mo', 'applicable': true },
      'data': [
        { 'details': 'Will, (206) 617-1111' },
        { 'details': 'Matt, (206) 617-5567' }
      ]
    },
    {
      'title': { id: 2, 'name': 'Standard Device Protection', 'price': '$XX.XX/mo', 'applicable': true },
      'data': [
        { 'details': 'Kirby, (206) 617-5567' }
      ]
    }
  ];
  total: Array<any> = [
    {
      'title': { id: 1, 'name': 'Total', 'price': '$XX.XX/mo', 'applicable': false }
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
