import { Component, OnInit, HostListener } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-pitch-page',
  templateUrl: './pitch-page.component.html',
  styleUrls: ['./pitch-page.component.scss']
})
export class PitchPageComponent implements OnInit {
  jsonDataPath = 'assets/json/suzySheep/pitchPage.json';
  pitchPageData: any;
  isDesktop: boolean;

  constructor(private commonService: CommonService) {
    this.commonService.getJsonData(this.jsonDataPath)
     .subscribe(responseJson => {
       this.pitchPageData = responseJson;
     });
   }

  ngOnInit() {
    if (window.innerWidth > 767) {
      this.isDesktop = true;
    } else {
        this.isDesktop = false;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth > 767) {
      this.isDesktop = true;
    } else {
      this.isDesktop = false;
    }
  }

}
