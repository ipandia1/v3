import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-promotions',
  templateUrl: './my-promotions.component.html',
  styleUrls: ['./my-promotions.component.scss']
})
export class MyPromotionsComponent implements OnInit {

  myPromotions: Array<any> = [
    { "title" : "Pending",  //Screen 1 and 2: Pending Data
      "promotionsData" : [
        {"offerTitle" : "Offer - Samsung Galaxy S8",
        "userInfo" : " Alex, (206) 861-9327",
        "multiLinesText": "",
        "offerStatus" : "Pending",
        "offerStatusDesc" : "valid through 12/1/2017",
        "completionTitle" : "2 of 3 steps completed",
        "enrollType" : "",
        "completionSteps":[
          {id:1,"stepCompleted": true, "stepDesc": "Signed up for T-mobile ONE rate plan"},
          {id:2,"stepCompleted": true, "stepDesc": "Purchased Samsung Galaxy S8 on an Equipment Installment Plan (EIP)"},
          {id:3,"stepCompleted": false, "stepDesc": "Traded-in your old device on 11/1/2017"}
          ],
        "benefitsTitle": "Credit you'll get",
        "benefitsDesc" : "Once all steps are completed, the $360 promotional savings will be credited in monthly bill credits of $15x24mos.",
        "termAndCondition" : "If you cancel wireless service, remaining balance may become due and you may lose promotional credits. Contact us for further details.",
        "offerInfo" : "Offer ID #42987",
        "offerValidity" : "From 02/10/2017 to 12/1/2017",
        "limited" : "Limited time offer and subject to change. "
      }]
    },
    { "title" : "Active", //Screen 3 and 4: Active Data
      "promotionsData" : [
        {"offerTitle" : "Offer - LG",
        "userInfo" : " Surendra, (610) 999-9170",
        "multiLinesText": "",
        "offerStatus" : "Enrolled",
        "offerStatusDesc" : "began on 02/10/2017",
        "completionTitle" : "Completed!",
        "enrollType" : "",
        "completionSteps":[
          {id:1,"stepCompleted": true, "stepDesc": "Signed up for T-mobile ONE rate plan"},
          {id:2,"stepCompleted": true, "stepDesc": "Purchased Samsung Galaxy S8 on an Equipment Installment Plan (EIP)"},
          {id:3,"stepCompleted": true, "stepDesc": "Traded-in your old device on 11/1/2017"}
          ],
        "benefitsTitle": "Credit you'll get",
        "benefitsDesc" : "You're currently receiving the full $360 device promo via monthly bill credites of $20 over 24 months.",
        "termAndCondition" : "If you cancel wireless service, remaining balance may become due and you may lose promotional credits. Contact us for further details.",
        "offerInfo" : "Promo offer ID #431538",
        "offerValidity" : "Valid 09/12/2017 to 12/1/2017",
        "limited" : "Limited time offer and subject to change. "
      }]
    },
    { "title" : "Past", //Screen 5: Past Data. To View this screen comment all other Screens for Past Data
      "promotionsData" : [
        {"offerTitle" : " Offer -iPhone 7 Trade UP",
        "userInfo" : " Alex, (206) 861-9327",
        "multiLinesText": "",
        "offerStatus" : "Expired",
        "offerStatusDesc" : "on 10/29/2017",
        "completionTitle" : "Steps completed: 1 of 3",
        "enrollType" : "",
        "completionSteps":[
          {id:1,"stepCompleted": true, "stepDesc": "Signed up for T-mobile ONE rate plan"},
          {id:2,"stepCompleted": false, "stepDesc": "Purchased Samsung Galaxy S8 on an Equipment Installment Plan (EIP)"},
          {id:3,"stepCompleted": false, "stepDesc": "Trade-in your old device by 11/11/17"}
          ],
        "benefitsTitle": "Expired",
        "benefitsDesc" : "This offer has expired.",
        "termAndCondition" : "If you cancel wireless service, remaining balance may become due and you may lose promotional credits",
        "offerInfo" : "Promo offer ID #431538",
        "offerValidity" : "Valid 09/12/2017 to 12/1/2017"
      }]
  },
  { "tabTitle" : "Past", //Screen 6: Past Data. To View this screen comment all other Screens for Past Data
    "promotionsData" : [
      {"offerTitle" : " Offer -iPhone 7 Trade UP",
      "userInfo" : " Alex, (206) 861-9327",
      "multiLinesText": "",
      "offerStatus" : "Not Enrolled",
      "offerStatusDesc" : "offer inactive",
      "completionTitle" : "Steps completed: 2 of 3",
      "enrollType" : "",
      "completionSteps":[
        {id:1,"stepCompleted": true, "stepDesc": "Signed up for T-mobile ONE rate plan"},
        {id:2,"stepCompleted": false, "stepDesc": "Purchased iPhone 7 on an Equipment Installment Plan (EIP)"},
        {id:3,"stepCompleted": true, "stepDesc": "Trade-in your old device on 10/29/17"}
        ],
      "benefitsTitle": "Not enrolled",
      "benefitsDesc" : "You are not enrolled in this promotion due to iPhone 7 return",
      "termAndCondition" : "If you cancel wireless service, remaining balance may become due and you may lose promotional credits",
      "offerInfo" : "Promo offer ID #431538",
      "offerValidity" : "Valid 09/12/2017 to 12/1/2017"
      }]
  },
  { "tabTitle" : "Past", //Screen 7: Past Data. To View this screen comment all other Screens for Past Data
    "promotionsData" : [
    {"offerTitle" : " Offer -iPhone 7 Trade UP",
    "userInfo" : " Alex, (206) 861-9327",
    "multiLinesText": "",
    "offerStatus" : "Completed",
    "offerStatusDesc" : "on 10/29/2017",
    "completionTitle" : "Completed!",
    "enrollType" : "",
    "completionSteps":[
      {id:1,"stepCompleted": true, "stepDesc": "Signed up for T-mobile ONE rate plan"},
      {id:2,"stepCompleted": true, "stepDesc": "Purchase Samsung Galaxy S8 on an Equipment Installment Plan (EIP)"},
      {id:3,"stepCompleted": true, "stepDesc": "Trade-in your old device by 11/11/17"}
      ],
    "benefitsTitle": "Completed",
    "benefitsDesc" : "You received 24 of 24 monthly bill credits totaling $360.",
    "termAndCondition" : "If you cancel wireless service, remaining balance may become due and you may lose promotional credits",
    "offerInfo" : "Promo offer ID #431538",
    "offerValidity" : "Valid 09/12/2017 to 12/1/2017"
    }]
  },
  { "tabTitle" : "Past", //Screen 8: Past Data. To View this screen comment all other Screens for Past Data
    "promotionsData" : [
      {"offerTitle" : " Offer -Samsung GS8",
    "userInfo" : " Alex, (206) 861-9327",
    "multiLinesText": "",
    "offerStatus" : "Manual enrolled",
    "offerStatusDesc" : "on 03/02/2017",
    "completionTitle" : "Completed!",
    "enrollType" : "Manual",
    "completionSteps":[
      {id:1,"stepCompleted": false, "stepDesc": "Signed up for T-mobile ONE rate plan"},
      {id:2,"stepCompleted": true, "stepDesc": "Purchase Samsung Galaxy S8 on an Equipment Installment Plan (EIP)"},
      {id:3,"stepCompleted": false, "stepDesc": "Trade-in your old devices by 11/11/17"}
      ],
    "benefitsTitle": "Completed",
    "benefitsDesc" : "You received 24 of 24 monthly bill credits totaling $360.",
    "termAndCondition" : "If you cancel wireless service, remaining balance may become due and you may lose promotional credits. Contact us for further details.",
    "offerInfo" : "Promo offer ID #431538",
    "offerValidity" : "Valid 09/12/2017 to 12/1/2017",
    "limited" : "Limited time offer and subject to change. "
    }]
  },
  { "tabTitle" : "Past", //Screen 9: Past Data. To View this screen comment all other Screens for Past Data
  "promotionsData" : [
    {"offerTitle" : " Offer -iPhone 7 Trade UP",
    "userInfo" : " Alex, (206) 861-9327",
    "multiLinesText": "Applied to multiple lines, View lines",
    "offerStatus" : "Enrolled",
    "offerStatusDesc" : "started on 02/10/2017",
    "completionTitle" : "Completed!",
    "enrollType" : "",
    "completionSteps":[
      {id:1,"stepCompleted": true, "stepDesc": "Signed up for T-mobile ONE rate plan"},
      {id:2,"stepCompleted": true, "stepDesc": "Purchase 2 Samsung Galaxy S8 on an Equipment Installment Plan (EIP)"},
      {id:3,"stepCompleted": true, "stepDesc": "Trade-in 2 old devices by 11/11/17"}
      ],
    "benefitsTitle": "Credits you're receiving",
    "benefitsDesc" : "You are currently receiving the $360 promotional offer via monthly bill credits.",
    "termAndCondition" : "If you cancel wireless service, remaining balance may become due and you may lose promotional credits",
    "offerInfo" : "Promo offer ID #42987",
    "offerValidity" : "Valid 02/11/2017 to 12/1/2017"
    }]
 }
];

  constructor() { }

  ngOnInit() {
  }
}
