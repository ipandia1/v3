import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-past-promotion',
  templateUrl: './past-promotion.component.html',
  styleUrls: ['./past-promotion.component.scss']
})
export class PastPromotionComponent implements OnInit {
  subTitle = 'Select line';
  dropdownId = 'id1';
  selectedLine = '';


  bladeInfo: Array<any> = [
    {title: 'Buy One Get One $215 off Apple Watch', desc: 'Activated:11/11/2018', desc2: '', url: ''},
    {title: 'Netflix On Us!', desc: 'Activated:11/11/2018', desc2: '', url: ''},
    {title: 'LG Superphone BOGO', desc: 'Activated:11/11/2018', desc2: '', url: ''},
    {title: '$150 Off iPhone7', desc: 'Activated:11/11/2018', desc2: '', url: ''},
  ];

  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];

  constructor() { }

  ngOnInit() {
    for  (const  names  of  this.selectLine) {
      const string1 = names.name.split(',');
      if (string1[0].length > 13) {
        names.name  =  string1[0].substring(0, 13) + '... , ' + string1[1] ;
      }
    }
    this.selectedLine = this.selectLine[0].name;
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue;
    }
  }
}
