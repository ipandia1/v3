import { Component } from '@angular/core';

@Component({
  selector: 'app-offer-model1',
  templateUrl: './offer-model1.component.html',
  styleUrls: ['./offer-model1.component.scss']
})
export class OfferModel1Component {
  visible: boolean;
  constructor() { }
    close() {
    this.visible = false;
  }
}
