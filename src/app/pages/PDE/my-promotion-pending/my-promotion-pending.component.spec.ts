import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPromotionPendingComponent } from './my-promotion-pending.component';

describe('MyPromotionPendingComponent', () => {
  let component: MyPromotionPendingComponent;
  let fixture: ComponentFixture<MyPromotionPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPromotionPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPromotionPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
