import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promotions-details',
  templateUrl: './promotions-details.component.html',
  styleUrls: ['./promotions-details.component.scss']
})
export class PromotionsDetailsComponent implements OnInit {

  myPromotions: Array<any> = [
          {id:1,"stepCompleted": true, "stepDesc": "Purchase 2 Samsung Galaxy S8’s on an Equipment Installment plan (EIP)"},
          {id:2,"stepCompleted": false, "stepDesc": "Sign up for T-Mobile ONE rate plan"}       
       ];
  creditdetails = 'You are currently receiving the $360 promotional offer via monthly bill credits.'
  enrolled = 'on 02/10/2017';
  termAndCondition = 'If you cancel wireless service, remaining balance may become due and you may lose promotional credits';
  offerInfo = 'Offer ID #42987';
  offerValidity = 'From 02/10/2017 to 12/1/2017';
  limited = 'Limited time offer and subject to change.';

  constructor() { }

  ngOnInit() {
  }
}
