import { Component } from '@angular/core';

@Component({
  selector: 'app-offer-model',
  templateUrl: './offer-model.component.html',
  styleUrls: ['./offer-model.component.scss']
})
export class OfferModelComponent {
  visible: boolean;
  constructor() { }
    close() {
    this.visible = false;
  }
}
