import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
enum errorVal { DocNotAccepted, InfoPrevious, TooManyFailed, Generic, verified }

@Component({
  selector: 'app-status-upload-doc',
  templateUrl: './status-upload-doc.component.html',
  styleUrls: ['./status-upload-doc.component.scss']
})
export class StatusUploadDocComponent implements OnInit {

  constructor(private _router: Router, private _routepar: ActivatedRoute) { }
  headermessge;
  headerdescription;
  status: string;
  ngOnInit() {
    this._routepar.paramMap.subscribe(params => {
      this.status = params.get('status');
    });
    if (this.status === 'upload') {
      this.headermessge = 'Upload received';
      this.headerdescription = 'Your documents are under review';
    } else if (this.status === 'DocNotAccepted') {
      this.headermessge = 'Unable to verify';
      this.headerdescription = 'Documents not accepted. Submit new documentation to verify status. ';
    } else if (this.status === 'InfoPrevious') {
      this.headermessge = 'Unable to verify';
      this.headerdescription = 'Information previously submitted. Status can’t be verified more than once.';
    } else if (this.status === 'TooManyFailed') {
      this.headermessge = 'Unable to verify';
      this.headerdescription = 'Maximum number of attempts reached';
    } else if (this.status === 'verified') {
      this.headermessge = 'Verified';
      this.headerdescription = 'Approved 07/15/2018';
    }else if (this.status === 'FinishVerification') {
      this.headermessge = 'Finish verification process';
      this.headerdescription = 'Upload your military documentation to complete verification.';
    }
  }
  redirtmessagedetials() {
    if (this.status === 'upload') {
      this._router.navigate(['/successUploadReceived']);
    } else if (this.status === 'DocNotAccepted') {
      this._router.navigate(['/alertMessage', { error: errorVal.DocNotAccepted }]);
    } else if (this.status === 'InfoPrevious') {
      this._router.navigate(['/alertMessage', { error: errorVal.InfoPrevious }]);
    } else if (this.status === 'TooManyFailed') {
      this._router.navigate(['/alertMessage', { error: errorVal.TooManyFailed }]);
    } else if (this.status === 'verified') {
      this._router.navigate(['/successVerified', { error: errorVal.verified }]);
    }
  }
}




