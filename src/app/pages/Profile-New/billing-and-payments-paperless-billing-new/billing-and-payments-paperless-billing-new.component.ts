import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-billing-and-payments-paperless-billing-new',
  templateUrl: './billing-and-payments-paperless-billing-new.component.html',
  styleUrls: ['./billing-and-payments-paperless-billing-new.component.scss']
})
export class BillingAndPaymentsPaperlessBillingNewComponent implements OnInit {
  ischecked1 = true;
  ischecked2 = false;
  ischecked3 = true;
  disable1 = true;
  visible = false;
  constructor(private commonService: CommonService) { }

  ngOnInit() {
  }

  toDisable(id) {
   
    if (id === 1) {
      this.ischecked2 = false;
      this.ischecked3 = true;
      this.disable1 = true;
      this.ischecked1 = true;
    }
    else {
    this.disable1 = false;
    this.ischecked2 = true;
    this.ischecked3 = false;
    this.ischecked1 = false;
    }
  }
  close() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  select() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  change() {
    this.commonService.showScroll(false);
    this.visible = true;
  }

}
