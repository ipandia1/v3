import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-privacy-notifications-marketing-communications-new',
  templateUrl: './privacy-notifications-marketing-communications-new.component.html',
  styleUrls: ['./privacy-notifications-marketing-communications-new.component.scss']
})
export class PrivacyNotificationsMarketingCommunicationsNewComponent implements OnInit {
  checkOpt1 = false;
  checkOpt2 = true;
  checkOpt3 = true;
  checkOpt4 = true;
  disableClass = true;
  subTitle = 'Select Line';
  selectedLine = '';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  subTitle1 = 'Language Preference';
  selectedLanguage = 'English';
  selectLanguages: Array<any> = [
    { name: 'English', disabled: false },
    { name: 'Spanish', disabled: false },
    { name: 'German', disabled: false }
  ];
  constructor() { }
  ngOnInit() {
    for  (const  names  of  this.selectLine) {
      const string1 = names.name.split(',');
      if (string1[0].length > 13) {
        names.name  =  string1[0].substring(0, 13) + '... , ' + string1[1] ;
      }
    }
    this.selectedLine = this.selectLine[0].name;
  }
  select1(value: string): void {
    this.selectedLine = value;
   }
   select2(value: string): void {
    this.selectedLanguage = value;
   }
}
