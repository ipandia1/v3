import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-billing-address-new',
  templateUrl: './billing-address-new.component.html',
  styleUrls: ['./billing-address-new.component.scss']
})
export class BillingAddressNewComponent implements OnInit {
  newBillingAddress: NewBillingAddress;
  selectedLine = 'Select';
  selectlineError: boolean;
  subTitle = 'State';
  selectLine: Array<any> = [
    { name: 'AL', disabled: false },
    { name: 'AK', disabled: false },
    { name: 'AZ', disabled: false },
    { name: 'AR', disabled: false }
  ];
  useBillAddress = true;
  constructor() { }
    ngOnInit() {
    this.newBillingAddress = {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: ''
    }
 }
 select(value: string): void {
  if (!value) {
    this.selectedLine = 'Select';
    this.selectlineError = true;
  }
  else {
    this.selectedLine = value;
    this.selectlineError = false;
   }
}
select1() {
     this.selectedLine = 'Select';
     this.selectlineError = true;
   }

}
export class NewBillingAddress {
  address1: string;
  address2: string;
  city: string;
  state: string;
  zipcode: string;
}
