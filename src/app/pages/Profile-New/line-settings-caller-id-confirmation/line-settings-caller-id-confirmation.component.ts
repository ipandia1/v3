import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-line-settings-caller-id-confirmation',
  templateUrl: './line-settings-caller-id-confirmation.component.html',
  styleUrls: ['./line-settings-caller-id-confirmation.component.scss']
})
export class LineSettingsCallerIdConfirmationComponent implements OnInit {
  callerIdSuccess = true; // True for success page / false for not success
  callerid = JSON.parse(localStorage.getItem('callerID'));
  userName = this.callerid ? this.callerid.firstName + ' ' + this.callerid.lastName : '';
  subTitle = 'Select Line';
  selectedLine = '';
  textValue: string;
  selectLine: Array<any> = [
    { name: 'Zach, (206) 123-4567', disabled: false},
    { name: 'Alice, (206) 234-5678', disabled: false},
    { name: 'Christian, (206)123-4567', disabled: false},
    { name: 'Kathy, (206)123-4568', disabled: false},
    { name: 'Dylan, (206)123-4569', disabled: false}
  ];
  constructor() { }

  ngOnInit() {
    this.selectedLine = this.selectLine[0].name;
  }
  select (value: string, pah: boolean): void {
    this.selectedLine = value;
  }
}
