import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import {Router } from '@angular/router';

@Component({
  selector: 'app-confirm-info',
  templateUrl: './confirm-info.component.html',
  styleUrls: ['./confirm-info.component.scss']
})
export class ConfirmInfoComponent implements OnInit {
  // test = {
  //   'firstName': 'Daniel', 'lastName': 'Kessler', 'dutyStatus': 'Active duty',
  // 'branchOfService': 'Air Force Reserve', 'DOB': 'February 18, 1983' };

  // Put the object into storage
  data: any;
  constructor(private _router: Router, private _location: Location) {
    if (localStorage.getItem('data')) {
      const retrieved = localStorage.getItem('data');
      this.data = JSON.parse(retrieved);
      //console.log('retrieved: ', this.data.firstName);
    } else {
      this.data = {
          'firstName': 'Daniel', 'lastName': 'Kessler', 'dutyStatus': 'Active Duty',
        'branch': 'Air Force Reserve', 'dobmonth': 'February', 'dobday': '18', 'dobyear': '1983' };
    }
  }

  ngOnInit() {

  }
  gotopreviouspage() {
    setTimeout(() => {
      this._location.back();
    }, 500);
  }
  confirm() {
    setTimeout(() => {
      this._router.navigate(['/uploadDoc']);
    }, 500);
  }
}
