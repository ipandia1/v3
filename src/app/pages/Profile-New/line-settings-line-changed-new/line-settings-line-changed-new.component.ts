import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-line-settings-line-changed-new',
  templateUrl: './line-settings-line-changed-new.component.html',
  styleUrls: ['./line-settings-line-changed-new.component.scss']
})
export class LineSettingsLineChangedNewComponent implements OnInit {
  subTitle = 'Select Line';
  dropdownId = 'id1';
  selectedLine = '';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  showPermissions = false;
  constructor(private _location: Location) { }
  ngOnInit() {
    for  (const  names  of  this.selectLine) {
      const string1 = names.name.split(',');
      if (string1[0].length > 13) {
        names.name  =  string1[0].substring(0, 13) + '... , ' + string1[1] ;
      }
    }
    this.selectedLine = this.selectLine[0].name;
  }
  gotopreviouspage() {
    this._location.back();
  }
  select(value: string): void {
    this.selectedLine = value;
   }
}
