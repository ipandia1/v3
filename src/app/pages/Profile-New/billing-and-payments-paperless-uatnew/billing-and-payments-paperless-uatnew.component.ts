import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-billing-and-payments-paperless-uatnew',
  templateUrl: './billing-and-payments-paperless-uatnew.component.html',
  styleUrls: ['./billing-and-payments-paperless-uatnew.component.scss']
})
export class BillingAndPaymentsPaperlessUatnewComponent implements OnInit {

  visible = false;
  constructor(private commonService: CommonService) { }

  ngOnInit() {
  }
  close() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  select() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  change() {
    this.commonService.showScroll(false);
    this.visible = true;
  }


}
