import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-auc-messages',
  templateUrl: './auc-messages.component.html',
  styleUrls: ['./auc-messages.component.scss']
})
export class AucMessagesComponent implements OnInit {
  permConfirm = true;
  headerMessage: string;
  constructor() {
   }


  ngOnInit() {
    if (this.permConfirm) {
      this.headerMessage = 'Change complete!';
    } else {
      this.headerMessage = 'Change failed';

    }
  }

}
