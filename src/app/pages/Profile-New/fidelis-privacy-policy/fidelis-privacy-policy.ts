import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fidelis-privacy-policy',
  templateUrl: './fidelis-privacy-policy.html',
  styleUrls: ['./fidelis-privacy-policy.scss']
})
export class FidelisPrivacyPolicyComponent implements OnInit {
  checkedOrNot: boolean = false;
  constructor() { }

  ngOnInit() {
    this.checkedOrNot = false;
  }

  onChangeCheckBox(event){
    this.checkedOrNot = event.target.checked;
  }
}
