import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tmobile-id-new',
  templateUrl: './tmobile-id-new.component.html',
  styleUrls: ['./tmobile-id-new.component.scss']
})
export class TmobileIdNewComponent implements OnInit {
  subTitle = 'Select Line';
  dropdownId = 'id1';
  selectedLine = '';
  showMore1 = true;
  showMore2 = true;
  showMore3 = true;
  showMore4 = true;
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  constructor() { }

  ngOnInit() {
    for  (const  names  of  this.selectLine) {
      const string1 = names.name.split(',');
      if (string1[0].length > 13) {
        names.name  =  string1[0].substring(0, 13) + '... , ' + string1[1] ;
      }
    }
    this.selectedLine = this.selectLine[0].name;
  }
  select(value: string): void {
    this.selectedLine = value;
   }
}
