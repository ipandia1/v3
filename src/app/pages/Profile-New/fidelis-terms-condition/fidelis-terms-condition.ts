import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fidelis-terms-condition',
  templateUrl: './fidelis-terms-condition.html',
  styleUrls: ['./fidelis-terms-condition.scss']
})
export class FidelisTermsConditionComponent implements OnInit {
  checkedOrNot = false;
  constructor() { }

  ngOnInit() {
    this.checkedOrNot = false;
  }

  onChangeCheckBox(event) {
    this.checkedOrNot = event.target.checked;
  }

}
