import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auc-change-perms-terms-and-cond',
  templateUrl: './auc-change-perms-terms-and-cond.component.html',
  styleUrls: ['./auc-change-perms-terms-and-cond.component.scss']
})
export class AucChangePermsTermsAndCondComponent implements OnInit {
  checkedOrNot : boolean = false;
  constructor() { }

  ngOnInit() {
    this.checkedOrNot = false;
  }

  onChangeCheckBox(event){
    this.checkedOrNot = event.target.checked; 
  }

}
