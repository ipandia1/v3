import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
enum errorVal { DocNotAccepted, InfoPrevious, TooManyFailed, Generic }

@Component({
  selector: 'app-upload-doc-landing',
  templateUrl: './upload-doc-landing.component.html',
  styleUrls: ['./upload-doc-landing.component.scss']
})
export class UploadDocLandingComponent implements OnInit {
  confirmInfo =
    [
      { title: '[Dynamic content]', content: '[Dynamic content]' },
      { title: '[Dynamic content]', content: '[Dynamic content]' },
      { title: '[Dynamic content]', content: '[Dynamic content]' },
      { title: '[Dynamic content]', content: '[Dynamic content]' }
    ];
  documentIncludes =
    [
      { value: '[Dynamic content]', example: '' },
      { value: '[Dynamic content]', example: '' },
      { value: '[Dynamic content]', example: '' },
      { value: 'Current military status', example: 'Example: “retired” or “active duty”' }
    ];
  errorModalTitle = '';
  errorModalContent = '';
  selectedFiles = [];
  errors = [];
  dragArea = false;
  fileExt = 'bmp, gif, jpg, png, pdf';
  maxSize = 10;
  successMsg = 'File uploaded:';
  addNewFile = false;
  check = '';
  errorModal = false;
  message: string;
  exceededSize = false;
  constructor(private router: Router) { }
  ngOnInit() {
  }
  @HostListener('dragover', ['$event']) onDragOver(event) {
    this.dragArea = true;
    event.preventDefault();
  }
  @HostListener('dragenter', ['$event']) onDragEnter(event) {
    this.dragArea = true;
    event.preventDefault();
  }
  @HostListener('dragend', ['$event']) onDragEnd(event) {
    this.dragArea = true;
    event.preventDefault();
  }
  @HostListener('dragleave', ['$event']) onDragLeave(event) {
    this.dragArea = false;
    event.preventDefault();
  }
  @HostListener('drop', ['$event']) onDrop(event) {
    this.dragArea = false;
    event.preventDefault();
    event.stopPropagation();
  }
  onFileSelected(event) {
    const files = event.target.files;
    this.saveFiles(files);
  }
  drop(event) {
    this.dragArea = false;
    event.preventDefault();
    event.stopPropagation();
    const files = event.dataTransfer.files;
    this.saveFiles(files);
  }
  allowDrop(event) {
    this.dragArea = true;
    event.preventDefault();
  }
  saveFiles(files) {
    if (files.length > 0) {
      const verifyFile = this.isValidFile(files);
      if (this.errorModal !== true) {
        if (this.check !== 'sameFile') {
          for (let j = 0; j < files.length; j++) {
            let fileSize = (files[j].size) / 1024;
            fileSize = Math.round(fileSize * 100) / 100;
            this.selectedFiles.push({ 'name': files[j].name, 'size': fileSize });
          }
        }
        this.addNewFile = true;
      }
    }
  }
  private isValidFile(files) {
    this.check = '';
    // to check file extensions
    const extensions = (this.fileExt.split(','))
      .map(function (x) { return x.toLocaleUpperCase().trim(); });
    for (let i = 0; i < files.length; i++) {
      const ext = files[i].name.toUpperCase().split('.').pop() || files[i].name;
      const exists = extensions.indexOf(ext);
      if (exists < 0) {
        this.exceededSize = false;
        this.errorModalTitle = 'Upload failed';
        this.errorModalContent = 'Hmm, it looks like there was a problem uploading your document. Please try again.';
        this.errorModal = true;
      }
      // to check file size
      const fileSizeinMB = files[i].size / (1024 * 1000);
      const size = Math.round(fileSizeinMB * 100) / 100;
      if (size > this.maxSize) {
        this.exceededSize = true;
        // open exceeded file size modal
        this.errorModalTitle = 'Exceeded file size';
        this.errorModalContent = 'The file you’re attempting to upload is over our max file size of 10MB. Try uploading a smaller file.';
        this.errorModal = true;
      }
    }
    // to check same file name
    if (this.selectedFiles && this.selectedFiles.length > 0) {
      let newfileName = '';
      for (let j = 0; j < this.selectedFiles.length; j++) {
        const existingFileName = (this.selectedFiles[j].name).toUpperCase().trim();
        for (let k = 0; k < files.length; k++) {
          newfileName = files[k].name.toUpperCase();
        }
        const sameFile = existingFileName.indexOf(newfileName);
        if (sameFile > -1) {
          this.check = 'sameFile';
        }
      }
    }
    return this.check;
  }
  remove(fileVal, index) {
    this.selectedFiles.splice(index, 1);
    if (this.selectedFiles.length === 0) {
      this.addNewFile = false;
    }
    if (this.errors && this.errors.length > 0) {
      for (let i = 0; i < this.errors.length; i++) {
        if (fileVal === this.errors[i].name) {
          this.errors.splice(i, 1);
        }
      }
    }
  }
  delaySubmit(selectedFiles, errors) {
    setTimeout(() => { this.filesSubmit(selectedFiles, errors); }, 700);
  }
  filesSubmit(selectedFiles, errors) {
    // uncomment below line and 'type' can be hard coded here to open particular error or modal page
    // this.errors.push({ 'name': '', 'type': errorVal.Generic });
    if (errors.length > 0) {
      for (let l = 0; l < errors.length; l++) {
        if (errors[l].type === errorVal.DocNotAccepted) { // to be handled from backend data
          this.message = 'DocNotAccepted';
          this.router.navigate(['/alertMessage', this.message]);
        }
        if (errors[l].type === errorVal.InfoPrevious) { // to be handled from backend data
          this.message = 'InfoPrevious';
          this.router.navigate(['/alertMessage', this.message]);
        }
        if (errors[l].type === errorVal.TooManyFailed) { // to be handled from backend data
          this.message = 'TooManyFailed';
          this.router.navigate(['/alertMessage', this.message]);
        }
        if (errors[l].type === errorVal.Generic) { // to be handled from backend data
          this.message = 'Generic';
          this.router.navigate(['/alertMessage', this.message]);
        }
      }
    } else if (errors.length === 0) {
      this.router.navigate(['/successUploadReceived']);
    }
  }
  close() {
    this.errorModal = false;
  }
}
