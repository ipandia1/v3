import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-coverage-device-assigned-number-new',
  templateUrl: './coverage-device-assigned-number-new.component.html',
  styleUrls: ['./coverage-device-assigned-number-new.component.scss']
})
export class CoverageDeviceAssignedNumberNewComponent implements OnInit {

  @Output('callBackValue') callBackValue = new EventEmitter<string>();
  subTitle = 'Select Line';
  dropdownId = 'id1';
  selectedLine = '';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Marky Mark, (206) 861-9327', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];

  constructor() { }

  ngOnInit() {
    for  (const  names  of  this.selectLine) {
      const string1 = names.name.split(',');
      if (string1[0].length > 13) {
        names.name  =  string1[0].substring(0, 13) + '... , ' + string1[1] ;
      }
    }
    this.selectedLine = this.selectLine[0].name;
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue; // can use selectedLine variable whenever we want to use the selected value
    }
  }
  select(value: string): void {
    this.selectedLine = value;
   }

}
