import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-success-verified',
  templateUrl: './success-verified.component.html',
  styleUrls: ['./success-verified.component.scss']
})
export class SuccessVerifiedComponent implements OnInit {

  data: any;
  emailId;
  firstName;
  lastName;

  approvalDate = 'July 16, 2018';

  constructor() {
    if (localStorage.getItem('data')) {
      const retrieved = localStorage.getItem('data');
      this.data = JSON.parse(retrieved);
      window.localStorage.clear();
    } else {
      this.data = {
          'firstName': 'Daniel', 'lastName': 'Kessler', 'email': 'daniel.kessler@t-mobile.com'};
    }
    this.emailId = this.data.email;
    this.firstName = this.data.firstName;
    this.lastName = this.data.lastName;
  }

  ngOnInit() {
   
  }

}
