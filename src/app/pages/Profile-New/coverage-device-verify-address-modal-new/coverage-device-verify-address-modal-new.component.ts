import { Component } from '@angular/core';

@Component({
  selector: 'app-coverage-device-verify-address-modal-new',
  templateUrl: './coverage-device-verify-address-modal-new.component.html',
  styleUrls: ['./coverage-device-verify-address-modal-new.component.scss']
})
export class CoverageDeviceVerifyAddressModalNewComponent {

  visible: boolean;
  constructor() { }
    close() {
      this.visible = false;
    }
  }
