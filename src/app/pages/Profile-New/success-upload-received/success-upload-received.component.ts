import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-success-upload-received',
  templateUrl: './success-upload-received.component.html',
  styleUrls: ['./success-upload-received.component.scss']
})
export class SuccessUploadReceivedComponent implements OnInit {
  @Input('firstName') firstName = 'Daniel';
  @Input('lastName') lastName = 'Kessler';
  @Input('emailId') emailId = 'daniel.kessler@t-mobile.com';
  @Input('trackingId') trackingId = 'HKQN-1673-5405';
  @Input('submissionDate') submissionDate = '07/15/2018';

  name = this.firstName + ' ' + this.lastName;

  constructor() { }

  ngOnInit() {
  }

}
