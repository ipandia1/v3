import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-coverage-device-device-address-new',
  templateUrl: './coverage-device-device-address-new.component.html',
  styleUrls: ['./coverage-device-device-address-new.component.scss']
})
export class CoverageDeviceDeviceAddressNewComponent implements OnInit {
  newBillingAddress: BillingAddress;
  subTitle = 'State';
  dropdownId = 'id1';
  selectedLine = 'Select';
  selectlineError: boolean;
  selectLine: Array<any> = [
    { name: 'AL', disabled: true },
    { name: 'AK', disabled: false },
    { name: 'AZ', disabled: false },
    { name: 'AR', disabled: false }
  ];
  constructor() { }
  ngOnInit() {
    this.newBillingAddress = {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipcode: ''
    }
  }
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue;
    }
  }
  select(value: string): void {
    if (!value) {
      this.selectedLine = 'Select';
      this.selectlineError = true;
    }
    else {
    this.selectedLine = value;
    this.selectlineError = false;
    }
   }
   select1() {
     this.selectedLine = 'Select';
     this.selectlineError = true;
   }
}
export class BillingAddress {
  address1: string;
  address2: string;
  city: string;
  state: string;
  zipcode: string;
}
