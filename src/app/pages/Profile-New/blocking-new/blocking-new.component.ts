import { Component, OnInit,  Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-blocking-new',
  templateUrl: './blocking-new.component.html',
  styleUrls: ['./blocking-new.component.scss']
})
export class BlockingNewComponent implements OnInit {
  subTitle = 'Select Line';
  dropdownId = 'id1';
  selectedLine = '';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  select(value: string): void {
    this.selectedLine = value;
   }
  constructor() { }

  ngOnInit() {
    for  (const  names  of  this.selectLine) {
      const string1 = names.name.split(',');
      if (string1[0].length > 13) {
        names.name  =  string1[0].substring(0, 13) + '... , ' + string1[1] ;
      }
    }
    this.selectedLine = this.selectLine[0].name;
  }
}
