import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { state } from '@angular/animations';

@Component({
  selector: 'app-language-settings',
  templateUrl: './language-settings.component.html',
  styleUrls: ['./language-settings.component.scss']
})
export class LanguageSettingsComponent implements OnInit {
  radioValue = [false, false];

  @Output('callBackValue') callBackValue = new EventEmitter<string>();

  subTitle = 'Select Line';
  pahText = 'Choose a preferred language for calling and messaging with your Team of Experts. If you change this line’s preference, \
  you’ll talk to a new team when you contact us.';
  nonPahText = 'Choose a preferred language for calling and messaging with your Team of Experts. ';
  selectedLine = '';
  textValue: string;
  selectLine: Array<any> = [
    { name: 'Alice, (206) 234-5678', disabled: false, pah: true},
    { name: 'Christian, (206)123-4567', disabled: false, pah: false },
    { name: 'Kathy, (206)123-4568', disabled: false, pah: true },
    { name: 'Dylan, (206)123-4569', disabled: false, pah: true }
  ];
  constructor() { }

  ngOnInit() {
    for (const names of this.selectLine) {
      const string1 = names.name.split(',');
      if (string1[0].length > 13) {
        names.name = string1[0].substring(0, 13) + '... , ' + string1[1];
      }
    }
    this.selectedLine = this.selectLine[0].name;
    this.textValue = this.pahText;
    this.radioValue[0] = true;
  }

  changeRadio(index) {
    this.radioValue = [false, false];
    this.radioValue[index] = true;
  }
  
  select (value: string, pah: boolean): void {
    this.selectedLine = value;
    if (pah) {
      this.textValue =  this.pahText;
    } else {
      this.textValue = this.nonPahText;
    }
  }

}
