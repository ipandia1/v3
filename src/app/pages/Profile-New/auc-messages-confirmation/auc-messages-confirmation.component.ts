import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-auc-messages-confirmation',
  templateUrl: './auc-messages-confirmation.component.html',
  styleUrls: ['./auc-messages-confirmation.component.scss']
})
export class AucMessagesConfirmationComponent implements OnInit {
  permConfirm = false;
  headerMessage: string;
  infotext:String;
  ctatext: string;
  constructor() {
   }


  ngOnInit() {
    if (this.permConfirm) {
      this.headerMessage = 'Success!';
      this.ctatext = 'Back to line settings';
      this.infotext='Your permission changes have been saved.';
    } else {
      this.headerMessage = 'Your changes didn’t save.';
      this.ctatext = 'Back to permissions';
      this.infotext='Go back to the permissions screen and try again.';
    }
  }

}
