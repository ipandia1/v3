import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-family-control-new',
  templateUrl: './family-control-new.component.html',
  styleUrls: ['./family-control-new.component.scss']
})
export class FamilyControlComponentNew implements OnInit {
  subTitle = 'Select Line';
  selectedLineData = [];
  lineData = [
    { title: 'Natasha', id: '(000) 000-0000', mobile: '(000) 000-0000' },
    { title: 'Justin', id: '(000) 000-0000', mobile: '(000) 000-0000' },
    { title: 'Samantha', id: '(000) 000-0000', mobile: '(000) 000-0000' },
    { title: '(000) 000-0000', id: '(000) 000-0000', mobile: '(000) 000-0000' }
  ];
  dynamicplaceholder = 'Find a line';
  constructor() { }
  ngOnInit() {
    this.selectedLineData = this.lineData;
    // for  (const  names  of  this.selectLine) {
    //   const string1 = names.name.split(',');
    //   if (string1[0].length > 13) {
    //     names.name  =  string1[0].substring(0, 13) + '... , ' + string1[1] ;
    //   }
    // }
    // this.selectedLine = this.selectLine[0].name;

  }
  // select(value: string): void {
  //   this.selectedLine = value;
  //  }

  selectedSearchValue(event) {
    if (event === '') {
      this.selectedLineData = this.lineData;
    } else {
      this.selectedLineData = this.lineData.filter(data =>
        data.title === event);
    }
  }
}
