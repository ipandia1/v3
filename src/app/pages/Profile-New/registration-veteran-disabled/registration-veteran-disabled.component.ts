import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { flatten } from '@angular/compiler';

@Component({
  selector: 'app-registration-veteran-disabled',
  templateUrl: './registration-veteran-disabled.component.html',
  styleUrls: ['./registration-veteran-disabled.component.scss']
})
export class RegistrationVeteranDisabledComponent implements OnInit {
  dumyyear: string;
  IsMatch: boolean;
  selectDateError: boolean;
  selectBranchError: boolean;
  selectedBranch: string;
  selectedDate: string;
  ischecked2: boolean;
  isDisabled: boolean;
  comparisondate = '3/5/2001';
  constructor(private _router: Router) {
  }

  veteranArray = [
    { 'name': 'Veteran or retiree', 'message': 'Those honorably discharged after serving in the US Military.' },
    { 'name': 'Active duty', 'message': 'Serving in the US Military under Title 10 orders.' },
    { 'name': 'National Guard or Reserve', 'message': 'Currently serving in the US Military under Title 32 orders.' },
    { 'name': 'Gold Star', 'message': 'A family member who has lost a loved one in service to the nation.' }
  ];
  selectedyear: any;
  selectedmonth: any;
  selectedDay: any;
  name = {
    dutyStatus: 'Veteran or retiree',
    firstName: '',
    lastName: '',
    branch: '',
    dobmonth: '',
    dobday: '',
    dobyear: '',
    selectedDate: '',
    email: '',
    confirmemail: '',
    phonenumber: ''
  };
  subTitle1 = 'Branch of service';
  subTitle2 = 'Discharge date';
  selectBranches: Array<any> = [
    { id: -1, name: 'Select branch of service', disabled: false },
    { id: 1, name: 'AL', disabled: false },
    { id: 2, name: 'AK', disabled: false },
    { id: 3, name: 'AZ', disabled: false },
    { id: 4, name: 'AR', disabled: false }
  ];
  dischargeDate: Array<any> = [
    { name: 'Selected Date', disabled: false },
    { name: '2013', disabled: false },
    { name: '2014', disabled: false },
    { name: '2015', disabled: false },
    { name: '2016', disabled: false }
  ];
  Month: Array<any> = [
    { id: 1, name: 'January', disabled: false },
    { id: 2, name: 'February', disabled: false },
    { id: 3, name: 'March', disabled: false },
    { id: 4, name: 'April', disabled: false },
    { id: 5, name: 'May', disabled: false },
    { id: 6, name: 'June', disabled: false },
    { id: 7, name: 'July', disabled: false },
    { id: 8, name: 'August', disabled: false },
    { id: 9, name: 'September', disabled: false },
    { id: 10, name: 'October', disabled: false },
    { id: 11, name: 'November', disabled: false },
    { id: 12, name: 'December', disabled: false },
  ];
  years: any[];
  days: any[];
  selected_Month: string;
  selected_Year: string;
  selected_Date: string;
  validdate: boolean;
  validDob: boolean;

  ngOnInit() {
    const numberOfYears = (new Date()).getFullYear() - 10;
    const values = numberOfYears - 1900;
    this.years = Array.from(new Array(values), (val, index) => index + 1900);
    this.days = Array.from(new Array(31), (val, index) => index + 1);

    const retrieved = localStorage.getItem('data');
    this.selected_Month = 'Date of birth';
    this.selected_Year = 'Selected Year';
    this.selected_Date = 'Selected Date';
    if (retrieved) {
      this.name = JSON.parse(retrieved);
      this.isDisabled = false;
      this.selectedBranch = this.name.branch;
      this.selectedDate = this.name.selectedDate;
      this.selectedmonth = this.name.dobmonth;
      this.selectedyear = Number(this.name.dobyear);
      this.selectedDay = Number(this.name.dobday);
    } else {
      this.selectedBranch = 'Select branch of service';
      this.selectedDate = 'Selected Date';
      this.selectedDay = 'Day';
      this.selectedyear = 'Year';
      this.selectedmonth = 'Month';
      this.name.dobday = '' + this.selectedDay;
      this.name.dobmonth = '' + this.selectedmonth;
      this.name.dobyear = '' + this.selectedyear;
      this.isDisabled = true;
    }
  }
  selectmonth(month: string) {
    this.selectedmonth = month;
    this.name.dobmonth = this.selectedmonth;
    this.verifydatevalues();
    this.validatedate();
  }
  verifydatevalues() {
     let selectedmonthid = this.Month.find(month => month.name === this.selectedmonth);
    if (this.selectedyear == 'Year') {
      this.dumyyear = '1901';
    } else {
      this.dumyyear = this.selectedyear;
    }
    if (selectedmonthid == undefined) {
      selectedmonthid = [];
      selectedmonthid['id'] = 1;
    }
    this.days = Array.from(new Array(this.getDaysInMonth(selectedmonthid.id, this.dumyyear)),
      (val, index) => index + 1);
    if (this.selectedDay > this.days.length) {
      this.selectedDay = this.days[this.days.length - 1];
        }
  }
  selectyear(year) {
    this.selectedyear = year;
    this.name.dobyear = '' + this.selectedyear;
    this.verifydatevalues();
    this.validatedate();
  }
  validatedate() {
    let monthformat;
    let currentdate;
    if (this.selectedmonth) {
      monthformat = this.Month.find(month => month.name === this.selectedmonth);
    }
    if (this.selectedmonth != 'Month' && this.selectedDay != 'Day' && this.selectedyear != 'Year') {
      currentdate = monthformat.id + '/' + this.selectedDay + '/' + this.selectedyear;
      this.validDob = false;
      if (currentdate == this.comparisondate) {
        this.validdate = true;
        this.isDisabled = true;
      } else {
        this.validdate = false;
        this.isDisabled = false;
      }
    } else {
      this.validDob = true;
      this.isDisabled = true;
    }
  }
  selectDay(day: number) {
    this.selectedDay = day;
    this.name.dobday = '' + this.selectedDay;
    this.validatedate();
  }
  checkLeapYear(year: number) {
    return (year % 400 === 0 || year % 100 !== 0) && (year % 4 === 0) ? true : false;
  }
  getDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }
  select(value: number): void {
    if (value == -1) {
      this.selectedBranch = 'Select branch of service';
      this.selectBranchError = true;
    } else {
      this.selectedBranch = this.selectBranches[value].name;
      this.name.branch = this.selectedBranch;
      this.selectBranchError = false;
    }
    this.dropdownvalidation();
  }
  dropdownvalidation() {
    if (this.selectedBranch === 'Select branch of service' || this.selectedDate === 'Selected Date'
      || this.selectedyear == 'Year' || this.selectedmonth == 'Month' || this.selectedDay == 'Day') {
      this.isDisabled = true;
    } else {
      this.isDisabled = false;
    }
  }  selectDate(value: string): void {
    if (value == 'Selected Date') {
      this.selectedDate = 'Selected Date';
      this.selectDateError = true;
    } else {
      this.selectedDate = value;
      this.name.selectedDate = this.selectedDate;
      this.selectDateError = false;
    }
    this.dropdownvalidation();
  }
  compareEmail() {
    if (this.name.email != this.name.confirmemail && this.name.confirmemail != '') {
      this.IsMatch = true;
    } else {
      this.IsMatch = false;
    }
  }
  submit() {
    localStorage.setItem('data', JSON.stringify(this.name));
    setTimeout(() => {
      this._router.navigate(['/confirmInfo']);
    }, 500);
  }

}
