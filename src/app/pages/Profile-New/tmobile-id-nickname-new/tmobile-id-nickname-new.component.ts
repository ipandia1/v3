import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tmobile-id-nickname-new',
  templateUrl: './tmobile-id-nickname-new.component.html',
  styleUrls: ['./tmobile-id-nickname-new.component.scss']
})
export class TmobileIdNicknameNewComponent implements OnInit {

  name: name;
  constructor() { }

  ngOnInit() {
    this.name = {
      firstName: '', lastName: ''
    }
  }

}

// tslint:disable-next-line:class-name
export class name {
  firstName: string;
  lastName: string ;
}
