import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-family-control-web-gaurd-new',
  templateUrl: './family-control-web-gaurd-new.component.html',
  styleUrls: ['./family-control-web-gaurd-new.component.scss']
})
// tslint:disable-next-line:component-class-suffix
export class FamilyControlWebGaurdComponentNew implements OnInit {
  public showDetails: Boolean = false;
  subTitle = 'Select Line';
  selectedLine = '';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  constructor() { }
  ngOnInit() {
    for  (const  names  of  this.selectLine) {
      const string1 = names.name.split(',');
      if (string1[0].length > 13) {
        names.name  =  string1[0].substring(0, 13) + '... , ' + string1[1] ;
      }
    }
    this.selectedLine = this.selectLine[0].name;
  }
  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  }
  select(value: string): void {
    this.selectedLine = value;
   }
}
