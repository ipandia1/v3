import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: "app-line-settings-caller-id",
  templateUrl: "./line-settings-caller-id.component.html",
  styleUrls: ["./line-settings-caller-id.component.scss"]
})
export class LineSettingsCallerIdComponent implements OnInit {
  callerID;
  subTitle = "Select Line";
  dropdownId = "id1";
  selectedLine = "";
  restricted = false; // for restricted page change this to false / true
  selectLine: Array<any> = [
    {
      name: "Zach, (206) 123-4567",
      firstName: "Zachary",
      lastName: "Frost",
      disabled: false
    },
    {
      name: "Sam, (206)-555-1212",
      firstName: "Zachary",
      lastName: "Frost",
      disabled: false
    },
    {
      name: "Gwen, (206)-555-1212",
      firstName: "Zachary",
      lastName: "Frost",
      disabled: false
    },
    {
      name: "Jill, (206)-555-1212",
      firstName: "Zachary",
      lastName: "Frost",
      disabled: false
    }
  ];

  constructor(private _location: Location, private router: Router) {}

  ngOnInit() {
     this.callerID = {
      firstName: "",
      lastName: ""
    };
    this.selectedLine = this.selectLine[0].name;
    this.callerID.firstName = this.selectLine[0].firstName;
    this.callerID.lastName = this.selectLine[0].lastName;
   
  }
  gotopreviouspage() {
    this._location.back();
  }
  select(value: string): void {
    this.selectedLine = value;
  }
  gotoConfirmation() {
      localStorage.setItem('callerID',JSON.stringify(this.callerID));
      this.router.navigate(['/lineSettingsSelectCalleridConfirmation']);
  }
}
