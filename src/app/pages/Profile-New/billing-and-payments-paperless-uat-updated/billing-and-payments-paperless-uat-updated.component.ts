import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-billing-and-payments-paperless-uat-updated',
  templateUrl: './billing-and-payments-paperless-uat-updated.component.html',
  styleUrls: ['./billing-and-payments-paperless-uat-updated.component.scss']
})
export class BillingAndPaymentsPaperlessUatUpdatedComponent implements OnInit {

  ischecked1 = true;
  ischecked2 = false;
  disable1 = true;
  visible = false;
  constructor(private commonService: CommonService) { }

  ngOnInit() {
  }

  toDisable(id) {
  
    if (id === 1) {
      this.ischecked2 = false;
      this.disable1 = true;
      this.ischecked1 = true;
    }
    else {
    this.disable1 = false;
    this.ischecked2 = true;
    this.ischecked1 = false;
    }
  }
  close() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  select() {
    this.visible = false;
    this.commonService.showScroll(true);
  }
  change() {
    this.commonService.showScroll(false);
    this.visible = true;
  }

}
