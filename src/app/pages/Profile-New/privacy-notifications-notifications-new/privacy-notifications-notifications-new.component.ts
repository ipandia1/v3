import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-privacy-notifications-notifications-new',
  templateUrl: './privacy-notifications-notifications-new.component.html',
  styleUrls: ['./privacy-notifications-notifications-new.component.scss']
})
export class PrivacyNotificationsNotificationsNewComponent implements OnInit {
  checkOpt1 = false;
  checkOpt2 = false;
  subTitle = 'Select Line';
  selectedLine = '';
  selectLine: Array<any> = [
    { name: 'John Legere, (206)-555-1212', disabled: false },
    { name: 'Sam, (206)-555-1212', disabled: false },
    { name: 'Gwen, (206)-555-1212', disabled: false },
    { name: 'Jill, (206)-555-1212', disabled: false }
  ];
  constructor() { }
  ngOnInit() {
    for  (const  names  of  this.selectLine) {
      const string1 = names.name.split(',');
      if (string1[0].length > 13) {
        names.name  =  string1[0].substring(0, 13) + '... , ' + string1[1] ;
      }
    }
    this.selectedLine = this.selectLine[0].name;
  }
  select(value: string): void {
    this.selectedLine = value;
   }
}
