import { Component, OnInit, Input } from '@angular/core';
enum error { DocNotAccepted, InfoPrevious, TooManyFailed, Generic }
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-alert-message',
  templateUrl: './alert-message.component.html',
  styleUrls: ['./alert-message.component.scss']
})
export class AlertMessageComponent implements OnInit {
  mainText2: string;
  btnText2: string;
  title: string;
  iconClass: string;
  title2: string;
  mainText: string;
  secondaryText: string;
  btnText1: string;
  footertext: string;
  constructor(private route: ActivatedRoute, private location: Location) { }
  status: string;
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.status = params.get('status');
    });
    const errorVal = this.status;
    switch (errorVal) {
      case 'DocNotAccepted':
        this.title = 'Military verification';
        this.iconClass = 'exclamation-error-64px';
        this.title2 = 'Document not accepted';
        this.mainText = 'Thanks for your submission; unfortunately, it was not accepted as validation of status. To try again, follow the link below to upload a new document.';
        this.mainText2 = '[Dynamic content provided by SheerID]';
        this.btnText1 = 'Upload new document';
        break;
      case 'InfoPrevious':
        this.title = 'Military verification';
        this.iconClass = 'exclamation-error-64px';
        this.title2 = 'Information previously submitted';
        this.mainText = 'Upon review, it was determined that the information you provided had been previously submitted. The same military member information cannot be submitted twice.';
        this.mainText2 = 'Please restart verification or contact Customer Care if you need additional details.';
        this.btnText1 = 'Contact Customer Care';
        this.btnText2 = 'Restart verification';
        break;
      case 'TooManyFailed':
        this.title = 'Military verification';
        this.iconClass = 'warning-red-outline';
        this.title2 = 'Too many failed attempts';
        this.mainText = 'You’ve reached the maximum number of attempts to upload your documentation. Please restart verification or contact Customer Care for further assistance.';
        this.btnText1 = 'Contact Customer Care';
        this.btnText2 = 'Restart verification';
        break;
      case 'Generic':
        this.title = 'Looks like we got our wires crossed.';
        this.iconClass = 'warning-red-outline';
        this.title2 = '';
        this.mainText = 'We weren’t able to process your request at this time. Please try again later.';
        this.btnText1 = 'Back to verification';
        break;
    }
  }
}
