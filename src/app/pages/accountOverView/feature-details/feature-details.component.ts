import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feature-details',
  templateUrl: './feature-details.component.html',
  styleUrls: ['./feature-details.component.scss']
})
export class FeatureDetailsComponent implements OnInit {
  benefitsList = {
    planName: 'T-Mobile ONE +',
    planAmt: '10.00',
    desc: 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vitae lorem leo. Ut nec est vitae tortor consequat venenatis sit amet ac ipsum.',
    title: 'Here\'s what you get with ONE Plus,in addition to all the benefits of T-Mobile ONE:',
    data : [ '20GB of 4G LTE mobile hotspot in the US', 'Unlimited 3G mobile hotspot after 20GB (up to 600 kbps)',
    'Unlimited HD streaming while in the US ', 'Unlimited data at 2x speeds in 210+ destinations abroad (up to 256 kbps)',
    'Unlimited Gogo in-flight Wi-Fi - on Gogo®-enabled flights to, from, or within the U.S.',
    'Voicemail to Text - Read voicemails on the go',
    'Name ID - Identify calls from unknown number'
    ]
  };
  constructor() { }

  ngOnInit() {
  }

}
