import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cost-details',
  templateUrl: './cost-details.component.html',
  styleUrls: ['./cost-details.component.scss']
})
export class CostDetailsComponent implements OnInit {
  monthlyDetails = {
    title: 'Monthly total',
    price: '$306.00'
  };

  discountDetails = {
    title: 'Applied discounts and promotions',
    price: '160.00',
    data: [
      {desc: '$20 of Autopay Discount'},
      {desc: '$10.99 Netflix On Us'}
    ]
  };

  costDetails = [
    {
      name: 'Natasha',
      totalAmount: '$43.00',
      data: [{
        desc: '(917) 7765-1111',
        subDesc: 'Voice Line',
        value: 'Included in plan',
        discountInfo: []
      },
      {
        desc: 'Equipment',
        subDesc: 'iPhone 6S 128 GB Rose Gold',
        value: '$30.00',
        discountInfo: []
      },
      {
        desc: 'Add-ons',
        subDesc: '1 add-on',
        value: '$13.00',
        discountInfo: []
      }],
      lineDetailUrl: '/lineDetail'
    },
    {
      name: 'Justin',
      totalAmount: '$10.00',
      data: [{
        desc: '(917) 7765-1111',
        subDesc: 'Voice Line',
        value: 'Included in plan',
        discountInfo: []
      },
      {
        desc: 'Equipment',
        subDesc: 'Samsung Galazy S7',
        value: '$10.00',
        discountInfo: [{
          title: 'Applied discounts and promotions',
          price: '',
          data: [
            { desc: '$20 EIP Discount' }
          ]
        }]
      },
      {
        desc: 'Add-ons',
        subDesc: 'No Add-on',
        value: '$0.00',
        discountInfo: []
      }],
      lineDetailUrl: '/lineDetail'
    },
    {
      name: 'Thadeu',
      totalAmount: '$63.00',
      data: [{
        desc: '(917) 7765-1111',
        subDesc: 'Voice Line',
        value: 'Included in plan',
        discountInfo: []
      },
      {
        desc: 'Equipment',
        subDesc: 'iPhone X 128 GB Rose Gold',
        value: '$28.00',
        discountInfo: []
      },
      {
        desc: 'Add-ons',
        subDesc: '3 Add-ons',
        value: '$35.00',
        discountInfo: []
      }],
      lineDetailUrl: '/lineDetail'
    },
    {
      name: 'Liz',
      totalAmount: '$0.00',
      data: [{
        desc: '(917) 7765-1111',
        subDesc: 'Voice Line',
        value: 'Included in plan',
        discountInfo: []
      },
      {
        desc: 'Equipment',
        subDesc: 'iPhone 6 Plus',
        value: 'Owned',
        discountInfo: []
      },
      {
        desc: 'Add-ons',
        subDesc: '3 Add-ons',
        value: '$0.00',
        discountInfo: []
      }],
      lineDetailUrl: '/lineDetail'
    },
    {
      name: 'Robert',
      totalAmount: '30.00',
      data: [{
        desc: '(917) 7765-1111',
        subDesc: 'Data only device',
        value: '',
        discountInfo: [{
          title: 'Applied discounts and promotions',
          price: '$20.00',
          data: [
            { desc: '$5 of Autopay Discount' },
            { desc: '$50 of Hybrid BAN' },
            { desc: '$10.99 Netflix On Us' },
          ]
        }]
      },
      {
        desc: 'Equipment',
        subDesc: 'Apple iPad Mini',
        value: '$10.00',
        discountInfo: []
      },
      {
        desc: 'Add-ons',
        subDesc: '3 Add-ons',
        value: '$0.00',
        discountInfo: []
      }],
      lineDetailUrl: '/lineDetail'
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
