import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-line-detail',
  templateUrl: './line-detail.component.html',
  styleUrls: ['./line-detail.component.scss']
})
export class LineDetailComponent implements OnInit {
  settings = { title: 'Profile and line', desc: 'Name, email, password...' };
  //  Device and Line usage starts
  phoneTitle = 'iPhone 6S 128 GB Rose Gold';
  phoneImg = 'assets/img/account-overview/line-detail/mobile.PNG';
  signalImg = 'assets/img/account-overview/line-detail/signal.PNG';
  deviceDetails1: Array<any> = [
    { descTitle: 'Months to upgrade', value: '4', unit: 'MO' },
    { descTitle: 'Insurance', value: 'Protected', unit: '' },
    { descTitle: 'Device support', value: '', unit: '' }
  ];
  usage: Array<any> = [
    { title: 'Line usage', val1: '20.5', val2: '129', val3: '33', unit1: 'GB', unit2: 'SMS', unit3: 'MIN' }
  ];
 // Equipment financing and active add ons- start
  // tslint:disable-next-line:member-ordering
  public addonValues = [
    { descTitle: 'T-Mobile ONE +', value: '$10.00/mo.', bottomDivider: false, topDivider: false },
    { descTitle: 'Netflix Premium', value: '$3.00/mo.', bottomDivider: false, topDivider: false }
  ];
  addon = 'Active add-ons';
  eqFinancing = 'Equipment financing';
  eqFinancingValues = {
    mnthlyCharges: 'Monthly charges',
    includes: 'Includes',
    includeValueList: ['iPhone 6s 128 GB', 'Beats Headphones']
  };
  constructor() { }
  ngOnInit() {
  }
}
