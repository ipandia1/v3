import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  deviceDetails = [
    {
      img: './assets/img/account-overview/line-detail/phone-placeholder.svg', additionalInfo: '', name: 'Natasha', number: '(917) 765-1111', amount: '$43.00',
      pmo: '/mo', addOnInfo: 'iPhone 6 Plus, 1 Add-On', url: '/lineDetailNew'
    },
    {
      img: './assets/img/account-overview/line-detail/phone-placeholder.svg', additionalInfo: 'Upgrade available', name: 'Justin', number: '(917) 765-1111',
      amount: '$10.00', pmo: '/mo',  addOnInfo: 'Galaxy S7, No Add-Ons', url: '/lineDetailNew'
    },
    {
      img: './assets/img/account-overview/line-detail/phone-placeholder.svg', additionalInfo: '', name: 'Thadeu', number: '(917) 765-1111', amount: '$63.00',
      pmo: '/mo', addOnInfo: 'iPhone X, 3 Add-Ons', url: '/lineDetailNew'
    },
    {
      img: './assets/img/account-overview/line-detail/phone-placeholder.svg', additionalInfo: '', name: 'Liz', number: '(917) 765-1111', amount: '$0.00',
      pmo: '/mo', addOnInfo: 'iPhone 6 Plus, No Add-On', url: '/lineDetailNew'
    },
    {
      img: './assets/img/account-overview/line-detail/phone-placeholder.svg', additionalInfo: 'high data usage', name: 'Robert', number: 'Data only device',
      amount: '$30.00', pmo: '/mo',  addOnInfo: 'iPad Mini, 1 Add-On', url: '/lineDetailNew'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
