import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CommonService } from '../../../services/common.service';
// import { ElementRef } from '@angular/core/src/linker/element_ref';

@Component({
  selector: 'app-line-detail-new',
  templateUrl: './line-detail-new.component.html',
  styleUrls: ['./line-detail-new.component.scss']
})
export class LineDetailNewComponent implements OnInit {
  settings = { title: 'Profile and line', desc: 'Name, email, password...' };
  //  Device and Line usage starts
  phoneTitle = 'iPhone 6S 128 GB Rose Gold';
  phoneImg = 'assets/img/account-overview/line-detail/mobile.PNG';
  signalImg = 'assets/img/account-overview/line-detail/signal.PNG';
  pencilImg = 'assets/img/account-overview/line-detail/pencilImg.PNG';
  deviceDetails: any;
  deviceRestrictedDetails: any;
  deviceDetails1: Array<any> = [
    { descTitle: 'Months to upgrade', value: '4', unit: 'MO' },
    { descTitle: 'Insurance', value: 'Protected', unit: '' },
    { descTitle: 'Device support', value: '', unit: '' }
  ];

  deviceBlade: Array<any> = [
    {description: 'Check device unlock status'},
    {description: 'Check voicemail'},
    {description: 'Suspend a line temporarily'},
    {description: 'I have another device/SIM to use'},
    {description: 'Device support'}
  ];

  usage: Array<any> = [
    { title: 'Line usage', val1: '20.5', val2: '129', val3: '33', unit1: 'GB', unit2: 'SMS', unit3: 'MIN' }
  ];
  // Equipment financing and active add ons- start
  // tslint:disable-next-line:member-ordering
  public addonValues = [
    { descTitle: 'T-Mobile ONE +', value: '$10.00/mo.', bottomDivider: false, topDivider: false },
    { descTitle: 'Netflix Premium', value: '$3.00/mo.', bottomDivider: false, topDivider: false }
  ];
  addon = 'Active add-ons';
  eqFinancing = 'Equipment financing';
  eqFinancingValues = {
    mnthlyCharges: 'Monthly charges',
    includes: 'Includes',
    includeValueList: ['iPhone 6s 128 GB', 'Beats Headphones']
  };
  lineDetails = { name: 'Eric', phone_number: '(917) 765-1111' };
  visible: boolean;
  fname: String;
  fullName: string;
  lname: string;
  index: number;
  editName: any = {
    firstName: '',
    lastName: ''
  };
  @ViewChild('firstName') firstName: ElementRef;  
  constructor(private commonService: CommonService,  private elementref: ElementRef) { }

  openEditModal() {
    this.fullName = this.lineDetails.name;
    this.index = this.fullName.indexOf(' ');  // Gets the first index where a space occours
    if (this.index >= 0) {
      this.fname = this.fullName.substr(0, this.index); // Gets the first part
      this.lname = this.fullName.substr(this.index + 1);  // Gets the text part
    } else {
      this.fname = this.fullName;
    }
    this.editName = {
      firstName: this.fname,
      lastName: this.lname
    };  
    setTimeout(() => this.elementref.nativeElement.querySelector('#firstName').focus(),
     0);
   // this.elementref.nativeElement.querySelector('#firstName').focus();
    this.visible = !this.visible;
    this.commonService.showScroll(false);
  }
  close() {
    this.visible = false;
    this.commonService.showScroll(true);
    this.elementref.nativeElement.querySelector('#name').focus();
  }

  save() {  
    this.visible = false;
    this.commonService.showScroll(true);
    this.elementref.nativeElement.querySelector('#name').focus();
    this.lineDetails.name = this.editName.firstName +' '+this.editName.lastName;
  }
  ngOnInit() {

     // lost or stolen details
  // this.deviceDetails = [
  // {deviceStatus: 'Device lost or stolen', deviceResponse: 'I found my device'},
  // {deviceStatus: 'Device lost or stolen', deviceResponse: 'Suspend line'}
  // ];

  // suspension details
  this.deviceDetails = [
    {deviceStatus: 'This line is suspended', deviceResponse: 'Restore service'}
    ];
// resricted details
  // this.deviceRestrictedDetails = [
  //   {title: 'Insufficient permissions', description: 'Only the primary account holder (PAH) can modify permission on this account.',
  //    subtitle: 'Please contact your PAH to:',
  //    list: [
  //      {	content: 	'Change your SIM' },
  //      {  content: 	'Report your device lost or stolen'},
  //      {  content: 	'Temporarily suspend your line'},
  //      {  content: 	'File a damaged device claim'}
  //    ]}
  // ];
}
backTabEvent(event, mainId, lastId) {
    const me = this;
    const allElements = document.getElementById(mainId).querySelectorAll
      ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
    if (document.activeElement === allElements[0]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          setTimeout(function () {
            // document.getElementById(lastId).focus();
            me.elementref.nativeElement.querySelector('#' + lastId).focus();

          }, 0);
        }
      }
    }
    if (document.activeElement === document.getElementById(mainId)) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
          event.preventDefault();
          setTimeout(function () {
            // document.getElementById(lastId).focus();
            me.elementref.nativeElement.querySelector('#' + lastId).focus();

          }, 0);
        }
      }
    }
    if (document.activeElement === allElements[allElements.length - 1]) {
      if (event.keyCode === 9) {
        if (event.shiftKey) {
        } else {
          event.preventDefault();
          setTimeout(function () {
            // document.getElementById(mainId).focus();
            me.elementref.nativeElement.querySelector('#' + mainId).focus();

          }, 0);
        }
      }
    }
  }

}
