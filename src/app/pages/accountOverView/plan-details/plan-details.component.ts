import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plan-details',
  templateUrl: './plan-details.component.html',
  styleUrls: ['./plan-details.component.scss']
})
export class PlanDetailsComponent implements OnInit {
  circleImg = './assets/img/account-overview/plan-detail/graycircle.png';

  discountDetails = {
    title: 'Applied discounts and promotions',
    price: '160.00',
    data: [
      {desc: '$20 of Autopay Discount'},
      {desc: '$10.99 Netflix On Us'}
    ]
  };

  associatedLineDetails = [
    { name: 'Natasha', phNo: '(917) 765-1111' },
    { name: 'Justin', phNo: '(917) 765-1111' },
    { name: 'Thadeu', phNo: '(917) 765-1111' },
    { name: 'Liz', phNo: '(917) 765-1111' }];

  planDetails = [
    { planName: 'Data, Talk & Text', included: 'Unlimited' },
    { planName: 'Gogo in-flight benefits', included: 'In-flight Texting and 1 Hour of free internet' },
    { planName: 'Free Video Streaming', included: 'Included' },
    { planName: 'Netflix On Us', included: 'Included' },
    { planName: 'Texting & data abroad', included: 'Included' },
    { planName: 'Family Allowance', included: 'Included' },
    { planName: 'Use your device in Mexico & Canada', included: 'Talk, Text, & Data Included' }];
    
  constructor() { }

  ngOnInit() {
  }

}
