import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {

  isCollapsed = true;

constructor() {
window.scrollTo(0, 0);
}

ngOnInit() {
}
clickToggle = function () {
this.isCollapsed = !this.isCollapsed;
} 

}
