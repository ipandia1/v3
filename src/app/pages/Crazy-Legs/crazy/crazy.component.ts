import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-crazy',
  templateUrl: './crazy.component.html',
  styleUrls: ['./crazy.component.scss']
})
export class CrazyComponent implements OnInit {

  isCollapsed = true;

  constructor() {
     window.scrollTo(0, 0);
   }

  ngOnInit() {
  }
  clickToggle = function () {
    this.isCollapsed = !this.isCollapsed;
  }

   public dataPassInfo: Array<Object> = [
    { descTitle: 'Taxes and fees included'},
    { descTitle: 'Unlimited talk, text & data' },
    { descTitle: 'Unlimited travel' },
    { descTitle: 'Netflix'},
    { descTitle: 'Family Allowance' }
  ];

   public dataPassInfo1: Array<Object> = [
    { descTitle: 'Taxes and fees included'},
    { descTitle: '16GB 4G LTE data' },
    { descTitle: 'Unlimited talk, text' },
    { descTitle: 'Netflix'},
    { descTitle: 'Unlimited travel' }
  ];

}
