import { Component } from '@angular/core';

@Component({
  selector: 'app-mconfirmation',
  templateUrl: './mconfirmation.component.html',
  styleUrls: ['./mconfirmation.component.scss']
})
export class MconfirmationComponent {
 public smallDevice:boolean=false;
    public name: string = "Kirby!";
    public showDetails: boolean = false;
  constructor() { }
  public dataPassInfo: Array<Object> = [
    { descTitle: 'Order no.', value: '0123456789' },
    { descTitle: 'Payment type', value: 'Visa ****1234' },
    { descTitle: 'Autopay date', value: 'June 14, 2018' },
    { descTitle: 'Name on account', value: 'Kirby Thorton' },
    { descTitle: 'T-Mobile account no.', value: '12346789' }
  ];

  public addItems: Array<Object> = [
    { descTitle: 'T-Mobile ONE', value: 'xxx.xx', effectTitle: 'Effective May 24, 2018' },
  
  ];
  public removeItems: Array<Object> = [
    { descTitle: 'Single line name service', value: '15.99' }
  ];
  detailsPayment = function () {
    this.showDetails = !this.showDetails;
  }
}
