import { Component, OnInit, ViewEncapsulation, ElementRef } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { PlatformLocation } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
import { SpinnerService } from './services/spinner.service';
import { CommonService } from './services/common.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  noOverlayV2: boolean;
  showLoaderV2: boolean;
  title = 'app';
  showLoader: boolean;
  noOverlay: boolean;
  constructor(private spinnerservice: SpinnerService, location: PlatformLocation, private router: Router,
    private commonService: CommonService, private _elementRef: ElementRef) {
    location.onPopState(() => {
      // console.log('pressed back!');
      this.showScroll();
    });

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
      this.spinnerservice.showspinner(false);
      this.spinnerservice.showoverlay(false);
      if (this.commonService.visibleGlobalheader === true) {
        const slectedEl = this._elementRef.nativeElement.children[0];
        const el = slectedEl.querySelectorAll('#gh-tab-focus');
        if (el !== null || el !== undefined) {
          el[0].focus();
        }
      }
    });

  }

  ngOnInit() {
    this.spinnerservice.showSpinner.subscribe((val: boolean) => {
      this.showLoader = val;
    });
    this.spinnerservice.showOverlay.subscribe((val: boolean) => {
      this.noOverlay = val;
    });

    this.spinnerservice.showSpinnerV2.subscribe((val: boolean) => {
      this.showLoaderV2 = val;
    });
    this.spinnerservice.showOverlayV2.subscribe((val: boolean) => {
      this.noOverlayV2 = val;
    });
    this.commonService.showGlobalHeader();
    this.commonService.showGlobalFooter();
    this.commonService.showTmoHeader();
  }
  showScroll() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('noScroll');
  }
  hideScroll() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('noScroll');
  }
}
