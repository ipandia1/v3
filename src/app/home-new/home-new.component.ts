import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-new',
  templateUrl: './home-new.component.html',
  styleUrls: ['./home-new.component.scss']
})
export class HomeNewComponent implements OnInit {
  selectedLine = 'Select';
  selectedVal = 'Select';
  showStyleGuide = false;
  showStyleGuideAlerts = false;
  showODF = false;
  showODFAddons = false;
  showFamilyAllowance = false;
  showOTPpart1 = false;
  showOTPpart2 = false;
  showSSU = false;
  showPDE = false;
  showErrorHandling = false;
  showAccountHistory = false;
  showAccountOverView = false;
  showDeviceUnlock = false;
  showProfile = false;
  showProfileNew = false;
  showAutoPay = false;
  showPA = false;
  showPlanComparision = false;
  showContactUs = false;
  showEipJoD = false;
  showPortIn = false;
  showChangePlan = false;
  showaddOnsPhase2 = false;
  showbalanceLatency = false;
  showstandalone = false;
  inWallet = false;
  inSession = false;
  claimFlow = false;
  pciPages = false;
  temporarysuspension = false;
  suzySheep = false;
  servicePromo = false;
  pageNames: Array<any> = [
    {name: 'Select', disabled: true},
    {name: 'Style Guide', disabled: false},
    {name: 'Style Guide- Alerts & Notification', disabled: false},
    {name: 'ODF', disabled: false},
    {name: 'ODF Add ons', disabled: false},
    {name: 'Family allowance Pages', disabled: false},
    {name: 'OTP - Part 1', disabled: false},
    {name: 'OTP - Part 2', disabled: false},
    {name: 'SSU', disabled: false},
    {name: 'PDE', disabled: false},
    {name: 'Error Handling', disabled: false},
    {name: 'Account History', disabled: false},
    {name: 'Account OverView', disabled: false},
    {name: 'Device Unlock', disabled: false},
    {name: 'Profile', disabled: false},
    {name: 'Profile New', disabled: false},
    {name: 'AutoPay', disabled: false},
    {name: 'PA', disabled: false},
    {name: 'Plan Comparision', disabled: false},
    {name: 'Contact Us', disabled: false},
    {name: 'EIP JOD', disabled: false},
    {name: 'Port In', disabled: false},
    {name: 'Change Plan' , disabled: false},
    {name: 'Add Ons Phase2' , disabled: false},
    {name: 'Balance Latency Treatment-PA' , disabled: false},
    {name: 'More payment options' , disabled: false},
    {name: 'In Wallet', disabled: false},
    {name: 'In Session', disabled: false},
    {name: 'Claim Flow', disabled: false},
    {name: 'PCI', disabled: false},
    {name: 'Temporary Suspension', disabled: false},
    {name: 'SuzySheep', disabled: false},
    {name: 'servicePromo', disabled: false}
  ];
  callBackValue = new EventEmitter<string>();
  public handleLine(callBackValue: string) {
    if (callBackValue !== '') {
      this.selectedLine = callBackValue; // can use selectedVal variable whenever we want to use the selected value
    }
  }
  constructor() { }

  ngOnInit() {
  const  selectitem = localStorage.getItem('selectedval');
    if (selectitem != null) {
    this.select(selectitem);
  }
  }
  select(value: string): any {
      this.selectedVal = value;
      this.callBackValue.emit(this.selectedVal);
      if (this.selectedVal === 'Style Guide') { this.showStyleGuide = true; } else { this.showStyleGuide = false; }
      if (this.selectedVal === 'Style Guide- Alerts & Notification') { this.showStyleGuideAlerts = true; } else { this.showStyleGuideAlerts = false; }
      if (this.selectedVal === 'ODF') { this.showODF = true; } else { this.showODF = false; }
      if (this.selectedVal === 'ODF Add ons') { this.showODFAddons = true; } else { this.showODFAddons = false; }
      if (this.selectedVal === 'Family allowance Pages') { this.showFamilyAllowance = true; } else { this.showFamilyAllowance = false; }
      if (this.selectedVal === 'OTP - Part 1') { this.showOTPpart1 = true; } else { this.showOTPpart1 = false; }
      if (this.selectedVal === 'OTP - Part 2') { this.showOTPpart2 = true; } else { this.showOTPpart2 = false; }
      if (this.selectedVal === 'SSU') { this.showSSU = true; } else { this.showSSU = false; }
      if (this.selectedVal === 'PDE') { this.showPDE = true; } else { this.showPDE = false; }
      if (this.selectedVal === 'Error Handling') { this.showErrorHandling = true; } else { this.showErrorHandling = false; }
      if (this.selectedVal === 'Account History') { this.showAccountHistory = true; } else { this.showAccountHistory = false; }
      if (this.selectedVal === 'Account OverView') { this.showAccountOverView = true; } else { this.showAccountOverView = false; }
      if (this.selectedVal === 'Device Unlock') { this.showDeviceUnlock = true; } else { this.showDeviceUnlock = false; }
      if (this.selectedVal === 'Profile') { this.showProfile = true; } else { this.showProfile = false; }
      if (this.selectedVal === 'Profile New') { this.showProfileNew = true; } else { this.showProfileNew = false; }
      if (this.selectedVal === 'AutoPay') { this.showAutoPay = true; } else { this.showAutoPay = false; }
      if (this.selectedVal === 'PA') { this.showPA = true; } else { this.showPA = false; }
      if (this.selectedVal === 'Plan Comparision') { this.showPlanComparision = true; } else { this.showPlanComparision = false; }
      if (this.selectedVal === 'Contact Us') { this.showContactUs = true; } else { this.showContactUs = false; }
      if (this.selectedVal === 'EIP JOD') { this.showEipJoD = true; } else { this.showEipJoD = false; }
      if (this.selectedVal === 'Port In') { this.showPortIn = true; } else { this.showPortIn = false; }
      if (this.selectedVal === 'Change Plan') { this.showChangePlan = true; } else { this.showChangePlan = false; }
      if (this.selectedVal === 'Add Ons Phase2') { this.showaddOnsPhase2 = true; } else { this.showaddOnsPhase2 = false; }
      if (this.selectedVal === 'Balance Latency Treatment-PA') { this.showbalanceLatency = true; } else { this.showbalanceLatency = false; }
      if (this.selectedVal === 'More payment options') { this.showstandalone = true; } else { this.showstandalone = false; }
      if (this.selectedVal === 'In Wallet') { this.inWallet = true; } else { this.inWallet = false; }
      if (this.selectedVal === 'In Session') { this.inSession = true; } else { this.inSession = false; }
      if (this.selectedVal === 'Claim Flow') { this.claimFlow = true; } else { this.claimFlow = false; }
      if (this.selectedVal === 'PCI') { this.pciPages = true; } else { this.pciPages = false; }
      if (this.selectedVal === 'Temporary Suspension') { this.temporarysuspension = true; } else { this.temporarysuspension = false; }
      if (this.selectedVal === 'SuzySheep') { this.suzySheep = true; } else { this.suzySheep = false; }
      if (this.selectedVal === 'servicePromo') { this.servicePromo = true; } else { this.servicePromo = false; }
      
      localStorage.setItem('selectedval', value);
   }
}
