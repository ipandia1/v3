import { RouterModule, Routes,  PreloadAllModules  } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HomeNewComponent } from './home-new/home-new.component';
import { StyleGuideHomeComponent } from './pages/StyleGuide-Components/styleguide-home/styleguide-home.component';
import { AlertsAndNotificationComponent } from './pages/StyleGuide-Components/alerts-notifications/alerts-and-notification/alerts-and-notification.component';
import { AlertsCriticalNavigationComponent } from './pages/StyleGuide-Components/alerts-notifications/alerts-critical-navigation/alerts-critical-navigation.component';
import { AlertsCriticalCloseComponent } from './pages/StyleGuide-Components/alerts-notifications/alerts-critical-close/alerts-critical-close.component';
import { AlertsWarningCloseComponent } from './pages/StyleGuide-Components/alerts-notifications/alerts-warning-close/alerts-warning-close.component';
import { AlertsWarningNavigationComponent } from './pages/StyleGuide-Components/alerts-notifications/alerts-warning-navigation/alerts-warning-navigation.component';
import { DividerComponent } from './pages/StyleGuide-Components/divider/divider.component';
import { ButtonComponent } from './pages/StyleGuide-Components/Button/button.component';
import { TypographyComponent } from './pages/StyleGuide-Components/typography/typography.component';
import { SpacersComponent } from './pages/StyleGuide-Components/spacers/spacers.component';
import { ColorsComponent } from './pages/StyleGuide-Components/colors/colors.component';
import { SelectionControlComponent } from './pages/StyleGuide-Components/selection-control/selection-control.component';
import { TooltipComponent } from './pages/StyleGuide-Components/tooltip/tooltip.component';
import { ModalComponent } from './pages/StyleGuide-Components/modal/modal.component';
import { ListStaticComponent } from './pages/StyleGuide-Components/list-static/list-static.component';
import { ListDynamicComponent } from './pages/StyleGuide-Components/list-dynamic/list-dynamic.component';
import { InputfieldsComponent } from './pages/StyleGuide-Components/inputfields/inputfields.component';
import { DropdownComponent } from './pages/StyleGuide-Components/dropdown/dropdown.component';
import { TabsComponent } from './pages/StyleGuide-Components/tabs/tabs.component';
import { PaginationComponent } from './pages/StyleGuide-Components/pagination/pagination.component';
import { SpinnerPageComponent } from './pages/StyleGuide-Components/spinner-page/spinner-page.component';
import { DatepickerComponent } from './pages/StyleGuide-Components/datepicker/datepicker.component';
import { SliderUsageComponent } from './pages/StyleGuide-Components/slider-usage/slider-usage.component';
import { DatepickerNewModalComponent } from './pages/StyleGuide-Components/datepicker-new-modal/datepicker-new-modal.component';
import { DatepickerFullScreenComponent } from './pages/StyleGuide-Components/datepicker-full-screen/datepicker-full-screen.component';
import { IconographyComponent } from './pages/StyleGuide-Components/iconography/iconography.component';
import { FiltersComponent } from './pages/StyleGuide-Components/filters/filters.component';
import { BreadcrumbComponent } from './pages/Reusable-Components/breadcrumb/breadcrumb.component';
import { SearchComponent } from './pages/StyleGuide-Components/search/search.component';
import { SamplePageComponent } from './pages/StyleGuide-Components/alerts-notifications/sample-page/sample-page.component';
// ODF Pages imports
import { UpgradeServicesComponent } from './pages/ODF-pages/upgrade-services/upgrade-services.component';
import { ConfirmationPaymentComponent } from './pages/ODF-pages/confirmation-payment/confirmation-payment.component';
import { ErrorPageComponent } from './pages/ODF-pages/errorpage/error-page.component';
import { ReviewPaymentComponent } from './pages/ODF-pages/review-payment/review-payment.component';
import { ReviewNoPaymentComponent } from './pages/ODF-pages/review-no-payment/review-no-payment.component';
import { ConflictModalComponent } from './pages/ODF-pages/conflict-modal/conflict-modal.component';
import { TaxModalComponent } from './pages/ODF-pages/tax-modal/tax-modal.component';
import { TermsAndConditionComponent } from './pages/ODF-pages/terms-and-condition/terms-and-condition.component';
import { UpgradeServicesErrorComponent } from './pages/ODF-pages/upgrade-services-error/upgrade-services-error.component';
import { ServiceAgreementComponent } from './pages/ODF-pages/service-agreement/service-agreement.component';
import { ElectronicSignatureTermsComponent } from './pages/ODF-pages/electronic-signature-terms/electronic-signature-terms.component';
import { TablesComponent } from './pages/StyleGuide-Components/tables/tables.component';
import { ProgressBarstyleComponent } from './pages/StyleGuide-Components/progress-bar/progress-bar.component';
// ODF Add ons
import { OdfConfirmationComponent } from './pages/ODF-add-ons/odf-confirmation/odf-confirmation.component';
import { OdfReviewPaymentComponent } from './pages/ODF-add-ons/odf-review-payment/odf-review-payment.component';
import { OdflandingComponent } from './pages/ODF-add-ons/odflanding/odflanding.component';
import { HeadlineModalComponent } from './pages/ODF-add-ons/Headline-modal/Headline-modal.component';
import { MultilineModalComponent } from './pages/ODF-add-ons/Multiline-modal/Multiline-modal.component';
import { EmptyModalComponent } from './pages/ODF-add-ons/Empty-modal/Empty-modal.component';
import { OdfConfirmationBearmodeComponent } from './pages/ODF-add-ons/odf-confirmation-Bearmode/odf-confirmation-Bearmode.component';
import { OdfReviewPaymentNewComponent } from './pages/ODF-add-ons/odf-review-payment-new/odf-review-payment-new.component';
import { OdflandingNewComponent } from './pages/ODF-add-ons/odflanding-new/odflanding-new.component';
// Family Allowance imports
import { BenefitComponent } from './pages/Familyallowances-pages/benefit/benefit.component';
import { NetflixModalComponent } from './pages/Familyallowances-pages/netflix-modal/netflix-modal.component';
// OTP imports
import { PayYourBillComponent } from './pages/OTP/pay-your-bill/pay-your-bill.component';
import { PayYourBillAddPayComponent } from './pages/OTP/pay-your-bill-add-pay/pay-your-bill-add-pay.component';
import { CreditCardComponent } from './pages/OTP/credit-card/credit-card.component';
import { CvvModalAllOthersComponent } from './pages/OTP/cvv-modal-all-others/cvv-modal-all-others.component';
import { BankModalComponent } from './pages/OTP/bank-modal/bank-modal.component';
import { BankInfoComponent } from './pages/OTP/bank-info/bank-info.component';
import { EditPaymentMethodComponent } from './pages/OTP/edit-payment-method/edit-payment-method.component';
import { EditAmountComponent } from './pages/OTP/edit-amount/edit-amount.component';
import { Error1Component } from './pages/OTP/error-1/error-1.component';
import { BladeComponent } from './pages/OTP/blade/blade.component';
import { Blade02Component } from './pages/OTP/blade-02/blade-02.component';
import { CreditCardUpdatedComponent } from './pages/OTP/credit-card-updated/credit-card-updated.component';
import { BankInformationUpdatedComponent } from './pages/OTP/bank-information-updated/bank-information-updated.component';
// import { OtpModal1Component } from './pages/OTP/otp-modal1/otp-modal1.component';
import { CvvModalAmexComponent } from './pages/OTP/cvv-modal-amex/cvv-modal-amex.component';
import { TermsAndConditionsComponent } from './pages/OTP/terms-and-conditions/terms-and-conditions.component';
import { EditPaymentMethodDisableComponent } from './pages/OTP/edit-payment-method-disable/edit-payment-method-disable.component';
import { DeleteCardModalcomponent } from './pages/OTP/delete-card-modal/delete-card-modal.component';
import { DeleteBankModalcomponent } from './pages/OTP/delete-bank-modal/delete-bank-modal.component';
import { EditAmountAltComponent } from './pages/OTP/edit-amount-alt/edit-amount-alt.component';
import { OtpBreakdownModal } from './pages/OTP/otp-breakdown-modal/otp-breakdown-modal.component';
import { OtpError2Component } from './pages/OTP/otp-error-2/otp-error-2.component';
import { OtpPaEditMethodComponent } from './pages/OTP/otp-pa-edit-method/otp-pa-edit-method.component';
import { OtpModalComponent } from './pages/OTP/otp-modal/otp-modal.component';
import { ConfirmationComponent } from './pages/OTP/confirmation/confirmation.component';
import { ConfirmationpitchpageComponent } from './pages/OTP/confirmationpitchpage/confirmationpitchpage.component';
import { ReplacecardModalComponent } from './pages/OTP/replacecard-modal/replacecard-modal.component';
import { CreditCardWithAddressComponent } from './pages/OTP/credit-card-with-address/credit-card-with-address.component';
// SSU imports
import { ShopPageComponent } from './pages/SSU/shop-page/shop-page.component';
import { ShopPageUpdatedComponent } from './pages/SSU/shop-page-updated/shop-page-updated.component';
import { ShopPageModalComponent } from './pages/SSU/shop-page-modal/shop-page-modal.component';
import { OrderStatusComponent } from './pages/SSU/order-status/order-status.component';
import { ShopTermsConditionsComponent } from './pages/SSU/shop-terms-conditions/shop-terms-conditions.component';
import { ShopPageSwapBannerContentComponent } from './pages/SSU/shop-page-swap-banner-content/shop-page-swap-banner-content.component';
import { ShopPageSwapBannerContentInMobileComponent } from './pages/SSU/shop-page-swap-banner-content-in-mobile/shop-page-swap-banner-content-in-mobile.component';
import { ShopPageCenterBannerContentComponent } from './pages/SSU/shop-page-center-banner-content/shop-page-center-banner-content.component';
import { ShopPageFgImageComponent } from './pages/SSU/shop-page-fg-image/shop-page-fg-image.component';

// DRP Agreement
import { DrpagreementComponent } from './pages/DRPagreement/drpagreement/drpagreement.component';
// PDE
import { OfferModelComponent } from './pages/PDE/offer-model/offer-model.component';
import { OfferModel1Component } from './pages/PDE/offer-model1/offer-model1.component';
import { MyPromotionsComponent } from './pages/PDE/my-promotions/my-promotions.component';
import { MyPromotionNoPendingComponent } from './pages/PDE/my-promotion-no-pending/my-promotion-no-pending.component';
import { PastPromotionComponent } from './pages/PDE/past-promotion/past-promotion.component';
import { PromotionsDetailsComponent } from './pages/PDE/promotions-details/promotions-details.component';
import { MyPromotionPendingComponent } from './pages/PDE/my-promotion-pending/my-promotion-pending.component';
// Crazy legs
import { CrazyComponent } from './pages/Crazy-Legs/crazy/crazy.component';
import { GridComponent } from './pages/Crazy-Legs/grid/grid.component';
import { PlanDetailComponent } from './pages/Crazy-Legs/plan-detail/plan-detail.component';
import { MconfirmationComponent } from './pages/Crazy-Legs/M-confirmation/Mconfirmation.component';
// Error handling imports
import { SystemErrorComponent } from './pages/Error-Handling/systemerror/systemerror.component';
import { PaymentDeclinedComponent } from './pages/Error-Handling/paymentdeclined/paymentdeclined.component';
import { ErrorStateModalComponent } from './pages/Error-Handling/errorstate-modal/errorstate-modal.component';
import { ServerErrorModalComponent } from './pages/Error-Handling/servererror-modal/servererror-modal.component';
// Service imports
import { CommonService } from './services/common.service';
// Account History imports
import { CurrentActivityComponent } from './pages/account-activity/current-activity/current-activity.component';
import { ConversationHistoryComponent } from './pages/account-activity/conversation-history/conversation-history.component';
import { BillHistoryModalV3Component } from './pages/account-activity/bill-history-modal-v3/bill-history-modal-v3.component';
import { PaymentReceiptComponent } from './pages/account-activity/payment-receipt/payment-receipt.component';
import { CancelModalComponent } from './pages/account-activity/cancel-modal/cancel-modal.component';
import { UsageOverviewComponent } from './pages/account-activity/usage-overview/usage-overview.component';
// device unlock
import { UnlockYourDeviceComponent } from './pages/Device-Unlocked/unlock-your-device/unlock-your-device.component';
import { StatusCheckComponent } from './pages/Device-Unlocked/status-check/status-check.component';
import { SelectlineComponent } from './pages/Device-Unlocked/selectline/selectline.component';
// Profile pages
import { ProfileHomeComponent } from './pages/profile/profile-home/profile-home.component';
import { BillingAndPaymentsComponent } from './pages/profile/billing-and-payments/billing-and-payments.component';
import { BillingAndPaymentsPaperlessBillingComponent } from './pages/profile/billing-and-payments-paperless-billing/billing-and-payments-paperless-billing.component';
import { BillingAndPaymentsOnComponent } from './pages/profile/billing-and-payments-on/billing-and-payments-on.component';
import { TmobileIdComponent } from './pages/profile/tmobile-id/tmobile-id.component';
import { TmobileIdNicknameComponent } from './pages/profile/tmobile-id-nickname/tmobile-id-nickname.component';
import { TmobileIdPasswordComponent } from './pages/profile/tmobile-id-password/tmobile-id-password.component';
import { TmobileIdSecuriityQuestionComponent } from './pages/profile/tmobile-id-securiity-question/tmobile-id-securiity-question.component';
import { TmobileIdPhoneNumberComponent } from './pages/profile/tmobile-id-phone-number/tmobile-id-phone-number.component';
import { MediaSettingsComponent } from './pages/profile/media-settings/media-settings.component';
import { LineSettingsComponent } from './pages/profile/line-settings/line-settings.component';
import { LineSettingsNicknameComponent } from './pages/profile/line-settings-nickname/line-settings-nickname.component';
import { LineSettingsSelectLineComponent } from './pages/profile/line-settings-select-line/line-settings-select-line.component';
import { LineSettingsLineChangedComponent } from './pages/profile/line-settings-line-changed/line-settings-line-changed.component';
import { BlockingComponent } from './pages/profile/blocking/blocking.component';
import { PrivacyNotificationsComponent } from './pages/profile/privacy-notifications/privacy-notifications.component';
import { PrivacyNotificationsMarketingCommunicationsComponent } from './pages/profile/privacy-notifications-marketing-communications/privacy-notifications-marketing-communications.component';
import { PrivacyNotificationsNotificationsComponent } from './pages/profile/privacy-notifications-notifications/privacy-notifications-notifications.component';
import { PrivacyNotificationsAdvertisingComponent } from './pages/profile/privacy-notifications-advertising/privacy-notifications-advertising.component';
import { PrivacyNotificationsInterestBasedAdsComponent } from './pages/profile/privacy-notifications-interest-based-ads/privacy-notifications-interest-based-ads.component';
import { MultipleDevicesComponent } from './pages/profile/multiple-devices/multiple-devices.component';
import { EmployeeDesignationCurrentLinesComponent } from './pages/profile/employee-designation-current-lines/employee-designation-current-lines.component';
import { EmployeeDesignationEditComponent } from './pages/profile/employee-designation-edit/employee-designation-edit.component';
import { CoverageDeviceComponent } from './pages/profile/coverage-device/coverage-device.component';
import { CoverageDeviceDeviceAddressComponent } from './pages/profile/coverage-device-device-address/coverage-device-device-address.component';
import { CoverageDeviceAssignedNumberComponent } from 
'./pages/profile/coverage-device-assigned-number/coverage-device-assigned-number.component';
import { FamilyControlComponent } from './pages/profile/family-control/family-control.component';
import { FamilyControlWebGaurdComponent } from './pages/profile/family-control-web-gaurd/family-control-web-gaurd.component';
import { FamilyControlFamilyAllowancesComponent } from './pages/profile/family-control-family-allowances/family-control-family-allowances.component';
import { CoverageDeviceVerifyAddressModalComponent } from './pages/profile/coverage-device-verify-address-modal/coverage-device-verify-address-modal.component';
// ProfileNew
import { HomePageComponent } from './pages/Profile-New/home-page/home-page.component';
import { PrivacyNotificationsNewComponent } from './pages/Profile-New/privacy-notifications-new/privacy-notifications-new.component';
import { PrivacyNotificationsInterestBasedAdsNewComponent } from './pages/Profile-New/privacy-notifications-interest-based-ads-new/privacy-notifications-interest-based-ads-new.component';
import { PrivacyNotificationsAdvertisingNewComponent } from './pages/Profile-New/privacy-notifications-advertising-new/privacy-notifications-advertising-new.component';
import { PrivacyNotificationsMarketingCommunicationsNewComponent } from './pages/Profile-New/privacy-notifications-marketing-communications-new/privacy-notifications-marketing-communications-new.component';
import { PrivacyNotificationsNotificationsNewComponent } from './pages/Profile-New/privacy-notifications-notifications-new/privacy-notifications-notifications-new.component';
import { EmployeeDesignationCurrentLinesNewComponent } from './pages/Profile-New/employee-designation-current-lines-new/employee-designation-current-lines-new.component';
import { EmployeeDesignationEditNewComponent } from './pages/Profile-New/employee-designation-edit-new/employee-designation-edit-new.component';
import { BillingAndPaymentsNewComponent } from './pages/Profile-New/billing-and-payments-new/billing-and-payments-new.component';
import { BillingAndPaymentsOnNewComponent } from './pages/Profile-New/billing-and-payments-on-new/billing-and-payments-on-new.component';
import { BillingAndPaymentsPaperlessBillingNewComponent } from './pages/Profile-New/billing-and-payments-paperless-billing-new/billing-and-payments-paperless-billing-new.component';
import { LineSettingsNewComponent } from './pages/Profile-New/line-settings-new/line-settings-new.component';
import { LineSettingsNicknameNewComponent } from './pages/Profile-New/line-settings-nickname-new/line-settings-nickname-new.component';
import { LineSettingsCallerIdNewComponent } from './pages/Profile-New/line-settings-caller-id-new/line-settings-caller-id-new.component';
import { LineSettingsSelectLineNewComponent } from './pages/Profile-New/line-settings-select-line-new/line-settings-select-line-new.component';
import { LineSettingsLineChangedNewComponent } from './pages/Profile-New/line-settings-line-changed-new/line-settings-line-changed-new.component';
import { CoverageDeviceNewComponent } from './pages/Profile-New/coverage-device-new/coverage-device.component';
import { CoverageDeviceDeviceAddressNewComponent } from './pages/Profile-New/coverage-device-device-address-new/coverage-device-device-address-new.component';
import { CoverageDeviceAssignedNumberNewComponent } from './pages/Profile-New/coverage-device-assigned-number-new/coverage-device-assigned-number-new.component';
import { CoverageDeviceVerifyAddressModalNewComponent } from './pages/Profile-New/coverage-device-verify-address-modal-new/coverage-device-verify-address-modal-new.component';
import { BlockingNewComponent } from './pages/Profile-New/blocking-new/blocking-new.component';
import { MultipleSettingsNewComponent } from './pages/Profile-New/multiple-settings-new/multiple-settings-new.component';
import { MediaSettingsNewComponent } from './pages/Profile-New/media-settings-new/media-settings-new.component';
import { FamilyControlComponentNew } from './pages/Profile-New/family-control-new/family-control-new.component';
import { FamilyControlWebGaurdComponentNew } from './pages/Profile-New/family-control-web-gaurd-new/family-control-web-gaurd-new.component';
import { FamilyControlFamilyAllowancesComponentNew } from './pages/Profile-New/family-control-family-allowances-new/family-control-family-allowances-new.component';
import { TmobileIdNewComponent } from './pages/Profile-New/tmobile-id-new/tmobile-id-new.component';
import { TmobileIdNicknameNewComponent } from './pages/Profile-New/tmobile-id-nickname-new/tmobile-id-nickname-new.component';
import { BillingAddressNewComponent } from './pages/Profile-New/billing-address-new/billing-address-new.component';
import { BillingAndPaymentsPaperlessUatnewComponent } from './pages/Profile-New/billing-and-payments-paperless-uatnew/billing-and-payments-paperless-uatnew.component';
import { PaperlessTermsAndConditionComponent } from './pages/Profile-New/paperless-terms-and-condition/paperless-terms-and-condition.component';
import { BillingAndPaymentsPaperlessUatUpdatedComponent } from './pages/Profile-New/billing-and-payments-paperless-uat-updated/billing-and-payments-paperless-uat-updated.component';
import { BillingAndPaymentsConfirmationComponent } from './pages/Profile-New/billing-and-payments-confirmation/billing-and-payments-confirmation.component';
import { E911AddressNewComponent } from './pages/Profile-New/e911-address-new/e911-address-new.component';
import { MilitaryStatusDetailComponent } from './pages/Profile-New/military-status-detail/military-status-detail.component';
import { RegistrationVeteranComponent } from './pages/Profile-New/registration-veteran/registration-veteran.component';
import { RegistrationVeteranDisabledComponent } from './pages/Profile-New/registration-veteran-disabled/registration-veteran-disabled.component';
import { ConfirmInfoComponent } from './pages/Profile-New/confirm-info/confirm-info.component';
import { SuccessVerifiedComponent } from './pages/Profile-New/success-verified/success-verified.component';
import { StatusUploadDocComponent } from './pages/Profile-New/status-upload-doc/status-upload-doc.component';
import { SuccessUploadReceivedComponent } from './pages/Profile-New/success-upload-received/success-upload-received.component';
import { AlertMessageComponent } from './pages/Profile-New/alert-message/alert-message.component';
import { UploadDocLandingComponent } from './pages/Profile-New/upload-doc-landing/upload-doc-landing.component';
import { UploadDocDefaultComponent } from './pages/Profile-New/upload-doc-default/upload-doc-default.component';
import { AucPermissionsComponent } from './pages/Profile-New/auc-permissions/auc-permissions.component';
import { AucMessagesComponent } from './pages/Profile-New/auc-messages/auc-messages.component';
import { AucMessagesConfirmationComponent } from './pages/Profile-New/auc-messages-confirmation/auc-messages-confirmation.component';
import { AucChangePermsTermsAndCondComponent } from './pages/Profile-New/auc-change-perms-terms-and-cond/auc-change-perms-terms-and-cond.component';
import { FidelisTermsConditionComponent } from './pages/Profile-New/fidelis-terms-condition/fidelis-terms-condition';
import { FidelisPrivacyPolicyComponent } from './pages/Profile-New/fidelis-privacy-policy/fidelis-privacy-policy';
import { LanguageSettingsComponent } from './pages/Profile-New/language-settings/language-settings.component';
import { LineSettingsCallerIdComponent } from './pages/Profile-New/line-settings-caller-id/line-settings-caller-id.component';
import { LineSettingsCallerIdConfirmationComponent } from './pages/Profile-New/line-settings-caller-id-confirmation/line-settings-caller-id-confirmation.component';

// AutoPay
import { FaqComponent } from './pages/AutoPay/faq/faq.component';
import { AutoPayOff2ErrorComponent } from './pages/AutoPay/auto-pay-off2-error/auto-pay-off2-error.component';
import { AutoPayOff2Component } from './pages/AutoPay/auto-pay-off2/auto-pay-off2.component';
import { AutoPayOff3Component } from './pages/AutoPay/auto-pay-off3/auto-pay-off3.component';
import { Confirmation3Component } from './pages/AutoPay/confirmation-3/confirmation-3.component';
import { AutopayConfirmationComponent } from './pages/AutoPay/autopay-confirmation/autopay-confirmation.component';
// PA
import { PaOtpModalComponent } from './pages/PA/pa-otp-modal/pa-otp-modal.component';
import { PaEditModalComponent } from './pages/PA/pa-edit-modal/pa-edit-modal.component';
import { PaEditV35ModalComponent } from './pages/PA/pa-edit-v3-5-modal/pa-edit-v3-5-modal.component';
import { PaEditV36ModalComponent } from './pages/PA/pa-edit-v3-6-modal/pa-edit-v3-6-modal.component';
import { paLanding1Component } from './pages/PA/pa-landing-1/pa-landing-1.component';
import { paLanding2Component } from './pages/PA/pa-landing-2/pa-landing-2.component';
import { paLanding3Component } from './pages/PA/pa-landing-3/pa-landing-3.component';
import { paLanding4Component } from './pages/PA/pa-landing-4/pa-landing-4.component';
import { paLanding5Component } from './pages/PA/pa-landing-5/pa-landing-5.component';
import { paLandingNoPaymentComponent } from './pages/PA/pa-landing-no-payment/pa-landing-no-payment.component';
import { PaConfirmComponent } from './pages/PA/pa-confirm/pa-confirm.component';
import { PaOtpEditMethodComponent } from './pages/PA/pa-otp-edit-method/pa-otp-edit-method.component';
import { PaOtpHomeComponent } from './pages/PA/pa-otp-home/pa-otp-home.component';
import { PaEditV31Component } from './pages/PA/pa-edit-v3-1/pa-edit-v3-1.component';
import { PaEditV32Component } from './pages/PA/pa-edit-v3-2/pa-edit-v3-2.component';
import { UsageDetail1Component } from './pages/account-activity/usage-detail1/usage-detail1.component';
import { PaymentHistoryComponent } from './pages/account-activity/payment-history/payment-history.component';
import { HistoricalBillsModalComponent } from './pages/account-activity/historical-bills-modal/historical-bills-modal.component';
// Plan Comparision
import { ReviewAutoPayAlreadyOnComponent } from './pages/plan-comparision/review-auto-pay-already-on/review-auto-pay-already-on.component';
import { ReviewAutoPayUpsellComponent } from './pages/plan-comparision/review-auto-pay-upsell/review-auto-pay-upsell.component';
import { ReviewPlanDetailsComponent } from './pages/plan-comparision/review-plan-details/review-plan-details.component';
import { ReviewLineDetailsComponent } from './pages/plan-comparision/review-line-details/review-line-details.component';
import { MvpReviewChangesComponent } from './pages/plan-comparision/mvp-review-changes/mvp-review-changes.component';
import { MvpReviewPlansComponent } from './pages/plan-comparision/mvp-review-plans/mvp-review-plans.component';
import { MvpReviewAddOnsComponent } from './pages/plan-comparision/mvp-review-add-ons/mvp-review-add-ons.component';
import { MvpReviewEquipmentComponent } from './pages/plan-comparision/mvp-review-equipment/mvp-review-equipment.component';
import { MvpConfirmDetailsComponent } from './pages/plan-comparision/mvp-confirm-details/mvp-confirm-details.component';
import { MvpReviewChanges2Component } from './pages/plan-comparision/mvp-review-changes2/mvp-review-changes2.component';
import { MvpReviewOrderDetailsByLineComponent } from './pages/plan-comparision/mvp-review-order-details-by-line/mvp-review-order-details-by-line.component';
import { MvpSelectPlanComponent } from './pages/plan-comparision/mvp-select-plan/mvp-select-plan.component';
// Contact Us Page
import { TeamDescriptionComponent } from './pages/contact-us/team-description/team-description.component';
// Account OverView
import { LandingPageComponent } from './pages/accountOverView/landing-page/landing-page.component';
import { LineDetailComponent } from './pages/accountOverView/line-detail/line-detail.component';
import { PlanDetailsComponent } from './pages/accountOverView/plan-details/plan-details.component';
import { CostDetailsComponent } from './pages/accountOverView/cost-details/cost-details.component';
import { FeatureDetailsComponent } from './pages/accountOverView/feature-details/feature-details.component';
import { LineDetailNewComponent } from './pages/accountOverView/line-detail-new/line-detail-new.component';

// EIP JOD
import { JodEndingSoonComponent } from './pages/eipJod/jod-ending-soon/jod-ending-soon.component';
import { JodEndingSoonOptionsComponent } from './pages/eipJod/jod-ending-soon-options/jod-ending-soon-options.component';
import { JodPoipPaymentComponent } from './pages/eipJod/jod-poip-payment/jod-poip-payment.component';
import { DocuSignFlowComponent } from './pages/eipJod/docu-sign-flow/docu-sign-flow.component';
import { DocuSignThankYouComponent } from './pages/eipJod/docu-sign-thank-you/docu-sign-thank-you.component';
import { EppExistingPaymentMethodComponent } from './pages/eipJod/epp-existing-payment-method/epp-existing-payment-method.component';
import { EppSelectInstallmentPlanComponent } from './pages/eipJod/epp-select-installment-plan/epp-select-installment-plan.component';
import { EppTermsAndConditionComponent } from './pages/eipJod/epp-terms-and-condition/epp-terms-and-condition.component';
import { SelectPaymentMethodComponent } from './pages/eipJod/select-payment-method/select-payment-method.component';
import { AddCardComponent } from './pages/eipJod/add-card/add-card.component';
import { PaymentAmountComponent } from './pages/eipJod/payment-amount/payment-amount.component';
import { EppConfirmationComponent } from './pages/eipJod/epp-confirmation/epp-confirmation.component';
import { DuplicatePaymentComponent } from './pages/eipJod/duplicate-payment/duplicate-payment.component';
import { PaymentSystemErrorComponent } from './pages/eipJod/payment-system-error/payment-system-error.component';
import { PaymentEstimatorComponent } from './pages/eipJod/payment-estimator/payment-estimator.component';
import { BalanceSummaryVariantsComponent } from './pages/eipJod/balance-summary-variants/balance-summary-variants.component';
import { JodLeaseVariantsComponent } from './pages/eipJod/jod-lease-variants/jod-lease-variants.component';
import { EipVariantsComponent } from './pages/eipJod/eip-variants/eip-variants.component';
import { ExtraDevicePaymentVariantsComponent } from './pages/eipJod/extra_device_payment_variants/extra_device_payment_variants.component';
import { SuccessStatusComponent } from './pages/port-in/success-status/success-status.component';
import { ProblemIdentifierComponent } from './pages/port-in/problem-identifier/problem-identifier.component';
import { ProblemFixComponent } from './pages/port-in/problem-fix/problem-fix.component';
import { MaximumAttemptsComponent } from './pages/port-in/maximum-attempts/maximum-attempts.component';

// change plan

import { SelectPlanComponent } from './pages/change-plan/select-plan/select-plan.component';
import { PlanComparisonComponent } from './pages/change-plan/plan-comparison/plan-comparison.component';
import { ReviewChangesComponent } from './pages/change-plan/review-changes/review-changes.component';
import { DetailBreakdownComponent } from './pages/change-plan/detail-breakdown/detail-breakdown.component';
import { PlanConfirmationComponent } from './pages/change-plan/plan-confirmation/plan-confirmation.component';
import {MBBPlanDetailComponent} from './pages/change-plan/mbb-plan-detail/mbb-plan-detail.component';
import { ReviewOrderComponent } from './pages/change-plan/review-order/review-order.component';
import { PlanComparisionDetailComponent } from './pages/change-plan/plan-comparison-detail/plan-comparison-detail.component';
import { PlanConfirmationUpdatedComponent } from './pages/change-plan/plan-confirmation-updated/plan-confirmation-updated.component';

// Addons Phase2
import { ReviewAndPayOrderComponent } from './pages/add-ons/review-and-pay-order/review-and-pay-order.component';
import { TaxesAndFeesComponent } from './pages/add-ons/taxes-and-fees/taxes-and-fees.component';
import { ConfirmationOrderComponent } from './pages/add-ons/confirmation-order/confirmation-order.component';
import { SignUpVerificationComponent } from './pages/add-ons/sign-up-verification/sign-up-verification.component';
import { AccountSuspendedComponent } from './pages/add-ons/account-suspended/account-suspended.component';
import { OneLastStepComponent } from './pages/add-ons/one-last-step/one-last-step.component';
import { LineSelectorComponent } from './pages/add-ons/line-selector/line-selector.component';
import { ValueSelectorComponent } from './pages/add-ons/value-added-service/value-added-service.component';
import { P360RemovalComponent } from './pages/add-ons/p360-removal/p360-removal.component';
import { P360ScaryComponent } from './pages/add-ons/p360-scary/p360-scary.component';
import { ServicesHubStatusComponent } from './pages/add-ons/services-hub-status/services-hub-status.component';
import { OnUsChooserComponent } from './pages/add-ons/on-us-chooser/on-us-chooser.component';
//Balance Latency
import { PaymentProcessingComponent } from './pages/balanceLatency/payment-processing/payment-processing.component';
import { SchduledCallComponent } from './pages/Reusable-Components/schduled_call/schduled_call.component';

import { InSessionComponent } from './pages/In-Session/in-session/in-session.component';
import { MyWalletComponent } from './pages/wallet/my-wallet/my-wallet.component';

// More payment options
import { MorePaymentOptionsComponent } from './pages/more-payment-options/more-payment-options.component';
import { ManageDataComponent } from './pages/add-ons/manage-data/manage-data.component';

//claim flow
import {confirmPayementMethodComponent} from './pages/claim-flow/confirm-payment-method/confirm-payment-method.component';

//Pci 
import {EnterCardInformationComponent} from './pages/PCI/enter-card-information/enter-card-information.component';
import {EnterBankInformationComponent} from './pages/PCI/enter-bank-information/enter-bank-information.component';
//Temporary suspention
import {TsLineselectorComponent } from './pages/TemporarySuspension/lineselector/lineselector.component';
import {TsConfirmationComponent} from './pages/TemporarySuspension/confirmation/confirmation.component';
import {TsReactivateComponent} from './pages/TemporarySuspension/reactivate/reactivate.component';
import {TsReactivationconfirmationComponent } from './pages/TemporarySuspension/reactivationconfirmation/reactivationconfirmation.component';

//Suzy Sheep
import { PitchPageComponent } from './pages/SuzySheep/pitch-page/pitch-page.component';
//Service Promos
import { DevicePromoListComponent } from './pages/service-promos/device-promo-list/device-promo-list.component';
import { PromoDetailsComponent } from './pages/service-promos/promo-details/promo-details.component';
import { PastPromosListComponent } from './pages/service-promos/past-promos-list/past-promos-list.component';
const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: '', component: HomeNewComponent },
  { path: 'StyleGuideHome', component: StyleGuideHomeComponent },
  { path: 'alertsAndNotification', component: AlertsAndNotificationComponent },
  { path: 'alertsCriticalNavigation', component: AlertsCriticalNavigationComponent },
  { path: 'alertsCriticalClose', component: AlertsCriticalCloseComponent },
  { path: 'alertsWarningNavigation', component: AlertsWarningNavigationComponent },
  { path: 'alertsWarningClose', component: AlertsWarningCloseComponent },
  { path: 'Divider', component: DividerComponent },
  { path: 'Button', component: ButtonComponent },
  { path: 'Typography', component: TypographyComponent },
  { path: 'Spacer', component: SpacersComponent },
  { path: 'colors', component: ColorsComponent },
  { path: 'selectionControl', component: SelectionControlComponent },
  { path: 'tooltip', component: TooltipComponent },
  { path: 'pagination', component: PaginationComponent },
  { path: 'listStatic', component: ListStaticComponent },
  { path: 'listDynamic', component: ListDynamicComponent },
  { path: 'tabs', component: TabsComponent },
  { path: 'modal', component: ModalComponent },
  { path: 'Inputfields', component: InputfieldsComponent },
  { path: 'spinner', component: SpinnerPageComponent },
  { path: 'slider', component: SliderUsageComponent },
  { path: 'samplePage', component: SamplePageComponent },
  { path: 'upgradeServices', component: UpgradeServicesComponent },
  { path: 'upgradeServicesError', component: UpgradeServicesErrorComponent },
  { path: 'confirmationPayment', component: ConfirmationPaymentComponent },
  { path: 'ReviewPayment', component: ReviewPaymentComponent },
  { path: 'errorPage', component: ErrorPageComponent },
  { path: 'ReviewNoPayment', component: ReviewNoPaymentComponent },
  { path: 'ConflictModal', component: ConflictModalComponent },
  { path: 'TaxModal', component: TaxModalComponent },
  { path: 'termsAndCondition', component: TermsAndConditionComponent },
  { path: 'Benefit', component: BenefitComponent },
  { path: 'payYourBill', component: PayYourBillComponent },
  { path: 'payYourBillAddPay', component: PayYourBillAddPayComponent },
  { path: 'creditCard', component: CreditCardComponent },
  { path: 'cvvModal', component: CvvModalAllOthersComponent },
  { path: 'bankModal', component: BankModalComponent },
  { path: 'editPaymentMethod', component: EditPaymentMethodComponent },
  { path: 'editAmount', component: EditAmountComponent },
  { path: 'shopPage', component: ShopPageComponent },
  { path: 'shopPageUpdated', component:  ShopPageUpdatedComponent },
  { path: 'shopPageSwapBannerContent', component: ShopPageSwapBannerContentComponent },
  { path: 'ShopPageSwapBannerContentInMobile', component: ShopPageSwapBannerContentInMobileComponent },
  { path: 'shopPageCenterBannerContent', component: ShopPageCenterBannerContentComponent },
  { path: 'shopPageFgImage', component: ShopPageFgImageComponent },
  { path: 'shopPageModal', component: ShopPageModalComponent },
  { path: 'Drpagreement', component: DrpagreementComponent },
  { path: 'dropdown', component: DropdownComponent },
  { path: 'OdfConfirmation', component: OdfConfirmationComponent },
  { path: 'OdfReviwPayment', component: OdfReviewPaymentComponent },
  { path: 'OrderStatus', component: OrderStatusComponent },
  { path: 'ServiceAgreement', component: ServiceAgreementComponent },
  { path: 'ElectronicSignatureTerms', component: ElectronicSignatureTermsComponent },
  { path: 'NetflixModal', component: NetflixModalComponent },
  { path: 'Odflanding', component: OdflandingComponent },
  { path: 'OdfReviewPaymentNew', component: OdfReviewPaymentNewComponent },
  { path: 'OdflandingNew', component: OdflandingNewComponent },
  { path: 'datepicker', component: DatepickerComponent },
  { path: 'Crazy', component: CrazyComponent },
  { path: 'Grid', component: GridComponent },
  { path: 'PlanDetail', component: PlanDetailComponent },
  { path: 'ReplacecardModal', component: ReplacecardModalComponent },
  { path: 'myPromotions', component: MyPromotionsComponent },
  { path: 'OfferModel', component: OfferModelComponent },
  { path: 'OfferModel1', component: OfferModel1Component },
  { path: 'BankInfo', component: BankInfoComponent },
  { path: 'Confirmation', component: ConfirmationComponent },
  { path: 'AutopayConfirmation', component: AutopayConfirmationComponent },
  { path: 'Confirmationpitch', component: ConfirmationpitchpageComponent },
  { path: 'SystemError', component: SystemErrorComponent },
  { path: 'ServerError', component: ServerErrorModalComponent },
  { path: 'PaymentDeclined', component: PaymentDeclinedComponent },
  { path: 'ErrorState', component: ErrorStateModalComponent },
  { path: 'error1', component: Error1Component },
  { path: 'blade', component: BladeComponent },
  { path: 'blade02', component: Blade02Component },
  { path: 'creditCardUpdated', component: CreditCardUpdatedComponent },
  { path: 'bankInformationUpdated', component: BankInformationUpdatedComponent },
  // { path: 'OTPModal1', component: OtpModal1Component },
  { path: 'cvvModalAmex', component: CvvModalAmexComponent },
  { path: 'OTPTermsAndConditions', component: TermsAndConditionsComponent },
  { path: 'currentActivity', component: CurrentActivityComponent },
  { path: 'conversationHistory', component: ConversationHistoryComponent },
  { path: 'UnlockYourDevice', component: UnlockYourDeviceComponent },
  { path: 'StatusUnknown', component: StatusCheckComponent },
  { path: 'editPaymentMethodDisable', component: EditPaymentMethodDisableComponent },
  { path: 'DeleteCard', component: DeleteCardModalcomponent },
  { path: 'DeleteBank', component: DeleteBankModalcomponent },
  { path: 'profileHome', component: ProfileHomeComponent },
  { path: 'profileNewHome', component: HomePageComponent },
  { path: 'editAmountAlt', component: EditAmountAltComponent },
  { path: 'editAmountAlt', component: EditAmountAltComponent },
  { path: 'otpbreakdown', component: OtpBreakdownModal },
  { path: 'otpModal', component: OtpModalComponent },
  { path: 'creditCardWithAddress', component: CreditCardWithAddressComponent },
  { path: 'error2', component: OtpError2Component },
  { path: 'OtpPaEdit', component: OtpPaEditMethodComponent },
  { path: 'billingAndPayments', component: BillingAndPaymentsComponent },
  { path: 'billingAndPaymentsPaperlessBilling', component: BillingAndPaymentsPaperlessBillingComponent },
  { path: 'billingAndPaymentsOn', component: BillingAndPaymentsOnComponent },
  { path: 'tMobileID', component: TmobileIdComponent },
  { path: 'tMobileIDNickname', component: TmobileIdNicknameComponent },
  { path: 'tMobileIDPassword', component: TmobileIdPasswordComponent },
  { path: 'tMobileIDSecurityQuestion', component: TmobileIdSecuriityQuestionComponent },
  { path: 'tmobileIDPhoneNumber', component: TmobileIdPhoneNumberComponent },
  { path: 'mediaSettings', component: MediaSettingsComponent },
  { path: 'lineSettings', component: LineSettingsComponent },
  { path: 'lineSettingsNickname', component: LineSettingsNicknameComponent },
  { path: 'lineSettingsSelectLine', component: LineSettingsSelectLineComponent },
  { path: 'lineSettingsLineChanged', component: LineSettingsLineChangedComponent },
  { path: 'blocking', component: BlockingComponent },
  { path: 'privacyNotification', component: PrivacyNotificationsComponent },
  { path: 'privacyNotificationMarketingCommunications', component: PrivacyNotificationsMarketingCommunicationsComponent },
  { path: 'privacyNotificationNotifications', component: PrivacyNotificationsNotificationsComponent },
  { path: 'privacyNotificationAdvertising', component: PrivacyNotificationsAdvertisingComponent },
  { path: 'privacyNotificationInterestBasedAds', component: PrivacyNotificationsInterestBasedAdsComponent },
  { path: 'multipleDevices', component: MultipleDevicesComponent },
  { path: 'employeeDesignationCurrentLines', component: EmployeeDesignationCurrentLinesComponent },
  { path: 'employeeDesignationEdit', component: EmployeeDesignationEditComponent },
  { path: 'coverageDevice', component: CoverageDeviceComponent },
  { path: 'coverageDeviceDeviceAddress', component: CoverageDeviceDeviceAddressComponent },
  { path: 'coverageDeviceAssignedNumber', component: CoverageDeviceAssignedNumberComponent },
  { path: 'coverageDeviceVerifyAddress', component: CoverageDeviceVerifyAddressModalComponent },
  { path: 'familyControl', component: FamilyControlComponent },
  { path: 'familyControlWebGaurd', component: FamilyControlWebGaurdComponent },
  { path: 'familyControlFamilyAllowances', component: FamilyControlFamilyAllowancesComponent },
  { path: 'BillHistory', component: BillHistoryModalV3Component },
  { path: 'faq', component: FaqComponent },
  { path: 'autoPayOff2Error', component: AutoPayOff2ErrorComponent },
  { path: 'autoPayOff2', component: AutoPayOff2Component },
  { path: 'autoPayOff3', component: AutoPayOff3Component },
  { path: 'confirmation-3', component: Confirmation3Component },
  { path: 'PAotpModal', component: PaOtpModalComponent },
  { path: 'PAeditModal', component: PaEditModalComponent },
  { path: 'PAeditV31', component: PaEditV31Component },
  { path: 'PaEditV32', component: PaEditV32Component},
  { path: 'PAmodal5', component: PaEditV35ModalComponent },
  { path: 'PAmodal6', component: PaEditV36ModalComponent },
  { path: 'paLanding1', component: paLanding1Component},
  { path: 'paLanding2', component: paLanding2Component},
  { path: 'paLanding3', component: paLanding3Component},
  { path: 'paLanding4', component: paLanding4Component},
  { path: 'paLanding5', component: paLanding5Component},
  { path: 'paOtpEditMethod', component: PaOtpEditMethodComponent },
  { path: 'paConfirm', component: PaConfirmComponent },
  { path: 'paOtpHome', component: PaOtpHomeComponent },
  { path: 'paLandingNoPayment', component: paLandingNoPaymentComponent},
  { path: 'privacyNotificationNew', component: PrivacyNotificationsNewComponent },
  { path: 'privacyNotificationInterestBasedAdsNew', component: PrivacyNotificationsInterestBasedAdsNewComponent },
  { path: 'privacyNotificationAdvertisingNew', component: PrivacyNotificationsAdvertisingNewComponent },
  { path: 'privacyNotificationMarketingCommunicationsNew', component: PrivacyNotificationsMarketingCommunicationsNewComponent },
  { path: 'privacyNotificationNotificationsNew', component: PrivacyNotificationsNotificationsNewComponent },
  { path: 'employeeDesignationCurrentLinesNew', component: EmployeeDesignationCurrentLinesNewComponent },
  { path: 'employeeDesignationEditNew', component: EmployeeDesignationEditNewComponent },
  { path: 'billingAndPaymentsNew', component: BillingAndPaymentsNewComponent },
  { path: 'billingAndPaymentsOnNew', component: BillingAndPaymentsOnNewComponent },
  { path: 'billingAndPaymentsPaperlessBillingNew', component: BillingAndPaymentsPaperlessBillingNewComponent },
  { path: 'lineSettingsNew', component: LineSettingsNewComponent },
  { path: 'lineSettingsNicknameNew', component: LineSettingsNicknameNewComponent },
  { path: 'lineSettingsCallerIdNew', component: LineSettingsCallerIdNewComponent },
  { path: 'lineSettingsSelectLineNew', component: LineSettingsSelectLineNewComponent },
  { path: 'lineSettingsCallerId', component: LineSettingsCallerIdComponent },
  { path: 'lineSettingsSelectCalleridConfirmation', component: LineSettingsCallerIdConfirmationComponent },
  { path: 'lineSettingsLineChangedNew', component: LineSettingsLineChangedNewComponent },
  { path: 'blockingNew', component: BlockingNewComponent },
  { path: 'coverageDeviceNew', component: CoverageDeviceNewComponent },
  { path: 'coverageDeviceDeviceAddressNew', component: CoverageDeviceDeviceAddressNewComponent },
  { path: 'coverageDeviceAssignedNumberNew', component: CoverageDeviceAssignedNumberNewComponent },
  { path: 'coverageDeviceVerifyAddressModalNew', component: CoverageDeviceVerifyAddressModalNewComponent },
  { path: 'multipleSettingsNew', component: MultipleSettingsNewComponent },
  { path: 'mediaSettingsNew', component: MediaSettingsNewComponent },
  { path: 'familyControlNew', component: FamilyControlComponentNew },
  { path: 'familyControlWebGaurdNew', component: FamilyControlWebGaurdComponentNew },
  { path: 'familyControlFamilyAllowancesNew', component: FamilyControlFamilyAllowancesComponentNew },
  { path: 'tmobileidNickname', component: TmobileIdNicknameNewComponent },
  { path: 'billingAddress', component: BillingAddressNewComponent },
  { path: 'TmobileIdNew', component: TmobileIdNewComponent },
  { path: 'TmobileIdNicknameNew', component: TmobileIdNicknameNewComponent },
  { path: 'billingAndPaymentsPaperlessUAT', component: BillingAndPaymentsPaperlessUatnewComponent },
  { path: 'PaperlessTermsAndCondition', component: PaperlessTermsAndConditionComponent },
  { path: 'billingAndPaymentsPaperlessUATUpdated', component: BillingAndPaymentsPaperlessUatUpdatedComponent },
  { path: 'billingAndPaymentsConfirmation', component: BillingAndPaymentsConfirmationComponent },
  { path: 'shopTermsConditions', component: ShopTermsConditionsComponent },
  { path: 'PaymentReceipt', component: PaymentReceiptComponent },
  { path: 'CancelModal', component: CancelModalComponent },
  { path: 'usageOverview', component: UsageOverviewComponent },
  { path: 'PromotionNoPending', component: MyPromotionNoPendingComponent },
  { path: 'PastPromotions', component: PastPromotionComponent },
  { path: 'PromotionsDetails', component: PromotionsDetailsComponent},
  { path: 'PromotionPending', component: MyPromotionPendingComponent},
  { path: 'E911Address', component: E911AddressNewComponent},
  { path: 'datepickerNewModal', component: DatepickerNewModalComponent},
  { path: 'usageDetails1/:category/:name/:date', component: UsageDetail1Component},
  { path: 'usageDetails1', component: UsageDetail1Component},
  { path: 'datepickerFullScreen', component: DatepickerFullScreenComponent},
  { path: 'HeadlineModal', component: HeadlineModalComponent},
  { path: 'MultilineModal', component: MultilineModalComponent},
  { path: 'EmptyModal', component: EmptyModalComponent},
  { path: 'OdfConfirmationBearmode', component: OdfConfirmationBearmodeComponent},
  { path: 'Iconography', component: IconographyComponent },
  { path: 'PaymentHistory', component: PaymentHistoryComponent },
  { path: 'HistoricalBillsModal', component: HistoricalBillsModalComponent},
  { path: 'fliters', component: FiltersComponent },
  {path:  'Breadcrumb', component: BreadcrumbComponent},
  { path: 'Search', component: SearchComponent },
  { path: 'tables', component: TablesComponent},
  { path: 'Mconfirmation', component: MconfirmationComponent},
  { path: 'reviewAutoPayAlreadyOn', component: ReviewAutoPayAlreadyOnComponent},
  { path: 'reviewAutoPayUpsell', component: ReviewAutoPayUpsellComponent},
  { path: 'reviewPlanDetails', component: ReviewPlanDetailsComponent},
  { path: 'reviewLineDetails', component: ReviewLineDetailsComponent},
  { path: 'mvpReviewChanges', component: MvpReviewChangesComponent},
  { path: 'mvpReviewPlans', component: MvpReviewPlansComponent},
  { path: 'mvpReviewAddOns', component: MvpReviewAddOnsComponent},
  { path: 'mvpReviewEquipment', component: MvpReviewEquipmentComponent},
  { path: 'mvpConfirmDetails', component: MvpConfirmDetailsComponent},
  { path: 'mvpReviewChanges2', component: MvpReviewChanges2Component},
  { path: 'mvpReviewOrderDetails', component: MvpReviewOrderDetailsByLineComponent},
  { path: 'mvpSelectPlan', component: MvpSelectPlanComponent},
  { path: 'teamDescription', component: TeamDescriptionComponent},
  { path: 'landingPage', component: LandingPageComponent},
  { path: 'lineDetail', component: LineDetailComponent},
  { path: 'planDetails', component: PlanDetailsComponent},
  { path: 'costDetails', component: CostDetailsComponent},
  { path: 'featureDetails', component: FeatureDetailsComponent},
  { path: 'militaryStatusDetail', component: MilitaryStatusDetailComponent},
  { path: 'registrationVeteran', component: RegistrationVeteranComponent},
  { path: 'registrationVeteranDisabled', component: RegistrationVeteranDisabledComponent},
  { path: 'confirmInfo', component: ConfirmInfoComponent},
  { path: 'successVerified', component: SuccessVerifiedComponent},
  { path: 'successUploadReceived', component: SuccessUploadReceivedComponent},
  { path: 'alertMessage/:status', component: AlertMessageComponent},
  { path: 'uploadDoc', component: UploadDocLandingComponent},
  { path: 'uploadDocDefault', component: UploadDocDefaultComponent},
  { path: 'aucPermissions', component: AucPermissionsComponent},
  { path: 'aucMessages', component: AucMessagesComponent},
  { path: 'aucMessagesConfirmation', component: AucMessagesConfirmationComponent},
  { path: 'aucTermsAndCondition', component: AucChangePermsTermsAndCondComponent},
  { path: 'fidTermsAndCondition', component: FidelisTermsConditionComponent},
  { path: 'fidPrivacyPolicy', component: FidelisPrivacyPolicyComponent},
  { path: 'statusUpload/:status', component: StatusUploadDocComponent},
  { path: 'jodEndingSoon', component: JodEndingSoonComponent},
  { path: 'jodEndingSoonOptions', component: JodEndingSoonOptionsComponent},
  { path: 'jodPoipPayment', component: JodPoipPaymentComponent},
  { path: 'docuSignFlow', component: DocuSignFlowComponent},
  { path: 'docuSignThankYou', component: DocuSignThankYouComponent},
  {path: 'progressBar', component: ProgressBarstyleComponent},
  {path: 'eppTermsAndCondition', component: EppTermsAndConditionComponent},
  {path: 'selectPaymentMethod', component: SelectPaymentMethodComponent},
  {path: 'addCard', component: AddCardComponent},
  {path: 'paymentAmount', component: PaymentAmountComponent},
  {path: 'eppConfirmation', component: EppConfirmationComponent},
  {path: 'duplicatePayment', component: DuplicatePaymentComponent},
  {path: 'paymentSystemError', component: PaymentSystemErrorComponent},
  {path: 'paymentEstimator', component: PaymentEstimatorComponent},
  {path: 'balanceSummaryVariants', component: BalanceSummaryVariantsComponent},
  {path: 'eppExistingPaymentMethod', component: EppExistingPaymentMethodComponent},
  {path: 'jodLeaseVariants', component: JodLeaseVariantsComponent},
    {path: 'eppSelectInstallmentPlan', component: EppSelectInstallmentPlanComponent},
  {path: 'eipVariants', component: EipVariantsComponent},
  {path: 'extraDevicePaymentVariants', component: ExtraDevicePaymentVariantsComponent},
  { path: 'lineDetailNew', component: LineDetailNewComponent},
  { path: 'successStatus', component: SuccessStatusComponent},
  { path: 'problemIdentifier', component: ProblemIdentifierComponent},
  { path: 'problemFix', component: ProblemFixComponent},
  { path: 'maximumAttempts', component: MaximumAttemptsComponent},
  {path: 'selectPlan' , component: SelectPlanComponent},
  {path: 'plancomparison' , component: PlanComparisonComponent},
  {path: 'reviewchange' , component: ReviewChangesComponent },
  {path: 'detailbreakdown' , component: DetailBreakdownComponent},
  {path: 'planconfirmation' , component: PlanConfirmationComponent},
  {path: 'planconfirmationupdated' , component: PlanConfirmationUpdatedComponent},
  {path: 'mbbplandetail' , component: MBBPlanDetailComponent},
  {path: 'reviewOrder' , component: ReviewOrderComponent},
  {path: 'PlanComparisionDetail', component: PlanComparisionDetailComponent},
  {path: 'reviewAndPayOrder', component: ReviewAndPayOrderComponent},
  {path: 'taxesAndFees', component: TaxesAndFeesComponent},
  {path: 'confirmationOrder', component: ConfirmationOrderComponent},
  {path: 'signupVerification', component: SignUpVerificationComponent},
  {path: 'accountSuspended', component: AccountSuspendedComponent},
  {path: 'paymentProcessing', component: PaymentProcessingComponent},
  {path: 'TeamDescriptionComponentReusable', component: SchduledCallComponent},
  {path: 'languageSettings', component: LanguageSettingsComponent},
  {path: 'insession', component: InSessionComponent},
  {path: 'wallet', component: MyWalletComponent},
  {path: 'onelaststep', component: OneLastStepComponent},
  {path: 'lineSelector', component: LineSelectorComponent},
  {path: 'valueSelector', component: ValueSelectorComponent},
  {path: 'removal', component: P360RemovalComponent},
  {path: 'scary', component: P360ScaryComponent},
  {path: 'servicesHubStatus', component: ServicesHubStatusComponent},
  {path: 'on-us-chooser', component:OnUsChooserComponent},
  {path: 'morepayment', component: MorePaymentOptionsComponent},
  {path: 'managedata', component: ManageDataComponent},
  {path: 'confirmPayment', component: confirmPayementMethodComponent},
  {path: 'enterCardInfo', component: EnterCardInformationComponent},
  {path: 'enterBankInfo', component: EnterBankInformationComponent},
  {path: 'tsconfirmation', component: TsConfirmationComponent},
  {path: 'tslineselector', component: TsLineselectorComponent},
  {path: 'tsreactive', component: TsReactivateComponent},
  {path: 'tsreactiveconfirmation', component: TsReactivationconfirmationComponent},
  {path: 'pitchPage', component: PitchPageComponent},
  {path: 'selectline', component: SelectlineComponent},
  {path: 'devicepromolist', component: DevicePromoListComponent},
  {path: 'promodetails', component: PromoDetailsComponent},
  {path: 'pastpromos', component: PastPromosListComponent }
];
export const routing = RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules});
