import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class SpinnerService {
  public showSpinner: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public showOverlay: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public showSpinnerV2: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public showOverlayV2: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  
     showspinner(value: boolean) {
          this.showSpinner.next(value);
     }
     showoverlay(value: boolean) {
        this.showOverlay.next(value);
    }

    showspinnerV2(value: boolean) {
        this.showSpinnerV2.next(value);
    }
    showoverlayV2(value: boolean) {
      this.showOverlayV2.next(value);
    }

}
